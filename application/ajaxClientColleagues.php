<?php require_once('Connections/CRMconnection.php'); ?>

<?php


if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_compname = "0";
if (isset($_GET['cnt_Id'])) {
  $colname_compname = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);

$companyQuery = sprintf("SELECT tblcompanylist.* FROM tblcompanylist , contacts WHERE contacts.company_id = tblcompanylist.comp_id AND contacts.id=%s", GetSQLValueString($colname_compname, "int"));
$companyResult = mysql_query($companyQuery, $CRMconnection) or die(mysql_error());
$resultArray = mysql_fetch_assoc($companyResult);
$resultNumRows = mysql_num_rows($companyResult);

$id = $resultArray['comp_id'];

$colname_rsColleagues = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rsColleagues = $_GET['cnt_Id'];
  
}
mysql_select_db($database_CRMconnection, $CRMconnection);

$query_rsColleagues = sprintf("SELECT contacts.id, contacts.first_name, contacts.surname,contacts.active, COUNT(contacts_interests.interest_id) as listcount 
  FROM contacts, contacts_interests , tblcompanylist 
  WHERE contacts.company_id= %s AND tblcompanylist.comp_id = contacts.company_id AND contacts_interests.contact_id = contacts.id 
  GROUP BY contacts.id ORDER BY contacts.active DESC, contacts.first_name ASC, contacts.surname ASC", GetSQLValueString($id, "int"));

$rsColleagues = mysql_query($query_rsColleagues, $CRMconnection) or die(mysql_error());
$row_rsColleagues = mysql_fetch_assoc($rsColleagues);
$totalRows_rsColleagues = mysql_num_rows($rsColleagues);

$inlist ="";
//quicly parse the id's to a list
do {
	$inlist.= GetSQLValueString($row_rsColleagues['cnt_Id'],"int");
	$inlist.=",";
	} while ($row_rsColleagues = mysql_fetch_assoc($rsColleagues));
	//trim commas off the string
	$inlist = rtrim($inlist,',');
	//rest recordset so you can use it again
if (mysql_num_rows($rsColleagues) > 0)  {
    mysql_data_seek($rsColleagues, 0);
    $row_rsColleagues = mysql_fetch_assoc($rsColleagues);
}
if ($inlist=="NULL"){
	echo ("no colleagues on any mail lists - add at least one to a list to view colleagues - working on a better way to do this");
}

$colname_rsColleagues2 = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rsColleagues2 = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsColleagues2 = sprintf("SELECT contacts.id, contacts.first_name, contacts.surname,contacts.active
  FROM contacts
  WHERE contacts.company_id
  IN (SELECT  contacts.company_id FROM contacts WHERE contacts.id=%s ) 
  AND contacts.id NOT IN ( SELECT contacts_interests.contact_id FROM contacts_interests) 
  ORDER BY contacts.active DESC, contacts.surname ASC, contacts.first_name ASC", GetSQLValueString($colname_rsColleagues2, "text"),
$inlist);

$rsColleagues2 = mysql_query($query_rsColleagues2, $CRMconnection) or die(mysql_error());
$row_rsColleagues2 = mysql_fetch_assoc($rsColleagues2);
$totalRows_rsColleagues2 = mysql_num_rows($rsColleagues2);



?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<fieldset><legend>Colleagues on lists</legend>
    
		<?php do { ?>
		 <?php if ($row_rsColleagues['active'] == 1) {?>
         <div style="background-color:#CCC; border:1px thin #333; font-size:0.9em; border-radius:3px; padding:2px; margin:2px"><a href="clientdetails.php?cnt_Id=<?php echo $row_rsColleagues['id']; ?>" class="clicklink"> <?php echo $row_rsColleagues['first_name']; ?> <?php echo $row_rsColleagues['surname']; ?> <?php if ($row_rsColleagues['active'] ==1){ echo "  --> ".$row_rsColleagues['listcount']." lists";}else{ echo "Inactive";} ?> </a></div>
		 <?php } else { ?>
		 <div style="background-color:#CCC; border:1px thin #333; font-size:0.9em; border-radius:3px; padding:2px; margin:2px"><a href="clientdetails.php?cnt_Id=<?php echo $row_rsColleagues['id']; ?>" class="clicklink" style="color:#999; "> <?php echo $row_rsColleagues['first_name']; ?> <?php echo $row_rsColleagues['surname']; ?> <?php if ($row_rsColleagues['active'] ==1){ echo "  --> ".$row_rsColleagues['listcount']." lists";}else{ echo "Inactive";} ?> </a></div>
          <?php } } while ($row_rsColleagues = mysql_fetch_assoc($rsColleagues)); ?>
      
 </fieldset>
 <fieldset>
 <legend>Colleagues not on lists</legend>
 <?php do { ?>
 <div style="background-color:#CCC; border:1px thin #333; font-size:0.9em; border-radius:3px; padding:2px; margin:2px">
 <a href="clientdetails.php?cnt_Id=<?php echo $row_rsColleagues2['id']; ?>" class="clicklink"> <?php echo $row_rsColleagues2['first_name']; ?> <?php echo $row_rsColleagues2['surname']; ?> <?php if ($row_rsColleagues2['active'] ==0){ echo "(Inactive)";} ?> </a></div>
  <?php } while ($row_rsColleagues2 = mysql_fetch_assoc($rsColleagues2)); ?>
 
 </fieldset>
</body>
</html>

<?php
mysql_free_result($colname_compname);
mysql_free_result($rsColleagues);
mysql_free_result($rsColleagues2);

?>
