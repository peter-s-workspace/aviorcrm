<?php
if(isset($_GET['q'])){
 $q = urlencode($_GET['q']);
 $jsonurl = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=".$q;
 $result = json_decode(file_get_contents($jsonurl), true);
 header("Content-Type: image/jpg");
 $img = imagecreatefromjpeg($result['responseData']['results'][0]['tbUrl']);
 imagejpeg($img);
 imagedestroy($img);
 exit;
}
?>
<html>
<body>
<form action='' method='post'>
<fieldset>
<input type='text' name='q' /><input type='submit' name='submit' />
</fieldset>
</form>
<?php
//number of images to display
$numdisplay = 10;

if(isset($_POST['q'])){
 $q = urlencode($_POST['q']);
 $jsonurl = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=".$q;
 $result = json_decode(file_get_contents($jsonurl), true);
 for($i=0;$i<$numdisplay;$i++)
 {echo "<img src='{$result['responseData']['results'][$i]['tbUrl']}' />"; }
}
?>
</body>
</html>