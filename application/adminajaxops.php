<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$response ="<h4>None Done</h4>";
//update user
if ((isset($_GET["MM_update"])) && ($_GET["MM_update"] == "ajaxupd")) {
  $updateSQL = sprintf("UPDATE tbluser SET user_name=%s, user_pword=%s, user_fname=%s, user_lname=%s, user_email=%s, user_level=%s, user_phone=%s, user_fname=%s, user_bday=%s, user_phone_ip=%s, user_coverage=%s, popbox=%s WHERE user_id=%s",
                       GetSQLValueString($_GET['user_name'], "text"),
                       GetSQLValueString($_GET['user_pword'], "text"),
                       GetSQLValueString($_GET['user_fname'], "text"),
                       GetSQLValueString($_GET['user_lname'], "text"),
                       GetSQLValueString($_GET['user_email'], "text"),
                       GetSQLValueString($_GET['user_level'], "text"),
                       GetSQLValueString($_GET['user_phone'], "text"),
                       GetSQLValueString($_GET['user_fname'], "text"),
                       GetSQLValueString($_GET['user_bday'], "date"),
                       GetSQLValueString($_GET['user_phone_ip'], "text"),
                       GetSQLValueString($_GET['user_coverage'], "text"),
                       GetSQLValueString($_GET['popbox'], "int"),
                       GetSQLValueString($_GET['user_id'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($updateSQL, $CRMconnection) or die(mysql_error());
  
  $response = "<h4>Updated User</h4>";
}

//add user
if ((isset($_GET["MM_insert"])) && ($_GET["MM_insert"] == "ajaxins")) {
  $insertSQL = sprintf("INSERT INTO tbluser (user_name, user_pword, user_fname, user_lname, user_email, user_level, user_phone, user_fname, user_bday) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_GET['user_name'], "text"),
                       GetSQLValueString($_GET['user_pword'], "text"),
                       GetSQLValueString($_GET['user_fname'], "text"),
                       GetSQLValueString($_GET['user_lname'], "text"),
                       GetSQLValueString($_GET['user_email'], "text"),
                       GetSQLValueString($_GET['user_level'], "text"),
                       GetSQLValueString($_GET['user_phone'], "text"),
                       GetSQLValueString($_GET['user_fname'], "text"),
                       GetSQLValueString($_GET['user_bday'], "date"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());

  $response = "<h4>Added User</h4>";
}
//delete user
if ((isset($_GET['user_id'])) && ($_GET['user_id'] != "") && (isset($_GET['MM_Delete_User'])) && ($_GET['MM_Delete_User']=="donow")) {
  $deleteSQL = sprintf("DELETE FROM tbluser WHERE user_id=%s",
                       GetSQLValueString($_GET['user_id'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($deleteSQL, $CRMconnection) or die(mysql_error());
  
  $response = ("<h4>User deleted </h4>");
}



?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>
</head>

<body>

<?php echo $response; ?>

</body>
</html>