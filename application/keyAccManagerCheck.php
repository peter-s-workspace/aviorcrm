<?php require_once('Connections/CRMconnection.php'); mysql_select_db($database_CRMconnection, $CRMconnection); ?>
<?php
if (!function_exists("GetSQLValueString")) {
    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        if (PHP_VERSION < 6) {
            $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
        }

        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}
$client_id = $_POST['client_id'];
$kamQuery = sprintf("select Company from tblcompanylist where id_kam1 = %s or id_kam2 = %s"  , GetSQLValueString($client_id , "int") , GetSQLValueString($client_id , "int"));
$kamResult = mysql_query($kamQuery , $CRMconnection) or die(mysql_error());
$returnData = array();

while($row = mysql_fetch_assoc($kamResult)){
    $returnData[] = $row;
}

echo json_encode($returnData);
?>