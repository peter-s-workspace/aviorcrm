<?php

//The list to merge can get long. Setting memory limit to 64M to avoid timeout issue.
ini_set("memory_limit","64M");
require_once('includes/classes/mailinglist.class.php');
if (!isset($_SESSION)) {
  session_start();
}
//Only if someone has logged in.
if(isset($_SESSION['MM_Username']) && isset( $_SESSION['MM_UserGroup'] ) &&  $_SESSION['MM_UserGroup'] != '' ) {

	$mailingListName = (isset( $_POST['listItem'] ) && trim($_POST['listItem'])) ? trim($_POST['listItem']) : '';
	$mergeLists = (isset( $_POST['mergeLists'] ) && trim($_POST['mergeLists'])) ? trim($_POST['mergeLists']) : '';
	
	if( $mailingListName != '' ) {
		$mailingListObj = new MailingList();
		
		$returnValue = $mailingListObj->save(array(
				'mailing_list_name' => $mailingListName,
				'mergeLists' => $mergeLists
		));
		
		echo $returnValue;
	
	} else {
		echo "Please enter name of list";
	
	}
}
exit();
?>