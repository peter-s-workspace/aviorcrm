<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_viewmlist = "SELECT * FROM aviorsharetrades ORDER BY idaviorsharetrades DESC";
$viewmlist = mysql_query($query_viewmlist, $CRMconnection) or die(mysql_error());
$row_viewmlist = mysql_fetch_assoc($viewmlist);
$totalRows_viewmlist = mysql_num_rows($viewmlist);

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userlist = "SELECT user_id, user_fname, user_lname FROM tbluser ORDER BY user_fname ASC";
$userlist = mysql_query($query_userlist, $CRMconnection) or die(mysql_error());
$row_userlist = mysql_fetch_assoc($userlist);
$totalRows_userlist = mysql_num_rows($userlist);
?>
<?php require_once('includes/sitevars.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><link type="text/css" href="css/kdes1/jquery-ui-1.8.4.custom.css" rel="stylesheet" />		
		<script src="js/jquery-1.9.1.min.js"></script>

<script src="js/jquery-migrate-1.1.1.min.js"></script>


		<script type="text/javascript" src="js/jquery-ui-1.8.4.custom.min.js"></script>
		<script type="text/javascript">
			$(function(){
				
				$('#addtradebox').hide();
				// Accordion
				//$("#accordion").accordion({ header: "h3" });
	
				// Tabs
				$('#tabs1').tabs();
				$('#tabs2').tabs();
	
				
				// Datepicker
				$('#orderdate').datepicker({
					inline: true,
					dateFormat:"yy-mm-dd"
				});
				$('#datefilled').datepicker({
					inline: true,
					dateFormat:"yy-mm-dd"
				});
				
				$('#btnaddrecord').click( function() {
					$('#addtradebox').show('fold');
				});
				
				
				$('#btnsavenewrecord').click( function() {
					$('#addtradebox').hide('fold');
					var tval = $('#form1').serialize();
					$('#showmasterlist').load('ajaxcomplianceviewmaster.php?'+tval);
					document.forms["form1"].reset();
					
				});
			
			$('#canceladdnew').click( function() {
				$('#addtradebox').hide('fold');
				document.forms["form1"].reset();
			})
			
			
			//stripe tables
			$('.stripeMe tr:even').addClass('alt');
					
							
			});
			function doshareupdate() {
					var tval =  $('#updateform').serialize();
					$('#showmasterlist').load('ajaxcomplianceviewmaster.php?'+tval);
					
			}
		</script>

<!-- InstanceEndEditable -->
<script language="javascript">
	$(function(){
		
		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					}
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);
				
					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
	})
</script>	
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			<div>
				<fieldset>
					<Legend>Quick Search</Legend>
					<input type="text" id="quicksearch" size="16" />
				</fieldset>
			</div> 
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<div class="content40">
<div id="tabs1">
      <ul>
        <li>Share Trades</li>
      </ul>
      <div id="tabs1-1"><fieldset>
        <input type="button" value="Add share trade" id="btnaddrecord"/>
        <fieldset id="addtradebox"><legend>Add share trade</legend><form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
  <table border="1" align="center" cellpadding="1" cellspacing="2">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Name</td>
      <td><select name="userid" id="userid">
        <?php
do {  
?>
        <option value="<?php echo $row_userlist['user_id']?>"><?php echo $row_userlist['user_fname']?> <?php echo $row_userlist['user_lname']?></option>
        <?php
} while ($row_userlist = mysql_fetch_assoc($userlist));
  $rows = mysql_num_rows($userlist);
  if($rows > 0) {
      mysql_data_seek($userlist, 0);
	  $row_userlist = mysql_fetch_assoc($userlist);
  }
?>
      </select></td>
      <td>Director:</td>
      <td><input type="checkbox" name="director" value="" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Share_code:</td>
      <td><input type="text" name="share_code" value="" size="12" /></td>
      <td>Action:</td>
      <td><select name="actiontype" id="actiontype">
        <option value="Buy" <?php if (!(strcmp("Buy", ""))) {echo "SELECTED";} ?>>Buy</option>
        <option value="Sell" <?php if (!(strcmp("Sell", ""))) {echo "SELECTED";} ?>>Sell</option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right" valign="top">Share_desc:</td>
      <td><textarea name="share_desc" cols="25" rows="5"></textarea></td>
      <td><p>Order date:</p>
        <p>Date filled:</p></td>
      <td><p>
        <input type="text" name="orderdate" id="orderdate" value="<?php echo date('Y-m-d'); ?>" size="16" />
      </p>
        <p>
          <input type="text" name="datefilled" id="datefilled" value="<?php echo date('Y-m-d'); ?>" size="16" />
        </p></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"># shares :</td>
      <td><input type="text" name="numsharesplanned" value="" size="12" /></td>
      <td>Nature of holding:</td>
      <td><select name="natureofholding">
        <option value="Direct" <?php if (!(strcmp("Direct", ""))) {echo "SELECTED";} ?>>Direct</option>
        <option value="Indirect" <?php if (!(strcmp("Indirect", ""))) {echo "SELECTED";} ?>>Indirect</option>
        <option value="Family" <?php if (!(strcmp("Family", ""))) {echo "SELECTED";} ?>>Family</option>
        </select></td>
    </tr>
    <tr valign="baseline">
      <td colspan="4" align="center" nowrap="nowrap">Initalising position: ?
        <input name="initpos" type="checkbox" id="initpos" value="" /></td>
    </tr>
    <tr valign="baseline">
      <td colspan="4" align="center" nowrap="nowrap">Closing out position ?:
        <input type="checkbox" name="closeoutpos" value="" /></td>
    </tr>
    <tr valign="baseline">
      <td colspan="4" align="center" nowrap="nowrap"><input type="button" id="canceladdnew" value="Cancel" /><input name="Button" type="button" id="btnsavenewrecord" value="Insert record" /></td>
      </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />
</form></fieldset>
      </fieldset><fieldset><legend>Trade List</legend>
      
        <p>Sorted by most recently added - click to view/edit detail</p>
       <span id="showmasterlist"> <table width="100%" border="1" cellpadding="1" cellspacing="2" class="stripeMe">
 <thead>
  <tr>
    <td>idaviorsharetrades</td>
    <td>userid</td>
    <td>share_code</td>
    <td>share_desc</td>
    <td>orderdate</td>
    <td>actiontype</td>
    </tr>
</thead>
<tbody>
  <?php do { ?>
    <tr>
      <td><a href="#" onclick="$('#showmasterlist').load('ajaxcomplianceedittrade.php?idaviorsharetrades=<?php echo $row_viewmlist['idaviorsharetrades']; ?>')"><?php echo $row_viewmlist['idaviorsharetrades']; ?></a></td>
      <td><?php echo $row_viewmlist['userid']; ?></td>
      <td><?php echo $row_viewmlist['share_code']; ?></td>
      <td><?php echo $row_viewmlist['share_desc']; ?></td>
      <td><?php echo $row_viewmlist['orderdate']; ?></td>
      <td><?php echo $row_viewmlist['actiontype']; ?></td>
      </tr>
    <?php } while ($row_viewmlist = mysql_fetch_assoc($viewmlist)); ?>
 </tbody>
</table>

<table border="0" align="center">
  <tr>
    <td><?php if ($pageNum_viewmlist > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_viewmlist=%d%s", $currentPage, 0, $queryString_viewmlist); ?>">First</a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_viewmlist > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_viewmlist=%d%s", $currentPage, max(0, $pageNum_viewmlist - 1), $queryString_viewmlist); ?>">Previous</a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_viewmlist < $totalPages_viewmlist) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_viewmlist=%d%s", $currentPage, min($totalPages_viewmlist, $pageNum_viewmlist + 1), $queryString_viewmlist); ?>">Next</a>
        <?php } // Show if not last page ?></td>
    <td><?php if ($pageNum_viewmlist < $totalPages_viewmlist) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_viewmlist=%d%s", $currentPage, $totalPages_viewmlist, $queryString_viewmlist); ?>">Last</a>
        <?php } // Show if not last page ?></td>
  </tr>

</table>
</span>
      </fieldset></div>
      
</div>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($viewmlist);

mysql_free_result($userlist);
?>
