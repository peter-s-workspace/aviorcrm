<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET["MM_update"])) && ($_GET["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE aviorsharetrades SET userid=%s, share_code=%s, share_desc=%s, director=%s, natureofholding=%s, orderdate=%s, datefilled=%s, initpos=%s, actiontype=%s, numsharesplanned=%s, closeoutpos=%s WHERE idaviorsharetrades=%s",
                       GetSQLValueString($_GET['userid'], "int"),
                       GetSQLValueString($_GET['share_code'], "text"),
                       GetSQLValueString($_GET['share_desc'], "text"),
                       GetSQLValueString(isset($_GET['director']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_GET['natureofholding'], "text"),
                       GetSQLValueString($_GET['orderdate'], "date"),
                       GetSQLValueString($_GET['datefilled'], "date"),
                       GetSQLValueString(isset($_GET['initpos']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_GET['actiontype'], "text"),
                       GetSQLValueString($_GET['numsharesplanned'], "double"),
                       GetSQLValueString(isset($_GET['closeoutpos']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_GET['idaviorsharetrades'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($updateSQL, $CRMconnection) or die(mysql_error());
}

$colname_editrecord = "-1";
if (isset($_GET['idaviorsharetrades'])) {
  $colname_editrecord = $_GET['idaviorsharetrades'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_editrecord = sprintf("SELECT * FROM aviorsharetrades WHERE idaviorsharetrades = %s", GetSQLValueString($colname_editrecord, "int"));
$editrecord = mysql_query($query_editrecord, $CRMconnection) or die(mysql_error());
$row_editrecord = mysql_fetch_assoc($editrecord);
$totalRows_editrecord = mysql_num_rows($editrecord);

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userlist = "SELECT * FROM tbluser ORDER BY user_fname ASC";
$userlist = mysql_query($query_userlist, $CRMconnection) or die(mysql_error());
$row_userlist = mysql_fetch_assoc($userlist);
$totalRows_userlist = mysql_num_rows($userlist);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="updateform">
  <table border="1" align="center" cellpadding="1" cellspacing="2">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Idaviorsharetrades:</td>
      <td><?php echo $row_editrecord['idaviorsharetrades']; ?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Userid:</td>
      <td><select name="userid">
        <?php 
do {  
?>
        <option value="<?php echo $row_userlist['user_id']?>" <?php if (!(strcmp($row_userlist['user_id'], htmlentities($row_editrecord['userid'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>><?php echo $row_userlist['user_fname']?> <?php echo $row_userlist['user_lname']?></option>
        <?php
} while ($row_userlist = mysql_fetch_assoc($userlist));
?>
      </select></td>
      <td>Director:</td>
      <td><input type="checkbox" name="director" value=""  <?php if (!(strcmp(htmlentities($row_editrecord['director'], ENT_COMPAT, 'utf-8'),""))) {echo "checked=\"checked\"";} ?> /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Share_code:</td>
      <td><input type="text" name="share_code" value="<?php echo htmlentities($row_editrecord['share_code'], ENT_COMPAT, 'utf-8'); ?>" size="14" /></td>
      <td>Nature of holding:</td>
      <td><select name="natureofholding">
        <option value="Direct" <?php if (!(strcmp("Direct", $row_editrecord['natureofholding']))) {echo "selected=\"selected\"";} ?>>Direct</option>
        <option value="Indirect" <?php if (!(strcmp("Indirect", $row_editrecord['natureofholding']))) {echo "selected=\"selected\"";} ?>>Indirect</option>
        <option value="Family" <?php if (!(strcmp("Family", $row_editrecord['natureofholding']))) {echo "selected=\"selected\"";} ?>>Family</option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right" valign="top">Share_desc:</td>
      <td><textarea name="share_desc" cols="19" rows="5"><?php echo htmlentities($row_editrecord['share_desc'], ENT_COMPAT, 'utf-8'); ?></textarea></td>
      <td><p>Order date:</p>
      <p>Date filled:</p></td>
      <td><p>
        <input type="text" name="orderdate" value="<?php echo htmlentities($row_editrecord['orderdate'], ENT_COMPAT, 'utf-8'); ?>" size="14" />
      </p>
      <p>
        <input type="text" name="datefilled" value="<?php echo htmlentities($row_editrecord['datefilled'], ENT_COMPAT, 'utf-8'); ?>" size="14" />
      </p></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Initialising position:</td>
      <td><input type="checkbox" name="initpos" value=""  <?php if (!(strcmp(htmlentities($row_editrecord['initpos'], ENT_COMPAT, 'utf-8'),""))) {echo "checked=\"checked\"";} ?> /></td>
      <td>Closing Out:</td>
      <td><input type="checkbox" name="closeoutpos" value=""  <?php if (!(strcmp(htmlentities($row_editrecord['closeoutpos'], ENT_COMPAT, 'utf-8'),""))) {echo "checked=\"checked\"";} ?> /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Actiontype:</td>
      <td><select name="actiontype" id="actiontype">
        <option value="Buy" <?php if (!(strcmp("Buy", $row_editrecord['actiontype']))) {echo "selected=\"selected\"";} ?>>Buy</option>
        <option value="Sell" <?php if (!(strcmp("Sell", $row_editrecord['actiontype']))) {echo "selected=\"selected\"";} ?>>Sell</option>
      </select></td>
      <td># shares:</td>
      <td><input type="text" name="numsharesplanned" value="<?php echo htmlentities($row_editrecord['numsharesplanned'], ENT_COMPAT, 'utf-8'); ?>" size="14" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><input type="button" value="Cancel" id="cancelupdate" onclick="$('#showmasterlist').load('ajaxcomplianceviewmaster.php')" /></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input name="Button" type="button" id="updaterecord" value="Update record" onclick="doshareupdate()" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="idaviorsharetrades" value="<?php echo $row_editrecord['idaviorsharetrades']; ?>" />
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($editrecord);

mysql_free_result($userlist);
?>
