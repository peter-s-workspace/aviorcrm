<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$user_phone_ip ="";
if (isset($_GET['user_phone_ip'])){
	$user_phone_ip= $_GET['user_phone_ip'];
}


$colname_searchres = "none";
if (isset($_GET['searchterm'])) {
  $colname_searchres = $_GET['searchterm'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_searchres = sprintf("SELECT contacts.* , tblcompanylist.Company FROM contacts LEFT OUTER JOIN tblcompanylist ON tblcompanylist.comp_id = contacts.company_id WHERE first_name LIKE %s OR contacts.surname LIKE %s OR contacts.landline  LIKE %s OR CONCAT(contacts.first_name,' ',contacts.surname ) LIKE %s ORDER BY first_name ASC ",GetSQLValueString("%" . $colname_searchres . "%", "text"), GetSQLValueString("%" . $colname_searchres . "%", "text"),GetSQLValueString("%" . $colname_searchres . "%", "text"),GetSQLValueString("%" . $colname_searchres . "%", "text"),GetSQLValueString("%" . $colname_searchres . "%", "text"));
$searchres = mysql_query($query_searchres, $CRMconnection) or die(mysql_error());
$row_searchres = mysql_fetch_assoc($searchres);
$totalRows_searchres = mysql_num_rows($searchres);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>
<script src="js/jquery-1.9.1.min.js"></script>

<script src="js/jquery-migrate-1.1.1.min.js"></script>


		<script type="text/javascript" src="js/jquery-ui-1.8.4.custom.min.js"></script>		<script type="text/javascript" src="js/jquery.quicksearch.js">
        </script>
		<script type="text/javascript">
			$(function(){

				
				//stripe tables
				$('.stripeMe tr:even').addClass('alt');
				//for resplist
				$('#filtersearch').quicksearch('#searchtable tbody tr');				


			});
			</script>
</head>


<body>
Quick Filter: <input name="filtersearch" id="filtersearch" type="text" />
 <table cellpadding="2" class="stripeMe" id="searchtable" style="width:100%">
	<thead>
                <tr class="tabbhead">
                  <td>Name</td>
                  <td>Institution</td>
                  <td>Phone</td>
                  <td>Cell</td>
                  <td></td>
                   </tr>
    </thead>
                <tbody>
                  <?php do { ?>
                    <tr>
                      <td><a href="clientdetails.php?cnt_Id=<?php echo $row_searchres['id']; ?>"><?php echo $row_searchres['first_name']; ?> <?php echo $row_searchres['surname']; ?></a></td>
                      <td><?php echo $row_searchres['Company']; ?></td>
                      <td><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_searchres['landline']; ?>'); $('#phonebox').dialog('open');"><?php echo $row_searchres['landline']; ?></a></td>
                      <td align="right"><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_searchres['mobile']; ?>')"><?php echo $row_searchres['mobile']; ?></a></td>
                      <td><span style="text-align:right;"><a href="mailto:<?php echo $row_searchres['email']; ?>"><img border="0" src="images/email-icon-16.gif" width="16" height="16" align="right" /></a></span></td>
                  </tr>
                    <?php } while ($row_searchres = mysql_fetch_assoc($searchres)); ?>
                </tbody>
  </table>

</body>
</html>
<?php
mysql_free_result($searchres);
?>
