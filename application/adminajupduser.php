<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}


$colname_cuser = "-1";
if (isset($_GET['user_id'])) {
  $colname_cuser = $_GET['user_id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_cuser = sprintf("SELECT * FROM tbluser WHERE user_id = %s", GetSQLValueString($colname_cuser, "double"));
$cuser = mysql_query($query_cuser, $CRMconnection) or die(mysql_error());
$row_cuser = mysql_fetch_assoc($cuser);
$totalRows_cuser = mysql_num_rows($cuser);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>
</head>

<body>
<h2> Update user:</h2>
<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
 <div style="border:1px solid #CCC;">
  <table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_name:</td>
      <td><input type="text" name="user_name" value="<?php echo htmlentities($row_cuser['user_name'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_pword:</td>
      <td><input type="text" name="user_pword" value="<?php echo htmlentities($row_cuser['user_pword'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_fname:</td>
      <td><input type="text" name="user_fname" value="<?php echo htmlentities($row_cuser['user_fname'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_lname:</td>
      <td><input type="text" name="user_lname" value="<?php echo htmlentities($row_cuser['user_lname'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_email:</td>
      <td><input type="text" name="user_email" value="<?php echo htmlentities($row_cuser['user_email'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_level:</td>
      <td><input type="text" name="user_level" value="<?php echo htmlentities($row_cuser['user_level'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_phone:</td>
      <td><input type="text" name="user_phone" value="<?php echo htmlentities($row_cuser['user_phone'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_nick:</td>
      <td><input type="text" name="user_fname" value="<?php echo htmlentities($row_cuser['user_fname'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_bday:</td>
      <td><input type="text" name="user_bday" value="<?php echo htmlentities($row_cuser['user_bday'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_clist:</td>
      <td><input type="text" name="user_phone_ip" value="<?php echo htmlentities($row_cuser['user_phone_ip'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_coverage:</td>
      <td><input type="text" name="user_coverage" value="<?php echo htmlentities($row_cuser['user_coverage'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Popbox:</td>
      <td><input type="text" name="popbox" value="<?php echo htmlentities($row_cuser['popbox'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="button" value="Update user" onclick="ajaxformget('form1', 'adminajaxops.php','#ajaxt1');" /></td>
    </tr>
  </table>
  <input type="hidden" name="user_id" value="<?php echo htmlentities($row_cuser['user_id'], ENT_COMPAT, 'utf-8'); ?>" />
  <input type="hidden" name="MM_update" value="ajaxupd" />

  </div>
</form>
<form id="form2">
<div style="border:1px solid #CCC;">
<input type="button" onclick="ajaxformget('form2', 'adminajaxops.php','#ajaxt1'); $('#ajaxt2').load('adminajuserlist.php');" value="Delete user" />
<input type="hidden" name="user_id" value="<?php echo htmlentities($row_cuser['user_id'], ENT_COMPAT, 'utf-8');?>" />
<input type="hidden" name="MM_Delete_User" value="donow" />
</div>
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($cuser);
?>
