<?php require_once('Connections/CRMconnection.php'); 
	require_once('includes/event_log/eventlog.class.php'); 
?>
<?php
if (!isset($_SESSION)) {
  session_start();
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$eventLogObj = new Eventlog();
//variables specifying whether to show resp or rest list
$showresplist="no";
$showrest="no";


if ((isset($_GET["MM_addresp"])) && ($_GET["MM_addresp"] == "donow")) {
  $insertSQL = sprintf("INSERT INTO cntresponsibility (cnt_id, respons_id) VALUES (%s, %s)",
                       GetSQLValueString($_GET['cnt_Id'], "int"),
                       GetSQLValueString($_GET['respons_id'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());
  $showresplist="yes";
  
   $sql = sprintf("SELECT CONCAT(contacts.first_name,' ',contacts.surname) AS 'user_fullname', tblcompanylist.Company AS 'cnt_company'  FROM contacts , tblcompanylist WHERE contacts.company_id = tblcompanylist.comp_id AND contacts.id=%s", GetSQLValueString($_GET['cnt_Id'], "int"));
   mysql_select_db($database_CRMconnection, $CRMconnection);
   $userRS = mysql_query($sql, $CRMconnection) or die(mysql_error());		
   $user_fullname  = mysql_result($userRS,0,'user_fullname');
   $cnt_company = mysql_result($userRS,0,'cnt_company');

  $eventLogObj->save(array(
		'event' => 'ADD_CLIENT_RESPONSIBILITY',
		'user' => $_SESSION['MM_Username'],
		'affected_user' => $user_fullname,
		'company' => $cnt_company,
		'affected_type' => 'client'));
}

if ((isset($_GET['idcntresponsibility'])) && ($_GET['idcntresponsibility'] != "") && (isset($_GET['MM_removeresp']))) {

$sql = sprintf("SELECT CONCAT(contacts.first_name,' ',contacts.surname) AS 'user_fullname', tblcompanylist.Company AS 'cnt_company' FROM contacts , tblcompanylist WHERE contacts.company_id = tblcompanylist.comp_id AND contacts.id=%s", GetSQLValueString($_GET['cnt_Id'], "int"));
   mysql_select_db($database_CRMconnection, $CRMconnection);
   $userRS = mysql_query($sql, $CRMconnection) or die(mysql_error());		
   $user_fullname  = mysql_result($userRS,0,'user_fullname');
  $cnt_company = mysql_result($userRS,0,'cnt_company');
  $eventLogObj->save(array(
		'event' => 'REMOVE_CLIENT_RESPONSIBILITY',
		'user' => $_SESSION['MM_Username'],
		'affected_user' => $user_fullname,
		'company' => $cnt_company,
		'affected_type' => 'client'));
		
  $deleteSQL = sprintf("DELETE FROM cntresponsibility WHERE idcntresponsibility=%s",
                       GetSQLValueString($_GET['idcntresponsibility'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($deleteSQL, $CRMconnection) or die(mysql_error());	
  $showrest="yes";

}

$colname_userdet = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_userdet = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userdet = sprintf("SELECT * FROM contacts WHERE id = %s", GetSQLValueString($colname_userdet, "int"));
$userdet = mysql_query($query_userdet, $CRMconnection) or die(mysql_error());
$row_userdet = mysql_fetch_assoc($userdet);
$totalRows_userdet = mysql_num_rows($userdet);



$colname_responsib = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_responsib = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_responsib = sprintf("SELECT responsiblist.response_desc, responsiblist.respons_id, cntresponsibility.idcntresponsibility FROM cntresponsibility, responsiblist WHERE cntresponsibility.cnt_id=%s AND responsiblist.respons_id=cntresponsibility.respons_id ORDER BY responsiblist.response_desc", GetSQLValueString($colname_responsib, "int"));
$responsib = mysql_query($query_responsib, $CRMconnection) or die(mysql_error());
$row_responsib = mysql_fetch_assoc($responsib);
$totalRows_responsib = mysql_num_rows($responsib);


$colname_resplist = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_resplist = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_resplist = sprintf("SELECT * FROM responsiblist WHERE responsiblist.respons_id NOT IN (SELECT respons_id FROM cntresponsibility WHERE cntresponsibility.cnt_id=%s) ORDER BY responsiblist.response_desc", GetSQLValueString($colname_resplist, "int"));
$resplist = mysql_query($query_resplist, $CRMconnection) or die(mysql_error());
$row_resplist = mysql_fetch_assoc($resplist);
$totalRows_resplist = mysql_num_rows($resplist);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>
</head>

<body>
<?php if ($showresplist =="yes"){ ?>
<legend>Responsibilities</legend>
  <?php   do { ?>
        <a href="#" id="r<?php echo $row_responsib['idcntresponsibility']; ?>" onclick="$('#r<?php echo $row_responsib['idcntresponsibility']; ?>').remove();showloader('#notresp'); $('#notresp').load('clientresponsibajax.php?cnt_Id=<?php echo $row_userdet['id']; ?>&idcntresponsibility=<?php echo $row_responsib['idcntresponsibility']; ?>&MM_removeresp=yes')" class="clicklink"><?php echo $row_responsib['response_desc']; ?></a>
        <?php } while ($row_responsib = mysql_fetch_assoc($responsib)); ?>
      <?php }
		

if ($showrest =="yes"){ 
?>
<legend>Available List</legend>
<?php do { ?>
        <a href="#" id="nr<?php echo $row_resplist['respons_id']; ?>" onclick="$('#nr<?php echo $row_resplist['respons_id']; ?>').remove();showloader('#isresp');$('#isresp').load('clientresponsibajax.php?cnt_Id=<?php echo $row_userdet['id']; ?>&MM_addresp=donow&respons_id=<?php echo $row_resplist['respons_id']; ?>')" class="clicklink"><?php echo $row_resplist['response_desc']; ?></a>
        <?php } while ($row_resplist = mysql_fetch_assoc($resplist)); ?>
      <?php
}
?>
        


</body>
</html>
<?php
mysql_free_result($userdet);

mysql_free_result($responsib);

mysql_free_result($resplist);
?>
