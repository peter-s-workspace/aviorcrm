<?php
	require_once("class.phpmailer.php");
	require_once('vendor/behat/transliterator/src/Behat/Transliterator/Transliterator.php');
	require_once('vendor/jeroendesloovere/vcard/src/VCard.php');
	require_once('vendor/jeroendesloovere/vcard/src/VCardException.php');
	require_once('Connections/CRMconnection.php');

	if ($_POST) {
		$name 			= filter_var($_POST['name'], FILTER_SANITIZE_STRING);
		$surname 		= filter_var($_POST['surname'], FILTER_SANITIZE_STRING);
		$email 			= filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
		$company 		= filter_var($_POST['company'], FILTER_SANITIZE_STRING);
		$mobile 		= filter_var($_POST['mobile'], FILTER_SANITIZE_NUMBER_INT);
		$phone 			= filter_var($_POST['phone'], FILTER_SANITIZE_NUMBER_INT);
		$userEmail  	= filter_var($_POST['userEmail'],FILTER_SANITIZE_EMAIL);
		$imageLocation  = $_POST['imageLocation'];
		$jobtitle 		= filter_var($_POST['jobtitle'], FILTER_SANITIZE_STRING);
		//////////////////////////////////////////////////////////////////
		//////////// Vcard Creation functionality ////////////////////////
		/////////////////////////////////////////////////////////////////

		$vCard 		= new JeroenDesloovere\VCard\VCard();

		$vCard->addName($surname, $name, '','','');
		//should work with outlook 2016
		$vCard->addJobtitle($jobtitle); 
		$vCard->addEmail($email);
		$vCard->addCompany($company);
		//seems to work with gmail email service can be removed as the client is using outlook
		$vCard->addRole($jobtitle);
		$vCard->addPhoneNumber($mobile,'CELL');
		$vCard->addPhoneNumber($phone ,'PREF;WORK');
		
		if (file_exists($imageLocation)) {
			$vCard->addPhoto($imageLocation);
		}
		//Store the Vcard
		$vCard->save();
		$file_name 			= $vCard->getFilename();
		$file_extension 	= $vCard->getFileExtension();

		$attachment_file 	= $file_name .'.'.$file_extension;
		mysql_select_db('avcrm');
		$sql 					= "select * from config;";
		$query 					= mysql_query($sql,$CRMconnection);

		while ($row 			= mysql_fetch_assoc($query)) {
			switch ($row['key']) {
				case 'smtp_host':
					$smtp_host = $row['value'];
					break;
				case 'smtp_port':
					$smtp_port 	= $row['value'];
					break;
				case 'smtp_username':
					$smtp_user  = $row['value'];
					break;
				case 'smpt_password':
					$smtp_pass 	= $row['value'];
					break;
				case 'email_from':
					$smtp_from 	= $row['value'];
					break;
				default;
					break;
			}
		}

		$mail 					= new PHPmailer();
		$mail->IsSMTP();
		$mail->Host 			= $smtp_host;
		$mail->SMTPDebug 		= 0;
		$mail->SMTPAuth 		= true;   							// turn on SMTP authentication
		$mail->Username 		= $smtp_user;  						// SMTP username
		$mail->Password 		= $smtp_pass; 						// SMTP password
		$mail->Port 			= $smtp_port; 
		$mail->From 			= $smtp_from;

		$mail->SetFrom($smtp_from, "Avior CRM" );	

		$mail->AddAddress($userEmail);
		$mail->isHTML(true);
		$mail->Body 			= "Please find the vCard for ".$name." ".$surname." attached to this email.";
		$mail->AddAttachment($attachment_file);
		$mail->Subject 			= "vCard | ". $name." ".$surname." | ".$company;
		
		$sendMail 				= $mail->send();
		$response 				= array();
		if (!$sendMail) {
			$response['status'] = "Error";
			//wording on the error message can be changed, I am open to suggestions.
			$response['message']= "Error sending the email ".$mail->ErrorInfo. ", please contact your administrator.";
		} else {
			// remove the file from the folder ,these can be captured but can waiste space as time goes by on the server.
			unlink($attachment_file);  
			$response['status'] = "success";
			$response['message']= "successfully sent the vCard to: ".$userEmail; 
		}
		echo json_encode($response);
 }
?>