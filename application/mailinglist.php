<?php
	require_once('Connections/CRMconnection.php');
	require_once('includes/classes/simplexlsx.class.php'); //extract excel files
	require_once('class.phpmailer.php');

	if (!$CRMconnection) {
		die('Error connecting to the database' .mysql_error($CRMconnection));
	} 
	
 	if (!empty($_POST)) {
	 	$file_name 	= $_FILES['upload']['name'];
	 	$tmp_name 	= $_FILES['upload']['tmp_name'];
	 	$size 		= $_FILES['upload']['size'];

	 	//move the file from temp location
	 	move_uploaded_file($_FILES['upload']['tmp_name'], $file_name);
	 	
	 	$worksheet 	= array();
		$object 	= new SimpleXLSX($file_name);
		
	    try {
			$worksheet  = $object->rows();
		} catch(Exception $e) {
			$excel_error = $e->getMessage();
		}
		
		mysql_select_db('avcrm');
		$sql 		= "select email,id from contacts;";
		$query 		= mysql_query($sql) or die($sql .''. mysql_error());
		
		$excel 		= array();
		$match		= array();
		$diff		= array();
		$sql_email 	= array();
		$errors 	= array();
		$response   = array();

		foreach ($worksheet as $item) {
			$excel_email = $item[0];
			if (!filter_var($excel_email,FILTER_VALIDATE_EMAIL) === false) {
				array_push($excel, $excel_email);
			} else {
				continue;
			}
		}


		while ($row = mysql_fetch_array($query)) {
			$email 	= $row['email'];
			$id 	= $row['id'];

			if (empty($email)) {
				continue;
			} else {
				$sql_email['email'][] = $email;
				$sql_email['id'][]	  = $id;
	 		}
		}
		
		$match 		= array_intersect($sql_email['email'], $excel);
		$diff  		= array_diff($sql_email['email'], $excel);
	
		//Save the matches to the database capture any errors that might happen during the saving
		foreach ($match as $index => $email) {
			$sql_insert = "insert into contacts_interests values('NULL','".$sql_email['id'][$index]."','".rand(20,10)."');";
			if (!mysql_query($sql_insert)) {
				array_push($errors, "Error saving saving user ".$value. " " .$sql_email['id'][$index] . " " .mysql_error());
			}
		}
		
		
		//no matches prepare the table
		if (empty($excel_error)) { 
			$no_match .= "<table border =1 width=500px>";
			$no_match .= "<tr>";
			$no_match .= "<th><strong>Emails not found on the Excel File</strong></th>";
			$no_match .= "</tr>";
			
			foreach ($diff as $email) {
				$no_match .= "<tr>";
				$no_match .= "<td>".$email."</td>";
				$no_match .= "</tr>";
			}	
			$no_match .= "</table>";
		}
		
		if (!empty($errors) && !empty($excel_error)) {
			echo '<pre>',print_r($errors),"</pre>";
		} else {
			//once processing is done the is no need to keep the file
			unlink($file_name);
		}

		//if a wrong excel file is uploaded that doesnt match the requirements alert the user
		if (empty($match)) {
			$error = "Please upload a compatible file ".$excel_error;
		}
		
	}	
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0"/>
		<title>Avior Capital Markets CRM</title>
		<link type="text/css" href="css/kdes1/jquery-ui-1.8.4.custom.css" rel="stylesheet" />	
		<script src="js/jquery-1.9.1.min.js"></script>
		<script src="js/jquery-migrate-1.1.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.4.custom.min.js"></script>
		<script type="text/javascript">
		$(function()
		{
			$('#container').tabs();
		})
		</script>
		<link href="css/2.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			body
			{
				padding-left 	: 0;
				padding-right 	: 0; 
			}

			#container
			{
				text-align 		: center; 
				padding 		: 100px;
				border 			: none !important;
				padding-right 	: 0;
				padding-left 	: 350px;  
			}
			#upload
			{
				float 			: left;
			}
			#tabs1-1
			{
				padding-right  	: 0;
				padding-left  	: 0; 
			}
			#uploadData
			{
				float 			: right;
			}
			#results
			{
				padding-top  	: 20px;
			}
		</style>
	</head>
	<body>
		<div id="container" style="width:500px;">
	    	<ul>
	        	<li>
	        		<a href="#tabs1-1">Upload File</a>
	    		</li>
	      	</ul>
	      	<div id="tabs1-1">
				<form id= "formUpload" method = "post" action = ""  enctype="multipart/form-data">
					<input type = "file" name="upload" id="upload" />
					<input type="hidden" name ="name" />
					<button type = "button"  id ="uploadData">Upload</button>
				</form>
			</div>
			<div id="results">
				<?php echo $error    ;?>
				<?php echo $no_match ;?>
			</div>
		</div>
		<script type="text/javascript">
			$("#uploadData").on("click",function(e){
				e.preventDefault();
				var file = $("#upload").val();
				if (file == "") {
					alert('Please upload a file')
				} else {
					$("form")[0].submit();
				}
			});
		</script>
	</body>
</html>