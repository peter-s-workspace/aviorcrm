<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE tbluser SET user_name=%s, user_pword=%s, user_fname=%s, user_lname=%s, user_email=%s, user_level=%s, user_phone=%s, user_nick=%s, user_bday=%s, user_phone_ip=%s, user_coverage=%s, popbox=%s WHERE user_id=%s",
                       GetSQLValueString($_POST['user_name'], "text"),
                       GetSQLValueString($_POST['user_pword'], "text"),
                       GetSQLValueString($_POST['user_fname'], "text"),
                       GetSQLValueString($_POST['user_lname'], "text"),
                       GetSQLValueString($_POST['user_email'], "text"),
                       GetSQLValueString($_POST['user_level'], "text"),
                       GetSQLValueString($_POST['user_phone'], "text"),
                       GetSQLValueString($_POST['user_nick'], "text"),
                       GetSQLValueString($_POST['user_bday'], "date"),
                       GetSQLValueString($_POST['user_phone_ip'], "text"),
                       GetSQLValueString($_POST['user_coverage'], "text"),
                       GetSQLValueString($_POST['popbox'], "int"),
                       GetSQLValueString($_POST['user_id'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($updateSQL, $CRMconnection) or die(mysql_error());

  $updateGoTo = "#";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_userdetails = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userdetails = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userdetails = sprintf("SELECT * FROM tbluser WHERE user_name = %s", GetSQLValueString($colname_userdetails, "text"));
$userdetails = mysql_query($query_userdetails, $CRMconnection) or die(mysql_error());
$row_userdetails = mysql_fetch_assoc($userdetails);
$totalRows_userdetails = mysql_num_rows($userdetails);
?>
<?php require_once('includes/sitevars.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><link type="text/css" href="css/kdes1/jquery-ui-1.8.4.custom.css" rel="stylesheet" />		
		<script src="js/jquery-1.9.1.min.js"></script>

<script src="js/jquery-migrate-1.1.1.min.js"></script>


		<script type="text/javascript" src="js/jquery-ui-1.8.4.custom.min.js"></script>
		<script type="text/javascript">
			$(function(){

				// Accordion
				//$("#accordion").accordion({ header: "h3" });
	
				// Tabs
				$('#tabs1').tabs();
				$('#tabs2').tabs();
	
				// Dialog			
				$('#dialog').dialog({
					autoOpen: false,
					width: 600,
					buttons: {
						"Ok": function() { 
							$(this).dialog("close"); 
						}, 
						"Cancel": function() { 
							$(this).dialog("close"); 
						} 
					}
				});
				
				// Dialog Link
				$('#dialog_link').click(function(){
					$('#dialog').dialog('open');
					return false;
				});

				// Datepicker
				$('#datepicker').datepicker({
					inline: true
				});
				
				// Slider
				$('#slider').slider({
					range: true,
					values: [17, 67]
				});
				
				// Progressbar
				$("#progressbar").progressbar({
					value: 20 
				});
				
				//hover states on the static widgets
				$('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); }, 
					function() { $(this).removeClass('ui-state-hover'); }
				);
				
			});
		</script>

<!-- InstanceEndEditable -->
<script language="javascript">
	$(function(){
		
		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					}
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);
				
					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
	})
</script>	
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			<div>
				<fieldset>
					<Legend>Quick Search</Legend>
					<input type="text" id="quicksearch" size="16" />
				</fieldset>
			</div> 
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<div class="content40">
<div id="tabs1">
      <ul>
        <li><a href="#tabs1-1">User details</a></li>
        <li><a href="#tabs1-2">Second</a></li>
        <li><a href="#tabs1-3">Third</a></li>
      </ul>
      <div id="tabs1-1"><fieldset><legend>User Details</legend>
          <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
            <table align="center">
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_id:</td>
                <td><?php echo $row_userdetails['user_id']; ?></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_name:</td>
                <td><input type="text" name="user_name" value="<?php echo htmlentities($row_userdetails['user_name'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_pword:</td>
                <td><input type="text" name="user_pword" value="<?php echo htmlentities($row_userdetails['user_pword'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_fname:</td>
                <td><input type="text" name="user_fname" value="<?php echo htmlentities($row_userdetails['user_fname'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_lname:</td>
                <td><input type="text" name="user_lname" value="<?php echo htmlentities($row_userdetails['user_lname'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_email:</td>
                <td><input type="text" name="user_email" value="<?php echo htmlentities($row_userdetails['user_email'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_level:</td>
                <td><input type="text" name="user_level" value="<?php echo htmlentities($row_userdetails['user_level'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_phone:</td>
                <td><input type="text" name="user_phone" value="<?php echo htmlentities($row_userdetails['user_phone'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_nick:</td>
                <td><input type="text" name="user_nick" value="<?php echo htmlentities($row_userdetails['user_nick'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_bday:</td>
                <td><input type="text" name="user_bday" value="<?php echo htmlentities($row_userdetails['user_bday'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_clist:</td>
                <td><input type="text" name="user_phone_ip" value="<?php echo htmlentities($row_userdetails['user_phone_ip'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">User_coverage:</td>
                <td><input type="text" name="user_coverage" value="<?php echo htmlentities($row_userdetails['user_coverage'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">Popbox:</td>
                <td><input type="text" name="popbox" value="<?php echo htmlentities($row_userdetails['popbox'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
              </tr>
              <tr valign="baseline">
                <td nowrap="nowrap" align="right">&nbsp;</td>
                <td><input type="submit" value="Update record" /></td>
              </tr>
            </table>
            <input type="hidden" name="MM_update" value="form1" />
            <input type="hidden" name="user_id" value="<?php echo $row_userdetails['user_id']; ?>" />
          </form>
          <p>&nbsp;</p>
      </fieldset></div>
      <div id="tabs1-2">Phasellus mattis tincidunt nibh. Cras orci urna, blandit id, pretium vel, aliquet ornare, felis. Maecenas scelerisque sem non nisl. Fusce sed lorem in enim dictum bibendum.</div>
      <div id="tabs1-3">Nam dui erat, auctor a, dignissim quis, sollicitudin eu, felis. Pellentesque nisi urna, interdum eget, sagittis et, consequat vestibulum, lacus. Mauris porttitor ullamcorper augue.</div>
</div>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($userdetails);
?>
