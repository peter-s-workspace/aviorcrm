<?php 
if (!isset($_SESSION)) {
  session_start();
}
require_once('Connections/CRMconnection.php'); 
require_once('includes/event_log/eventlog.class.php'); 
require_once('includes/classes/company.class.php'); 

if(isset($_SESSION['MM_Username']) && isset( $_SESSION['MM_UserGroup'] ) &&  $_SESSION['MM_UserGroup'] == 'Administrator' ) {

	if( isset( $_POST['comp_update'] ) ) {
		$companyId = (isset($_POST['comp_id']) && is_numeric($_POST['comp_id']) && $_POST['comp_id'] > 0) ? $_POST['comp_id'] : 0;
		$companyObj = new Company($companyId);
		$formUpdateResult = $companyObj->updateCompany( $_POST );
		echo $formUpdateResult;
	}
}
?>
