	<?php 
	require_once('Connections/CRMconnection.php'); 
	require_once('includes/classes/hipchat.class.php');
?>
<?php

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
  $isValid = False;
  if (!empty($UserName)) {
    $arrUsers = Explode(",", $strUsers);
    $arrGroups = Explode(",", $strGroups);
    if (in_array($UserName, $arrUsers)) {
      $isValid = true;
    }
    if (in_array($UserGroup, $arrGroups)) {
      $isValid = true;
    }
    if (($strUsers == "") && true) {
      $isValid = true;
    }
  }
  return $isValid;
}
$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0)
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo);
  exit;
}

require_once('includes/sitevars.php');
require_once('includes/classes/company.class.php');
require_once('includes/classes/notes.class.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$companyId = isset( $_GET['id'] ) ? $_GET['id'] : 0;
$backToClient = isset( $_GET['back_cid'] ) ? $_GET['back_cid'] : 0;

$formSuccessfullyUpdated = (isset($_GET['updated']) && $_GET['updated'] == 'success') ? true : false;

if( $companyId == 0 ) { //Go back to admin main if there's no ID.
	header('Location: companyratings.php');
	exit();
}

$companyObj = new Company($companyId);
$notesObj = new Notes();
$companyDetails = $companyObj->getCompanyProfile();
$companyName = isset($companyDetails[0]['company']) ? $companyDetails[0]['company'] : '';
$employees = array();
$companyNotes = array();
if($companyName != '') {
	$employees = $companyObj->employees($companyDetails[0]['id']);
}


/**
 * Get data for filter drop downs
 */
$filterDataType = $companyObj->getTypeData();
$filterDataAnalyst = $companyObj->getAnalystData();
$filterDataClient = $companyObj->getClientData();


/**
 * Create string of semi-colon separated emails, which will be used for copying
 */
$emailString = "";
foreach ($employees as $emp ) {
    if($emp['active'] == "1"){
        $emailString .= $emp['email'] . ";";
    }
}
$hipchat_instance 		  = new hipchat();

$get_hipchat_room_name    = $hipchat_instance->get_all_rooms();

$get_rooms                = "SELECT hipchat_room_id FROM tblcompany_hipchatrooms where company_id = ".$_GET['id']. "";

$rooms                    = null;
$run_rooms_query          = mysql_query($get_rooms, $CRMconnection) or die (mysql_error());
if (!$run_rooms_query)  {
  die ('Error fetching Data '. mysql_error($CRMconnection));
} else {  
  while ($row         = mysql_fetch_array($run_rooms_query)) {
    $company_room      = $row['hipchat_room_id'];
    //for the client
    foreach ($get_hipchat_room_name as $room_key => $room) {
        if ($company_room == $room->getId()) {
            $company_rooms [] = $room->getName();
        }
    }
  } 
}
if(!isset($company_rooms)) {
	$company_rooms = [];
}
$hipchat_room_name 	= implode(",", $company_rooms);
/**
* Create company folder using Company ID. This will be where the public shared company files are stored.
*/
$companyObj->createSharedDirectory();


/**
* Get contact type data for add note form
*/
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsCtype = "SELECT * FROM tblcontacttype ORDER BY ContactType ASC";
$rsCtype = mysql_query($query_rsCtype, $CRMconnection) or die(mysql_error());
$row_rsCtype = mysql_fetch_assoc($rsCtype);
$totalRows_rsCtype = mysql_num_rows($rsCtype);

/**
* Get data for note submittions
*/
$colname_userdet = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_userdet = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userdet = sprintf("SELECT * FROM contacts, tblcompanylist,tblcompanytier WHERE contacts.id = %s AND tblcompanylist.comp_id=contacts.company_id AND tblcompanytier.id_companytier = tblcompanylist.id_companytier", GetSQLValueString($colname_userdet, "int"));
$userdet = mysql_query($query_userdet, $CRMconnection) or die(mysql_error());
$row_userdet = mysql_fetch_assoc($userdet);
$totalRows_userdet = mysql_num_rows($userdet);

$colname_userd = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userd = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userd = sprintf("SELECT * FROM tbluser WHERE tbluser.user_name = %s", GetSQLValueString($colname_userd, "text"));
$userd = mysql_query($query_userd, $CRMconnection) or die(mysql_error());
$row_userd = mysql_fetch_assoc($userd);
$totalRows_userd = mysql_num_rows($userd);


########################### Get Analyst Data #############
 $note_admins_allowed = array('Administrator','NoteAdmin');

  if (in_array($_SESSION['MM_UserGroup'], $note_admins_allowed)) {
      $show_analyst_dropdown = "";
  } else {
      $show_analyst_dropdown = "none";
  }

  $analyst_query      = "SELECT CONCAT(u.user_fname,' ',u.user_lname) as full_name, u.user_name FROM avcrm.tbluser u 
                     WHERE u.user_level <> 'Administrator' OR u.user_level <> 'NoteAdmin' ORDER BY full_name ASC;";

 $run_analyst_query   = mysql_query($analyst_query, $CRMconnection);
  
 if ($run_analyst_query) {
    $analyst_select         = "<select name=analyst_list id=analyst_list>";

    $analyst_select        .= "<option selected value=''> -- Select Analyst -- </option>";
    while ($analyst_row     = mysql_fetch_array($run_analyst_query)) {
     $analyst_option_name   = urlencode($analyst_row['full_name']);
     $analyst_display_name  = $analyst_row['full_name'];
     $analyst_username      = $analyst_row['user_name'];
     ### Spaces have an issue when setting them as option values need to be encoded ##########
     $analyst_select       .= "<option value=$analyst_username>".$analyst_display_name."</option>";
    }
    $analyst_select        .= "</select>";
 }                 
########################### End Analyst Data #############
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
<link rel="manifest" href="favicons/manifest.json">
<link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<link type="text/css" href="css/kdes1/jquery-ui-1.9.2.custom.css" rel="stylesheet" />

<link type="text/css" href="css/datatables/dataTables.twitter.bootstrap.css" rel="stylesheet" />
<link type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css" rel="stylesheet" />

<link type="text/css" href="css/uploadify.css" rel="stylesheet" />
<link type="text/css" href="css/skipjack.css" rel="stylesheet" />
<script src="js/jquery-1.11.1.min.js"></script>
<!-- InstanceEndEditable -->
<script language="javascript">
	$(function(){

		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					},
					appendTo: '.quick-search-container'
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);

					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
	})
</script>
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			<div class="quick-search-container">
					<input type="text" id="quicksearch" size="16" />
			</div>
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<h2>Institution Details : <?php echo $companyDetails[0]['company']; ?></h2>
<div class="content30" style="position:relative;">
<div id="tabs1">
		<ul>
			<li><a href="#tabs1-1">Details</a></li>
			<li><a href="#tabs1-2">People</a></li>
			<li><a href="#tabs1-3">Responsibility List Files</a></li>
		</ul>
		<div id="tabs1-1">
			<fieldset>
				<legend>View Institution Details</legend>
				  <table border="0" width="100%" >
					<tr>
						<td nowrap="nowrap">Name:</td>
						<td><?php
							  $outcolor="";
							  $tve= $companyDetails[0]['companytier'];

							  switch ($tve){
							  case 'Platinum':
								$outcolor= "#E5E4E2";
								$color = "#333333";
								break;
							  case 'Gold' :
								$outcolor= "#B8860B";
								$color = "#FFF";
								break;
							  case 'Silver' :
								$outcolor= "#C0C0C0";
								$color = "#333333";
								break;
							  case 'Blue' :
								$outcolor= "#0A238C";
								$color = "#FFF";
								break;
							case 'Bronze' :
								$outcolor= "#f4a460";
								$color = "#333333";
								break;
							case 'N/A' :
							case 'Not Assigned' :
								$outcolor= "#fff";
								$color = "#333333";
								break;
							  default :
								$outcolor= "#CE1620";
								$color = "#FFF";
							  }
							  ?>
							  <span style="width:100%;display:block; font-size:1em; background:<?php echo $outcolor; ?>; padding:4px; border-radius:3px; color: <?php echo $color; ?>;">
									<?php echo $companyDetails[0]['company']; ?>
							  </span></td>
						<td width="50%">
							<?php if( $_SESSION['MM_UserGroup'] == 'Administrator' ) { ?>
							<div class="static-edit-link"><a href="companydetails-edit.php?id=<?php  echo $companyId;?>">Edit</a></div>
						 <?php } ?>
					 	</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Geography:</td>
						<td colspan="2"><?php echo $companyDetails[0]['geography']; ?></td>

					</tr>
					<tr>
						<td nowrap="nowrap">Status:</td>
						<td><?php echo $companyDetails[0]['company_status']; ?></td>
						<td rowspan="6">
							<div class="company-image-details">
								<?php if(file_exists("uploads/companyp/".$companyDetails[0]['id']."/1.jpg") ) { ?>
								<img src="uploads/companyp/<?php echo $companyDetails[0]['id']; ?>/1.jpg" class="complogo" alt="Logo Image" border="0" /><br />
								<?php } ?>
								<div class="desc">
									<span class="website"><?php echo $companyDetails[0]['website']; ?></span><br />
									<?php if (isset($companyDetails[0]['social_fb']) && $companyDetails[0]['social_fb'] != '') { ?>
										<a href="<?php echo $companyDetails[0]['social_fb']; ?>" target="_blank" title="Click to View"><img src="images/social_FB.jpg" alt="Facebook" width="20" border="0" /></a>&nbsp;
									<?php }  ?>
									<?php if (isset($companyDetails[0]['social_twitter']) && $companyDetails[0]['social_twitter'] != '') { ?>
										<a href="<?php echo $companyDetails[0]['social_twitter']; ?>" target="_blank" title="Click to View"><img src="images/social_Tw.jpg" alt="Twitter" width="20" border="0" /></a>&nbsp;
									<?php }  ?>
									<?php if (isset($companyDetails[0]['social_linkedin']) && $companyDetails[0]['social_linkedin'] != '') { ?>
										<a href="<?php echo $companyDetails[0]['social_linkedin']; ?>" target="_blank" title="Click to View"><img src="images/social_Li.jpg" alt="LinkedIn" width="20" border="0" /></a>&nbsp;
									<?php }  ?>
								</div>
							</div>
							<br />
						</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Type:</td>
						<td><?php echo $companyDetails[0]['companytype']; ?></td>
					</tr>
					<tr>
						<td nowrap="nowrap">Protection Type:</td>
						<td><?php
								$selected_drm = isset($companyDetails[0]['drm_status']) ? $companyDetails[0]['drm_status'] : '';
								if( $selected_drm == 1 ) echo 'Watermark';
								else if( $selected_drm == 2 ) echo 'DRM';
								else if( $selected_drm == 3 ) echo 'Web Viewer';
                                else echo 'Not Set';
							?>
						</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Portal Access:</td>
						<td><?php
								if( $companyDetails[0]['portal_access'] ) echo 'Enabled';
								else echo 'Disabled';
							?>
						</td>
					</tr>   
					
					<tr>
						<td nowrap="nowrap">Hipchat Room:</td>
						<td><?php
								if( $companyDetails[0]['companytier'] || isset($hipchat_room_name)) echo $companyDetails[0]['companytier'].','.$hipchat_room_name;
								else echo 'No Hipchat Room linked';
							?>
						</td>
					</tr>     
					<tr>
						<td nowrap="nowrap">Address 1:</td>
						<td><?php echo $companyDetails[0]['address_1']; ?></td>

					</tr>
					<tr>
						<td nowrap="nowrap">Address 2:</td>
						<td><?php echo $companyDetails[0]['address_2']; ?></td>

					</tr>
					<tr>
						<td nowrap="nowrap">Switchboard 1:</td>
						<td><?php echo  $companyDetails[0]['switchboard_1']; ?> <a style="float:right;" href="#" onclick="$('#phonebox').load('http://<?php echo ''; ?>/command.htm?number=<?php echo ''; ?>')"> <img src="images/phoneicon.png" width="20" height="20" align="absmiddle" /></a></td>
					</tr>
					<tr>
						<td nowrap="nowrap">Switchboard 2:</td>
						<td><?php echo  $companyDetails[0]['switchboard_2']; ?><a style="float:right;" href="#" onclick="$('#phonebox').load('http://<?php echo ''; ?>/command.htm?number=<?php echo ''; ?>')"> <img src="images/phoneicon.png" width="20" height="20" align="absmiddle" /></a></td>
						<td rowspan="2"></td>
					</tr>
					<tr>
						<td  colspan="2">
							<div class="other-cnt">
							<?php  if(isset( $companyDetails[0]['id_kam1'])){ ?>
								<div class="image-circle">
									<?php if(file_exists("uploads/clientp/".$companyDetails[0]['id_kam1']."/1.jpg") ) { ?>
									<div class="image"><a href="clientdetails.php?cnt_Id=<?php echo $companyDetails[0]['id_kam1']; ?>" title="View Profile"><img src="uploads/clientp/<?php echo $companyDetails[0]['id_kam1'];?>/1.jpg" /></a></div>
									<?php } else { echo '<div class="no-image">No picture yet.</div>'; } ?>
									<div class="desc">KAM 1</div>
								</div>
							<?php }  ?>

							<?php  if(isset( $companyDetails[0]['id_kam2'])){ ?>
								<div class="image-circle">
									<?php if(file_exists("uploads/clientp/".$companyDetails[0]['id_kam2']."/1.jpg") ) { ?>
									<div class="image"><a href="clientdetails.php?cnt_Id=<?php echo $companyDetails[0]['id_kam2']; ?>" title="View Profile"><img src="uploads/clientp/<?php echo $companyDetails[0]['id_kam2'];?>/1.jpg" /></a></div>
									<?php } else { echo '<div class="no-image">No picture yet.</div>'; } ?>
									<div class="desc">KAM 2</div>
								</div>
							<?php }  ?>

							<?php  if(isset( $companyDetails[0]['id_dealer1'])){ ?>
								<div class="image-circle">
									<?php if(file_exists("uploads/clientp/".$companyDetails[0]['id_dealer1']."/1.jpg") ) { ?>
									<div class="image"><a href="clientdetails.php?cnt_Id=<?php echo $companyDetails[0]['id_dealer1']; ?>" title="View Profile"><img src="uploads/clientp/<?php echo $companyDetails[0]['id_dealer1'];?>/1.jpg" /></a></div>
									<?php } else { echo '<div class="no-image">No picture yet.</div>'; } ?>
									<div class="desc">Dealer 1</div>
								</div>
							<?php }  ?>

							<?php  if(isset( $companyDetails[0]['id_dealer2']) ){ ?>
								<div class="image-circle">
									<?php if(file_exists("uploads/clientp/".$companyDetails[0]['id_dealer2']."/1.jpg") ) { ?>
									<div class="image"><a href="clientdetails.php?cnt_Id=<?php echo $companyDetails[0]['id_dealer2']; ?>" title="View Profile"><img src="uploads/clientp/<?php echo $companyDetails[0]['id_dealer2'];?>/1.jpg" /></a></div>
									<?php } else { echo '<div class="no-image">No picture yet.</div>'; } ?>
									<div class="desc">Dealer 2</div>
								</div>
							<?php }  ?>
								</div>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<?php if($backToClient > 0 ){ ?>
							<p><a href="clientdetails.php?cnt_Id=<?php echo $backToClient; ?>" class="btn">Back</a></p>
							<?php } else { ?>
							<p><a href="companyratings.php" class="btn">Back</a></p>
							<?php } ?>
						</td>
					</tr>
				  </table>
			</fieldset>
		</div>
		<div id="tabs1-2">
			<fieldset>
                <input type="button" value="Copy Email Addresses" id="copyEmailButton"/>
                <input type="hidden" id="emailAddresses" class="emailAddresses" value="<? echo $emailString ?>"/>
					<legend>View People</legend>
					  <table class="table table-striped dataTable" id="people-list" width="100%" >
						<thead>
							<tr>
								<th>Name</th>
								<th>Designation</th>
								<th># of mailing lists</th>
							</tr>
						</thead>
						<tbody>
							<?php if (sizeof($employees) > 0) {
								for ($x=0; $x < sizeof($employees); $x++) {
								$inactiveClass = $employees[$x]['active'] == 0 ? 'style="color:#666666;"' : "";
							?>
								<tr>
									<td><a href="clientdetails.php?cnt_Id=<?php echo $employees[$x]['id'];?>" <?php echo $inactiveClass; ?> title="Click to view profile"><?php echo $employees[$x]['client_name']; ?></a></td>
									<td><?php echo $employees[$x]['job_title']; ?></td>
									<td class="center"><?php echo $employees[$x]['mailing_lists']; ?></td>
								</tr>
							<?php }// end for
							} else { ?>
						<?php } ?>
						</tbody>
					  </table>
				</fieldset>
		</div>
		<div id="tabs1-3">
			<fieldset>
					<legend>Upload Shared files</legend>
					<div id="pubfileUpload">You have a problem with your javascript</div>
			</fieldset>
			<fieldset>
					<legend>Shared files</legend>
					<div id="shared-files"></div>
			</fieldset>
		</div>
	</div>
</div>
<div class="content50">
<div id="tabs2">
		<ul>
			<li><a href="#tabs2-1">Recent Contact</a></li>
			<li><a href="#tabs2-2">News</a></li>
			<li><a href="#tabs2-3">Social</a></li>
		</ul>
    
    
		<div id="tabs2-1">
			<fieldset>
				  <legend>Notes</legend>
				  <input type="button" id="addnotebutton" value="Add Note" />
				  <input type="button" id="cancelnotebutton" value="Cancel" />
				  <!-- add notes form -->
				  <form id="addnoteform" style="background:#ccc;">
					  <table>
						  <tr>
						  	<td valign="top">Note:<br /> 
						  	<textarea name="note_txt" rows="4"></textarea>
							</td>
						  	<td valign="top">Date:<br /> 
								<input name="note_date" type="text" id="note_date" value="<?php echo date("Y-m-d"); ?>" /><br />
								Time<br />
								<input type="time" name="note_time" id="note_time" value=""><br />
								Type<br />
								<select name="note_type" id="note_type">
							  <?php do { ?>
								  <option value="<?php echo $row_rsCtype['ContactType']?>"><?php echo $row_rsCtype['ContactType']?></option>
							  <?php
						} while ($row_rsCtype = mysql_fetch_assoc($rsCtype));
						  $rows = mysql_num_rows($rsCtype);
						  if($rows > 0) {
							  mysql_data_seek($rsCtype, 0);
							  $row_rsCtype = mysql_fetch_assoc($rsCtype);
						  }
						?>
								</select>
								<div style="display: <?php echo $show_analyst_dropdown;?>">
				                    Note By<br/>
				                    <?php 
				                      echo $analyst_select;
				                    ?>
                </div>

						</td>
						<td>
							<input type="button" id="savenewnotebutton" value="Save" /><input type="hidden" name="MM_noteadd" value="addnote" />
							<input type="hidden" name="note_by" value="<?php echo $_SESSION['MM_Username']; ?>" />
							<input type="hidden" name="notebyNick" value="<?php echo $row_userd['user_fname']; ?>" />
              
							<input type="hidden" name="note_cnt_Id" value="<?php echo $row_userdet['id']; ?>" />
							<input type="hidden" name="note_cnt_name" value="<?php echo $row_userdet['first_name']; ?> <?php echo $row_userdet['surname']; ?>" />
							<input type="hidden" name="cntCompany" value="<?php echo $companyDetails[0]['company']; ?>" />
					   </td>
				   </tr>
				  </table>
			  </form>
			  </fieldset>
            
			<fieldset>
				<legend>Contact Notes</legend>
                <button onclick="location.href='search_notes.php';" style="display: inline-block; height: 30px; width: 150px;">Global Note Search</button>
                
                <button style="display: inline-block; height: 30px; width: 130px; float:right; display:none;" id="export-datatable">Export Data</button>                
                
                
       <table border="0" cellspacing="0" cellpadding="0" style="margin:10px 0;">
            <tbody>
                <tr>
                    <td style="padding-left:0;">Start Date:</td>
                    <td><input name="mindate" id="mindate" type="text"></td>
                </tr>
                <tr>
                    <td style="padding-left:0;">End Date:</td>
                    <td><input name="maxdate" id="maxdate" type="text"></td>
                </tr>
            </tbody>
        </table>
        
				<table class="table table-striped dataTable" id="company-notes"  >
						<thead>
							<tr>
								<th>Date</th>
								<th>Note</th>
								<th>Type</th>
								<th>By</th>
								<th>Client</th>
							</tr>
						</thead>
            
						<thead class="filters">
							<tr>
								<th>&nbsp;</th>
								<th>
									<input class="filter" type="text" id="search-filter-note" name="search-filter-note" value="" />                  	
                </th>                
								<th>
                	<select class="filter" id="search-filter-type" name="search-filter-type">                  	
	                  <option value="">&nbsp;</option>
                    <?php for($t=0;$t<sizeof($filterDataType);$t++) { ?>
  	                <option value="<?php echo $filterDataType[$t]['type']?>"><?php echo $filterDataType[$t]['type']?></option>                                              
                    <?php } ?>            
                  </select>
                </th>
								<th>                
                	<select class="filter" id="search-filter-analyst" name="search-filter-analyst">
	                  <option value="">&nbsp;</option>
                    <?php for($t=0;$t<sizeof($filterDataAnalyst);$t++) { ?>
  	                <option value="<?php echo $filterDataAnalyst[$t]['analyst']?>"><?php echo $filterDataAnalyst[$t]['analyst']?></option>                                              
                    <?php } ?>                                          
                  </select>
                </th>
								<th>
                	<select class="filter" id="search-filter-client" name="search-filter-client">
	                  <option value="">&nbsp;</option>
                    <?php for($t=0;$t<sizeof($filterDataClient);$t++) { ?>
  	                <option value="<?php echo $filterDataClient[$t]['client']?>"><?php echo $filterDataClient[$t]['client']?></option>                                              
                    <?php } ?>                           
                  </select>
                </th>                                
							</tr>
						</thead>                        
						<tbody>
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
						</tbody>
					  </table>
            
	   <!-- Dialog for showing all the email counts !-->
	   <div id="EmailDialog">
	   		<table border="0" width="100%" id="email-reads-count" class="table table-striped dataTable" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th>Date</th>
							<th>Ip Address</th>
							<th>Host Name</th>
							<th>Reader</th>
							<th>Report Title</th>
						</tr>
					</thead>
			</table>
	   </div>            
     
	   <div id="WebviewDialog">
	   		<table border="0" width="100%" id="webview-opens-count" class="table table-striped dataTable" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th>Date</th>
							<th>Ip Address</th>
							<th>Reader</th>
							<th>Report Title</th>
						</tr>
					</thead>
			</table>
	   </div>                 

			</fieldset>
		</div>
		<div id="tabs2-2">
			<fieldset>
					<legend>News</legend>
					  <p>Coming Soon</p>
				</fieldset>
		</div>
		<div id="tabs2-3">
			<fieldset>
					<legend>Social</legend>
					<p>Coming Soon</p>
				</fieldset>
		</div>
	</div>

</div>
</div>
</div>

<script type="text/javascript" src="js/swfobject.js"></script>
<script type="text/javascript" src="js/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="js/jquery.linkify.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="js/companydetails.js"></script>
<script type="text/javascript">

	$(document).ready(function(){		
					
		$('#tabs1, #tabs2').tabs();
		$('.website').linkify();

		var cid = "<?php echo $companyDetails[0]['id']; ?>";
		var cIdEnc = "<?php echo sha1(md5($companyDetails[0]['id'])); ?>";

		var compDetails = new companyDetails(cid, cIdEnc);

		compDetails.peopleToDatatable();
		compDetails.setResponsibilityUploader();
		compDetails.displayResponsibilityFiles();
		compDetails.getCompanyNotes();


        var btn = document.getElementById('copyEmailButton');
        btn.addEventListener('click' , function(event){


           var hiddenInput = document.getElementById('emailAddresses');
            hiddenInput.select();
            var str = hiddenInput.value;
            if(window.navigator.appVersion.indexOf("Win") != -1){
                window.prompt("Copy emails to clipboard: Ctrl+C, Enter", str);
            } else if(window.navigator.appVersion.indexOf("Mac") != -1){
                window.prompt("Copy emails to clipboard: Cmd+C, Enter", str);
            }

        });
		var oTable 			= $('#email-reads-table').DataTable({"autoWidth" :false, "order":[[1, "desc"]]});
		var oTableCount		= $('#email-reads-count').DataTable({"autoWidth" :false, "order":[[0, "desc"]]});

		$('#addnoteform').hide();
		$('#addreminderform').hide();
		$('#cancelnotebutton').hide();
		
		//hide add notes
		$('#addnotebutton').click( function(){
			$('#addnotebutton').hide();
			$('#addnoteform').show('fold');
			$('#cancelnotebutton').show('fold');
			return false;
		});		
		
		//cancel new note button operations
		$('#cancelnotebutton').click( function(){
			$('#addnoteform').hide();
			$('#addnotebutton').show();
			$('#cancelnotebutton').hide();
			return false;
		});		
		
		$('#savenewnotebutton').on('click',function(){
			var form_data 		= $("#addnoteform").serializeArray();
				
			var newDate = new Date();
			var DefaultTime = newDate.getHours() + ':' + newDate.getMinutes() + ':' + newDate.getSeconds();
			console.log(form_data[2].value);
			//index 2 is the note time value  			
			if (form_data[2].value == "") {
				form_data[2].value = DefaultTime;
			} 
			$.ajax({
				type: "POST",
				url: "ajaxActionNotes.php",
				dataType : 'JSON',
				data: $.param(form_data)
				}).done(function( data ) {

						var editLink = 	'<a href="javascript://" class="edit-button" data-id="'+data.note_id+'"><small>Edit</small></a>';
						var deleteLink = 	'<a href="javascript://" class="delete-button" data-id="'+data.note_id+'"><small>Delete</small></a>';
						var action = editLink+' <br /><br /> '+deleteLink;
						oTable.row.add([data.note_date+" "+data.note_time, data.note_text, data.note_type, data.notebyNick, action ]).draw();
						//$('#contact-list-table').linkify();
						$( "#addnoteform").trigger( "reset" );		
				
				});
			$('#addnoteform').hide('fold');
			$('#cancelnotebutton').hide('fold');
			$('#addnotebutton').show('fold');
	});			
				
	});

</script>
<script type="text/javascript" src="js/getEmails.js"></script>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
