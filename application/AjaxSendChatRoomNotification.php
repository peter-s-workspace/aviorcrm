<?php
require_once("includes/classes/hipchat.class.php");
require_once("Connections/CRMconnection.php");

if (isset($_POST) && !empty($_POST)) {

	$hip_chat_instance 		= new hipchat();
	$hip_chat_rooms 		= array();
	
	$hip_chat_message 		= filter_var($_POST['message'], FILTER_SANITIZE_STRING);
	$note_by 				= filter_var($_POST['note_by'], FILTER_SANITIZE_STRING);

	$platform 				= filter_var($_POST['platform'], FILTER_SANITIZE_STRING);
	######### This code should handle if either the company hipchat or contact hipchat has been set and add them into an array ######################
	if ($platform == 'production') {

		foreach ($_POST['client_rooms'] as $room_array_key => $room_array) {
			foreach ($room_array as $room_id_key => $room_id) {
				# code...
				$hip_chat_rooms [] 	= $room_id;
			}
		}
	} else {
		################# Get the Test keys ##################################
		$hipchat_test_rooms_ids 			= GetTestRoomKeys($database_CRMconnection, $CRMconnection);
		$hip_chat_rooms 					= $hipchat_test_rooms_ids;
		################# End Logic ##################################
	}

	foreach ($hip_chat_rooms as  $room_key => $room_id) {
		############ Send the message to the particular rooms being specified #######################
		$hip_chat_instance->send_hipchat_message($room_id, $note_by, $hip_chat_message);
	}
	########## End code Here ############################
}

function GetTestRoomKeys ($datasource, $connection_object) {
	$test_keys 			= "SELECT * FROM avcrm.config";
	mysql_select_db($datasource);
	$run_query 			= mysql_query($test_keys , $connection_object) or die('Error ' . mysql_error());

	while ($config_row	= mysql_fetch_array($run_query)) {
		  $key 	= $config_row['key'];
		  switch ($key) {
		  	case 'hipchat_test_rooms_ids':
		  		# code...
		  		$hipchat_room_ids = $config_row['value'];
		  		break;
		  	default:
		  		# code...
		  		break;
		  }

	}
	################ return an array of the rooms in this function instead of the caller #######
	return explode(',', $hipchat_room_ids);
	################						####################	
}
?>