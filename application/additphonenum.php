<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST['cnt_id'])) && ($_POST['cnt_id'] != "") && (isset($_POST['dodel']))) {
  $deleteSQL = sprintf("DELETE FROM addphone WHERE cnt_id=%s AND phone_num =%s",
                       GetSQLValueString($_POST['cnt_id'], "int"),
					   GetSQLValueString($_POST['phone_num'], "text"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($deleteSQL, $CRMconnection) or die(mysql_error());
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "add_addnum")) {
  $insertSQL = sprintf("INSERT INTO addphone (cnt_id, phone_num) VALUES (%s, %s)",
                       GetSQLValueString($_POST['cnt_id'], "int"),
                       GetSQLValueString($_POST['phone_num'], "text"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());
   
   $insertGoTo = "clientdetails.php?cnt_Id=".GetSQLValueString($_POST['cnt_id'], "int");
  header(sprintf("Location: %s", $insertGoTo));
  
}

$colname_rsaddnumbs = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rsaddnumbs = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsaddnumbs = sprintf("SELECT * FROM addphone WHERE cnt_id = %s", GetSQLValueString($colname_rsaddnumbs, "int"));
$rsaddnumbs = mysql_query($query_rsaddnumbs, $CRMconnection) or die(mysql_error());
$row_rsaddnumbs = mysql_fetch_assoc($rsaddnumbs);
$totalRows_rsaddnumbs = mysql_num_rows($rsaddnumbs);

$colname_rsCnt = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rsCnt = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsCnt = sprintf("SELECT * FROM contacts WHERE id = %s", GetSQLValueString($colname_rsCnt, "int"));
$rsCnt = mysql_query($query_rsCnt, $CRMconnection) or die(mysql_error());
$row_rsCnt = mysql_fetch_assoc($rsCnt);
$totalRows_rsCnt = mysql_num_rows($rsCnt);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>
</head>
<body>
<p><strong><a href="clientdetails-edit.php?cnt_Id=<?php if (isset($_GET['cnt_Id'])){echo $_GET['cnt_Id'];} else if(isset($_GET['cnt_id'])){ echo $_GET['cnt_id'];} ?>">&lt;&lt; Back</a></strong></p>
<p>Additional phone numbers for: </p>
<h4><?php echo $row_rsCnt['cnt_Fname']; ?> <?php echo $row_rsCnt['cnt_Lastname']; ?><br />
  <?php echo $row_rsCnt['cnt_company']; ?>
</h4>
<strong>Add new number:</strong>
<form method="POST" action="<?php echo $editFormAction; ?>" name="add_addnum">
  
<input type="text" name="phone_num" />
<input type="submit" value="add" />
<input type="hidden" name="MM_insert" value="add_addnum" />
<input type="hidden" name="cnt_id" value="<?php echo $row_rsCnt['cnt_Id']; ?>"  />
</form>

<p><strong>Current numbers:</strong></p>
<?php do { ?>
  <form name="remove_adnum" method="POST" action="<?php echo $editFormAction; ?>" >
  <?php echo $row_rsaddnumbs['phone_num']; ?> <input name="delete" type="submit" value="delete" />
  <input type="hidden" name="cnt_id" value="<?php echo $row_rsaddnumbs['cnt_id']; ?>" />
  <input type="hidden" name="phone_num" value="<?php echo $row_rsaddnumbs['phone_num']; ?>" />
  <input type="hidden" name="dodel" value="yes" />
  </form>
  <?php } while ($row_rsaddnumbs = mysql_fetch_assoc($rsaddnumbs)); ?>
</body>
</html>
<?php
mysql_free_result($rsaddnumbs);

mysql_free_result($rsCnt);
?>
