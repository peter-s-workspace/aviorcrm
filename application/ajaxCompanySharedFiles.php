<?php 
if (!isset($_SESSION)) {
  session_start();
}

require_once 'includes/classes/company.class.php'; 

if (isset($_SESSION['MM_Username'])) {

	if( isset( $_GET['cid'] ) ) {
		$companyId = (isset($_GET['cid']) && is_numeric($_GET['cid']) && $_GET['cid'] > 0) ? $_GET['cid'] : 0;
		$companyObj = new Company($companyId);
		$responsiblityFiles = $companyObj->sharedFiles();
		echo json_encode($responsiblityFiles);
	}
}