<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}


function getdata($sharecode, $exchangecode){
       // Create DOM from URL or file
       $html = file_get_contents("http://www.bloomberg.com/quote/".$sharecode.":".$exchangecode);
        //echo $html;
		$sharenamepattern = '/<h2>(.*?)<\/h2>/s';
		$sharename = preg_match($sharenamepattern, $html, $sharenamearray);
		//echo $html;
		$pricepattern = '/<span\\s+class=\"\\s+price\">(.*?)<\/span>/s';
		$price = preg_match($pricepattern, $html, $pricearray);
	if(count($pricearray)<2){
		 $pricepattern = '/<span\\s+class=\"big_value\\s+price\">(.*?)<\/span>/s';
		 $price = preg_match($pricepattern, $html, $pricearray);
		}
		$formattedout = "<span class='shname'>".$sharenamearray[1]."<span><span class='shcode'>".$sharecode.":".$exchangecode."</span><span class='shprice'>".$pricearray[1]."</span></span>";       
		echo $formattedout;
}



?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsquotes = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_rsquotes = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsquotes = sprintf("SELECT * FROM quotes WHERE username = %s ORDER BY `code` ASC", GetSQLValueString($colname_rsquotes, "text"));
$rsquotes = mysql_query($query_rsquotes, $CRMconnection) or die(mysql_error());
$row_rsquotes = mysql_fetch_assoc($rsquotes);
$totalRows_rsquotes = mysql_num_rows($rsquotes);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
  <script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
  <script>
  $(function() {
	$('#btnaddquote').click( function(){
		$('#addshcode').show();
		$('#btnaddquote').hide();
	})
	$('#btncanceladd').click( function(){
		$('#addshcode').hide();
		$('#btnaddquote').show();
	})
	
	$('#btnsavequote').click(function(){
		var fdata = $('#addshcode').serialize();
		$('#quotelist').load('addquote.php?MM_insert=form1&'+fdata);
		$('#addshcode').hide();
		$('#btnaddquote').show();
	})
		
		
				  
  })
  
  
  </script>
  
  
</head>

<body><div>

<fieldset>
<legend>Quote box</legend>
<div id="addquote">
<input type="button" value="Add quote" id="btnaddquote" />

<form style="display:none;" name="addshcode" id="addshcode">
<label>Share Code<input name="code" type="text" size="8" maxlength="8" /></label>
<label>Exchange Code<input name="exchange" type="text" size="8" maxlength="4" /></label>
<input name="Submit" type="button" id="btnsavequote" value="Add" />
<input type="button" value="Cancel" id="btncanceladd" />
</form>
</div>
</fieldset>
<fieldset>
<div id="quotelist">
<ul>
  <?php do { ?>
    <li>
      <?php getdata($row_rsquotes['code'],$row_rsquotes['exchange']) ?>
      
    </li>
    <?php } while ($row_rsquotes = mysql_fetch_assoc($rsquotes)); ?>
</ul>
</div>
</fieldset>
</div>


</body>
</html>
<?php
mysql_free_result($rsquotes);
?>
