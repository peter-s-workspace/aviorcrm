<?php 
ini_set('max_execution_time', 300); //300 seconds = 5 minutes

if (!isset($_SESSION)) {
  session_start();
}
require_once('includes/classes/company.class.php'); 
require_once('includes/classes/notes.class.php'); 

$postData = $_POST;
$exportData = false;
if (isset($_GET['export']) && $_GET['export'] == 'true') {
	$postData = $_GET;
	$getData = $_GET;	
	$exportData = true;	
}

if(isset($_SESSION['MM_Username']) ) {
	
	$entity = (isset($postData['entity_alias'])) ? $postData['entity_alias'] : '';
	
	switch($entity) {
		
		case 'webview-opens':

			$notesObj = new Notes(false);		
			$email = (isset($postData['email'])) ? trim($postData['email']) : '';
			$reportTitle = (isset($postData['report_title'])) ? trim($postData['report_title']) : '';
			$reportTicker = (isset($postData['report_ticker'])) ? trim($postData['report_ticker']) : '';
			$contactName = (isset($postData['contact_name'])) ? trim($postData['contact_name']) : '';															
																			
			$webviewOpenData = $notesObj->getWebviewOpens($email, $reportTitle, $reportTicker, $contactName);			
			echo $webviewOpenData;
			exit();

		break;
		
		case 'read-emails':

			$notesObj = new Notes(false);		
			$taskId = (isset($postData['task_id'])) ? intval($postData['task_id']) : 0;		
			
			$readEmailData = $notesObj->getReadEmailsByTaskId($taskId);											
			echo $readEmailData;
			exit();

		break;

		case 'webview-metadata':

			//Set default variable
			$metaData = [];

			//Get post data
			$reportId = (isset($postData['reportid']) && is_numeric($postData['reportid']) && $postData['reportid'] > 0) ? $postData['reportid'] : '';
			$email = (isset($postData['email']) && trim($postData['email'] != '')) ? trim($postData['email']) : '';

			//Get webview open meta data
			$notesObj = new Notes();
			$metaData = $notesObj->getWebViewOpenMetaData($reportId, $email);			

			//Output data
			echo $metaData;
			exit();

		break;

		case 'company-contact-notes':
			if( isset( $postData['comp_id'] ) ) {
				$companyId = (isset($postData['comp_id']) && is_numeric($postData['comp_id']) && $postData['comp_id'] > 0) ? $postData['comp_id'] : '';
				
				$filterData = [];				
				$filterData['minDate'] = (isset($postData['min_date']) && trim($postData['min_date']) != '') ? $postData['min_date'] : null;
				$filterData['maxDate'] = (isset($postData['max_date']) && trim($postData['max_date']) != '') ? $postData['max_date'] : null;												

				$filterData['note'] = (isset($postData['search-filter-note']) && trim($postData['search-filter-note']) != '') ? trim($postData['search-filter-note']) : null;
				$filterData['type'] = (isset($postData['search-filter-type']) && trim($postData['search-filter-type']) != '') ? trim($postData['search-filter-type']) : null;
				$filterData['analyst'] = (isset($postData['search-filter-analyst']) && trim($postData['search-filter-analyst']) != '') ? trim($postData['search-filter-analyst']) : null;
				$filterData['client'] = (isset($postData['search-filter-client']) && trim($postData['search-filter-client']) != '') ? trim($postData['search-filter-client']) : null;
				
				$companyObj = new Company($companyId);
				$datatablesFormat = true;
				$notesObj = new Notes($datatablesFormat);
				
				if ($exportData == false) {
					$companyNotes = $notesObj->getCompanyNotes($companyId, $filterData);					
					echo $companyNotes;
				} else {
					$companyNotesArray = $notesObj->getCompanyNotesExportData($companyId, $filterData, $getData);																			
					$notesObj->exportData('company-contact-notes',$companyNotesArray);
					exit();
				}
			}
		break;
		
		case 'user-contact-list':

			if( isset( $getData['cnt_Id'] ) ) {
				
				$contactId = (isset($getData['cnt_Id']) && is_numeric($getData['cnt_Id']) && $getData['cnt_Id'] > 0) ? $getData['cnt_Id'] : '';				
				
				$sortNames = isset($getData['sortNames']) ? explode(',', $getData['sortNames']) : [];									
				$sortValues = isset($getData['sortValues']) ? explode(',', $getData['sortValues']) : [];																	
				
				$filterData = [];				
				$filterData['minDate'] = (isset($getData['min_date']) && trim($getData['min_date']) != '') ? $getData['min_date'] : null;
				$filterData['maxDate'] = (isset($getData['max_date']) && trim($getData['max_date']) != '') ? $getData['max_date'] : null;												
				$filterData['note'] = (isset($getData['search-filter-note']) && trim($getData['search-filter-note']) != '') ? trim($getData['search-filter-note']) : null;
				$filterData['type'] = (isset($getData['search-filter-type']) && trim($getData['search-filter-type']) != '') ? trim($getData['search-filter-type']) : null;
				$filterData['analyst'] = (isset($getData['search-filter-analyst']) && trim($getData['search-filter-analyst']) != '') ? trim($getData['search-filter-analyst']) : null;

				$notesObj = new Notes();
				
				$doesContactBelongToAvior = $notesObj->doesContactBelongToAvior($contactId);
				
				if ($doesContactBelongToAvior == false) {
					//$userNoteDataArray = $notesObj->getUserNotesDataForExport($contactId, $filterData, $sortNames, $sortValues);
					$userNoteDataArray = $notesObj->getUserNotesWithAviorLibDataForExport($contactId, $filterData, $sortNames, $sortValues);														
				} else {					
					$userNoteDataArray = $notesObj->getAviorUserNotesDataForExport($contactId, $filterData, $sortNames, $sortValues);																			
				}
				
				$notesObj->exportData('user-contact-list',$userNoteDataArray);								
				exit();
			}
	}
}
