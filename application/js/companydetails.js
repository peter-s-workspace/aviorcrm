var companyDetails = function(cid,cIdEnc) {
	this.cid = cid;
	this.cIdEnc = cIdEnc;
};

var oTableCount = "";

companyDetails.prototype.peopleToDatatable = function(){		
			
			var peopleTable = $('#people-list').DataTable({
			"bSort": false,
			 "sDom": '<"H"ri<"clear">f><"clear">t<"F"lp><"clear">',
			 "aLengthMenu":[[10,25,50,100,-1],[10,25,50,100,"All"]],
			"aoColumns": [ 
						{ "bSortable": false },
						{ "bSortable": false },
						{ "bSortable": false }
					],
			 "iDisplayLength": 50,
			  "pagingType": "full",
			 "oLanguage": {
						"sLengthMenu": "Display _MENU_ records per page",			
						"sLengthMenu": "Page size _MENU_",
						"sZeroRecords": '<div class="noentries">No entries exist</div>',
						"sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
						"sInfoEmpty": "Showing 0 to 0 of 0 records",
						"sInfoFiltered": "(filtered from _MAX_ total records)",
						"sProcessing": '<img src="images/ajax-loader.gif" alt="">'
					}
			});				
			
};
	
companyDetails.prototype.getCompanyNotes =  function() {
			
	var compID = this.cid;		
					
	$('#export-datatable').hide();
	
	//Preload images
	$('<img/>')[0].src = "images/loader-xsm.gif";
	$('<img/>')[0].src = "images/opened.png";
	$('<img/>')[0].src = "images/pages.png";
	$('<img/>')[0].src = "images/spacer.gif";

	var companyNotesTable = $('#company-notes').DataTable({	
			 "dom": 'l<"H"rif><"clear">t<"F"p><"clear">',
			 "order": [[ 0, "desc" ]],
			 "aLengthMenu":[[10,25,50,100,-1],[10,25,50,100,"All"]],
			 "iDisplayLength": 25,
			 "pagingType": "full",
			 "sAjaxSource": "ajaxDatatables.php",
			 "aaSorting": [],
			 "search": { "regex": true },
				"columnDefs": [
    			{ "searchable": false, "targets": [0,1,2,3,4] },
					{ "width": "600px", "targets": 1 },
					{ "width": "100px", "targets": 3 },
					{ "class": "nw", "targets": 0 }									
  			],			
			 "processing": true,
			  "serverSide": true,
				"searchDelay": 3500,
			  "fnServerData": function (sSource, aoData, fnCallback) {
					
						aoData = getAjaxPostData(aoData);
	
						$.ajax({
							"dataType": 'json',
							"type": "POST",
							"url": "ajaxDatatables.php",
							"data": aoData,
							async: true,
							"success": fnCallback
						}).done(function( result ) {
							
							$('#export-datatable').show();
							
							bindCountBadges();

							generateWebviewOpenBadges();
							
							//This breaks the behaviour of the datatable
							//$('#company-notes').linkify();
						})
					},
				"oLanguage": {
						"sLengthMenu": "Display _MENU_ records per page",			
						"sLengthMenu": "Page size _MENU_",
						"sZeroRecords": '<div class="noentries">No entries exist</div>',
						"sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
						"sInfoEmpty": "Showing 0 to 0 of 0 records",
						"sInfoFiltered": "(filtered from _MAX_ total records)",
						"sProcessing": '<img src="images/ajax-loader.gif" alt="">'
					}
	});
							
		/*		
    // Setup - add a text input to each footer cell
    $('#company-notes tfoot.filters th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Filter" />' );
    } );
		*/
						
		// Apply the search
    companyNotesTable.columns().each( function () {
        var that = this; 			
        $( 'select.filter', this.footer() ).on( 'change', function () {
            if ( that.search() !== this.value ) {
                that.search( this.value ).draw();
            }
        } );				
				
        $( 'input.filter', this.footer() ).on( 'keyup', function () {
            if ( that.search() !== this.value ) {
                that.search( this.value ).draw();
            }
        } );								
    } );				
		
				
		function getAjaxPostData(aoData) {
			
			var minDate = $("#mindate").val();
			var maxDate = $("#maxdate").val();
				
			aoData.push(
				{ "name": "entity_alias", "value": 'company-contact-notes'},
				{ "name":"comp_id","value":compID},
				
				{ "name": "min_date", "value": minDate},
				{ "name": "max_date", "value": maxDate},
								
				{ "name": "search-filter-note", "value": $('#search-filter-note').val()},								
				{ "name": "search-filter-type", "value": $('#search-filter-type').val()},												
				{ "name": "search-filter-analyst", "value": $('#search-filter-analyst').val()},												
				{ "name": "search-filter-client", "value": $('#search-filter-client').val()}
			);	
						
			return aoData;
		}
		
		function addDataForExport(jsonObject) {
			
			var minDate = $("#mindate").val();
			var maxDate = $("#maxdate").val();
			
			jsonObject['entity_alias'] = 'company-contact-notes';
			jsonObject['comp_id'] = compID;			
			
			jsonObject['min_date'] = minDate;						
			jsonObject['max_date'] = maxDate;									
			
			jsonObject['search-filter-note'] = $('#search-filter-note').val();
			jsonObject['search-filter-type'] = $('#search-filter-type').val();
			jsonObject['search-filter-analyst'] = $('#search-filter-analyst').val();
			jsonObject['search-filter-client'] = $('#search-filter-client').val();												
						
			return jsonObject;
		}		
		
		var delayInputCapture = (function(){
			var timer = 0;
			return function(callback, ms){
				clearTimeout (timer);
				timer = setTimeout(callback, ms);
			};
		})();				
						
		$("#mindate").datepicker({ 
			dateFormat: 'yy-mm-dd',
			onSelect: function () {
				
					var minDate = $("#mindate").val();
					var maxDate = $("#maxdate").val();				
					
					var d1 = Date.parse(minDate);
					var d2 = Date.parse(maxDate);						
					
					if (d1 > d2) {
						$("#maxdate").val(minDate);
					}
					
					if(minDate != '' && maxDate != '') {
						companyNotesTable.ajax.reload();						
					}
			},
			changeMonth: true,
			changeYear: true
		});		
		
		$("#maxdate").datepicker({
			dateFormat: 'yy-mm-dd',			
			onSelect: function () {
				
					var minDate = $("#mindate").val();
					var maxDate = $("#maxdate").val();				
					
					var d1 = Date.parse(minDate);
					var d2 = Date.parse(maxDate);						
					
					if (d2 < d1) {
						$("#mindate").val(maxDate);
					}
									
					if(minDate != '' && maxDate != '') {
						companyNotesTable.ajax.reload();
					}
			},
			changeMonth: true,
			changeYear: true
		});				
		
		$('#export-datatable').click(function(){

			var data = companyNotesTable.ajax.params();
			
			//Append filter data
			data = addDataForExport(data);
			
			var exportUrl = 'ajaxDatatables.php?export=true&'+$.param(data);
			window.location.href = exportUrl;
		});
		
		$("#EmailDialog").hide();		
		$("#WebviewDialog").hide();				

		// Ensure Webview badges are run when paging
		// TODO: figure out how to seelect all children
		$("a[aria-controls='contact-list-table']").click(function(){			// $("[myAttribute='my value']")
			var click = $(this).attr('id');
			console.log("BUTTON CLICK");
			// Cancel previous request if there is one
			generateWebviewOpenBadges();
		});

		$("#contact-list-table_length").change(function(){			// $("[myAttribute='my value']")
			var click = $(this).attr('id');
			console.log("SELECT");
			generateWebviewOpenBadges();
		});				
		
};

//https://bibwild.wordpress.com/2016/05/06/sequential-jquery-ajax-using-recursive-creation-of-promises/

function generateWebviewOpenBadges() {

	//console.log('generateWebviewOpenBadges');
	
	//Add animating image
	$("[data-notetype='webviewopen']").removeClass('badge').text('');
	$("[data-notetype='webviewopen']").parent('td').append('<img src="images/loader-xsm.gif" class="loading-image" style="margin-left:10px;">');			

	$("[data-notetype='webviewopen']").each(function () {	
		var reportid = $(this).data('reportid');
		var email = $(this).data('email');
		var count = $(this).data('count');

		var contactName = $(this).data('contact');
		var reportTicker = $(this).data('rticker');		
		var reportTitle = $(this).data('rtitle');						

		if (parseInt(reportid) > 1 && email != '') {

			//console.log('WebviewOpen AJAX call: reportid=' + reportid + ", email=" + email);

			var postData = { 'entity_alias': 'webview-metadata', 'reportid': reportid, 'email':email};
			var webviewOpenElement = $(this);			
			
			$.ajax({
				dataType: 'json',
				type: "POST",
				url: "ajaxDatatables.php",
				data: postData,
				async: true
			}).done(function (result) {					

				//console.log('Ajax result');
				//console.log(result);	
				
				var numOpened = result.opened;
				var numPages = result.pages;
				var duration = result.duration;

				var openedBin = calculateOpendedBinValue(numOpened);
				var numpagesBin = calculateNumPagesBinValue(numPages);
				var durationBin = calculateDurationBinValue(result.secs);

				//Remove animating image
				webviewOpenElement.parent('td').find('.loading-image').remove();
				webviewOpenElement.parent('td').find('.webviewopen-metadata').remove();				
				//webviewOpenElement.parent('td').append('<div class="webviewopen-metadata"><span class="tooltip opened ' + openedBin + '">' + numOpened + ' <img src="images/opened.png" /><span class="tooltiptext">Opened ' + numOpened + '  times</span></span><span class="tooltip pages ' + numpagesBin + '">' + numPages + ' <img src="images/pages.png" /><span class="tooltiptext">Number of pages ' + numPages + ' </span></span><span class="duration ' + durationBin + '">' + duration +' <img src="images/spacer.gif" /></span></div>');
				webviewOpenElement.parent('td').append(
					'<div class="webviewopen-metadata">'+
					'<span class="metabadge tooltip opened ' + openedBin + 
							'" data-notetype="' + "webviewopen" + '"' +
							'" data-reportid="' + reportid+ '"' +
							'" data-rticker="' + reportTicker + '"' +
							'" data-rtitle="' + reportTitle + '"' +
							'" data-contact="' + contactName + '"' +
							'" data-email="' + email + '"' +
							'" data-count="' + count + '"' +
					'>' + numOpened + ' <img src="images/opened.png" />'+
					'<span class="tooltiptext">Opened ' + numOpened + '  times</span></span>'+
					'<span class="tooltip pages ' + numpagesBin + '">' + numPages + ' <img src="images/pages.png" />'+
					'<span class="tooltiptext">Number of pages ' + numPages + ' </span></span>'+
					'<span class="duration ' + durationBin + '">' + duration +' <img src="images/spacer.gif" /></span>'+
					'</div>');				
			});
			
		}	
	});

	bindWebviewMetaBadges();	
}

function calculateOpendedBinValue(value) {
	var binValue = 1;
	var max = 3;
	var divided = value / max;

	if (divided > 0.6666666666666666) {
		binValue = 3;
	} else if (divided > 0.3333333333333333) {
		binValue = 2;
	}
	//console.log('calculateOpendedBinValue, max ' + max + ', value:' + value + ', divided:' + divided + ', bin value:' + binValue);
	return 'bin' + binValue;
}

function calculateNumPagesBinValue(value) {
	var binValue = 1;
	var max = 5;
	var divided = value / max;

	if (divided > 0.6666666666666666) {
		binValue = 3;
	} else if (divided > 0.3333333333333333) {
		binValue = 2;
	}
	//console.log('calculateNumPagesBinValue, max ' + max + ', value:' + value + ', divided:' + divided + ', bin value:' + binValue);
	return 'bin' + binValue;
}

function calculateDurationBinValue(value) {
	var binValue = 1;
	var max = 300;
	var divided = value / max;

	if (divided > 0.6666666666666666) {
		binValue = 3;
	} else if (divided > 0.3333333333333333) {
		binValue = 2;
	}
	//console.log('calculateDurationBinValue, max ' + max + ', value:' + value + ', divided:' + divided + ', bin value:' + binValue);
	return 'bin' + binValue;
}


function bindCountBadges() {
	
	$( ".badge.count[data-notetype='emailread']" ).on( "click", function() {	
		var taskid = $(this).data('taskid');
		var count = $(this).data('count');
		if (parseInt(count) > 1) {
			openReadEmailsAviorCRM(taskid,count);
		}
		
	});
	
	/*
	$( ".badge.count[data-notetype='webviewopen']" ).on( "click", function() {	
	
		var count = $(this).data('count');	
		var contactName = $(this).data('contact');
		var reportTicker = $(this).data('rticker');		
		var reportTitle = $(this).data('rtitle');			
		var email = $(this).data('email');						
								
		if (parseInt(count) > 1) {
			openWebviewOpensAviorCRM(email, reportTitle, reportTicker, contactName, count);
		}
		
	});	
	*/
}

function bindWebviewMetaBadges() {

	$( ".dataTable" ).off("click", ".metabadge");
	
	$( ".dataTable" ).on("click", ".metabadge", function(){
		var notetype = $(this).data('notetype');		
		
		if (notetype == 'webviewopen') {
			var count = $(this).data('count');	
			var contactName = $(this).data('contact');
			var reportTicker = $(this).data('rticker');		
			var reportTitle = $(this).data('rtitle');			
			var email = $(this).data('email');						
									
			if (parseInt(count) > 1) {
				openWebviewOpensAviorCRM(email, reportTitle, reportTicker, contactName, count);
			}			
		}		
	});
}

companyDetails.prototype.setResponsibilityUploader = function(){
	
	var compIdEnc = this.cIdEnc;
	var that = this;
	$('#pubfileUpload').uploadify({
		'uploader': 'js/uploadify.swf',
		'script': 'js/uploadify.php',
		'folder': 'uploads/public/company/'+compIdEnc,
		'cancelImg': 'js/cancel.png',
		'multi': false,
		'buttonText': 'Select Files',
		'checkScript': 'js/check.php',
		'displayData': 'speed',
		'simUploadLimit': 2	,
		'auto': true,
		'onComplete': function(event, queueID, fileObj, response, data) {
			that.displayResponsibilityFiles();

		}
	});
};

companyDetails.prototype.displayResponsibilityFiles = function(){

	var compID = this.cid;
		$.ajax({
		  url: "ajaxCompanySharedFiles.php",
		  global: false,
		  type: "GET",
		  dataType: 'json',
		  data: {'cid': compID},
		  success : function(result){
			if( result.length > 0 ) {
				var files = '<ul class="shared-files-list">';
				for(x=0; x < result.length; x++ ) {
					files += '<li><a href='+result[x].path+'>'+result[x].name+'</a></li>';
				}
				files += '</ul>';
				$('#shared-files').html(files);
			}
		  }
	   });
};	

