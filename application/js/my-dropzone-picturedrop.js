Dropzone.options.myFormDropzone = { // The camelized version of the ID of the form element
  // The configuration we've talked about above
  autoProcessQueue: false,
  addRemoveLinks :true,
  uploadMultiple: true,
  acceptedFiles: 'image/*',
  parallelUploads: 1,
  maxFiles: 1,
  previewsContainer: '.picturedrop',
  clickable: true,
  forceFallback: false,
  paramName:'Filedata',
  // The setting up of the dropzone
  fallback: function() {
  		$('.my-drop-message').hide();
		$('.dz-fallback').html('');
		$('#my-form-dropzone').addClass('has-fallback');
	},
	
  init: function() {
    var myDropzone = this;
    // First change the button to actually tell Dropzone to process the queue.
    this.element.querySelector("input[type=submit]").addEventListener("click", function(e) {
      // Make sure that the form isn't actually being sent.
		e.preventDefault();
		e.stopPropagation();
		if (myDropzone.getQueuedFiles().length > 0) {                        
			
			myDropzone.processQueue();  

		} else {                       
			if( $('#my-form-dropzone').valid()){
				
					//Manually post the data.
					var form = $('#my-form-dropzone');
					var data = form.serializeArray();
					var hipchat = $("#hipchat").select2('data');
					var hipchat_data = new Array();
					var jsonData = {};
					$.each(data, function (i, v) {
						jsonData[v.name] = v.value;
					});

					$.each(hipchat, function(i,v){
						jsonData[v.text+"_hipchat"] = v.id;
					});
				
					$.ajax({
						type: "POST",
						url: "adminmain.php",
						dataType: "text",
						data: jsonData, 
					})
					.done(function( msg ) {
						
						if (msg == 'true') {
							$("#duplicate").dialog('open');
							$("div#message").html("The Client Already exist");
							return false;	
						} else if( msg != 'error') {	
							window.location.href="clientdetails.php?cnt_Id="+msg;
						} else {
							$('#error-messages').html('<p>Please fill in all required fields.</p>').show();
							$('#success-messages').hide(); 
						}
					});
			}
		}  
    });
		this.on("drop", function(file, response) {
			 $('#cpicture').append('<div class="notice error"><em>Note: Images need to be saved to desktop first before dropping</em></div>');					  
			  
		});
		this.on("thumbnail", function(file, response, myEvent) {
			$('.my-drop-message').hide();
			$('.notice').remove();
			$('.error').remove();
			
		});
		
		this.on("removedfile", function(file, response, myEvent) {
			$('.my-drop-message').show();
			//$('.error').hide();
		});
		
		this.on("error", function(file,error) {
				$(".error").remove();
				$(".dz-error").hide();
				if( error == 'You can not upload any more files.' ) {
					$('#cpicture').append('<div class="error">You can not upload any more files. <br />Please clear image first</div>');
				} else {
					$('#cpicture').append('<div class="error">'+error+'</div>');
				}
			});
      this.on("success", function(file, response, myEvent) {
                  //window.setTimeout(function() { window.location.href = "files.php"; }, 1000);
				  if(response > 0){
					 window.location.href="clientdetails.php?cnt_Id="+response;
				  } else {
					$('#error-messages').html('<p>Please fill in all required fields.</p>').show();
					$('#success-messages').hide(); 
				  }
          });   
	  this.on("complete",function(file){
			this.removeFile(file);					  
	  });
  }
}

$(document).ready(function(){
$('#my-form-dropzone').validate({
			errorClass:'validate-error',
			errorPlacement: function(error, element) {		
				error.appendTo( element.parents('tr').find("span.validation-error").addClass('error') );													
			},
			rules: {
				cnt_Fname: {required: true },
				cnt_Lastname: {required: true },
				cnt_email: {required: true, email: true },
				cnt_company: {required: true }				
			},
			messages: {		
				cnt_Fname: { required: 'Please enter first name' },
				cnt_Lastname: { required: 'Please enter last name' },
				cnt_email: { required: 'Please enter email address', email: 'Please enter a valid email address' },
				cnt_company: { required: 'Please select company' }
			}
		});
 });