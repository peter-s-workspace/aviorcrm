var ReadCountData 	= [];
var ReadCountTable	= "";
var WebviewCountTable	= "";
function getReadEmails(apiURL,  emailAddress , oTable, oTableCount)
{
	$.ajax({
		url 		: apiURL,
		type 		: "post",
		data 		: ({'email' : emailAddress}),
		dataType 	: "text",
		success : function(responseData)
		{

			var jsonData 	= $.parseJSON(responseData);
			ReadCountData 	= jsonData['EmailRead'];
			ReadCountTable = oTableCount;
			 //added to check if is also undefined as this can be a string null or object undefined
			if (jsonData['message'] != null || jsonData['message'] === undefined) {
		 		$('#emails-read-tab').show();

			 	for(i = 0; i<jsonData['EmailRead'].length; i++ )
			 	{
			 		value = jsonData['EmailRead'][i];
			 		oCount = value[0]['count'];
			 		oTaskid = value['Reads']['task_id'];
			 		oDate 	= value['Reads']['date_created'];
			 		oTitle  = value['Reads']['report_ticker'] + " | " + value['Reads']['report_title'];
			 		oSender = value['Reads']['sender'];

			 		if (oTaskid != null) {
				 		if (oCount > 1) {
				 			oTable.row.add([ '<a href="javascript:void(0);" class="clicklink" onClick="openReadEmails(\''+apiURL+'\','+oTaskid+','+oCount+');">'+oCount+'</a>', oDate, oSender, oTitle]);	
				 		} else {
				 			oTable.row.add([oCount, oDate, oSender, oTitle]);
				 		}
			 		}
			 	};
			
				oTable.draw();	
		 	}
			
		},
		error 	: function(XHqr, Exception)
		{
			console.log(XHqr.status + " : " + Exception.responseText);
		}
	});

}

function openReadEmails(apiURL, task_id, emails_count)
{

	if( ReadCountTable) {
		ReadCountTable.clear();
	} else {
		ReadCountTable = $('#email-reads-count').DataTable({"autoWidth" :false, "order":[[0, "desc"]]});
	}

	apiURL 		= apiURL+'PerTaskId';


	$.ajax({
		url 		: apiURL,
		type 		: "post",
		data 		: ({'task_id' : task_id}),
		dataType 	: "text",
		success : function(responseData)
		{
			var jsonData = $.parseJSON(responseData);
			if (jsonData['message'] != null || jsonData['message'] === undefined) {
				$.each(jsonData,function(key,value){
					oDate 		= value['EmailRead']['date_created'];	
				 	oTitle  	= value['EmailRead']['report_title'];
				 	oIpAddress	= value['EmailRead']['ip_address'];
				 	oHostName	= value['EmailRead']['host_name'];
				 	oReader    = value['EmailRead']['reader'];

			 		ReadCountTable.row.add([oDate,oIpAddress,oHostName,oReader, oTitle]);
				});
			}
			ReadCountTable.draw();
			$("#EmailDialog").dialog({"width" : 800,'title' : 'Emails ('+emails_count+')'});
		},
		error 	: function(XHqr, Exception)
		{
			console.log(XHqr.status + " : " + Exception.responseText);
		}

	});	
	
}


function openReadEmailsAviorCRM(task_id, emails_count)
{

	if( ReadCountTable) {
		ReadCountTable.clear();
	} else {
		ReadCountTable = $('#email-reads-count').DataTable({"autoWidth" :false, "order":[[0, "desc"]]});
	}
	
	apiURL = 'ajaxDatatables.php';

	$.ajax({
		url 		: apiURL,
		type 		: "post",
		data 		: ({'entity_alias' : 'read-emails', 'task_id' : task_id}),
		dataType 	: "text",
		success : function(responseData)
		{
			var jsonData = $.parseJSON(responseData);
			if (jsonData['message'] != null || jsonData['message'] === undefined) {
				$.each(jsonData,function(key,value){
					oDate 		= value['date_created'];	
				 	oTitle  	= value['report_title'];
				 	oIpAddress	= value['ip_address'];
				 	oHostName	= value['host_name'];
				 	oReader    = value['reader'];
					
					/*
					console.log('oDate: '+oDate);
					console.log('oIpAddress: '+oIpAddress);
					console.log('oHostName: '+oHostName);
					console.log('oReader: '+oReader);
					console.log('oTitle: '+oTitle);																				
					*/

			 		ReadCountTable.row.add([oDate,oIpAddress,oHostName,oReader, oTitle]);
				});
			}
			ReadCountTable.draw();
			$("#EmailDialog").dialog({"width" : 800,'title' : 'Email reads ('+emails_count+')'});
		},
		error 	: function(XHqr, Exception)
		{
			console.log(XHqr.status + " : " + Exception.responseText);
		}

	});	
	
}


function openWebviewOpensAviorCRM(email, reportTitle, reportTicker, contactName, emails_count)
{

	if(WebviewCountTable) {
		WebviewCountTable.clear();
	} else {
		WebviewCountTable = $('#webview-opens-count').DataTable({"autoWidth" :false, "order":[[0, "desc"]]});
	}

	apiURL = 'ajaxDatatables.php';

	$.ajax({
		url 		: apiURL,
		type 		: "post",
		data 		: ({
				'entity_alias' : 'webview-opens',
				'email' : email,
				'report_title' : reportTitle,
				'report_ticker' : reportTicker,
				'contact_name' : contactName,
		}),
		dataType 	: "text",
		success : function(responseData)
		{
			var jsonData = $.parseJSON(responseData);
			if (jsonData['message'] != null || jsonData['message'] === undefined) {
				$.each(jsonData,function(key,value){
					oDate 		= value['date_created'];	
				 	oTitle  	= value['report_title'];
				 	oIpAddress	= value['ip_address'];
				 	oReader    = value['reader'];

			 		WebviewCountTable.row.add([oDate,oIpAddress,oReader, oTitle]);
				});
			}
			WebviewCountTable.draw();
			$("#WebviewDialog").dialog({"width" : 800,'title' : 'Webview opens ('+emails_count+')'});
		},
		error 	: function(XHqr, Exception)
		{
			console.log(XHqr.status + " : " + Exception.responseText);
		}

	});	
	
}
