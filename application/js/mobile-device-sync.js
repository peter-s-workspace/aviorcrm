/* Device Sync functions for Client details and Client Edit Pages*/
function addDeviceSync(apiUrl, emailAddress, activeMonthMax)
{ 
	if (apiUrl != '' && emailAddress != '' && activeMonthMax > 0) {
		apiUrl += "email="+emailAddress;
		$.ajax({ url: apiUrl, type: "GET", dataType: "json",
				success : function(data){ populateSyncTabs(data, activeMonthMax); }
			});
	}
	
}
	
function populateSyncTabs(resultData,activeMonthMax)
{
	
	if( typeof resultData.devices !== 'undefined' ) {
	var iosSyncData = [], androidSyncData  = [];
		//Loop through result set, and separate ios and android data.
		for (x=0; x < resultData.devices.length; x++) {

			if (resultData.devices[x]['type'] == 'ios') {
				//Collect last sync data.
				iosSyncData.push(resultData.devices[x]);
				//No need to repeat this step
				if (iosSyncData.length == 1) {
					$('#ipad-activity-image, #ipad-activity-tab').show();
					$('#ipad-activity-image').addClass('sync-active');
				}
			}
			if( resultData.devices[x]['type'] == 'android') {
				//androidSyncData.push(resultData.devices[x]['last_sync']);
				androidSyncData.push(resultData.devices[x]);
				if (androidSyncData.length == 1) {
					$('#android-activity-tab, #android-activity-image').show();
					$('#android-activity-image').addClass('sync-active');
				}
			} 
		}
		//If there's data in the array, populate table.
		if( iosSyncData.length > 0) {
			var iosContent = '', iosSyncDate = '', iosSyncTime = '', iosMonths = [], iosDeviceName = '', iosDeviceModel= '', iosDeviceOSVersion = '';
			for (v=0; v < iosSyncData.length; v++) {
				//Separate time, date, and get the months difference.
				iosDeviceName = iosSyncData[v]['device_name'] ? iosSyncData[v]['device_name'] : '';
				iosDeviceModel = iosSyncData[v]['device_model'] ? iosSyncData[v]['device_model'] : '';
				iosDeviceOSVersion = iosSyncData[v]['os_version'] ? iosSyncData[v]['os_version'] : '';
				iosSyncTime = moment(iosSyncData[v]['last_sync']).format("HH:mm:ss");
				iosSyncDate = moment(iosSyncData[v]['last_sync']).format("YYYY-MM-DD");
				iosMonths.push( moment().diff(moment(iosSyncData[v]).format("YYYY-MM-DD"),'months'));
				iosContent += '<tr>';
				iosContent += '<td>'+ iosDeviceName+'</td>';
				iosContent += '<td>'+ iosDeviceModel+'</td>';
				iosContent += '<td>'+ iosDeviceOSVersion+ '</td>';
				iosContent += '<td>'+ iosSyncTime+'</td>';
				iosContent += '<td>'+ iosSyncDate+'</td>';
				iosContent += '</tr>';
			}
			//Add data container.
			$('#ios-sync-data').html(iosContent);
			if(iosMonths.length > 0 ) {
				//The minimum value in the array is the latest sync date. Check if it's greater than the  max active months,
				//and add correct class.
				if( Math.min.apply(Math,iosMonths) >=activeMonthMax ) {
					$('#ipad-activity-image').removeClass('sync-active').addClass('sync-inactive');
				}//end if min
			}
		} else {
			//No sync dates, switch classes.
			$('#ipad-activity-image').removeClass('sync-active').addClass('sync-inactive');
		}
		
		if( androidSyncData.length > 0) {
			var androidSyncDate = '', androidSyncTime = '', androidMonths = [], androidContent = '', androidDeviceName = '', androidDeviceModel= '', androidDeviceOSVersion = '';
			
			for (v=0; v < androidSyncData.length; v++) {
				androidSyncTime = moment(androidSyncData[v]['last_sync']).format("HH:mm:ss");
				androidSyncDate = moment(androidSyncData[v]['last_sync']).format("YYYY-MM-DD");
				androidDeviceName = androidSyncData[v]['device_name'] ? androidSyncData[v]['device_name'] : '';
				androidDeviceModel = androidSyncData[v]['device_model'] ? androidSyncData[v]['device_model'] : '';
				androidDeviceOSVersion = androidSyncData[v]['os_version'] ? androidSyncData[v]['os_version'] : '';
				
				androidMonths.push( moment().diff(moment(androidSyncData[v]).format("YYYY-MM-DD"),'months'));
				androidContent += '<tr>';
				androidContent += '<td>'+ androidDeviceName+'</td>';
				androidContent += '<td>'+ androidDeviceModel+'</td>';
				androidContent += '<td>'+ androidDeviceOSVersion+'</td>';
				androidContent += '<td>'+ androidSyncTime+'</td>';
				androidContent += '<td>'+ androidSyncDate+'</td>';
				androidContent += '</tr>';
			}
			
			$('#android-sync-data').html(androidContent);
			if(androidMonths.length > 0 ) {
				if( Math.min.apply(Math,androidMonths) >= activeMonthMax ) {
					$('#android-activity-image').removeClass('sync-active').addClass('sync-inactive');
				}
			}
		} else{
			$('#android-activity-image').removeClass('sync-active').addClass('sync-inactive');
		}
	} 
}