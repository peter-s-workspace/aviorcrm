<?php

if (!isset($_SESSION)) {
  session_start();
}
$authorized = false;
//Only start checking for permission if the username and user group found in session
if( isset($_SESSION['MM_Username']) && isset($_SESSION['MM_UserGroup'] ) ) {
	
	$user_groups = explode(',',$_SESSION['MM_UserGroup']);
	if(in_array("Administrator",$user_groups) ) {
		$authorized = true;
	}
}
// Remove file if authorized and ID available
if(isset($_POST['id']) && $authorized == true) {
	$folder = 'uploads/clientp/'.$_POST['id'].'/1.jpg';
	$image = '..'.'/'. $folder;
	//Check if file exists..
	if( file_exists($image)) {
		unlink($image);
	} 
} elseif(isset($_POST['cid']) && $authorized == true){
	
	$folder = 'uploads/companyp/'.$_POST['cid'].'/1.jpg';
	$image = '..'.'/'. $folder;
	//Check if file exists..
	if( file_exists($image)) {
		unlink($image);
	} 

} else if ($authorized == false) {
	// Send 401 response.
	$GLOBALS['http_response_code'] = 401;
	$code = 401;
	$text = 'Unauthorized';
	$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
	header($protocol . ' ' . $code . ' ' . $text);
	exit();
}