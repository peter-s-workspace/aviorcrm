$(document).ready(function () {

	var oTable;
	oTable = $('#contact-list-table').DataTable({
		initComplete: function () {

			bindCountBadges();

			var columnIndex = 0;
			this.api().columns().every(function () {

				if (columnIndex != 0 && columnIndex != 4) {
					var column = this;
					var filterName = '';

					if (columnIndex == 1) {
						filterName = 'search-filter-note';
					} else if (columnIndex == 2) {
						filterName = 'search-filter-type';
					} else if (columnIndex == 3) {
						filterName = 'search-filter-analyst';
					}


					if (columnIndex == 1) {
						var input = $('<input id="' + filterName + '" name="' + filterName + '" value="" />')
							.appendTo($(column.footer()).empty());
						/*
						.on( 'keyup', function () {
								var val = $.fn.dataTable.util.escapeRegex(
										$(this).val()
								);
								this.search(val).draw() ;
	
								column
										.search( val ? '^'+val+'$' : '', true, false )
										.draw();
						} );										
						*/
					} else {
						//exclude filters for column 0 and column 4
						var select = $('<select id="' + filterName + '" name="' + filterName + '"><option value=""></option></select>')
							.appendTo($(column.footer()).empty())
							.on('change', function () {
								var val = $.fn.dataTable.util.escapeRegex(
									$(this).val()
								);

								column
									.search(val ? '^' + val + '$' : '', true, false)
									.draw();
							});

						column.data().unique().sort().each(function (d, j) {
							select.append('<option value="' + d + '">' + d + '</option>')
						});
					}
				}
				columnIndex++;
			});
		},
		"columnDefs": [{
			"targets": 4,
			"orderable": false
		}
		],
		//"sDom": '<"H"ri<"clear">f><"clear">t<"F"lp><"clear">',
		"sDom": '<"H"ri<"clear">><"clear">t<"F"lp><"clear">',
		"order": [[0, "desc"]],
		"pagingType": "full",
		"oLanguage": {
			"sLengthMenu": "Display _MENU_ records per page",
			"sLengthMenu": "Page size _MENU_",
			"sZeroRecords": '<div class="noentries">No entries exist</div>',
			"sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
			"sInfoEmpty": "Showing 0 to 0 of 0 records",
			"sInfoFiltered": "(filtered from _MAX_ total records)",
			"sProcessing": '<img src="images/ajax-loader.gif" alt="">'
		}
	});

	//Disable linkify on #contact-list-table because it breaks the datatable
	$('#contact-list-table').on('draw.dt', function () {
		//$('#contact-list-table').linkify();		
	});

	$('#search-filter-note').keyup(function () {
		oTable.search($(this).val()).draw();
	});


	$.fn.dataTable.ext.search.push(
		function (settings, data, dataIndex) {

			var tableId = settings.nTable.getAttribute('id');

			if (tableId == 'contact-list-table') {

				var min = $('#mindate').datepicker("getDate");
				var max = $('#maxdate').datepicker("getDate");
				var position = 10;
				var rowData = [data[0].slice(0, position), ' ', data[0].slice(position)].join('');

				var rowDate = new Date(rowData);

				/*
				console.log('min: '+min);
				console.log('max: '+max);			
				console.log('rowData: '+rowData);									
				console.log('rowDate: '+rowDate);						
				*/

				if (min == null && max == null) { return true; }
				if (min == null && rowDate <= max) { return true; }
				if (max == null && rowDate >= min) { return true; }
				if (rowDate <= max && rowDate >= min) { return true; }
				return false;
			} else {
				return true;
			}
		}
	);

	//$('#contact-list-table').linkify();
	//$('#client-notes').linkify();	

	$("#mindate").datepicker({
		dateFormat: 'yy-mm-dd',
		onSelect: function (date) {
			minDateFilter = new Date(date).getTime();
			oTable.draw();
		},
		changeMonth: true,
		changeYear: true
	}).keyup(function () {
		minDateFilter = new Date(this.value).getTime();
		oTable.draw();
	});

	$("#maxdate").datepicker({
		dateFormat: 'yy-mm-dd',
		onSelect: function (date) {
			maxDateFilter = new Date(date).getTime();
			oTable.draw();
		},
		changeMonth: true,
		changeYear: true
	}).keyup(function () {
		maxDateFilter = new Date(this.value).getTime();
		oTable.draw();
	});

	$('#export-contact-list-table').click(function () {

		var sortingDataArray = $('#contact-list-table').dataTable().fnSettings().aaSorting;
		var sortingData = [];

		var sortNames = [];
		var sortValues = [];

		for (var t = 0; t < sortingDataArray.length; t++) {
			data = sortingDataArray[t];

			switch (data[0]) {

				case 0: //date
					sortNames.push('date');
					sortValues.push(data[1]);
					break;
				case 1: //note
					sortNames.push('note');
					sortValues.push(data[1]);
					break;
				case 2: //type
					sortNames.push('type');
					sortValues.push(data[1]);
					break;
				case 3: //by
					sortNames.push('by');
					sortValues.push(data[1]);
					break;
			}
		}

		var exportUrl = 'ajaxDatatables.php?';
		var getVariables = [];

		getVariables['export'] = 'true';
		getVariables['entity_alias'] = 'user-contact-list';
		getVariables['cnt_Id'] = $('#cnt_Id').val();

		getVariables['min_date'] = $('#mindate').val();
		getVariables['max_date'] = $('#maxdate').val();

		getVariables['search-filter-note'] = $('#search-filter-note').val();
		getVariables['search-filter-type'] = $('#search-filter-type').val();
		getVariables['search-filter-analyst'] = $('#search-filter-analyst').val();

		for (var key in getVariables) {
			exportUrl += key + "=" + encodeURIComponent(getVariables[key]) + "&";
		};

		exportUrl = exportUrl.replace(/^&+|&+$/g, '');

		exportUrl = exportUrl + '&sortNames=' + sortNames.toString();
		exportUrl = exportUrl + '&sortValues=' + sortValues.toString();

		//console.log(exportUrl);
		window.location.href = exportUrl;
	});

	/* Custom filtering function which will search data in column four between two values */
	$.fn.dataTable.ext.search.push(
		function (settings, data, dataIndex) {
			var min = parseInt($('#mindate').val(), 10);
			var max = parseInt($('#maxdate').val(), 10);
			var date = parseFloat(data[0]) || 0; // use data for the date column

			if ((isNaN(min) && isNaN(max)) ||
				(isNaN(min) && date <= max) ||
				(min <= date && isNaN(max)) ||
				(min <= date && date <= max)) {
				return true;
			}
			return false;
		}
	);


	function bindCountBadges() {

		$(".dataTable").on("click", ".badge.count", function () {
			var notetype = $(this).data('notetype');

			if (notetype == 'emailread') {
				var taskid = $(this).data('taskid');
				var count = $(this).data('count');
				if (parseInt(count) > 1) {
					openReadEmailsAviorCRM(taskid, count);
				}
			} else if (notetype == 'webviewopen') {
				var count = $(this).data('count');
				var contactName = $(this).data('contact');
				var reportTicker = $(this).data('rticker');
				var reportTitle = $(this).data('rtitle');
				var email = $(this).data('email');

				if (parseInt(count) > 1) {
					openWebviewOpensAviorCRM(email, reportTitle, reportTicker, contactName, count);
				}
			}

		});
	}

	function bindWebviewMetaBadges() {

		$(".dataTable").on("click", ".metabadge", function () {

			var notetype = $(this).data('notetype');

			if (notetype == 'webviewopen') {
				var count = $(this).data('count');
				var contactName = $(this).data('contact');
				var reportTicker = $(this).data('rticker');
				var reportTitle = $(this).data('rtitle');
				var email = $(this).data('email');

				if (parseInt(count) > 1) {
					openWebviewOpensAviorCRM(email, reportTitle, reportTicker, contactName, count);
				}
			}			
		});
	}

	// Event listener to the two range filtering inputs to redraw on input
	$('#mindate, #maxdate').change(function () {
		//alert('asdfasdf');
		oTable.draw();
	});

	var deleteDialog = $("#confirm-delete").dialog({
		resizable: false,
		autoOpen: false,
		height: 200,
		modal: true,
		buttons: {
			"Yes": deleteNote,
			Cancel: function () {
				$(this).dialog("close");
			}
		}
	});

	function deleteNote() {
		var noteId = $('#deleteNoteId').val();
		var delRowIndx = $('#delRowIndx').val();
		if (noteId > 0) {
			$.ajax({
				type: "POST",
				url: "ajaxActionNotes.php",
				data: { 'action': 'delete', 'noteId': noteId }
			}).done(function (result) {
			});
			oTable.row(delRowIndx).remove().draw();
			//$('#contact-list-table').linkify();
		}
		$(this).dialog("close");
	}

	$('#contact-list-table tbody').on('click', '.delete-button', function () {
		var noteId = $(this).data('id');
		var delRowIndx = oTable.row($(this).parents('tr')).index();
		$('#delRowIndx').val(delRowIndx);
		$('#deleteNoteId').val(noteId);
		deleteDialog.dialog("open");
	});

	var dialog = $("#notes-edit-form").dialog({
		autoOpen: false,
		height: 400,
		width: 400,
		modal: true,
		buttons: {
			"Update Note": updateNote,
			Cancel: function () {
				dialog.dialog("close");
				$("#update-notes-form").trigger("reset");
			}
		}
	});

	var clientNoteDialog = $('#client-notes-edit-form').dialog({
		autoOpen: false,
		height: 400,
		width: 400,
		modal: true,
		buttons: {
			"Update Note": updateClientNote,
			Cancel: function () {
				clientNoteDialog.dialog("close");
				$("#client-notes-form").trigger("reset");
			}
		}
	});

	$('#edit-client-note').on('click', function () {
		var clientNoteText = $('#client-notes').text();
		$('#clienteditnote').val(clientNoteText);
		clientNoteDialog.dialog("open");
	});

	
	function updateClientNote() {
		var newClientNote = $('#clienteditnote').val();
		var clientId = $('#edit-client-note').data('clientid');
		$.ajax({
			type: "POST",
			url: "ajaxActionNotes.php",
			data: { 'action': 'update-note', 'clientId': clientId, 'noteText': newClientNote }
		}).done(function (result) {
			clientNoteDialog.dialog("close");
			$('#client-notes').text(newClientNote);
			//$('#client-notes').linkify();
			$("#update-notes-form").trigger("reset");
			$('#noteupresult').text('Updated');
		});

			/*
			var form_data 		= $("#addnoteform").serializeArray();
			
			var newDate = new Date();
			var DefaultTime = newDate.getHours() + ':' + newDate.getMinutes() + ':' + newDate.getSeconds();
			console.log(form_data[2].value);

			//index 2 is the note time value  			
			if (form_data[2].value == "") {
				form_data[2].value = DefaultTime;
			} 
			
			$.ajax({
				type: "POST",
				url: "ajaxActionNotes.php",
				dataType : 'JSON',
				data: $.param(form_data),
				}).done(function( data ) {


						var editLink = 	'<a href="javascript://" class="edit-button" data-id="'+data.note_id+'"><small>Edit</small></a>';
						var deleteLink = 	'<a href="javascript://" class="delete-button" data-id="'+data.note_id+'"><small>Delete</small></a>';
						var action = editLink+' <br /><br /> '+deleteLink;

						/*
							This code should fix the wrong time been assigned into the notes when they are created
					    
						
						
						//Test if the note time from the data object has been set or not
						if (data.note_time != " ") {
							oTable.row.add([data.note_date+" "+data.note_time, data.note_text, data.note_type, data.notebyNick, action ]).draw();
						} else {
							//use default time if there is no note time being set
							oTable.row.add([data.note_date+" "+DefaultTime, data.note_text, data.note_type, data.notebyNick, action ]).draw();
						}
						
						//$('#contact-list-table').linkify();
					   $( "#addnoteform").trigger( "reset" );
					 	var variableString = "{" +JSON.stringify(data).split("{").pop();
					    var variableJSON = JSON.parse(variableString);
					   //-------------------HipChat post---------------------------//
						var hipchatpost = new Object();
						var client_company = $("#cntCompany").val();
						var client_name    = $("#note_cnt_name").val();

						hipchatpost.message   = variableJSON["notebyNick"] + " added a " + variableJSON["note_type"] + " note to " + client_name + " from "+ client_company + ": " + variableJSON["note_text"] + ". " + variableJSON["note_date"];

						var hipchatJSONstring = JSON.stringify(hipchatpost);
						//console.log(hipchatJSONstring);
						if (bearer != "" && chat_room != "") {
							$.ajax({
								type: "POST",
								url: "https://avior.hipchat.com/v2/room/"+chat_room+"/notification?notify=true",
								dataType: "JSON",
								headers: {"Authorization" : "Bearer " + bearer},
								data: JSON.parse(hipchatJSONstring)
							}).done(function() {
								console.log("Hipchat success");
							}).fail(function (xhr, status, error) {
								console.log("Hipchat fail");
								console.log(xhr);
								console.log(error);
							});
						}
						

						//normal message to send the note via ajax using the room chat id 
						var message 		= variableJSON["notebyNick"] + " added a " + variableJSON["note_type"] + " note to " + client_name + " from "+ client_company + ": " + variableJSON["note_text"] + ". " + variableJSON["note_date"];
						$.ajax({
							type 		: "POST",
							url 		: "AjaxSendChatRoomNotification.php",
							dataType 	: "text",
							data 	 	: ({"client_rooms" : client_hipchat_rooms,"message" : message, "note_by" : variableJSON['notebyNick']}),
							success 	: function(responseData) {
								console.log(responseData);
								return false;
							},
							error 		: function (xhr, error) {
								console.log(xhr + " " + error);
							}	
							});
							
						//---------------------------------------------------------//		
				
				});
		$('#addnoteform').hide('fold');
		$('#cancelnotebutton').hide('fold');
		$('#addnotebutton').show('fold');

	
		$('#cnt_override_drm').change(function(){
			if($(this).val() == '3') {
			$('#webviewerwithprint').removeClass('hide');				
			$('#webviewerwithprint').addClass('show');
		}
	});
		*/
	}
	
	
	function updateNote() {
		var newTextData = $('#editnote').val();
		var editNoteId = $('#editnoteId').val();
		var rowinxd = $('#editRowIdx').val();
		if (newTextData != '') {
			$.ajax({
				type: "POST",
				url: "ajaxActionNotes.php",
				data: { 'action': 'update', 'noteId': editNoteId, 'noteText': newTextData }
			}).done(function (result) {
				dialog.dialog("close");
				var tableRow = oTable.row(rowinxd).data();
				tableRow[1] = newTextData;
				oTable.row(rowinxd).data(tableRow).draw();
				//$('#contact-list-table').linkify();
				$("#update-notes-form").trigger("reset");
			});
		}
	}

	$('#contact-list-table tbody').on('click', '.edit-button', function () {

		var noteTextData = oTable.row($(this).parents('tr')).data()[1];
		var noteId = $(this).data('id');
		var rowIndex = oTable.row($(this).parents('tr')).index();
		$('#editnote').html(noteTextData);
		$('#editnoteId').val(noteId);
		$('#editRowIdx').val(rowIndex);

		dialog.dialog("open");
	});

	$('#savenewnotebutton').on('click', function () {
		var form_data 		= $("#addnoteform").serializeArray();
			
		var newDate = new Date();
		var DefaultTime = newDate.getHours() + ':' + newDate.getMinutes() + ':' + newDate.getSeconds();
		console.log(form_data[2].value);

		//index 2 is the note time value  			
		if (form_data[2].value == "") {
			form_data[2].value = DefaultTime;
		} 

		$.ajax({
			type: "POST",
			url: "ajaxActionNotes.php",
			dataType: 'JSON',
			data: $.param(form_data)
		}).done(function (data) {

			var editLink = '<a href="javascript://" class="edit-button" data-id="' + data.note_id + '"><small>Edit</small></a>';
			var deleteLink = '<a href="javascript://" class="delete-button" data-id="' + data.note_id + '"><small>Delete</small></a>';
			var action = editLink + ' <br /><br /> ' + deleteLink;

			/*
				This code should fix the wrong time been assigned into the notes when they are created
			*/
			var newDate = new Date();
			var DefaultTime = newDate.getHours() + ':' + newDate.getMinutes() + ':' + newDate.getSeconds();

			//Test if the note time from the data object has been set or not
			if (data.note_time) {
				oTable.row.add([data.note_date + " " + data.note_time, data.note_text, data.note_type, data.notebyNick, action]).draw();

			} else {
				//use default time if there is no note time being set
				oTable.row.add([data.note_date + " " + DefaultTime, data.note_text, data.note_type, data.notebyNick, action]).draw();
			}

			//$('#contact-list-table').linkify();
			$("#addnoteform").trigger("reset");
			var variableString = "{" + JSON.stringify(data).split("{").pop();
			var variableJSON = JSON.parse(variableString);
			//-------------------HipChat post---------------------------//
			var hipchatpost = new Object();

			var client_company = $("#cntCompany").val();
			var client_name = $("#note_cnt_name").val();

			hipchatpost.message = variableJSON["notebyNick"] + " added a " + variableJSON["note_type"] + " note to " + client_name + " from " + client_company + ": " + variableJSON["note_text"] + ". " + variableJSON["note_date"];

			var hipchatJSONstring = JSON.stringify(hipchatpost);
			//console.log(hipchatJSONstring);
			
			/*
			Avoid Sending messages to the hipchat Tier Room this should only be allowed on production
			Please make sure staging config is staging and not Production this will brek the logic	
			*/
			if (platform  == 'production') {
				if (bearer != "" && chat_room != "") {
					$.ajax({
						type: "POST",
						url: "https://avior.hipchat.com/v2/room/" + chat_room + "/notification?notify=true",
						dataType: "JSON",
						headers: { "Authorization": "Bearer " + bearer },
						data: JSON.parse(hipchatJSONstring)
					}).done(function () {
						console.log("Hipchat success");
					}).fail(function (xhr, status, error) {
						console.log("Hipchat fail");
						console.log(xhr);
						console.log(error);
					});
				}
			} 
			
			//normal message to send the note via ajax using the room chat id 
			var message = variableJSON["notebyNick"] + " added a " + variableJSON["note_type"] + " note to " + client_name + " from " + client_company + ": " + variableJSON["note_text"] + ". " + variableJSON["note_date"];
			$.ajax({
				type: "POST",
				url: "AjaxSendChatRoomNotification.php",
				dataType: "text",
				data: ({ "client_rooms": client_hipchat_rooms, "message": message, "note_by": variableJSON['notebyNick'],"platform" : platform}),
				success: function (responseData) {
					console.log(responseData);
					return false;
				},
				error: function (xhr, error) {
					console.log(xhr + " " + error);
				}
			});
			//---------------------------------------------------------//		
		
		});
		$('#addnoteform').hide('fold');
		$('#cancelnotebutton').hide('fold');
		$('#addnotebutton').show('fold');
	});

	$('#cnt_override_drm').change(function () {
		if ($(this).val() == '3') {
			$('#webviewerwithprint').removeClass('hide');
			$('#webviewerwithprint').addClass('show');
		} else {
			$('#webview_print').val(0);
			$('#webviewerwithprint').addClass('hide');
			$('#webviewerwithprint').removeClass('show');
		}
	});
});
