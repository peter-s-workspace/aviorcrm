$(document).ready(function(){
	
		// Main Tabs
		$('#tabs1').tabs();
		/* Client Admin Tab */
		$('#addnewclient,#editclient').hide();
		/*if there's an error, then show form */
		if( $('#MM_insert_error').val() == 1 || $('#MM_insert_success').val() == 1) {
				$('#addnewclient').show();
		}
		
		$('#add_new_client_link').click(function(){
			$('#editclient').hide();
			$('#addnewclient').show();
		});
		
		$('#edit-user-admin').load('ajax-useradmin.php');
		

		$('#edit_client_link').click(function(){
			$('#addnewclient').hide();
			$('#editclient').show();
		});
		

		$('#frmaddcomp').validate({
            errorClass:'validate-error',
            errorPlacement: function(error, element) {		
                error.appendTo( element.parents('tr').find("span.validation-error").addClass('error') );													
            },
            rules: {
				company_name: { required: true },
				company_tier: { required: true },
				company_type: { required: true },
				company_website: { required: false, url: true },
				social_fb: { required: false, url: true },
				social_twitter: { required: false, url: true },
				social_linkedin: { required: false, url: true }
            },
            messages: {		
				company_name: { required: 'Please enter Institution Name' },
				company_tier: { required: 'Please select Tier' },
				social_fb: { url: 'Please enter a valid URL starting with http(s)://' },
				social_twitter: { url: 'Please enter a valid URL starting with http(s)://' },
				social_linkedin: { url: 'Please enter a valid URL starting with http(s)://' }
            }
        });
		
		
		$('#cnt_search').autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						if(ui.item.id > 0 ) {
							$('#cnt_Id').val(ui.item.id);
							$('#btnsubmitEditClient').attr('disabled',false);
						}
					}
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);

					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
			
		
		$('#btncanceladdnewc').click(function(){
			$('#addnewclient').hide('fold');
			return false;
		});
		
		$('#btncanceleditclient').click(function(){
			$('#editclient').hide('fold');
			return false;
		});
		

			
		$("#cnt_bdate").datepicker({
						showOn: "button",
						changeMonth: true,
						changeYear: true,
						buttonImage: "images/av-calendar.gif",
						buttonImageOnly: true,
						buttonText: "Select date",
						dateFormat: "yy-mm-dd"
						});
		
		var color_selector = {'Platinum':'#E5E4E2','Gold':'#B8860B','Silver':'#C0C0C0','Not Assigned':'#CE1620','Blue': '#0066FF','Bronze':'#f4a460'};
		$('#my-form-dropzone select[name="cnt_company"]').change(function(){
			if( $(this).val() != '' ) { 
				get_hipchat_rooms ($(this).val());
				var companytier = $(this).find(':selected').data('tier');
				if( color_selector[companytier] ) {
					$('#selected_company_tier').html(companytier).css('background-color', color_selector[companytier]);
				} else {
					$('#selected_company_tier').html(companytier);
				}
			} else {
				$('#selected_company_tier').html('').css('background-color', '#fff');
			}
		});
		
		/* Company Admin Tab */
		$('#edit-company-container, #add-new-company-container, #edit-company-drm, #rename-company-container, #edit-company-form').hide();

		$('#edit-company').click(function(){
			$('#edit-company-container, #rename-company-container, #edit-company-drm').show();
			$('#add-new-company-container').hide();
		});
		
		$('#add-new-company').click(function(){
			$('#add-new-company-container').show();
			$('#edit-company-container, #rename-company-container, #edit-company-drm, #edit-company-form').hide();
		});
		
		/* Hip Chat Room logic here */
		$("#chat_room_insert").hide(); 
		$('#add_chat_room').click(function() {
			$("#chat_room_insert").show();
		});


		$('#closeChat').on("click",function(){
			$("#chat_room_insert").hide();
		});	

		
		$("#addchatroomform").validate({
			errorClass:'validate-error',
            errorPlacement: function(error, element) {	
                    error.appendTo( element.parents('tr').find("span.validation-error").addClass('error'));													
            },
            rules: {
            	chat_room_name: "required",
			},
			messages: {
				chat_room_name: "Please enter chat room name",
			}
			
		});


		$("#addchatroomformupdate").validate({
			errorClass:'validate-error',
            errorPlacement: function(error, element) {	
                    error.appendTo( element.parents('tr').find("span.validation-error").addClass('error'));													
            },
            rules: {
            update_chat_room_name: "required",
			update_chat_room_token: "required"
			},
			messages: {
				update_chat_room_name: "Please enter chat room name",
				update_chat_room_token: "Please enter chat room token"
			}
			
		 });
		
		
		$("#saveChatRoom").on("click", function(e){
				e.preventDefault();
				var form_valid = $("#addchatroomform").valid();
				if (form_valid === false) {
					return;
				} else {
					var form_data = $("#addchatroomform").serialize();
					$.ajax({
					  url: "ajaxSaveChatroom.php",
					  global: false,
					  type: "POST",
					  data: form_data,
					  success : function(response){
						if (response == "") {
							alert('Room already exists');
						} else {
							var jsonResponse = JSON.parse(response);
						  	alert(jsonResponse.message);
						  	location.reload();
						}
					  
					  }
				});
			}
		});
		/* Hip Chat Room logic End */

		$('#cancel_button').live('click',function(){
			$('#edit-company-form').html('').hide();
		});
		

	
		/*Company ID available - go to correct tab, and return edit form*/
		var selectedCompany  = getUrlParameter('comp');
		if( selectedCompany > 0 ) {
			$('#tabs1').tabs({selected: 1});
			$('#edit-company-container').show();
			$('#editCompanyForm select').val(selectedCompany);
			getCompanyForm(selectedCompany);

			//Strips url parameters used to redirect from Add Company to the newly
			//created companies edit page.
			var state = {};
			var title = 'adminmain';
			var url = 'adminmain.php';
			history.pushState(state, title, url);

		} else if(selectedCompany == 'add') {
					$('#tabs1').tabs({selected: 1});
					$('#add-new-company-container').show();
					$('#edit-company-container, #rename-company-container, #edit-company-drm, #edit-company-form').hide();
		}
		
		var selectedUser = getUrlParameter('u');
		if(selectedUser == 1 ){
			$('#tabs1').tabs({selected: 2});
		}

		/* Update company details */
		$('#editCompany').live('submit',function(){
			var formData = $(this).serialize();
			if( $('#editCompany').valid()){
				$.ajax({
						  url: "ajaxCompanySave.php",
						  global: false,
						  type: "POST",
						  data: formData,
						  success : function(result){
							if( result.length > 0 ) {
								if( result == "success" ) {
									$('#editCompanyResponse').html('<p><font color="red">Institution details updated successfully</font></p>');
									$('#editCompanyForm select').val('');
								} else {
									$('#editCompanyResponse').html('<p><font color="red">Could not update at the moment. Please try again later.</font></p>');	
								}
							}
						  }
					   });
			}
			return false;
		});
		
		$('#editCompanyForm').submit(function(){
			var selectedCompany = $('#editCompanyForm select').val();
			getCompanyForm(selectedCompany);
			return false;
		});

		
});

function get_hipchat_rooms (company_id) {
		$.ajax({
				url 		: "ajaxGetHipchatRooms.php",
				type 		: "post",
				dataType  	: "text",
				data 		: ({"company_id" : company_id}),
				success    	: function (responseData) {
					var jsonResponse 		= $.parseJSON(responseData);
					if (jsonResponse != "") {
						jsonResponse  = jsonResponse.map(function(e) {
							return JSON.stringify(e);
						});
						$("#rooms").css("display","");
						$("#hipchat_rooms").html("Hipchat Rooms :" + jsonResponse.join(","));
					} else {
						$("#rooms").css("display","none");
						$("#hipchat_rooms").html("");
					} 
				},
				error 		: function(jqXHR, exception){
					console.log("There was an error " + jqXHR.responseText);
				}	

		});	
}

$(function(){
	//stripe tables
	$('.stripeMe tr:even').addClass('alt');
	$('#filterclist').quicksearch('#clist tbody tr');
	
	/*
	$('.itemDelete').live('click', function() {
		$(this).closest('li').remove();
	});
	*/
	// Dialog			
	$('#dialog').dialog({
		autoOpen: false,
		width: 600,
		buttons: {
			"Ok": function() { 
				$(this).dialog("close"); 
			}, 
			"Cancel": function() { 
				$(this).dialog("close"); 
			} 
		}
	});
	

	// Dialog Link
	$('#dialog_link').click(function(){
		$('#dialog').dialog('open');
		return false;
	});
	
	//hide add reminder box
	$('#addreminder').hide();

	//reminder button -open form
	$('#btnaddreminder').click(function(){
		$('#addreminder').show('fold');
		return false;
	});
	//close reminder form
	$('#btnCancelRemind').click(function(){
		$('#addreminder').hide('fold');
		return false;
	});

	//hover states on the static widgets
	$('#dialog_link, ul#icons li').hover(
		function() { $(this).addClass('ui-state-hover'); }, 
		function() { $(this).removeClass('ui-state-hover'); }
	);
	
});

function getUrlParameter(param){
 	
	var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == param){
            return sParameterName[1];
        }
    }
	return 0;
}

function getCompanyForm(selectedCompany){
	if( selectedCompany > 0 ) {
	$.ajax({
		  url: "ajaxCompanyEdit.php",
		  global: false,
		  type: "POST",
		  data: ({id:selectedCompany}),
		  dataType: "html",
		  success : function(companyData){
			if( companyData.length > 0 ) {
				$('#edit-company-form').html(companyData).hide().fadeIn(500);
				$('#rename-company-container, #edit-company-drm').hide();
			} 
		  }
	   });
	} else {
		$('#edit-company-form').html('').hide();
	}
}

// check for change in a specific div
function updateCompany(update_column, val ,comp_id)
{
	var targetdiv = '#companycode_'+comp_id;
	$.ajax({
			url : "ajaxopscompanyupdate.php",
			type: "POST",
			data : {'update_column':update_column, 'val': val,'comp_id':comp_id },
			success: function(data, textStatus, jqXHR) {
				switch(update_column) {
					case 'tier':
						$(targetdiv).html('Tier Updated');
					break;
					case 'status':
						$(targetdiv).html('Status Updated');
						
					break;
					case 'type':
						$(targetdiv).html('Type Updated');
					break;
				}
			},
			 error: function (jqXHR, textStatus, errorThrown){
				 $(targetdiv).html('Update failed');
			}
		});
}