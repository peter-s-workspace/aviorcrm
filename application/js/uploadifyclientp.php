<?php
/*
Uploadify v2.1.0
Release Date: August 24, 2009

Copyright (c) 2009 Ronnie Garcia, Travis Nickels

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


if (!empty($_FILES) ) {
	$tempFile = $_FILES['Filedata']['tmp_name'];	
	//$targetPath = $_SERVER['DOCUMENT_ROOT'] . $path . $_REQUEST['folder'] . '/';
	$targetPath = '..' . $_REQUEST['folder'] . '/';
	$targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];
	
	//make directory if it doens not yet exist		
  if(!is_dir($targetPath))
 	 {
	
	mkdir($targetPath, 0777);
	} 
	
	// $fileTypes  = str_replace('*.','',$_REQUEST['fileext']);
	// $fileTypes  = str_replace(';','|',$fileTypes);
	// $typesArray = split('\|',$fileTypes);
	// $fileParts  = pathinfo($_FILES['Filedata']['name']);
	
	// if (in_array($fileParts['extension'],$typesArray)) {
		// Uncomment the following line if you want to make the directory if it doesn't exist
		// mkdir(str_replace('//','/',$targetPath), 0755, true);
		
		move_uploaded_file($tempFile,$targetFile);
	
	// *** Include the class  
include("resize-class.php");
	//resize the file quickly
		$width = 150;
	   $height = 150;
	// *** 1) Initialize / load image  
		$resizeObj = new resize($targetFile);  
  
	// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)  
		$resizeObj -> resizeImage($width,$height, 'auto');  
  
	// *** 3) Save image  
		$resizeObj -> saveImage($targetPath."1.jpg", 100);  
		//remove original
		unlink($targetFile);
		
		echo "1";
	// } else {
	// 	echo 'Invalid file type.';
	// }
}
?>