function callMitel (user_extension, phone_number_to_dial, client_name,client_surname) {

		var client_full_name 	= client_name + ' ' + client_surname;
		$("#mitel_pop_up").dialog("open");
		$("#call_number").text(client_full_name);
		
		/* 
			this code can  be enabled to display the phone icon on clientdetails.php page
			var elements 	= document.getElementsByClassName('phone_disable');
			$.each(elements , function(i,k){
				console.log(i);
				k.style.pointerEvents 	= "none";
				k.style.cursor 			= "default";
			});
		*/
	
		$.ajax({
			url 	: "initiateMitelCall.php",
			type 	: "post",
			data 	: ({"user_extension" : user_extension, "phone_number" : phone_number_to_dial}),
			success	: function (responseData) {
				console.log("Initiated a call to phone number " + phone_number_to_dial + " " + "for user_extension " + user_extension);
				/*$.each(elements , function(i,k){
				 console.log(i);
					k.style.pointerEvents 	= "auto";
					k.style.cursor 			= "pointer";
				}); */
			},
			error  	: function (jqXHR, exception) {
				console.log(jqXHR.status + ' : ' + jqXHR.responseText);
			}	

		});

}

