CREATE TABLE `tblcompanygeography` (
  `id_geography` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `display_status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_geography`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tblcompanygeography VALUES ('','Local',1);
INSERT INTO tblcompanygeography VALUES ('','International',1);
INSERT INTO tblcompanygeography VALUES ('','Africa',1);

ALTER TABLE `tblcompanylist`
ADD COLUMN `id_geography` int(11)  NULL after `id_companytier`;
