<?php require_once('../Connections/CRMconnection.php'); ?>
<?php


# Recursively deletes a directory structure.
function removeDirectory($path) {
 	$files = glob($path . '/*');
	foreach ($files as $file) {
		is_dir($file) ? removeDirectory($file) : unlink($file);
	}
	rmdir($path);
 	return;
}


$upload_path = "../uploads";



# remove all directories that aren't essential to file structure.
$all_upload_directories = glob($upload_path . '/*');

foreach($all_upload_directories as $upload_dir){

	$key = substr($upload_dir, 11);

	if ($key != "public" && $key != "clientp" && $key != "companyp"){
	 	removeDirectory($upload_dir);
	}
}

# Remove the files and NOT directories in this path
$all_upload_directories = glob($upload_path . '/public/*');

foreach($all_upload_directories as $upload_dir){

	$key = substr($upload_dir, 18);

	if ($key != "company"){
	 	unlink($upload_dir);
	}
}

# Remove all the directories in this path
$all_upload_directories = glob($upload_path . '/public/company/*');

foreach($all_upload_directories as $upload_dir){
	$key = substr($upload_dir, 26);
	removeDirectory($upload_dir);
}


?>