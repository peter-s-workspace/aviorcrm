
ALTER TABLE `contacts` 
ADD COLUMN `portal_access` CHAR(1)  COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `drm_status`,
ADD COLUMN `webview_print` CHAR(1)  COLLATE utf8_general_ci NOT NULL DEFAULT '0' AFTER `portal_access`;

ALTER TABLE `tblcompanylist` 
ADD COLUMN `portal_access` CHAR(1)  COLLATE utf8_general_ci NOT NULL DEFAULT '1' AFTER `modified_date`, 
ADD COLUMN `webview_print` CHAR(1)  COLLATE utf8_general_ci NOT NULL DEFAULT '0' AFTER `portal_access`;


