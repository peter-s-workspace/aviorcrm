


/*---------------------------------------------------------------------------
				Create Employees table
	---------------------------------------------------------------------------*/

USE `avcrm_new` ;

DROP TABLE IF EXISTS employees;

CREATE TABLE IF NOT EXISTS `employees`
  (
    `id`          	INT         	NOT NULL AUTO_INCREMENT,
    `first_name`  	VARCHAR(50) 	NOT NULL,
    `last_name`   	VARCHAR(50) 	NOT NULL,
    `alias`       	VARCHAR(50) 	NULL DEFAULT NULL,
    `email`       	VARCHAR(50) 	NULL DEFAULT NULL,
    `phone`       	VARCHAR(25) 	NOT NULL,
    `phone_ip`      VARCHAR(50)   NULL,
    `birthday`    	DATE        	NULL DEFAULT NULL,
    `user_id`     	INT         	NOT NULL,

    INDEX `fk_employees_user1_idx` (`user_id` ASC),
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_employees_users1`
      FOREIGN KEY (`user_id`)
      REFERENCES `users` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
  ) ENGINE = InnoDB;



/*---------------------------------------------------------------------------
				Populate Employees table
	---------------------------------------------------------------------------*/


SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE employees;

INSERT INTO employees (`user_id`, `first_name`, `last_name`, `alias`, `email`
                      , `phone`, `phone_ip`)
SELECT U.user_id, U.user_fname, U.user_lname, U.user_nick
      , U.user_email, U.user_phone, U.user_phone_ip
  FROM avcrm.tbluser U
WHERE U.user_fname IS NOT NULL
ORDER BY 1;

SET FOREIGN_KEY_CHECKS = 1;
