USE `avcrm_new` ;

/* ----------------------------------------------------------------------------
    Tables that contains mappings to the tbluser are sometimes mapped to a
    username that is not an email address. This causes issues with migration.
    Some individuals have been identified correctly and we will manually
    update them so it help the migration.

  ----------------------------------------------------------------------------*/

/* ----------------------------------------------------------------------------
      tblnotes.note_by = tbluser.user_name
     NaeemTilly         --> naeem@avior.co.za
     KarenLourens       --> karen@avior.co.za
  ----------------------------------------------------------------------------*/

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_SAFE_UPDATES = 0;

  UPDATE `avcrm`.tblnotes N
  SET N.note_by = (SELECT user_name
  								FROM `avcrm`.tbluser C
  								WHERE C.user_fname = 'Naeem'
  										AND C.user_lname = 'Tilly')
  WHERE N.note_by = 'NaeemTilly';

  UPDATE `avcrm`.tblnotes N
  SET N.note_by = (SELECT user_name
  								FROM `avcrm`.tbluser C
  								WHERE C.user_fname = 'Karen'
  										AND C.user_lname = 'Kelly')
  WHERE N.note_by = 'KarenLourens';

  /* ----------------------------------------------------------------------------
      tblevent_logs.user = tbluser.user_name
       NaeemTilly         --> naeem@avior.co.za
      KarenKelly          --> karen@avior.co.za
    ----------------------------------------------------------------------------*/

  UPDATE `avcrm`.tblevent_logs N
  SET N.user = (SELECT user_name
  								FROM `avcrm`.tbluser C
  								WHERE C.user_fname = 'Naeem'
  										AND C.user_lname = 'Tilly')
  WHERE N.user = 'NaeemTilly';


    UPDATE `avcrm`.tblevent_logs N
    SET N.user = (SELECT user_name
    								FROM `avcrm`.tbluser C
                    WHERE C.user_fname = 'Karen'
    										AND C.user_lname = 'Kelly')
    WHERE N.user = 'KarenKelly';

/* ----------------------------------------------------------------------------
      cnt_listnames.ownerusername = tbluser.user_name
       NaeemTilly         --> naeem@avior.co.za
    ----------------------------------------------------------------------------*/

    UPDATE `avcrm`.cnt_listnames N
    SET N.ownerusername = (SELECT user_name
    								FROM `avcrm`.tbluser C
    								WHERE C.user_fname = 'Naeem'
    										AND C.user_lname = 'Tilly')
    WHERE N.ownerusername = 'NaeemTilly';

SET SQL_SAFE_UPDATES = 1;
SET FOREIGN_KEY_CHECKS = 1;
