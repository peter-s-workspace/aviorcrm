
/*-------------------------------------
		Populate User table
	-------------------------------------*/
USE `avcrm_new` ;

SET FOREIGN_KEY_CHECKS = 0;

/*---------------------------------------------------------------------------
		Populate users
	---------------------------------------------------------------------------*/
TRUNCATE users;

INSERT INTO users (id, user_name, password, active, user_type_id)
SELECT T.user_id, T.user_name, T.user_pword, IF(T.user_status = 1, 1, 0), 1
FROM avcrm.tbluser T;

/*---------------------------------------------------------------------------
		Populate UserType
	---------------------------------------------------------------------------*/
TRUNCATE user_type;
INSERT INTO user_type (Name)
VALUES('Employee'), ('Contact');

/*---------------------------------------------------------------------------
		Populate Roles
	---------------------------------------------------------------------------*/
TRUNCATE roles;
INSERT INTO roles (Description)
SELECT DISTINCT U.user_level
FROM avcrm.tbluser U;

/*---------------------------------------------------------------------------
		Populate users_roles
	---------------------------------------------------------------------------*/

TRUNCATE users_roles;

INSERT INTO users_roles (roles_id, user_id)
SELECT R.ID, U.Id
FROM avcrm.tbluser T
JOIN users U
  ON T.user_id = U.id
JOIN Roles R
  ON R.Description = T.user_level;

SET FOREIGN_KEY_CHECKS = 1;
