



USE `avcrm_new` ;


SET FOREIGN_KEY_CHECKS = 0;

/*---------------------------------------------------------------------------
				Insert company_type table
	---------------------------------------------------------------------------*/
TRUNCATE company_type;

INSERT INTO company_type (`id`, `name`, `description`, `display_order`)
SELECT C.id_companytype, C.companytype, CONCAT('Description of ', C.companytype)
    , C.display_order
FROM avcrm.tblcompanytype C
ORDER BY C.display_order;


-- -----------------------------------------------------
-- Table `company_status`
-- -----------------------------------------------------
TRUNCATE company_status;

INSERT INTO `company_status` (`id`,`name`,`description`,`display_order`)
SELECT  C.id_companystatus, C.company_status
    , CONCAT('Description of ', C.company_status, ' status')
    , C.display_order
FROM avcrm.tblcompanystatus C;


-- -----------------------------------------------------
-- Table `company_tier`
-- -----------------------------------------------------
TRUNCATE company_tier;

INSERT INTO `company_tier` (`id`,`name`,`description`,`display_order`)
SELECT C.id_companytier, C.companytier
    , CONCAT('Description of ', C.companytier, ' tier'), C.display_order
FROM avcrm.tblcompanytier C;


-- -----------------------------------------------------
-- Table `company_tier`
-- -----------------------------------------------------
TRUNCATE communication_type;

INSERT INTO `communication_type` (`id`, `name`, `description`)
VALUES (1, 'switchboard',  'Phone number to a company switchboard')
			 , (2, 'website',  	'Link to a web address')
			 , (3, 'facebook',  	'Link to facebook account')
       , (4, 'twitter',  		'Link to twitter account')
       , (5, 'linkedin', 	'Link to linkedin account');

-- -----------------------------------------------------
-- Table `relationship_type`
-- -----------------------------------------------------
TRUNCATE relationship_type;

INSERT INTO `relationship_type` (`id`,`name`,`description`)
VALUES (1, 'Key accounts manager', 'Decription of  KAM')
			 , (2, 'Dealer', 'Decription of  dealer');

-- -----------------------------------------------------
-- Table `company_geography`
-- -----------------------------------------------------
TRUNCATE company_geography;

INSERT INTO `company_geography` (`id`,`name`)
VALUES (1, 'Local')
     , (2, 'International')
     , (3, 'Africa');




SET FOREIGN_KEY_CHECKS = 1;
