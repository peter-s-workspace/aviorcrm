

SET FOREIGN_KEY_CHECKS = 0;
/*---------------------------------------------------------------------------
				Create company_type table
	---------------------------------------------------------------------------*/

USE `avcrm_new` ;

DROP TABLE IF EXISTS company_type;

CREATE TABLE IF NOT EXISTS `company_type`
	 (
		  `id` 						INT 					NOT NULL 	AUTO_INCREMENT,
		  `name` 					VARCHAR(30) 	NOT NULL ,
		  `description` 	VARCHAR(45) 	NOT NULL ,
		  `display_order` INT 					NULL,

		  PRIMARY KEY (`id`)
	) ;


-- -----------------------------------------------------
-- Table `company_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS company_status;

CREATE TABLE IF NOT EXISTS `company_status`
	(
	  `id` 							INT 					NOT NULL AUTO_INCREMENT,
	  `name` 						VARCHAR(30) 	NOT NULL,
	  `description` 		VARCHAR(45) 	NOT NULL,
	  `display_order` 	INT 					NULL,
	  PRIMARY KEY (`id`)
	  ) ;


-- -----------------------------------------------------
-- Table `company_tier`
-- -----------------------------------------------------
DROP TABLE IF EXISTS company_tier;

CREATE TABLE IF NOT EXISTS `company_tier`
	 (
	  `id` 							INT		 				NOT NULL AUTO_INCREMENT,
	  `name` 						VARCHAR(30) 	NOT NULL ,
	  `description` 		VARCHAR(50) 	NOT NULL ,
	  `display_order` 	INT 					NULL 			DEFAULT NULL,

	  PRIMARY KEY (`id`)
	  );


/*---------------------------------------------------------------------------
				Create communication_type table
	---------------------------------------------------------------------------*/

DROP TABLE IF EXISTS communication_type;

CREATE TABLE IF NOT EXISTS `communication_type`
	(
	  `id` 						INT 						NOT NULL,
	  `name` 					VARCHAR(30) 	NOT NULL,
	  `description` 	VARCHAR(50) 	NOT NULL,

	  PRIMARY KEY (`id`)
	)	ENGINE = InnoDB;

/*---------------------------------------------------------------------------
					Create company_geography table
		---------------------------------------------------------------------------*/
	CREATE TABLE IF NOT EXISTS `company_geography`
			(
			  `id` 		INT NOT NULL,
			  `name` 	VARCHAR(45) NOT NULL,

			  PRIMARY KEY (`id`)
			)	ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `relationship_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS relationship_type;

CREATE TABLE IF NOT EXISTS `relationship_type`
	(
	  `id` 						INT 						NOT NULL,
	  `name` 					VARCHAR(45) 	NOT NULL,
	  `description` 	VARCHAR(100) 	NULL,

	  PRIMARY KEY (`id`)
	)	ENGINE = InnoDB;

SET FOREIGN_KEY_CHECKS = 1;
