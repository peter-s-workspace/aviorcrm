
USE `avcrm_new` ;

SET FOREIGN_KEY_CHECKS = 0;

/*---------------------------------------------------------------------------
				Create company table
	---------------------------------------------------------------------------*/

DROP TABLE IF EXISTS company;

CREATE TABLE IF NOT EXISTS `company`
	(
	  `id` 										INT 							NOT NULL,
	  `company` 							VARCHAR(100) 			NOT NULL,
	  `drm_status` 						INT 							NULL,
	  `company_type_id` 			INT 							NOT NULL,
	  `company_status_id` 		INT 							NOT NULL,
	  `company_tier_id`				INT 							NOT NULL,
		`company_geography_id` 	INT 							NOT NULL DEFAULT 1,
		`modified_date` 				DATETIME 					NULL,

	  PRIMARY KEY (`id`),
	  UNIQUE INDEX `company` (`company` ASC),
	  CONSTRAINT `fk_company_company_status1`
			FOREIGN KEY (`company_status_id`)
			REFERENCES `company_status` (`id`)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	  CONSTRAINT `fk_company_company_tier1`
			FOREIGN KEY (`company_tier_id`)
			REFERENCES `company_tier` (`id`)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	  CONSTRAINT `fk_company_company_type1`
			FOREIGN KEY (`company_type_id`)
			REFERENCES `company_type` (`id`)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		CONSTRAINT `fk_company_company_geography1`
			FOREIGN KEY (`company_geography_id`)
			REFERENCES `company_geography` (`id`)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
    )	ENGINE = InnoDB;


/*---------------------------------------------------------------------------
				Create company_address table
	---------------------------------------------------------------------------*/


DROP TABLE IF EXISTS company_address;

CREATE TABLE IF NOT EXISTS `company_address`
	 (
	  `id` 					INT 						NOT NULL	AUTO_INCREMENT,
	  `company_id` 	INT 						NOT NULL,
	  `address` 			VARCHAR(255) 	NULL,

	  PRIMARY KEY (`id`),
	  INDEX `fk_company_address_company1_idx` (`company_id` ASC),
	  CONSTRAINT `fk_company_address_company1`
		FOREIGN KEY (`company_id`)
		REFERENCES `company` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
    )	ENGINE = InnoDB;


/*---------------------------------------------------------------------------
				Create communication table
	---------------------------------------------------------------------------*/


DROP TABLE IF EXISTS communication;

CREATE TABLE IF NOT EXISTS `communication`
	(
	  `company_id` 					INT 						NOT NULL,
	  `communication_type_id` 	INT 						NOT NULL,
	  `coms_value` 					VARCHAR(255) 	NOT NULL,

	  INDEX `fk_company_communication_communication_type1_idx` (`communication_type_id` ASC),
	  INDEX `fk_company_communication_company1_idx` (`company_id` ASC),
	  CONSTRAINT `fk_company_communication_communication_type1`
		FOREIGN KEY (`communication_type_id`)
		REFERENCES `communication_type` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	  CONSTRAINT `fk_company_communication_company1`
		FOREIGN KEY (`company_id`)
		REFERENCES `company` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
    )	ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `company_notes`
	(
	  `id` INT NOT NULL AUTO_INCREMENT,
	  `company_note` TEXT NOT NULL,
	  `date` DATE NOT NULL,
	  `company_Id` INT(11) NOT NULL,
	  `user_id` INT NULL COMMENT 'The UserId should mapped to User logged onto the system. From the UserID we can map to employee or contact',
	  PRIMARY KEY (`id`),
	  INDEX `fk_tblcompanynotes_tblcompanylist1_idx` (`company_Id` ASC),
	  CONSTRAINT `fk_tblcompanynotes_tblcompanylist1`
		FOREIGN KEY (`company_Id`)
		REFERENCES `company` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
    )	ENGINE = InnoDB;
