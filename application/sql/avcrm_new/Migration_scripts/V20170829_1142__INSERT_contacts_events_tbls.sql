



USE `avcrm_new` ;


SET FOREIGN_KEY_CHECKS = 0;

-- -----------------------------------------------------
-- Table `interests`
-- -----------------------------------------------------
TRUNCATE note_type;

-- 1. Insert note_type from avcrm.contacts
INSERT INTO `note_type` (`id`, `note_type`)
VALUES (1, 'Information');

-- 2. Insert all notes from avcrm.tblnotes

INSERT INTO `note_type` (`note_type`)
  SELECT C.Contacttype
  FROM avcrm.tblcontacttype C
  ORDER By 1;


-- -----------------------------------------------------
-- Table `contacts`
-- -----------------------------------------------------
TRUNCATE contact_notes;

  -- 1. Insert all notes from avcrm.tblnotes

  INSERT INTO `contact_notes` (`Id`, `note_txt`, `note_date`, `cntCompany`
                            , `note_type_Id`, `contacts_id`, `users_id`)
  SELECT N.note_Id
			  , IfNull(N.note_txt, '') AS 'Note_Text'
        , N.autodate
        , N.cntCompany
			  , T.id  				         AS 'note_type_Id'
			  , IfNull(C.id, 0) 	     AS 'contacts_id'
			  , IfNull(U.user_id, 0) 	 AS 'users_id'
  FROM avcrm.tblnotes N
  JOIN note_type T
    ON N.note_type = T.note_type
  LEFT JOIN avcrm.contacts C
	       ON N.note_cnt_Id = C.id
  LEFT JOIN avcrm.tbluser U
	       ON N.note_by = U.user_name
        -- AND N.notebyNick = U.user_nick
  ORDER By 1;

  -- 2. Insert notes from avcrm.contacts

  INSERT INTO contact_notes (`note_txt`, `cntCompany`, `note_type_Id`
                            , `contacts_id`, `users_id`)
		SELECT C.note
				,  CL.Company
				, 1                   AS 'note_type_Id'
				, C.id                AS 'contacts_id'
        , 0                   AS 'users_id'
		FROM `avcrm`.contacts C
		LEFT JOIN `avcrm`.tblcompanylist CL
					 ON C.company_id = CL.comp_id
		WHERE C.note IS NOT NULL;

-- 2. Map missing contacts by first name and surname

-- SELECT DISTINCT N.note_cnt_id, N.note_cnt_name
-- FROM contact_notes CN
--   JOIN avcrm.tblnotes N
--   	ON CN.id = N.note_id
-- WHERE CN.contacts_id = 0
-- 	AND  N.note_cnt_name NOT LIKE '%test%';

SET SQL_SAFE_UPDATES = 0;

  UPDATE contact_notes CN
  JOIN avcrm.tblnotes N
  	ON CN.id = N.note_id
  JOIN avcrm.contacts C
  	ON N.note_cnt_name = CONCAT(C.first_name, ' ', C.surname)
  SET CN.contacts_id = C.id
  WHERE CN.contacts_id = 0;


-- 3. Map to users where possible

-- SELECT COUNT(*)
-- FROM contact_notes CN
-- WHERE CN.users_id = 0;
--
-- SELECT COUNT(*)
--    FROM contact_notes CN
--      JOIN avcrm.tblnotes N
--   	    ON CN.id = N.note_id
--      JOIN avcrm.tbluser U
-- 	    ON N.notebyNick = U.user_nick
--       AND N.note_by = U.user_name
--  WHERE CN.users_id = 0;


-- -----------------------------------------------------
-- Table `favourite_list`
-- -----------------------------------------------------


TRUNCATE favourite_list;

INSERT INTO `favourite_list` (`id`, `name`, `owner_user_id`
												, `shared`, `editable_by_others`, `description`)
SELECT CL.`listid`,
				CL.`listname`,
                IfNull(U.user_id, 0)		'User_id',
				CL.`shared`,
				CL.`editablebyothers`
                , CONCAT('Description of ', CL.`listname`)
FROM `avcrm`.cnt_listnames CL
LEFT JOIN `avcrm`.tbluser U
			ON U.user_name = CL.`ownerusername`
ORDER BY 1;

-- -----------------------------------------------------
-- Table `user_favourite_list`
-- -----------------------------------------------------
TRUNCATE user_favourite_list;

INSERT INTO `user_favourite_list` (`id`, `favourite_lists_id`, `users_id`, `contacts_id`)
SELECT TF.tblFav_id
			 , TF.listid
			, IFNull(U.user_id, 0) 		AS 'users_id'
            , TF.tbl_cnt_id
FROM `avcrm`.tblfavourites TF
LEFT JOIN `avcrm`.tbluser U
			ON U.user_name = TF.`fav_Username`	;

-- -----------------------------------------------------
-- Table `crm_event_type`
-- -----------------------------------------------------
TRUNCATE crm_event_type;

INSERT INTO crm_event_type (id, name, description)
SELECT E.`event_id`,
			E.`event_alias`,
			E.`event_description`
FROM `avcrm`.`tblevents`E;

/* -----------------------------------------------------
-- Table `crm_event_logs`
Issue here with duplicate in contacts table

-- -----------------------------------------------------  */
-- TRUNCATE crm_event_logs;
--
--
-- INSERT INTO `crm_event_logs` (`id`, `user_id`, `affected_user_id`, `user_type_id`
--  														, `before_values`, `after_values`, `date_created`
--                                                          , `crm_event_type_id`)
--
--   -- 1. migrate clients
--   SELECT EL.`id`,
--   			IfNull(U.user_id, 0)				AS 'user_id'	,
--   			ifNull(C.id, 0) 						AS 'affected_user_id',
--               2 										 	AS 'user_type_id',
--   			EL.`before_values`,
--   			EL.`after_values`,
--   			EL.`date_created`,
--               EL.`event_id`
--   FROM `avcrm`.`tblevent_logs` EL
--   LEFT JOIN `avcrm`.tbluser U
--   			ON EL.user =  U.user_name
--   LEFT JOIN (	-- This is faster than join on CONCAT
--   						SELECT  C.id, CONCAT(C.first_name, ' ', C.surname) AS C_name
--   							FROM avcrm.contacts C
--                       ) C
--   			ON EL.affected_user = C.C_name
--
--   WHERE EL.affected_usertype = 'client'			-- user_type_id  = 2
--
--   UNION ALL
--
--   -- 2. migrate employees
--   SELECT EL.`id`,
--   			IfNull(U.user_id, 0)				AS 'user_id'	,
--   			ifNull(E.user_id, 0) 				AS 'affected_user_id',
--               1 										 	AS 'user_type_id',
--   			EL.`before_values`,
--   			EL.`after_values`,
--   			EL.`date_created`,
--               EL.`event_id`
--   FROM `avcrm`.`tblevent_logs` EL
--   LEFT JOIN `avcrm`.tbluser U
--   			ON EL.user =  U.user_name
--   LEFT JOIN `avcrm`.tbluser E
--   			ON EL.affected_user =  E.user_name
--
--   WHERE EL.affected_usertype = 'user'			-- user_type_id  = 1
--   ORDER BY 1;

 /* ------------------------------------------------------------ */

 SET SQL_SAFE_UPDATES = 1;

SET FOREIGN_KEY_CHECKS = 1;
