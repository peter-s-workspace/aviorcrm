
USE `avcrm_new` ;

SET FOREIGN_KEY_CHECKS = 0;


-- -----------------------------------------------------------------------------
-- Table `contacts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `contacts`;

CREATE TABLE IF NOT EXISTS `contacts`
	(
	  `id` 								INT 					NOT NULL 	AUTO_INCREMENT,
	  `first_name` 				VARCHAR(50) 	NOT NULL,
	  `last_name` 				VARCHAR(50) 	NOT NULL,
	  `landline` 					VARCHAR(25) 	NULL 			DEFAULT NULL,
	  `fax` 							VARCHAR(25) 	NULL 			DEFAULT NULL,
	  `mobile` 						VARCHAR(25) 	NULL 			DEFAULT NULL,
	  `email` 						VARCHAR(50) 	NOT NULL,
	  `company_id` 				INT 					NULL 			DEFAULT NULL,
	  `job_title` 				VARCHAR(100) 	NULL 			DEFAULT NULL,
	  `bloomberg_user` 		VARCHAR(30) 	NULL 			DEFAULT NULL,
	  `bloomberg_status` 	INT 					NULL 			DEFAULT NULL,
	  `birthday` 					DATE 					NULL 			DEFAULT NULL,
	  `active` 						BIT(1)				NOT NULL	DEFAULT 1,
	  `drm_status` 				TINYINT(1)		NULL,
	  `user_id` 					INT						NULL,

	  PRIMARY KEY (`id`),
	  INDEX `fk_company` (`company_id` ASC),
	  INDEX `fk_contacts_User1_idx` (`user_id` ASC),

-- Not sure about thses indexes
	  INDEX `cnt_Fname` (`first_name` ASC),
	  INDEX `cnt_Lastname` (`last_name` ASC),
	  INDEX `cnt_phone` (`landline` ASC),
	  INDEX `cnt_cell` (`mobile` ASC),
	  INDEX `cnt_email` (`email`(50) ASC),

	  CONSTRAINT `fk_company`
		FOREIGN KEY (`company_id`)
		REFERENCES `company` (`id`),
	  CONSTRAINT `fk_contacts_users1`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
    )	ENGINE = InnoDB;


    -- -----------------------------------------------------
-- Table `contacts_additional`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `contacts_additional`;

CREATE TABLE IF NOT EXISTS `contacts_additional`
	(
	  `contacts_id` 			INT 					NOT NULL,
	  `phone_number` 			VARCHAR(25) 	NOT NULL,
	  `phone_description` VARCHAR(50) 	NOT NULL,

	  INDEX `fk_addphone_contacts1_idx` (`contacts_id` ASC),
	  CONSTRAINT `fk_addphone_contacts1`
		FOREIGN KEY (`contacts_id`)
		REFERENCES `contacts` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
	)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `sector`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `sector`;

CREATE TABLE IF NOT EXISTS `sector`
	(
	  `id` 				INT 						NOT NULL,
	  `name` 			VARCHAR(45) 	NOT NULL,
	  `description` 	VARCHAR(45) 	NOT NULL,

	  PRIMARY KEY (`id`)
  )	ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contact_sector`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `contacts_has_sector`;

CREATE TABLE IF NOT EXISTS `contacts_has_sector`
	(
	  `contacts_id` 	INT 	NOT NULL,
	  `sector_id` 		INT 	NOT NULL,

	  PRIMARY KEY (`contacts_id`, `sector_id`),
	  INDEX `fk_contacts_has_sector_sector1_idx` (`sector_id` ASC),
	  INDEX `fk_contacts_has_sector_contacts1_idx` (`contacts_id` ASC),
	  CONSTRAINT `fk_contacts_has_sector_contacts1`
		FOREIGN KEY (`contacts_id`)
		REFERENCES `contacts` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	  CONSTRAINT `fk_contacts_has_sector_sector1`
		FOREIGN KEY (`sector_id`)
		REFERENCES `sector` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
    )	ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `company_has_contacts`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `mail_lists`;

CREATE TABLE IF NOT EXISTS `mail_lists`
	(
	  `id` 						INT 					NOT NULL 	AUTO_INCREMENT,
	  `name` 					VARCHAR(50) 	NOT NULL,
	  `description` 	VARCHAR(100) 	NOT NULL,

	  PRIMARY KEY (`id`)
  )	ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contacts_has_mail_lists`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `contacts_has_mail_lists`;

CREATE TABLE IF NOT EXISTS `contacts_has_mail_lists`
(
  `contacts_id` 	INT 	NOT NULL,
  `mail_lists_id` 	INT 	NOT NULL,

  PRIMARY KEY (`contacts_id`, `mail_lists_id`),
  INDEX `fk_contacts_has_mail_lists_mail_lists1_idx` (`mail_lists_id` ASC),
  INDEX `fk_contacts_has_mail_lists_contacts1_idx` (`contacts_id` ASC),
  CONSTRAINT `fk_contacts_has_mail_lists_contacts1`
    FOREIGN KEY (`contacts_id`)
    REFERENCES `contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contacts_has_mail_lists_mail_lists1`
    FOREIGN KEY (`mail_lists_id`)
    REFERENCES `mail_lists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    )	ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `company_has_contacts`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `company_has_contacts`;

CREATE TABLE IF NOT EXISTS `company_has_contacts`
	(
	  `company_id`				INT 	NOT NULL,
	  `contacts_id` 				INT 	NOT NULL,
	  `relationship_type_id` 	INT 	NOT NULL,

	  PRIMARY KEY (`company_id`, `contacts_id`, `relationship_type_id`),
	  INDEX `fk_company_has_contacts_contacts1_idx` (`contacts_id` ASC),
	  INDEX `fk_company_has_contacts_company1_idx` (`company_id` ASC),
	  INDEX `fk_company_has_contacts_relationship_type1_idx` (`relationship_type_id` ASC),
	  CONSTRAINT `fk_company_has_contacts_company1`
		FOREIGN KEY (`company_id`)
		REFERENCES `company` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	  CONSTRAINT `fk_company_has_contacts_contacts1`
		FOREIGN KEY (`contacts_id`)
		REFERENCES `contacts` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	  CONSTRAINT `fk_company_has_contacts_relationship_type1`
		FOREIGN KEY (`relationship_type_id`)
		REFERENCES `relationship_type` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
    )	ENGINE = InnoDB;

 /* ------------------------------------------------------------ */

SET FOREIGN_KEY_CHECKS = 1;
