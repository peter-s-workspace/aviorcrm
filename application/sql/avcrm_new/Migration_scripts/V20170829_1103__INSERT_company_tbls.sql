



USE `avcrm_new` ;


SET FOREIGN_KEY_CHECKS = 0;

-- -----------------------------------------------------
-- Table `company`
-- -----------------------------------------------------
TRUNCATE company;

INSERT INTO `company` (`id`, `company`, `drm_status`, `modified_date`,
											`company_type_id`, `company_status_id`, `company_tier_id`,
											`company_geography_id`)

SELECT C.comp_id, C.Company, C.drm_status, C.modified_date, C.id_companytype
		, IfNULL(C.id_companystatus, 4), C.id_companytier, IfNULL(C.id_geography, 1)
FROM avcrm.tblcompanylist C;

-- -----------------------------------------------------
-- Table `company_address`
-- -----------------------------------------------------
TRUNCATE company_address;

INSERT INTO `company_address` (`company_id`, `address`)
SELECT Ad.comp_id, Ad.address
FROM (
      SELECT  C.comp_id, C.address_1 AS 'address', 1 as 'Rank'
      FROM avcrm.tblcompanylist C
      WHERE C.address_1 IS NOT NULL
    UNION
      SELECT C.comp_id, C.address_2, 2
      FROM avcrm.tblcompanylist C
      WHERE C.address_2 IS NOT NULL
    UNION
      SELECT C.comp_id, C.address_3, 3
      FROM avcrm.tblcompanylist C
      WHERE C.address_3 IS NOT NULL
    UNION
      SELECT  C.comp_id, C.address_4, 4
      FROM avcrm.tblcompanylist C
      WHERE C.address_4 IS NOT NULL
    ) Ad
ORDER BY Ad.comp_id, Ad.Rank;



/*---------------------------------------------------------------------------
		Insert communication
	---------------------------------------------------------------------------*/

TRUNCATE communication;

INSERT INTO `communication` (`company_id`, `communication_type_id`, `coms_value`)
SELECT Ad.comp_id, Ad.comms_type, Ad.comms_value
FROM (
      SELECT  C.comp_id
					, 1  							AS 'comms_type'
					, C.switchboard_1 AS 'comms_value'
					, 1								AS 'Rank'
      FROM avcrm.tblcompanylist C
      WHERE C.switchboard_1 IS NOT NULL
    UNION
      SELECT C.comp_id
					, 1
					, C.switchboard_2
					, 2
      FROM avcrm.tblcompanylist C
      WHERE C.switchboard_2 IS NOT NULL
    UNION
      SELECT C.comp_id
					, 2
					, C.website
					, 3
      FROM avcrm.tblcompanylist C
      WHERE C.website IS NOT NULL
    UNION
      SELECT C.comp_id
					, 3
					, C.social_fb
					, 4
      FROM avcrm.tblcompanylist C
      WHERE C.social_fb IS NOT NULL
    UNION
      SELECT C.comp_id
					, 4
					, C.social_twitter
					, 5
      FROM avcrm.tblcompanylist C
      WHERE C.social_twitter IS NOT NULL
    UNION
      SELECT C.comp_id
					, 5
					, C.social_linkedin
					, 6
      FROM avcrm.tblcompanylist C
      WHERE C.social_linkedin IS NOT NULL
    ) Ad
ORDER BY Ad.comp_id, Ad.Rank;




SET FOREIGN_KEY_CHECKS = 1;
