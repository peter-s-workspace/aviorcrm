



USE `avcrm_new` ;


SET FOREIGN_KEY_CHECKS = 0;

-- -----------------------------------------------------
-- Table `contacts`
-- -----------------------------------------------------
TRUNCATE contacts;

INSERT INTO `contacts` (`id`, `first_name`, `last_name`,
											`landline`, `fax`, `mobile`, `email`,
                                            `company_id`, `job_title`, `bloomberg_user`, `bloomberg_status`,
                                            `birthday`, `active`, `drm_status`)
SELECT C.id, C.first_name, C.surname
     , C.landline, C.fax, C.mobile, c.email
     , C.company_id, C.jobtitle, C.bloomberg_user, C.bloomberg_status
     , C.dob, C.active, C.drm_status
FROM avcrm.contacts C;


-- -----------------------------------------------------
-- Table `sector`
-- -----------------------------------------------------
TRUNCATE sector;

  INSERT INTO `sector` (`id`, `name`, `description`)
  SELECT R.respons_id, R.response_desc, CONCAT('Description of ', R.response_desc)
    FROM avcrm.responsiblist R
  ORDER By 1 ;

-- -----------------------------------------------------
-- Table `contacts_has_sector`
-- -----------------------------------------------------
TRUNCATE contacts_has_sector;

INSERT INTO `contacts_has_sector` (`contacts_id`, `sector_id`)
    SELECT DISTINCT C.cnt_id, C.respons_id
    FROM avcrm.cntresponsibility C;

-- -----------------------------------------------------
-- Table `mail_lists`
-- -----------------------------------------------------
TRUNCATE mail_lists;

INSERT INTO `mail_lists` (`id`, `name`, `description`)
	SELECT  I.id, I.description,  CONCAT('Description of ', I.description)
    FROM avcrm.interests I;


-- -----------------------------------------------------
-- Table `contacts_has_mail_lists`
-- -----------------------------------------------------
TRUNCATE contacts_has_mail_lists;

INSERT INTO `contacts_has_mail_lists` (`contacts_id`, `mail_lists_id`)
  SELECT DISTINCT C.contact_id, C.interest_id
  FROM avcrm.contacts_interests C;



/*---------------------------------------------------------------------------
		Insert contacts_additional
	---------------------------------------------------------------------------*/

TRUNCATE contacts_additional;

INSERT INTO `contacts_additional` (`contacts_id`, `phone_number`, `phone_description`)
SELECT ad.cnt_id, ad.phone_num
		, If(ad.phone_num NOT LIKE '0%', 'Not a number'
									, CASE
											WHEN CAST( SUBSTRING(ad.phone_num, 2, 1) AS UNSIGNED)  = 0 THEN 'International'
											WHEN CAST( SUBSTRING(ad.phone_num, 2, 1) AS UNSIGNED)  BETWEEN 1 AND 5  THEN  'Landline'
											WHEN CAST( SUBSTRING(ad.phone_num, 2, 1) AS UNSIGNED)  BETWEEN 6 AND 8 THEN  'Cellphone'
											ELSE 'unknown'
										END) as 'Description'

FROM avcrm.addphone ad
WHERE ad.phone_num IS NOT NULL;


/*---------------------------------------------------------------------------
		Insert company_has_contacts
	---------------------------------------------------------------------------*/
TRUNCATE company_has_contacts;

INSERT INTO `company_has_contacts` (`company_id`, `contacts_id`, `relationship_type_id`)
SELECT DISTINCT Rel.company_id, Rel.contact_id, Rel.relationship_type_id
FROM (
      SELECT C.comp_id AS 'company_id'
           , C.id_kam1 AS 'contact_id'
           , 1         AS 'relationship_type_id'
					 , 1				 AS 'rank'
      FROM avcrm.tblcompanylist C
      WHERE IfNull(C.id_kam1, 0) <> 0
    UNION
      SELECT C.comp_id
           , C.id_kam2
           , 1
					 , 2
      FROM avcrm.tblcompanylist C
      WHERE IfNull(C.id_kam2, 0) <> 0
    UNION
      SELECT C.comp_id
           , C.id_dealer1
           , 2
					 , 3
      FROM avcrm.tblcompanylist C
      WHERE IfNull(C.id_dealer1, 0) <> 0
    UNION
      SELECT C.comp_id
           , C.id_dealer2
           , 2
					 , 4
      FROM avcrm.tblcompanylist C
      WHERE IfNull(C.id_dealer2, 0) <> 0
    ) AS Rel
	ORDER BY Rel.company_id, Rel.rank;




 /* ------------------------------------------------------------ */

SET FOREIGN_KEY_CHECKS = 1;
