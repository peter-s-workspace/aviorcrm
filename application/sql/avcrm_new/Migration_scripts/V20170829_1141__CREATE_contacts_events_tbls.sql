



USE `avcrm_new` ;


SET FOREIGN_KEY_CHECKS = 0;

-- -----------------------------------------------------
-- Table `note_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `note_type`;

CREATE TABLE IF NOT EXISTS `note_type`
	(
	  `Id` 				INT 						NOT NULL 	AUTO_INCREMENT,
	  `note_type` 	VARCHAR(128) 	NOT NULL,
	  UNIQUE INDEX `ContactType` (`note_type` ASC),

	  PRIMARY KEY (`Id`)
	)	ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contact_notes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `contact_notes`;

CREATE TABLE IF NOT EXISTS `contact_notes`
	(
	  `Id` 						INT 						NOT NULL 	AUTO_INCREMENT,
	  `note_txt` 				TEXT 					NOT NULL,
	  `note_date` 			TIMESTAMP 		NOT NULL 	DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Why are there 2 dates. Is it create date and sent date?',
	  `cntCompany` 		VARCHAR(100) 	NULL 			COMMENT 'This default should be the Contacts company, but contacts may change companies',
	  `note_type_Id` 		INT 						NOT NULL,
	  `contacts_id` 		INT 						NOT NULL,
	  `users_id` 				INT 						NOT NULL,

		PRIMARY KEY (`Id`),
		  INDEX `fk_contact_notes_contacts1_idx` (`contacts_id` ASC),
		  INDEX `fk_contact_notes_users1_idx` (`users_id` ASC),
		  INDEX `fk_contact_notes_note_type1_idx` (`note_type_Id` ASC),
		  CONSTRAINT `fk_contact_notes_contacts1`
			FOREIGN KEY (`contacts_id`)
			REFERENCES `avcrm2`.`contacts` (`id`)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		  CONSTRAINT `fk_contact_notes_users1`
			FOREIGN KEY (`users_id`)
			REFERENCES `avcrm2`.`users` (`id`)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		  CONSTRAINT `fk_contact_notes_note_type1`
			FOREIGN KEY (`note_type_Id`)
			REFERENCES `avcrm2`.`note_type` (`Id`)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
    )	ENGINE = InnoDB;

		-- -----------------------------------------------------
		-- Table `EventType`
		-- -----------------------------------------------------
		DROP TABLE IF EXISTS `crm_event_type`;

		CREATE TABLE IF NOT EXISTS `crm_event_type`
	(
	  `id` 				INT		 				NOT NULL 	AUTO_INCREMENT,
	  `name` 			VARCHAR(64) 	NULL DEFAULT NULL,
	  `description` 	VARCHAR(64) 	NULL DEFAULT NULL,

	  PRIMARY KEY (`id`)
	);

	-- -----------------------------------------------------
	-- Table `crm_event_logs`
	-- -----------------------------------------------------
	DROP TABLE IF EXISTS `crm_event_logs`;


	CREATE TABLE IF NOT EXISTS `crm_event_logs`
		(
		  `id` 								INT	UNSIGNED 	NOT NULL,
		  `user_id` 					INT 					NOT NULL,
		  `affected_user_id` 	INT 					NOT NULL,
			`user_type_id` 			INT 					NOT NULL,
		  `before_values` 		TEXT 					NULL,
		  `after_values` 			TEXT 					NULL,
		  `date_created` 			DATETIME 			NOT NULL,
		  `crm_event_type_id` INT		 				NOT NULL,

		  PRIMARY KEY (`id`),
		  INDEX `fk_crm_event_logs_crm_event_type1_idx` (`crm_event_type_id` ASC),
			INDEX `fk_crm_event_logs_user_type1_idx` (`user_type_id` ASC),
		  CONSTRAINT `fk_crm_event_logs_crm_event_type1`
				FOREIGN KEY (`crm_event_type_id`)
				REFERENCES `crm_event_type` (`id`)
				ON DELETE NO ACTION
				ON UPDATE NO ACTION,
			CONSTRAINT `fk_crm_event_logs_user_type1`
		    FOREIGN KEY (`user_type_id`)
		    REFERENCES `avcrm2`.`user_type` (`id`)
		    ON DELETE NO ACTION
		    ON UPDATE NO ACTION
	    )	ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `favourite_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `favourite_list`;

CREATE TABLE IF NOT EXISTS `favourite_list`
			(
			  `id` 									INT 						NOT NULL AUTO_INCREMENT,
			  `name` 								VARCHAR(40) 		NOT NULL,
			  `owner_user_id` 			INT 						NOT NULL,
			  `shared` 							TINYINT(1) 			NOT NULL DEFAULT 0,
			  `editable_by_others`	TINYINT(1) 			NOT NULL DEFAULT 0,
			  `description` 				VARCHAR(100) 		NULL,

			  PRIMARY KEY (`id`)
			);

	-- -----------------------------------------------------
	-- Table `user_favourite_list`
	-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_favourite_list`;


CREATE TABLE IF NOT EXISTS `user_favourite_list`
	(
	  `id` 									INT 	NOT NULL AUTO_INCREMENT,
	  `favourite_lists_id` 	INT 	NOT NULL,
	  `users_id` 						INT 	NOT NULL,
	  `contacts_id` 				INT 	NOT NULL,

	  PRIMARY KEY (`id`),
	  INDEX `fk_user_favourite_list_users1_idx` (`users_id` ASC),
	  INDEX `fk_user_favourite_list_contacts1_idx` (`contacts_id` ASC),
	  INDEX `fk_user_favourite_list_favourite_lists1_idx` (`favourite_lists_id` ASC),
	  CONSTRAINT `fk_user_favourite_list_users1`
		FOREIGN KEY (`users_id`)
		REFERENCES `users` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	  CONSTRAINT `fk_user_favourite_list_contacts1`
		FOREIGN KEY (`contacts_id`)
		REFERENCES `contact` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	  CONSTRAINT `fk_user_favourite_list_favourite_lists1`
		FOREIGN KEY (`favourite_lists_id`)
		REFERENCES `favourite_lists` (`id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
    );


SET FOREIGN_KEY_CHECKS = 1;
