


SELECT `cnt_listnames`.`listid`,
    `cnt_listnames`.`listname`,
    `cnt_listnames`.`ownerusername`,
    `cnt_listnames`.`shared`,
    `cnt_listnames`.`editablebyothers`
FROM `avcrm`.`cnt_listnames`
ORDER BY 1 ;


SELECT FL.id									`listid`,
				FL.name							`listname`,
                U.user_name					`ownerusername`,
                FL.shared						`shared`,
                FL.editable_by_others	`editablebyothers`
FROM favourite_list FL
LEFT JOIN Users U
				ON FL.owner_user_id = U.id
ORDER BY 1 ;




SELECT `tblfavourites`.`tblFav_id`,
    `tblfavourites`.`fav_Username`,
    `tblfavourites`.`tbl_cnt_Id`,
    `tblfavourites`.`tbl_cnt_Name_Desc`,
    `tblfavourites`.`tbl_Tiering`,
    `tblfavourites`.`fav_nick`,
    `tblfavourites`.`listid`
FROM `avcrm`.`tblfavourites`
ORDER BY 1;

SELECT UF.id					`tblFav_id`,
				U.user_name		`fav_Username`,
                UF.contacts_id	`tbl_cnt_Id`,
                C.C_Desc			`tbl_cnt_Name_Desc`,
   -- `tblfavourites`.`tbl_Tiering`,
  --  `tblfavourites`.`fav_nick`,
				UF.favourite_lists_id `listid`
FROM user_favourite_list UF
LEFT JOIN Users U
				ON UF.users_id = U.id
LEFT JOIN (
						SELECT C.id, concat(C.first_name, ' ', C.last_name) C_Desc
                        FROM contacts C
                    ) C
				ON UF.contacts_id = C.id
ORDER BY 1 ;


/*	-------------------------------------------------------------------------------
			
    -------------------------------------------------------------------------------*/
    
SELECT U.user_id, U.user_name
			-- , U.user_nick
			, F.tbl_cnt_id
            , CONCAT(C.first_name, ' ', C.surname)
            -- , F.tbl_cnt_Name_Desc
            , L.*
FROM `avcrm`.tbluser U
LEFT JOIN  `avcrm`.tblfavourites F
	ON U.user_name = F.fav_Username
LEFT JOIN `avcrm`.cnt_listnames L
	ON F.listid = L.listid
LEFT JOIN `avcrm`.contacts C
			ON C.id = F.tbl_cnt_id
ORDER BY U.user_id, F.tblFav_id;

SELECT U.id						'user_id'	
			, U.user_name		'user_name'
			, UF.contacts_id		'tbl_cnt_id'
            , CONCAT(C.first_name, ' ', C.last_name)
			, FL.id						'listid'
            , FL.name					'listname'
            , UO.user_name		'ownerusername'
            , FL.shared
            , FL.editable_by_others	'editablebyothers'
FROM users U
LEFT JOIN user_favourite_list UF
	ON U.id  = UF.users_id
LEFT JOIN favourite_list FL
	ON UF.favourite_lists_id = FL.id
LEFT JOIN users UO
			ON UO.id = UF.users_id
LEFT JOIN contacts C
			ON C.id = UF.contacts_id
ORDER BY U.id, FL.id;




