
SELECT *
FROM avcrm.tbluser U
WHERE U.user_id = 4
ORDER BY 1;

SELECT U.id  					`user_id`,
				U.user_name		`user_name`,
				U.password	    	`user_pword`,
				E.first_name		`user_fname`,
				E.last_name		`user_lname`,
                E.email				`user_email`,
				R.description		`user_level`,
                E.phone				`user_phone`,
                E.alias					`user_nick`,
                E.birthday			`user_bday`,
                'Mapp to favourites table'							`user_clist`,
                NULL					`user_coverage`,
                0							`popbox`,
                E.phone_ip			`user_phone_ip`,
				U.active				`user_status`
FROM  users U
JOIN users_roles UR
	ON UR.user_id = U.id
JOIN roles R
	ON R.id = UR.roles_id
JOIN user_type UT
	ON UT.id = U.user_type_id
JOIN employees E
	ON E.user_id = U.id
/*	favourites mapping o clist */
ORDER By 1 ;

