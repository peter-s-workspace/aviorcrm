

/*---------------------------------------------------------------------------
		Comparision queries
	---------------------------------------------------------------------------*/
    
-- 1. avcrm

SELECT `tblnotes`.`note_Id`,
				-- `tblnotes`.`note_by`,
				`tblnotes`.`notebyNick`,
				`tblnotes`.`note_cnt_Id`,
				`tblnotes`.`note_cnt_name`,
				`tblnotes`.`cntCompany`,
				`tblnotes`.`note_txt`,
				`tblnotes`.`note_type`,
				`tblnotes`.`note_date`,
				`tblnotes`.`Autodate`,
				`tblnotes`.`note_time`
FROM `avcrm`.`tblnotes`
ORDER BY 1 ;


-- 2. avcrm_new

SELECT CN.id 															`note_Id`
			, U.user_name												`note_by`						-- userName
           -- ,  `tblnotes`.`notebyNick`,
			, CN.contacts_id												`note_cnt_Id`
			, CONCAT(C.first_name, ' ', C.last_name)		`note_cnt_name`
			, CN.cntCompany											`cntCompany`
			, CN.note_txt													`note_txt`
			, NT.note_type												`note_type`
			, CN.note_date												`note_date`
			, CN.note_date												`Autodate`
			, CN.note_date												`note_time`
FROM contact_notes CN
JOIN note_type	NT
	ON CN.note_type_id = NT.id
LEFT JOIN contacts C
			 ON C.id = CN.contacts_id
LEFT JOIN users U
			ON CN.users_id = U.id
ORDER BY 1 ;
            





