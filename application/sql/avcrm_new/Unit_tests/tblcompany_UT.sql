
/*---------------------------------------------------------------------------
		Comparision queries
	---------------------------------------------------------------------------*/
    
-- 1. avcrm
SELECT `tblcompanylist`.`comp_id`,
    `tblcompanylist`.`id_companytier`,
 --    `tblcompanylist`.`id_geography`,
    `tblcompanylist`.`id_companystatus`,
    `tblcompanylist`.`id_companytype`,
    IF(id_kam1 < id_kam2, concat_ws(' | ',id_kam1, id_kam2)
										, concat_ws(' | ', id_kam2, id_kam1))					'id_kam',
	IF(id_dealer1 < id_dealer2, concat_ws(' | ', id_dealer1, id_dealer2)
											  , concat_ws(' | ', id_dealer2, id_dealer1))		'id_dealer',
    `tblcompanylist`.`Company`,
    `tblcompanylist`.`drm_status`,
    `tblcompanylist`.`company_rating`,
     NullIf(concat_ws(' | ', address_1, address_2, address_3, address_4), '')		'Address',
	 NullIf(concat_ws(' | ', switchboard_1, switchboard_2), '')								'Switchboard',
    
    `tblcompanylist`.`website`,
    `tblcompanylist`.`social_fb`,
    `tblcompanylist`.`social_twitter`,
    `tblcompanylist`.`social_linkedin`
   -- `tblcompanylist`.`modified_date`
FROM `avcrm`.`tblcompanylist`
-- WHERE comp_id IN (23) 
ORDER BY 1;

-- 2. avcrm_new
SELECT C.id 	`comp_id`,
				C.company_tier_id	`id_companytier`,
                
			   -- `tblcompanylist`.`id_geography`,
				C.company_status_id 	`id_companystatus`,
				C.company_type_id	`id_companytype`,
                ifnull(CM.id_kam, '')			`id_kam`,
                ifnull(CM.id_dealer, '')		`id_dealer`,
				-- `tblcompanylist`.`id_kam1`,
			--     `tblcompanylist`.`id_kam2`,
			--     `tblcompanylist`.`id_dealer1`,
			--     `tblcompanylist`.`id_dealer2`,
				C.company	`Company`,
				C.drm_status,
				CT.name					`company_rating`,
				CA.Address,
				M.Switchboard,
				M.website, 
				M.social_fb,
				M.social_twitter,
				M.social_linkedin
			--	''										AS `modified_date`
FROM company C
JOIN company_tier CT
	ON C.company_tier_id = CT.id
LEFT JOIN (
						SELECT company_id
									, group_concat(CA.address ORDER BY CA.id -- 
																				 SEPARATOR ' | ') 	AS Address
						   FROM company_address CA
						GROUP BY CA.company_id
					) CA
				ON C.id = CA.company_id
LEFT JOIN (
						SELECT M.company_id
								, group_concat(CASE WHEN M.communication_type_id = 1 THEN M.coms_value END
															SEPARATOR ' | ')																						AS Switchboard
								, MAX(CASE WHEN M.communication_type_id = 2 THEN M.coms_value END)					AS website
								, MAX(CASE WHEN M.communication_type_id = 3 THEN M.coms_value END)					AS social_fb
								, MAX(CASE WHEN M.communication_type_id = 4 THEN M.coms_value END)					AS social_twitter
								, MAX(CASE WHEN M.communication_type_id = 5 THEN M.coms_value END)					AS social_linkedin
						  FROM communication M
						GROUP BY M.company_id
					) M
				ON  C.id = M.company_id
/*----						Join on contacts for KAM and dealers 						------*/
LEFT JOIN (
						SELECT H.company_id
									, group_concat(CASE WHEN H.relationship_type_id = 1 THEN H.contacts_id END
																ORDER BY H.contacts_id SEPARATOR ' | ')																				AS 'id_kam'
									,  group_concat(CASE WHEN H.relationship_type_id = 2 THEN H.contacts_id END
																ORDER BY H.contacts_id SEPARATOR ' | ')																				AS 'id_dealer'
						FROM company_has_contacts H
						GROUP BY H.company_id 
					) CM
				ON C.id = CM.company_id
-- WHERE C.id IN (23)
ORDER BY 1;


/*---------------------------------------------------------------------------
		Debugging
	---------------------------------------------------------------------------*/
    


/*
SELECT *
FROM `avcrm`.tblcompanylist TC
WHERE TC.comp_id = 81;

SELECT CA.*
FROM company_address CA
WHERE CA.company_id = 81;

SELECT *
FROM company C
WHERE C.id = 4;

SELECT * FROM communication_type


SELECT DISTINCT TC.company_rating
FROM `avcrm`.tblcompanylist TC;         

SELECT DISTINCT TC.CompanyTier
FROM `avcrm`.tblcompanylist TC;     

    
SELECT TC.comp_id, TC.Company, TC.id_companytier, TC.CompanyTier
			, TC.company_rating, T.companytier
FROM `avcrm`.tblcompanylist TC
LEFT JOIN `avcrm`.tblcompanytier T
			ON TC.id_companytier = T.id_companytier
WHERE TC.id_companytier <> TC.CompanyTier
		--  TC.company_rating <> T.companytier;
       
*/


                        