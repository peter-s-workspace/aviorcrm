

/*---------------------------------------------------------------------------
		Comparision queries
	---------------------------------------------------------------------------*/
    
-- 1. avcrm
SELECT `contacts`.`id`,
				`contacts`.`first_name`,
				`contacts`.`surname`,
				`contacts`.`landline`,
				`contacts`.`fax`,
				`contacts`.`mobile`,
				`contacts`.`email`,
				`contacts`.`company_id`,
				`contacts`.`jobtitle`,
				`contacts`.`bloomberg_user`,
				CAST(`contacts`.`bloomberg_status` AS UNSIGNED)	`bloomberg_status`,
				`contacts`.`note`,
			--	`contacts`.`favourite`,
				`contacts`.`dob`,
				`contacts`.`active`,
				`contacts`.`drm_status`
                 , AP.phone_num
                 , CI.interest_id
                 , CI.description
                 , R.respons_id
                 , R.respons_desc
FROM `avcrm`.contacts
LEFT JOIN `avcrm`.addphone AP
			 ON `contacts`.`id` = AP.cnt_id
LEFT JOIN (
						SELECT CI.contact_id
								, group_concat(DISTINCT CI.interest_id ORDER BY CI.interest_id
																				SEPARATOR ' | ') 	AS interest_id
								, group_concat(DISTINCT I.description ORDER BY I.id
																				SEPARATOR ' | ') 	AS description
						FROM `avcrm`.contacts_interests CI
						JOIN `avcrm`.interests I
							ON CI.interest_id = I.id
						GROUP BY CI.contact_id
					) CI
				ON `avcrm`.contacts.id = CI.contact_id
LEFT JOIN (
						SELECT CR.cnt_id
										, group_concat(DISTINCT  R.respons_id ORDER BY R.respons_id
																						SEPARATOR ' | ') 				AS respons_id
										, group_concat(DISTINCT R.response_desc ORDER BY R.respons_id
																						SEPARATOR ' | ') 				AS respons_desc
							FROM  `avcrm`.cntresponsibility CR
							JOIN  `avcrm`.responsiblist R
								ON CR.respons_id = R.respons_id
							GROUP BY CR.cnt_id
					)	R
				ON `avcrm`.contacts.id = R.cnt_id
-- WHERE `avcrm`.contacts.id = 75
ORDER BY 1;


-- 2. avcrm_newAVR

SELECT C.`id`,
				C.`first_name`,
				C.last_name											`surname`,
				C.`landline`,
				C.`fax`,
				C.`mobile`,
				C.`email`,
				C.`company_id`,
				C.job_title												`jobtitle`,
				C.`bloomberg_user`,
				C.`bloomberg_status`,
				N.note_txt												`note`,
				C.birthday												`dob`,
				CAST(C.active AS UNSIGNED)				`active`, 
				CAST(C.drm_status AS UNSIGNED)		`drm_status` 
                , CA.phone_number								`phone_num`
                , ML.interest_id
                , ML.description
                , S.respons_id
                , S.respons_desc
                -- INTO OUTFILE '/Users/brendonpotgieter/development/aviorcrm/application/sql/avcrm_new/Unit_tests/contacts_avcrm_new.csv'
FROM   contacts C
LEFT JOIN contacts_additional CA
			ON C.id = CA.contacts_id
LEFT JOIN (
						SELECT CH.contacts_id
										, group_concat(DISTINCT CH.mail_lists_id ORDER BY CH.mail_lists_id 
																						SEPARATOR ' | ') 	AS interest_id
										, group_concat(DISTINCT ML.name ORDER BY ML.id
																						SEPARATOR ' | ') 	AS description
							FROM contacts_has_mail_lists CH
							JOIN mail_lists ML
								ON CH.mail_lists_id = ML.id
							GROUP BY CH.contacts_id
                    ) ML
				ON C.id = ML.contacts_id
LEFT JOIN (
						SELECT CS.contacts_id
										, group_concat(DISTINCT S.id ORDER BY S.id
																						SEPARATOR ' | ') 				AS respons_id
										, group_concat(DISTINCT S.name ORDER BY S.id
																						SEPARATOR ' | ') 				AS respons_desc
							FROM  contacts_has_sector CS
							JOIN  sector S
								ON CS.sector_id = S.id
							GROUP BY CS.contacts_id 
					) S
				ON C.id = S.contacts_id
LEFT JOIN (
						SELECT CN.contacts_id, CN.note_txt
						FROM contact_notes CN
                        WHERE CN.note_type_Id = 1
					) N
				ON C.id = N.contacts_id
-- WHERE C.id = 75
ORDER BY 1 ;





/*---------------------------------------------------------------------------
		Debugging
	---------------------------------------------------------------------------*/