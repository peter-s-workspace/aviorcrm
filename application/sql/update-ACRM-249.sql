insert into `avcrm_cf`.`config` (`key`, `value`) values ('title', 'CRM - Corporate Finance');



-- CONTACT deletions

-- Delete addphone
DELETE FROM `avcrm_cf`.`addphone` where cnt_id in ( SELECT id FROM `avcrm_cf`.`contacts` WHERE `contacts`.`company_id` IN (SELECT `comp_id` FROM `avcrm_cf`.`tblcompanylist` WHERE id_companytype != 9));

-- Delete notes
DELETE FROM `avcrm_cf`.`tblnotes` where note_cnt_Id in ( SELECT id FROM `avcrm_cf`.`contacts` WHERE `contacts`.`company_id` NOT IN (SELECT `comp_id` FROM `avcrm_cf`.`tblcompanylist` WHERE id_companytype = 9));

-- Delete interests
DELETE FROM `avcrm_cf`.`contacts_interests` where contact_id in ( SELECT id FROM `avcrm_cf`.`contacts` WHERE `contacts`.`company_id` NOT IN (SELECT `comp_id` FROM `avcrm_cf`.`tblcompanylist` WHERE id_companytype = 9));

-- Delete responsibilities
DELETE FROM `avcrm_cf`.`cntresponsibility` where cnt_id in ( SELECT id FROM `avcrm_cf`.`contacts` WHERE `contacts`.`company_id` NOT IN (SELECT `comp_id` FROM `avcrm_cf`.`tblcompanylist` WHERE id_companytype = 9));

-- Delete sms lists
DELETE FROM `avcrm_cf`.`smslists` where contactid in ( SELECT id FROM `avcrm_cf`.`contacts` WHERE `contacts`.`company_id` NOT IN (SELECT `comp_id` FROM `avcrm_cf`.`tblcompanylist` WHERE id_companytype = 9));

-- Delete favourites
DELETE FROM `avcrm_cf`.`tblfavourites` where tbl_cnt_Id in ( SELECT id FROM `avcrm_cf`.`contacts` WHERE `contacts`.`company_id` NOT IN (SELECT `comp_id` FROM `avcrm_cf`.`tblcompanylist` WHERE id_companytype = 9));

-- Delete CONTACTS first to avoid constraints on Company
DELETE FROM`avcrm_cf`.`contacts` WHERE `contacts`.`company_id` NOT IN (SELECT `comp_id` FROM `avcrm_cf`.`tblcompanylist` WHERE id_companytype = 9);



-- COMPANY deletions
-- Delete Company
DELETE FROM `avcrm_cf`.`tblcompanylist` WHERE id_companytype != 9;


-- LOGS
-- Clear event logs
truncate table `avcrm_cf`.`tblevent_logs`;


-- delete FILES...
-- ....

# Sure we just need to change the files path???


# tblCalendar has one entry and doesnt seem to be used by much




