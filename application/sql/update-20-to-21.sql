/* Used to delete companies from the database */

/* Company Name : South Easter Fund Management */

/* Delete from sms list */
DELETE FROM smslists WHERE smslists.contactid IN (
    SELECT id FROM contacts WHERE contacts.company_id = (SELECT comp_id FROM tblcompanylist WHERE tblcompanylist.Company = 'South Easter Fund Management' LIMIT 0,1)
);

/* Delete from contact interests */
DELETE FROM contacts_interests WHERE contacts_interests.contact_id IN (
    SELECT id FROM contacts WHERE contacts.company_id = (SELECT comp_id FROM tblcompanylist WHERE tblcompanylist.Company = 'South Easter Fund Management' LIMIT 0,1)
);

/* Delete from contacts */
DELETE FROM contacts WHERE contacts.company_id = (SELECT comp_id FROM tblcompanylist WHERE tblcompanylist.Company = 'South Easter Fund Management' LIMIT 0,1);

/* Delete from notes*/
DELETE FROM tblnotes WHERE tblnotes.cntCompany = 'South Easter Fund Management';

/* Delete from company*/
DELETE FROM tblcompanylist WHERE tblcompanylist.Company = 'South Easter Fund Management';


/* Company Name : Threadneedle Investments UK*/

DELETE FROM smslists WHERE smslists.contactid IN (
    SELECT id FROM contacts WHERE contacts.company_id = (SELECT comp_id FROM tblcompanylist WHERE tblcompanylist.Company = 'Threadneedle Investments UK' LIMIT 0,1)
);

/* Delete from contact interests */
DELETE FROM contacts_interests WHERE contacts_interests.contact_id IN (
    SELECT id FROM contacts WHERE contacts.company_id = (SELECT comp_id FROM tblcompanylist WHERE tblcompanylist.Company = 'Threadneedle Investments UK' LIMIT 0,1)
);

/* Delete from contacts */
DELETE FROM contacts WHERE contacts.company_id = (SELECT comp_id FROM tblcompanylist WHERE tblcompanylist.Company = 'Threadneedle Investments UK' LIMIT 0,1);

/* Delete from notes*/
DELETE FROM tblnotes WHERE tblnotes.cntCompany = 'Threadneedle Investments UK';

/* Delete from company*/
DELETE FROM tblcompanylist WHERE tblcompanylist.Company = 'Threadneedle Investments UK';

/* Company : Capital Property Fund Ltd */

DELETE FROM smslists WHERE smslists.contactid IN (
    SELECT id FROM contacts WHERE contacts.company_id = (SELECT comp_id FROM tblcompanylist WHERE tblcompanylist.Company = 'Capital Property Fund Ltd' LIMIT 0,1)
);

/* Delete from contact interests */
DELETE FROM contacts_interests WHERE contacts_interests.contact_id IN (
    SELECT id FROM contacts WHERE contacts.company_id = (SELECT comp_id FROM tblcompanylist WHERE tblcompanylist.Company = 'Capital Property Fund Ltd' LIMIT 0,1)
);

/* Delete from contacts */
DELETE FROM contacts WHERE contacts.company_id = (SELECT comp_id FROM tblcompanylist WHERE tblcompanylist.Company = 'Capital Property Fund Ltd' LIMIT 0,1);

/* Delete from notes*/
DELETE FROM tblnotes WHERE tblnotes.cntCompany = 'Capital Property Fund Ltd';

/* Delete from company*/
DELETE FROM tblcompanylist WHERE tblcompanylist.Company = 'Capital Property Fund Ltd';
