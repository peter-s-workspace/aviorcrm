/* Create config table */
CREATE TABLE `config`(
	`key` varchar(64) COLLATE latin1_swedish_ci NULL  , 
	`value` varchar(64) COLLATE latin1_swedish_ci NULL  , 
	UNIQUE KEY `unique_key`(`key`) 
) ENGINE=MyISAM DEFAULT CHARSET='latin1';

/*Platform has been setup as 'development' on my machine. This can be changed to staging or production, depending on the enviromnment. Please add other details where NULL*/
insert  into `config`(`key`,`value`) values ('platform','staging'),('smtp_host',NULL),('smtp_port',NULL),('smtp_username',NULL),('smpt_password',NULL),('email_from',NULL);


/*Update version */
-- INSERT INTO version VALUES ('15', '1', now());