UPDATE `avcrm`.`config`
SET `value` = (SELECT CASE when `key` 		= 'read_room_key' THEN '8izhw3nc7HQmt9hevKHnxkbZoMEcYkOTlLyHsMCk'
				    	   when  `key` 	= 'send_message_key'  THEN 'kWyTqzKoTSvBCar9gxHDRGwSfSHUEZqDxy7oz016'
				    	   when `key` 		= 'add_room_key'  THEN 'jrtOGiPjuJEKr3J83mIW3vBSahNdd0xKCxLOltSv'
               END)
WHERE `key` in ('read_room_key', 'send_message_key', 'add_room_key');