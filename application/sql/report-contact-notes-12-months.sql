/* contact notes 12 months */
select 
note_id,
user_fname, user_lname,
notebyNick,
note_cnt_name,
cntCompany,
note_type,
note_txt,
note_date,
note_time
from 
tblnotes,
tbluser
where tbluser.user_name = tblnotes.note_by  
and note_date > '2016-06-30' and note_date < '2017-07-01' 
order by note_date desc;

/* Contact notes for select users and date range */
select 
note_id,
user_fname, user_lname,
notebyNick,
note_cnt_name,
cntCompany,
note_type,
note_txt,
note_date,
note_time
from 
tblnotes,
tbluser
where tbluser.user_name = tblnotes.note_by
and tbluser.user_id in
(
79,
80,
82,
93,
97,
122,
145
)
and note_date >= '2017-06-01' and note_date < '2017-07-12' 
order by note_date desc;

/* contact notes for select contact and dates */
select 
note_id,
user_fname, user_lname,
notebyNick,
note_cnt_name,
cntCompany,
note_type,
note_txt,
note_date,
note_time
from 
tblnotes,
tbluser
where note_cnt_name like '%Narsingh'
and note_date > '2012-01-01' and note_date < '2017-08-25' 
order by note_date desc;


/*contact notes for select company and date range */
select 
note_id,
user_fname, user_lname,
notebyNick,
note_cnt_name,
cntCompany,
note_type,
note_txt,
note_date,
note_time
from 
tblnotes,
tbluser
where tbluser.user_name = tblnotes.note_by  
and note_date > '2016-08-24' and note_date < '2017-08-25'
and cntCompany like 'Investec%Asset%'
order by note_date desc;

