
ALTER TABLE `tblcnt` CHANGE `cnt_bloomberg_status` `cnt_bloomberg_status` CHAR(1)  COLLATE utf8_general_ci NULL AFTER `cnt_blmuser`;

/* Add new */
INSERT INTO responsiblist (response_desc) VALUES ('Beverages');
INSERT INTO responsiblist (response_desc) VALUES ('Economics');
INSERT INTO responsiblist (response_desc) VALUES ('Education');
INSERT INTO responsiblist (response_desc) VALUES ('Fixed Income');
INSERT INTO responsiblist (response_desc) VALUES ('FMCG');
INSERT INTO responsiblist (response_desc) VALUES ('Food Producers');
INSERT INTO responsiblist (response_desc) VALUES ('Investment Strategy');
INSERT INTO responsiblist (response_desc) VALUES ('Luxury Goods');
INSERT INTO responsiblist (response_desc) VALUES ('Telecoms');
INSERT INTO responsiblist (response_desc) VALUES ('Tobacco');

/* Rename */
UPDATE responsiblist SET response_desc = 'Oil &amp; Chemicals' WHERE response_desc = 'Chemicals';
UPDATE responsiblist SET response_desc = 'Hotels &amp; Casinos' WHERE response_desc = 'Gaming and Leisure';
UPDATE responsiblist SET response_desc = 'Investment Strategy' WHERE response_desc = 'Strategy';


/* Combine Gold and Platinum into Diversified Miners*/
/* Add Diversified Miners*/
INSERT INTO responsiblist (response_desc) VALUES ('Diversified Miners');
/* Update Client Reponsibility List with new response ID */
UPDATE cntresponsibility SET cntresponsibility.respons_id = (SELECT respons_id FROM responsiblist WHERE responsiblist.response_desc = 'Diversified Miners' LIMIT 0,1) WHERE
cntresponsibility.respons_id IN ( SELECT responsiblist.respons_id FROM responsiblist WHERE responsiblist.response_desc IN ('Gold','Platinum'));

/* Remove Gold and Platinum */
DELETE FROM responsiblist WHERE responsiblist.response_desc IN('Gold','Platinum');

/*Update version */
INSERT INTO version VALUES ('11', '1', now());