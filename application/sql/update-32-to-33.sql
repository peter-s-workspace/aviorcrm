/* These are the latest credentials for the mitel config*/
INSERT INTO `avcrm`.`config`
(`key`,
`value`)
VALUES(
'mitel_server',
'196.50.239.156')
,('mitel_app_name','ACM_CRM_Integration')
,('mitel_company_name','Avior Capital Markets Pty (Ltd)')
,('mitel_app_password','AviorCRM09091')
,('mitel_local_password','AviorCRM09091')
,('mitel_database_id','3')
,('mitel_icp_address','192.168.105.2')
,('java_path', 'java -cp OIGJavaCallControlSample.jar oigjavacallcontrolsample');

ALTER TABLE avcrm.contacts ADD (mitel_full_name VARCHAR(200) NULL, mitel_phone_number VARCHAR(11) NULL);

UPDATE avcrm.contacts SET landline = REPLACE(landline,'+','00') WHERE id IN(4563,4604,4772,4773,4780,4781,4782,4784,4813);

ALTER TABLE avcrm.tbluser ADD user_extension INT(11) NULL  AFTER user_status;

UPDATE avcrm.tbluser
SET user_extension = (CASE user_email WHEN "alex@avior.co.za" 		THEN 2862
									  WHEN "jennifer@avior.co.za" 	THEN 2865	
									  WHEN "peter@avior.co.za" 		THEN 2865
									  WHEN "jacques@avior.co.za" 	THEN 2867	
									  WHEN "claudio@avior.co.za" 	THEN 2868
									  WHEN "vijay@avior.co.za" 		THEN 2869
									  WHEN "irvin@avior.co.za" 		THEN 2872
									  WHEN "earl@avior.co.za" 		THEN 2873	
									  WHEN "carina@avior.co.za" 	THEN 2874
									  WHEN "neels@avior.co.za" 		THEN 2875
									  WHEN "samantha@avior.co.za" 	THEN 2876	
									  WHEN "cvs@avior.co.za" 		THEN 2878
									  WHEN "clintonl@avior.co.za" 	THEN 2880
									  WHEN "brendon@avior.co.za" 	THEN 2882
									  WHEN "bongi@avior.co.za" 		THEN 2883
									  WHEN "craig@avior.co.za" 		THEN 2888
									  WHEN "deborah@avior.co.za" 	THEN 2889
									  WHEN "genee@avior.co.za" 		THEN 2892
									  WHEN "yolandi@avior.co.za" 	THEN 2895
									  WHEN "odirile@avior.co.za" 	THEN 2897
									  WHEN "ashen@avior.co.za" 		THEN 2898
									  WHEN "davidt@avior.co.za"		THEN 2961
									  WHEN "boitshepo@avior.co.za"	THEN 2962
									  WHEN "wade@avior.co.za" 		THEN 2966
									  WHEN "naeem@avior.co.za" 		THEN 2968
									  WHEN "matthews@avior.co.za"	THEN 2970
									  WHEN "richard@avior.co.za" 	THEN 2973
									  WHEN "karen@avior.co.za"		THEN 2974
									  WHEN "traders@avior.co.za"	THEN 2976
									  WHEN "atiyyah@avior.co.za"	THEN 2977
									  WHEN "harryp@avior.co.za"		THEN 2980
									  WHEN "justin@avior.co.za"		THEN 2988
									  WHEN "tiyani@avior.co.za"		THEN 2990
									  WHEN "kearney@avior.co.za"	THEN 2992
									  WHEN "stephan@avior.co.za"	THEN 2996
									  WHEN "vicky@avior.co.za" 		THEN 2998
					END)	
WHERE user_email IN ('alex@avior.co.za',				
					'jennifer@avior.co.za',					
					'peter@avior.co.za',					
					'jacques@avior.co.za',					
					'claudio@avior.co.za',					
					'vijay@avior.co.za',					
					'irvin@avior.co.za',					
					'earl@avior.co.za',					
					'carina@avior.co.za',					
					'neels@avior.co.za',					
					'samantha@avior.co.za',					
 					'cvs@avior.co.za',					
 					'clintonl@avior.co.za',					
					'brendon@avior.co.za',					
 					'bongi@avior.co.za',								
 					'craig@avior.co.za',					
					'deborah@avior.co.za',					
					'genee@avior.co.za',					
					'yolandi@avior.co.za',					
					'odirile@avior.co.za',					
 					'ashen@avior.co.za',					
					'davidt@avior.co.za',					
					'boitshepo@avior.co.za',					
					'wade@avior.co.za',					
					'naeem@avior.co.za',					
					'matthews@avior.co.za',					
					'richard@avior.co.za',					
					'karen@avior.co.za',					
					'traders@avior.co.za',					
					'atiyyah@avior.co.za',					
 					'harryp@avior.co.za',					
 					'justin@avior.co.za',					
 					'tiyani@avior.co.za',					
					'kearney@avior.co.za',					
					'stephan@avior.co.za',					
					'vicky@avior.co.za');			
					