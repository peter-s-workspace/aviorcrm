-- Alter current contact/interest table
-- Will drop the 'cnt_interest' column only once the correct foreign keys have been added to the 'interest_id' column
ALTER TABLE `avcrm`.`tblcntinterests` DROP COLUMN `cnt_mailadd` , CHANGE COLUMN `cnt_Int_id` `id` INT(11) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `cnt_Id` `contact_id` INT(11) NOT NULL , RENAME TO  `avcrm`.`contacts_interests` ;

-- Create new Interest table
CREATE  TABLE `avcrm`.`interests` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `description` VARCHAR(64) NOT NULL ,
  PRIMARY KEY (`id`) );

-- Alter contacts_interests again to add foreign key constraint
ALTER TABLE `avcrm`.`contacts_interests` ADD COLUMN `interest_id` INT AFTER `contact_id`, ADD CONSTRAINT `fk_interest` FOREIGN KEY (`interest_id`) REFERENCES `avcrm`.`interests` (`id`);

-- Populate new Interest table using distinct values from 'cnt_interest' in contact_interest table
INSERT INTO `avcrm`.`interests`(`description`) SELECT DISTINCT cnt_interest FROM `avcrm`.`contacts_interests`;

-- Update contacts_interst to include the new foreign id's
UPDATE `avcrm`.`contacts_interests` as ContactsInterests SET interest_id = (select id from `avcrm`.`interests` as Interests where Interests.description = ContactsInterests.cnt_interest);

-- Drop the cnt_interest column
ALTER TABLE `avcrm`.`contacts_interests` DROP COLUMN `cnt_interest`;

-- Alter and rename tblcnt table
ALTER TABLE `avcrm`.`tblcnt` DROP COLUMN `cnt_interest` , CHANGE COLUMN `cnt_Id` `id` INT(11) NOT NULL AUTO_INCREMENT  , CHANGE COLUMN `cnt_Fname` `first_name` VARCHAR(128) NOT NULL  , CHANGE COLUMN `cnt_Lastname` `surname` VARCHAR(128) NOT NULL  , CHANGE COLUMN `cnt_phone` `landline` VARCHAR(128) NULL DEFAULT NULL  , CHANGE COLUMN `cnt_fax` `fax` VARCHAR(128) NULL DEFAULT NULL  , CHANGE COLUMN `cnt_cell` `mobile` VARCHAR(128) NULL DEFAULT NULL  , CHANGE COLUMN `cnt_email` `email` VARCHAR(256) NOT NULL  , CHANGE COLUMN `cnt_company` `company` VARCHAR(128) NOT NULL  , CHANGE COLUMN `cnt_jobtitle` `jobtitle` VARCHAR(128) NULL DEFAULT NULL  , CHANGE COLUMN `cnt_blmuser` `bloomberg_user` VARCHAR(128) NULL DEFAULT NULL  , CHANGE COLUMN `cnt_bloomberg_status` `bloomberg_status` CHAR(1) NULL DEFAULT NULL  , CHANGE COLUMN `cnt_note` `note` TEXT NULL DEFAULT NULL  , CHANGE COLUMN `cnt_Favourite` `favourite` TEXT NULL DEFAULT NULL  , CHANGE COLUMN `cnt_bdate` `dob` DATE NULL DEFAULT NULL  , CHANGE COLUMN `cnt_active` `active` INT(11) NOT NULL DEFAULT '0'  , RENAME TO  `avcrm`.`contacts` ;

-- Change 'active' values 0 to 1 and 1 to 0. (5 Queries)
-- Add temporary active column
ALTER TABLE `avcrm`.`contacts` ADD COLUMN `temp_active` INT(11) NOT NULL  AFTER `active` ;
-- Copy values from 'active' to 'temp_active'
UPDATE `avcrm`.`contacts` contacts , `avcrm`.`contacts` contacts2 SET contacts.temp_active = contacts2.active where contacts.id = contacts2.id;
-- Change 'active' values using 'temp_active' values
UPDATE `avcrm`.`contacts` SET `active` = 1 WHERE `temp_active` = 0;
UPDATE `avcrm`.`contacts` SET `active` = 0 WHERE `temp_active` = 1;
-- Drop 'temp_active' column
ALTER TABLE `avcrm`.`contacts` DROP COLUMN `temp_active`;





