/*
SQLyog Ultimate v8.55 
MySQL - 5.1.33-community : Database - avcrm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `tblevent_logs` */

DROP TABLE IF EXISTS `tblevent_logs`;

CREATE TABLE `tblevent_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `user` varchar(64) DEFAULT NULL,
  `affected_user` varchar(64) DEFAULT NULL,
  `affected_usertype` varchar(64) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tblevent_logs` */

/*Table structure for table `tblevents` */

DROP TABLE IF EXISTS `tblevents`;

CREATE TABLE `tblevents` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_alias` varchar(64) DEFAULT NULL,
  `event_description` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `tblevents` */

insert  into `tblevents`(`event_id`,`event_alias`,`event_description`) values (1,'ADD_USER','Added User'),(2,'UPDATE_USER','Updated User'),(3,'DELETE_USER','Deleted User'),(4,'UPDATE_USER_PWD','Reset User Password'),(5,'ADD_CLIENT','Added Client'),(6,'UPDATE_CLIENT_DETAILS','Updated Client Details'),(7,'UPDATE_CLIENT_INTERESTS','Updated mail address in mailing list'),(8,'UPDATE_CLIENT_PIC','Updated Client picture'),(9,'DELETE_CLIENT','Deleted Client'),(10,'REMOVE_CLIENT_MAILINGLIST','Removed Client from mailing list'),(11,'ADD_CLIENT_TO_MAILINGLIST','Added Client to mailing list'),(12,'ADD_CLIENT_RESPONSIBILITY','Added Client Responsibility'),(13,'REMOVE_CLIENT_RESPONSIBILITY','Removed Client Responsibility'),(14,'UPDATE_CLIENT_STATUS','Updated Client Active Status');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
