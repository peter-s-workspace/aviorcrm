INSERT INTO tblcontacttype (`ContactType`) VALUES ('Lunch');
/* Alter table tblfavourites */
ALTER TABLE `tblfavourites` 
	ADD KEY `tbl_cnt_Id`(`tbl_cnt_Id`), COMMENT='';
	

/* Create table - tblcompanystatus */
CREATE TABLE `tblcompanystatus`(
	`id_companystatus` int(11) NOT NULL  auto_increment , 
	`company_status` varchar(64) COLLATE latin1_swedish_ci NULL  , 
	`cdate` datetime NULL  , 
	`mdate` datetime NULL  , 
	PRIMARY KEY (`id_companystatus`) 
) ENGINE=MyISAM DEFAULT CHARSET='latin1';

/* Add data - tblcompanystatus */
insert  into `tblcompanystatus`(`id_companystatus`,`company_status`,`cdate`,`mdate`) values (1,'Active ','2014-07-14 11:31:52',NULL),(2,'Dormant','2014-07-14 11:31:52',NULL),(3,'Prospect','2014-07-14 11:31:52',NULL),(4,'Cold Prospect','2014-07-14 11:31:52',NULL);

/* Create table - tblcompanytier */
CREATE TABLE `tblcompanytier`(
	`id_companytier` int(11) NOT NULL  auto_increment , 
	`companytier` varchar(64) COLLATE latin1_swedish_ci NULL  , 
	`companytier_status` char(1) COLLATE latin1_swedish_ci NULL  , 
	`cdate` datetime NULL  , 
	`mdate` datetime NULL  , 
	PRIMARY KEY (`id_companytier`) 
) ENGINE=MyISAM DEFAULT CHARSET='latin1';

/* Add data - tblcompanytier */
insert  into `tblcompanytier`(`id_companytier`,`companytier`,`companytier_status`,`cdate`,`mdate`) values (1,'Platinum','1','2014-07-15 10:33:31',NULL),(2,'Gold','1','2014-07-15 10:33:31',NULL),(3,'Silver','1','2014-07-15 10:33:31',NULL),(4,'Not Assigned','1','2014-07-15 10:33:31',NULL);



/* Create table - tblcompanytype */
CREATE TABLE `tblcompanytype`(
	`id_companytype` int(11) NOT NULL  auto_increment , 
	`companytype` varchar(64) COLLATE latin1_swedish_ci NULL  , 
	`companytype_status` char(1) COLLATE latin1_swedish_ci NULL  , 
	`cdate` datetime NULL  , 
	`mdate` datetime NULL  , 
	PRIMARY KEY (`id_companytype`) 
) ENGINE=MyISAM DEFAULT CHARSET='latin1';

/* Add data - tblcompanytype */
insert  into `tblcompanytype`(`id_companytype`,`companytype`,`companytype_status`,`cdate`,`mdate`) values (1,'Long Only','1','2014-07-14 11:36:13',NULL),(2,'Broker','1','2014-07-14 11:36:13',NULL),(3,'Hedge Fund','1','2014-07-14 11:36:13',NULL),(4,'Private Client manager','1','2014-07-14 11:36:13',NULL),(5,'Multi Manager','1','2014-07-14 11:36:13',NULL),(6,'Controlled Client','1','2014-07-14 11:36:13',NULL);

/* Alter table  - tblcompanylist */
ALTER TABLE `tblcompanylist` 
	ADD COLUMN `id_companytier` INT(11)   NULL AFTER `comp_id`, 
	ADD COLUMN `id_companystatus` INT(11)   NULL AFTER `id_companytier`, 
	ADD COLUMN `id_companytype` INT(11)   NULL AFTER `id_companystatus`, 
	ADD COLUMN `id_kam1` INT(11)   NULL AFTER `id_companytype`, 
	ADD COLUMN `id_kam2` INT(11)   NULL AFTER `id_kam1`, 
	ADD COLUMN `id_dealer1` INT(11)   NULL AFTER `id_kam2`, 
	ADD COLUMN `id_dealer2` INT(11)   NULL AFTER `id_dealer1`, 
	CHANGE `Company` `Company` VARCHAR(128)  COLLATE utf8_general_ci NOT NULL AFTER `id_dealer2`, 
	CHANGE `CompanyTier` `CompanyTier` INT(11)   NOT NULL DEFAULT '1' AFTER `Company`, 
	CHANGE `company_rating` `company_rating` VARCHAR(45)  COLLATE utf8_general_ci NULL DEFAULT 'Not Assigned' AFTER `CompanyTier`, 
	ADD COLUMN `address_1` VARCHAR(255)  COLLATE utf8_general_ci NULL AFTER `company_rating`, 
	ADD COLUMN `address_2` VARCHAR(255)  COLLATE utf8_general_ci NULL AFTER `address_1`, 
	ADD COLUMN `website` VARCHAR(255)  COLLATE utf8_general_ci NULL AFTER `address_2`, 
	ADD COLUMN `modified_date` DATETIME   NULL AFTER `website`, 
	ADD KEY `CompanyTier`(`CompanyTier`), 
	ADD KEY `id_tier_index`(`id_companytier`), COMMENT='';

/* Update new columns - tblcompanylist */	
UPDATE tblcompanylist SET tblcompanylist.id_companytier = (SELECT tblcompanytier.id_companytier FROM tblcompanytier WHERE tblcompanytier.companytier = TRIM(tblcompanylist.company_rating));
UPDATE tblcompanylist SET tblcompanylist.id_companytier = (SELECT tblcompanytier.id_companytier FROM tblcompanytier WHERE tblcompanytier.companytier = 'Not Assigned') WHERE tblcompanylist.id_companytier IS NULL;

UPDATE tblcompanylist SET tblcompanylist.id_companystatus = (SELECT tblcompanystatus.id_companystatus FROM tblcompanystatus WHERE tblcompanystatus.company_status = 'Dormant') WHERE tblcompanylist.company_rating = 'Inactive';
UPDATE tblcompanylist SET tblcompanylist.id_companystatus = (SELECT tblcompanystatus.id_companystatus FROM tblcompanystatus WHERE tblcompanystatus.company_status = 'Active') WHERE tblcompanylist.id_companystatus IS NULL;

UPDATE tblcompanylist SET tblcompanylist.id_companytype = (SELECT tblcompanytype.id_companytype FROM tblcompanytype WHERE tblcompanytype.companytype = 'Broker') WHERE tblcompanylist.company_rating = 'Executing Local Broker';
UPDATE tblcompanylist SET tblcompanylist.id_companytype = (SELECT tblcompanytype.id_companytype FROM tblcompanytype WHERE tblcompanytype.companytype = 'Controlled Client') WHERE tblcompanylist.id_companytype IS NULL;

/*Update version */
INSERT INTO version VALUES ('1.4', '1', now());