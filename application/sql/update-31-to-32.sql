
ALTER TABLE `contacts` MODIFY `webview_print` CHAR(1) DEFAULT NULL;
ALTER TABLE `contacts` MODIFY `portal_access` CHAR(1) DEFAULT NULL;

update `contacts` set webview_print = NULL where webview_print = '0';
update `contacts` set portal_access = NULL where portal_access = '0';


CREATE TABLE IF NOT EXISTS `tblcompany_hipchatrooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `hipchat_room_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
); 

CREATE TABLE IF NOT EXISTS `tblclient_hipchatrooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `hipchat_room_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
); 

INSERT INTO `avcrm`.`config`
(`key`,
`value`)
VALUES(
'Platinum clients',
'OQ02QbnTDAwkN5gOlrbbQUE3nMmNoh7aq2okOdxS')
,('Gold Clients','kvLyRp3RFzH8gDuHu6s5LWHehSWC1CvnGmPTAW9g')
,('Silver Clients','XhyZWbvBUWy7OkSNMEL0GK0zUnaUhtvM0zJjnEM2')
,('Bronze Clients','h67wWAboBV8TuXAd1F7KIm7Nz3G19kxA58okQqsl')
,('Blue Clients','fqHH16WypgkCzMysXYT8oRVozZCBkpLMGy0c7Vxc')
,('read_room_key','Uq2CpeOeNW8TaKeK4Ob5ICYJJKhIflczyqOzEr3k')
,('add_room_key','POyTG17xeiM9ZXy7QH1ZygSvQJvcdb3Lf80W6H0l')
,('send_message_key','B6h1MUSJQ5S0Qe1f2gSfKIDwm57SCanemyuGPyak')
,('get_users_key','7rsTo73O6igUgXzeV6Aic6a83LKja6OoLyz9OiTt')
;



