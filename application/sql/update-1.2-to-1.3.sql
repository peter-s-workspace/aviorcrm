/* Alter cntresponsibility and tblcntinterests. Add indexes*/

ALTER TABLE `cntresponsibility` 
	ADD KEY `cnt_id`(`cnt_id`), COMMENT='';

ALTER TABLE `tblcntinterests` 
	ADD KEY `client_Id_index`(`cnt_Id`), COMMENT='';

-- --------------------- VERSION ------------------------
--
-- Database version is not represented by any model objects. It is used by the
-- database manager to incrementally update the database when the application is deployed.
--

update version set active = 0;
INSERT INTO version VALUES ('1.3', '1', now());