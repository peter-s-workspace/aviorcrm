-- ACRM-255 Duplicates
DELETE FROM `avcrm`.`tblcompanylist` WHERE `Company` = 'ORA Fund Managers (Pty)Ltd';
DELETE FROM `avcrm`.`tblcompanylist` WHERE `Company` = 'Atlantic Asset Management (Pty) Ltd';

-- First delete employees to avoid constraints
DELETE FROM `avcrm`.`contacts` WHERE `contacts`.`company_id` IN (SELECT `comp_id` FROM `tblcompanylist` WHERE `Company` = 'Katy Test Company');
-- Delete Company
DELETE FROM `avcrm`.`tblcompanylist` WHERE `Company` = 'Katy Test Company';

DELETE FROM `avcrm`.`contacts` WHERE `first_name` = 'Rene' AND `surname` = 'Prinsloo' AND `active` = 0;

UPDATE `avcrm`.`cnt_listnames` SET `shared` = 1;