ALTER TABLE `tblcompanylist` 
ADD COLUMN `switchboard_1` varchar(128)  COLLATE utf8_general_ci NULL after `address_4`, 
ADD COLUMN `switchboard_2` varchar(128)  COLLATE utf8_general_ci NULL after `switchboard_1`, 
ADD COLUMN `social_fb` varchar(255)  COLLATE utf8_general_ci NULL after `website`, 
ADD COLUMN `social_twitter` varchar(255)  COLLATE utf8_general_ci NULL after `social_fb`, 
ADD COLUMN `social_linkedin` varchar(255)  COLLATE utf8_general_ci NULL after `social_twitter`;


/*Update version */
INSERT INTO version VALUES ('13', '1', now());