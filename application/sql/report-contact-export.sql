/* Contacts and mailing list count */
SELECT 
c.id, 
c.first_name, 
c.surname, 
c.landline, 
c.mobile, 
c.email, 
c.jobtitle, 
t.Company, 
c.active, 
c.drm_status, 
t.drm_status as comp_status,
count( contacts_interests.contact_id ) as mailing_lists
FROM 
contacts c LEFT JOIN contacts_interests ON c.id=contacts_interests.contact_id
INNER JOIN tblcompanylist t ON  t.comp_id = c.company_id
GROUP BY c.id
order by 
 t.company, c.surname;

 /* Companies with DRM = 2 and mailing list count */
SELECT 
t.comp_id, 
t.Company, 
t.drm_status as comp_status, 
count( contacts_interests.contact_id ) as mailing_lists
FROM 
contacts c LEFT JOIN contacts_interests ON c.id=contacts_interests.contact_id
INNER JOIN tblcompanylist t ON  t.comp_id = c.company_id
where t.drm_status = 2
GROUP BY t.comp_id
order by 
 t.Company;