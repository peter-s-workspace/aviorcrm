DELIMITER $$

DROP PROCEDURE IF EXISTS `avcrm`.`sp_getWebViewOpenMetaData` $$
CREATE PROCEDURE  `avcrm`.`sp_getWebViewOpenMetaData`(
	  IN report_id 	INT,
	  IN username VARCHAR(128)
	)
	BEGIN
		/***************************************************************************
			** Auth: BR Potgieter
			** Date: 2018/04/11
			** Desc: The sp calculates the stats for report reads via Webview opens.
							Number of time report opened, max number of pages read and
							max time spent on a report per open
			**************************
			** Change History
			**************************
			** JIRA  				 	Date        		Author  	Description 
			**  -----------------		---------------   	-------   		------------------------------------
			**  AVCRM-352   	2018/04/11      BRP      	Create sp		
		***************************************************************************/

		SELECT 1	AS 'Result'
					, F.user
					, F.report_id
					, Max(F.Open_no)	AS 'Open_No'
					, MAX(F.No_Pages)	AS 'No_Pages'
					, MAX(F.Total_Time_sec)	AS 'Total_Time_sec'
					, SEC_TO_TIME(MAX(F.Total_Time_sec)) AS 'Total_Time'
						
		FROM (
					SELECT                          
									-- VALUES
									A.user
									, A.report_id
									, A.Read_No											AS 'Open_No'
									, COUNT(DISTINCT ifnull(A.Pages, 1))								AS  'No_Pages'									
									, ifnull(SUM(A.pg_read_duration), 1)					AS 'Total_Time_sec'
									, SEC_TO_TIME( ifnull(SUM(A.pg_read_duration), 0) )		AS 'Total_Time'
						FROM (
										/*Part 2: Match page views to Webview open events*/
										SELECT O.user, O.report_id, O.Last_read, O.Next_Read_date, O.Read_No
														, '----Pages-----' AS 'Result'
														, P.date_created, P.id, P.param	AS 'Pages'
														, @Read_No
														, @pg_order := IF(O.Read_no = @Read_No
																					, @pg_order+1, 1)					AS 'Page_order'
														/* Calc for 1st page based on 1page P.date_created - the Webview open O.last_read date	*/
														, @readDuration := TIMESTAMPDIFF(second, IF(@pg_order = 1, O.Last_read,@read_time), P.date_created) AS 'pg_read_duration_set'
														, IF(@readDuration > 900, 900, @readDuration)		AS 'pg_read_duration'					
										-- 			-- Set previous values
														, @read_time
														, @read_time := P.date_created							AS 'nxt_read_time'
														, @Read_No := O.Read_no 								AS 'nxt_Read_no'
														
											FROM (
														/*Part 1 get Webview opens date range between current read and the next read.*/
														SELECT E.user, E.report_id, E.First_read, E.Last_read
																	, @dense := @dense+1										AS 'Read_No'
																	, @post_date														AS 'Next_Read_date'		-- dense = 1 represents the last read
																	, @post_date := E.Last_read								AS 'post_date_set'
															FROM (
																		/*Part 0 get Webview opens*/
																		SELECT E.user
																				, E.report_id
																				, E.description
																				, MAX(E.id)					  AS 'event_id'
																				, MIN(E.date_created) AS 'First_read'
																				, MAX(E.date_created) AS 'Last_read'
																			FROM `avior`.event_logs E 
																		WHERE  E.description = 'Webview Document'
																				AND E.report_id IS NOT NULL					-- excludes report events pre 2017/12/01
																				AND E.report_id = report_id 						
																				AND E.user =  username
																		GROUP BY E.report_id, E.user, E.description 
																			, UNIX_TIMESTAMP(E.date_created) DIV 300
																		ORDER BY E.report_id, E.user, event_id DESC
																		) E
																		-- This is required to rest variables
																	, (SELECT @dense:=0, @post_date:=now()) var1
																) O	
												/* Webview opens to their corresponsing Page views*/
												LEFT JOIN `avior`.event_logs P						
															ON P.report_id = O.report_id
														AND P.user = O.user
														AND P.description = 'Webview Page View'
														AND P.date_created BETWEEN O.First_read AND O.Next_Read_date
												, (SELECT @Read_No := 0, @pg_order:=1, @read_time:=0, @readDuration := 0) var2
												ORDER BY O.report_id, O.user, O.Read_No, P.id DESC
										) A
									, (SELECT @maxTime := 0, @MaxPages := 0) T
						GROUP BY A.user, A.report_id, A.Read_No   
						/* OPTION TO try WITH ROLLUP -- A.user, A.report_id HAVING A.user IS NOT NULL 
																AND  A.report_id IS NOT NULL
							Needs to be tested*/
					) F
		GROUP BY F.user, F.report_id 
        ;

	END $$

	DELIMITER ;
    

