/* Add address_3 and address_4 */
ALTER TABLE `tblcompanylist` 
	ADD COLUMN `address_3` varchar(255)  COLLATE utf8_general_ci NULL after `address_2`, 
	ADD COLUMN `address_4` varchar(255)  COLLATE utf8_general_ci NULL after `address_3`, 
	CHANGE `website` `website` varchar(255)  COLLATE utf8_general_ci NULL after `address_4`, 
	CHANGE `modified_date` `modified_date` datetime   NULL after `website`, COMMENT='';

/* Add display order */
ALTER TABLE `tblcompanystatus` 
	ADD COLUMN `display_order` int(11)   NULL after `company_status`, 
	CHANGE `cdate` `cdate` datetime   NULL after `display_order`, 
	CHANGE `mdate` `mdate` datetime   NULL after `cdate`, COMMENT='';

UPDATE tblcompanystatus SET display_order = 1 WHERE TRIM(company_status) = 'Active';
UPDATE tblcompanystatus SET display_order = 2 WHERE TRIM(company_status) = 'Prospect';
UPDATE tblcompanystatus SET display_order = 3 WHERE TRIM(company_status) = 'Cold Prospect';
UPDATE tblcompanystatus SET display_order = 4 WHERE TRIM(company_status) = 'Dormant';

/* Add display order */
ALTER TABLE `tblcompanytier` 
	ADD COLUMN `display_order` int(11)   NULL after `companytier_status`, 
	CHANGE `cdate` `cdate` datetime   NULL after `display_order`, 
	CHANGE `mdate` `mdate` datetime   NULL after `cdate`, COMMENT='';	
	
INSERT  INTO `tblcompanytier`(`companytier`,`companytier_status`,`display_order`,`cdate`,`mdate`) VALUES ('Internal','1',7,NULL,NULL),('Executing Local Broker','1',6,NULL,NULL),('Corporate Contact','1',5,NULL,NULL),('Blue','1',1,NULL,NULL);	

UPDATE tblcompanytier SET display_order = 4 WHERE TRIM(companytier) = 'Platinum';
UPDATE tblcompanytier SET display_order = 3 WHERE TRIM(companytier) = 'Gold';
UPDATE tblcompanytier SET display_order = 2 WHERE TRIM(companytier) = 'Silver';
UPDATE tblcompanytier SET display_order = 8 WHERE TRIM(companytier) = 'Not Assigned';

/* Add 'Other' as company type */
INSERT  INTO `tblcompanytype`(`companytype`,`companytype_status`,`cdate`,`mdate`) VALUES ('Other','1',NULL,NULL);

/* Add Roadshow and Site Visit */
INSERT  INTO `tblcontacttype`(`ContactType`) VALUES ('Roadshow'),('Site Visit');

/* Update version */
/* INSERT INTO version VALUES ('1.5', '1', now()); */