/* Add new bloomberg status  - We are not going to remove the old status yet */
ALTER TABLE `tblcnt` ADD COLUMN `cnt_bloomberg_status` int(11) NOT NULL DEFAULT '0' after `cnt_blmuser`;

/* Set new bloomberg user status to 1 wherethe old bloomber user started with 'Y'. */
UPDATE tblcnt SET tblcnt.cnt_bloomberg_status = 1 WHERE tblcnt.cnt_blmuser LIKE 'Y%';

/*Update version */
INSERT INTO version VALUES ('10', '1', now());