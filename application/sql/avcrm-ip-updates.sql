-- update tbluser set user_phone_ip='' where user_phone_ip='';
-- old			new
-- 192.168.0.76	192.168.102.73	
-- 192.168.0.66	192.168.102.74
-- 192.168.0.57	192.168.102.75	
-- 192.168.0.81	192.168.102.81  !! duplicates
-- 192.168.0.83	192.168.102.83  !! duplicates
-- 192.168.0.84	192.168.102.84	
-- 192.168.0.85	192.168.102.85	
-- 192.168.0.87	192.168.102.87	
-- 192.168.0.88	192.168.102.88	
-- 192.168.0.91	192.168.102.91	
-- 192.168.0.92	192.168.102.92  !! duplicates	
-- 192.168.0.93	192.168.102.93	
-- 192.168.0.94	192.168.102.94	
-- 192.168.0.65	192.168.102.96  !! Peter C - joburg

select user_fname, user_lname, user_email, user_phone, user_phone_ip from tbluser where user_phone_ip in ( '192.168.0.76', '192.168.0.66', '192.168.0.57', '192.168.0.81', '192.168.0.83', '192.168.0.84', '192.168.0.85', '192.168.0.87', '192.168.0.88', '192.168.0.91', '192.168.0.92', '192.168.0.93', '192.168.0.94', '192.168.0.65') and (user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za' order by user_phone_ip ;

+------------+------------+--------------------------------+------------+---------------+
| user_fname | user_lname | user_email                     | user_phone | user_phone_ip |
+------------+------------+--------------------------------+------------+---------------+
| Rob        | Kiley      | robk@groombridgesecurities.com | 410        | 192.168.0.57  |
| Samantha   | Campbell   | samantha@avior.co.za           | 207        | 192.168.0.76  |
| Vincent    | Masoloke   | vincent@avior.co.za            | 0214405962 | 192.168.0.76  |
| Roelof     | Brand      | roelof@avior.co.za             | 0214405981 | 192.168.0.81  |
| Kevin      | Mattison   | kevin@avior.co.za              | NULL       | 192.168.0.83  |
| Mark       | Hodgson    | mark@avior.co.za               | 213        | 192.168.0.84  |
| De Wet     | Schutte    | dewet@avior.co.za              | NULL       | 192.168.0.85  |
| Carolina   | Viljoen    | carolina@avior.co.za           | NULL       | 192.168.0.87  |
| WJ         | De Vries   | wj@avior.co.za                 | NULL       | 192.168.0.88  |
| Jiten      | bechoo     | jiten@avior.co.za              | 0214405991 | 192.168.0.91  |
| Robyn      | Turner     | robyn@avior.co.za              | 0214405992 | 192.168.0.92  |
| Rob        | Brownlee   | robb@avior.co.za               | 0214405992 | 192.168.0.92  |
| Gareth     | Visser     | gareth@avior.co.za             | NULL       | 192.168.0.94  |
+------------+------------+--------------------------------+------------+---------------+
13 rows in set (0.00 sec)


update tbluser set user_phone_ip='192.168.102.73' where user_phone_ip='192.168.0.76' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.74' where user_phone_ip='192.168.0.66' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.75' where user_phone_ip='192.168.0.57' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.81' where user_phone_ip='192.168.0.81' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.83' where user_phone_ip='192.168.0.83' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.84' where user_phone_ip='192.168.0.84' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.85' where user_phone_ip='192.168.0.85' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.87' where user_phone_ip='192.168.0.87' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.88' where user_phone_ip='192.168.0.88' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.91' where user_phone_ip='192.168.0.91' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.92' where user_phone_ip='192.168.0.92' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.93' where user_phone_ip='192.168.0.93' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.94' where user_phone_ip='192.168.0.94' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';
update tbluser set user_phone_ip='192.168.102.96' where user_phone_ip='192.168.0.65' and ( user_phone not like '011%' or user_phone is null) and user_email not like 'peter@avior.co.za';


select user_fname, user_lname, user_email, user_phone, user_phone_ip from tbluser where user_phone_ip in 
( 
'192.168.0.76', 
'192.168.0.66', 
'192.168.0.57', 
'192.168.0.81', 
'192.168.0.83', 
'192.168.0.84', 
'192.168.0.85', 
'192.168.0.87', 
'192.168.0.88', 
'192.168.0.91', 
'192.168.0.92', 
'192.168.0.93', 
'192.168.0.94', 
'192.168.0.65'
) order by user_phone_ip;

select user_fname, user_lname, user_email, user_phone, user_phone_ip from tbluser 
where user_phone_ip in 
( 
'192.168.0.76', 
'192.168.0.66', 
'192.168.0.57', 
'192.168.0.81', 
'192.168.0.83', 
'192.168.0.84', 
'192.168.0.85', 
'192.168.0.87', 
'192.168.0.88', 
'192.168.0.91', 
'192.168.0.92', 
'192.168.0.93', 
'192.168.0.94', 
'192.168.0.65'
)
 and 
 (user_phone not like '011%' or user_phone is null) 
 and 
 user_email not like 'peter@avior.co.za' 
 order by user_phone_ip ;
