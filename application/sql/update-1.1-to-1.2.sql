/* Alter tblevent_logst */
ALTER TABLE `tblevent_logs` 
	ADD COLUMN `before_values` text  COLLATE utf8_general_ci NULL after `affected_usertype`, 
	ADD COLUMN `after_values` text  COLLATE utf8_general_ci NULL after `before_values`, 
	CHANGE `date_created` `date_created` datetime   NOT NULL after `after_values`, COMMENT='';

-- --------------------- VERSION ------------------------
--
-- Database version is not represented by any model objects. It is used by the
-- database manager to incrementally update the database when the application is deployed.
--

update version set active = 0;
INSERT INTO version VALUES ('1.2', '1', now());