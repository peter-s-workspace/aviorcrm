-- ACRM-189 --
ALTER TABLE contacts_interests DROP COLUMN tbl_int_Tier;
ALTER TABLE contacts ADD drm_status int NULL DEFAULT NULL;

-- ACRM-202 --
ALTER TABLE tblnotes ADD `note_time` time NULL DEFAULT NULL;

UPDATE config SET `value` = '1.9.0' WHERE `key` = 'version';