INSERT INTO tblcompanytype (`companytype`, `companytype_status`, `display_order`,`cdate`, `mdate`)
VALUES ('Not Assigned',1,11,NOW(),NULL);

/* Set 'Not Assigned' as default for all companies that are not assigned a type. */
UPDATE tblcompanylist SET tblcompanylist.id_companytype = (SELECT tblcompanytype.id_companytype FROM tblcompanytype WHERE tblcompanytype.companytype = 'Not Assigned' LIMIT 0,1) WHERE tblcompanylist.id_companytype IS NULL 

/*Update version */
-- INSERT INTO version VALUES ('14', '1', now());