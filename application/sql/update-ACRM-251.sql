

# Recent contacts is a subset of Contact Notes
DELETE FROM `avcrm_cf`.`tblnotes`;

# Association of user and call lists
DELETE FROM `avcrm_cf`.`tblfavourites` ;

# Call list names
DELETE FROM `avcrm_cf`.`cnt_listnames` where listname != 'Default' ;
