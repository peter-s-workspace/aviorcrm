/* find all companies with DRM protection */
select * from tblcompanylist t where t.drm_status =2 order by t.comp_id desc;

/* find all contacts that override company with DRM protection */
select * from 
contacts c,
tblcompanylist t
where 
c.company_id = t.comp_id and
t.drm_status = 2 and
(c.drm_status=2 or c.drm_status=1 or c.drm_status = 3)
order by id desc;

/* update all companies with DRM to use Webview instead (with safety catch)*/
update 
tblcompanylist
set drm_status=3
where 
drm_status=2 and 1=2;

select * from  contacts c, tblcompanylist t where  c.company_id = t.comp_id and c.drm_status = 2 order by id desc;

/* update all contacts with DRM override */
update 
contacts
set drm_status=3
where 
drm_status=2;

/* Companies with DRM=2 and mailing list count */
SELECT 
t.comp_id, 
t.Company, 
t.drm_status as comp_status, 
count( contacts_interests.contact_id ) as mailing_lists
FROM 
contacts c LEFT JOIN contacts_interests ON c.id=contacts_interests.contact_id
INNER JOIN tblcompanylist t ON  t.comp_id = c.company_id
where t.drm_status = 2
GROUP BY t.comp_id
order by 
 t.Company;