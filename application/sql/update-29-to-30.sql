
DELETE FROM `avior`.`job_interests` WHERE `job_interests`.`interest_id` in (34,166,68,23,29,17,33,7,19,26);

DELETE FROM `avcrm`.`contacts_interests` WHERE `contacts_interests`.`interest_id` IN (34,166,68,23,29,17,33,7,19,26);

DELETE FROM `avcrm`.`interests` WHERE `interests`.`id` IN (34,166,68,23,29,17,33,7,19,26);

UPDATE `avcrm`.`interests` SET `interests`.`description` = "Banks" WHERE `interests`.`description` = "Banks New";
UPDATE `avcrm`.`interests` SET `interests`.`description` = "Food Producers" WHERE `interests`.`description` = "Food Producers New";
UPDATE `avcrm`.`interests` SET `interests`.`description` = "Insurance" WHERE `interests`.`description` = "Insurance New";
UPDATE `avcrm`.`interests` SET `interests`.`description` = "Speciality Finance" WHERE `interests`.`description` = "Speciality Finance New";
UPDATE `avcrm`.`interests` SET `interests`.`description` = "Steel" WHERE `interests`.`description` = "Steel New";