<?php require_once('../Connections/CRMconnection.php'); ?>
<?php

# Recursively deletes a directory structure.
function removeDirectory($path) {
 	$files = glob($path . '/*');
	foreach ($files as $file) {
		is_dir($file) ? removeDirectory($file) : unlink($file);
	}
	rmdir($path);
 	return;
}

# Get all Corporate contacts
$pruneNonCorporateFiles = sprintf("SELECT * FROM `avcrm_cf`.`contacts`");
mysql_select_db($database_CRMconnection, $CRMconnection);
$Result = mysql_query($pruneNonCorporateFiles, $CRMconnection) or die(mysql_error());
$row = mysql_fetch_assoc($Result);
$ids = array();

# Push ids to a list
do {
	array_push($ids, $row['id']);
} while ($row = mysql_fetch_assoc($Result));

# Get all Corporate companies
$pruneNonCorporateFiles = sprintf("SELECT * FROM `avcrm_cf`.`tblcompanylist`");
mysql_select_db($database_CRMconnection, $CRMconnection);
$Result = mysql_query($pruneNonCorporateFiles, $CRMconnection) or die(mysql_error());
$row = mysql_fetch_assoc($Result);
$company_ids = array();

# Push ids to list
do {
	array_push($company_ids, $row['comp_id']);
} while ($row = mysql_fetch_assoc($Result));



$client_path = "../uploads/clientp";
$company_path = "../uploads/companyp";

# load all existing directories
$all_client_directories = glob($client_path . '/*');
$all_company_directories = glob($company_path . '/*');


# remove all client directories that aren't corporate
foreach($all_client_directories as $client_dir){
	$key = substr($client_dir, 19);

	if (!in_array($key, $ids)){
		removeDirectory($client_dir);
	}
}

# remove all company directories that aren't corporate
foreach($all_company_directories as $company_dir){
	$key = substr($company_dir, 20);

	if (!in_array($key, $company_ids)){
		removeDirectory($company_dir);
	}
}




?>