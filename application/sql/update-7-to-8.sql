/* Update tblcompanytype - ACRM-61 */
insert into `tblcompanytier` (`id_companytier`, `companytier`, `companytier_status`, `display_order`, `cdate`, `mdate`) values('9','Bronze','1','9',NULL,NULL);
insert into `tblcompanytier` (`id_companytier`, `companytier`, `companytier_status`, `display_order`, `cdate`, `mdate`) values('10','N/A','1','10',NULL,NULL);

UPDATE tblcompanytier SET display_order=1 WHERE companytier = 'Blue';
UPDATE tblcompanytier SET display_order=2 WHERE companytier = 'Bronze';
UPDATE tblcompanytier SET display_order=3 WHERE companytier = 'Silver';
UPDATE tblcompanytier SET display_order=4 WHERE companytier = 'Gold';
UPDATE tblcompanytier SET display_order=5 WHERE companytier = 'Platinum';
UPDATE tblcompanytier SET display_order=6 WHERE companytier = 'N/A';
UPDATE tblcompanytier SET display_order=7 WHERE companytier = 'Not Assigned';
UPDATE tblcompanytier SET companytier_status=0 WHERE companytier = 'Internal';
UPDATE tblcompanytier SET companytier_status=0 WHERE companytier = 'Executing Local Broker';
UPDATE tblcompanytier SET companytier_status=0 WHERE companytier = 'Corporate Contact';


/* Update tblcompanytype - ACRM-60 */
UPDATE tblcompanytype SET companytype = 'Executing Broker'  WHERE companytype= 'Broker';
INSERT INTO `tblcompanytype` (`companytype`,`companytype_status`,`cdate`, `mdate`) VALUES ('Property Only', '1', NULL,NULL);
INSERT INTO `tblcompanytype` (`companytype`,`companytype_status`,`cdate`, `mdate`) VALUES ('Corporate Contact','1',NULL,NULL);
INSERT INTO `tblcompanytype` (`companytype`,`companytype_status`,`cdate`, `mdate`) VALUES ('Internal','1',NULL,NULL);

/*Add display order and update new column */
ALTER TABLE `tblcompanytype` ADD COLUMN `display_order` INT(11) NULL AFTER `companytype_status`;
UPDATE tblcompanytype SET display_order=1 WHERE tblcompanytype.companytype = 'Executing Broker';
UPDATE tblcompanytype SET display_order=2 WHERE tblcompanytype.companytype = 'Controlled Client';
UPDATE tblcompanytype SET display_order=3 WHERE tblcompanytype.companytype = 'Corporate Contact';
UPDATE tblcompanytype SET display_order=4 WHERE tblcompanytype.companytype = 'Hedge Fund';
UPDATE tblcompanytype SET display_order=5 WHERE tblcompanytype.companytype = 'Internal';
UPDATE tblcompanytype SET display_order=6 WHERE tblcompanytype.companytype = 'Long Only';
UPDATE tblcompanytype SET display_order=7 WHERE tblcompanytype.companytype = 'Multi manager';
UPDATE tblcompanytype SET display_order=8 WHERE tblcompanytype.companytype = 'Private Client Manager';
UPDATE tblcompanytype SET display_order=9 WHERE tblcompanytype.companytype = 'Property Only';
UPDATE tblcompanytype SET display_order=10 WHERE tblcompanytype.companytype = 'Other';

/*Update version */
INSERT INTO version VALUES ('8', '1', now());