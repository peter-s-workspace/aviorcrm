ALTER TABLE `tblnotes` 
	CHANGE `note_cnt_Id` `note_cnt_Id` double   NULL after `notebyNick`, 
	CHANGE `note_cnt_name` `note_cnt_name` varchar(128)  COLLATE utf8_general_ci NULL after `note_cnt_Id`;

/* Must use a join to prevent inserting duplicates*/
INSERT INTO avcrm.tblcompany_hipchatrooms(company_id, hipchat_room_id)
SELECT comp_id, 4490909
FROM avcrm.tblcompanylist C
LEFT JOIN avcrm.tblcompany_hipchatrooms  H
		ON C.comp_id = H.company_id
	AND H.hipchat_room_id = 4490909
WHERE H.company_id IS NULL
