<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO tbluser (user_name, user_pword, user_fname, user_lname, user_email, user_level, user_phone, user_bday, user_phone_ip) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['user_name'], "text"),
                       GetSQLValueString($_POST['user_pword'], "text"),
                       GetSQLValueString($_POST['user_fname'], "text"),
                       GetSQLValueString($_POST['user_lname'], "text"),
                       GetSQLValueString($_POST['user_email'], "text"),
                       GetSQLValueString($_POST['user_level'], "text"),
                       GetSQLValueString($_POST['user_phone'], "text"),
                       GetSQLValueString($_POST['user_bday'], "date"),
                       GetSQLValueString($_POST['user_phone_ip'], "text"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());
}

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_users = "SELECT * FROM tbluser ORDER BY user_level ASC, user_fname ASC";
$users = mysql_query($query_users, $CRMconnection) or die(mysql_error());
$row_users = mysql_fetch_assoc($users);
$totalRows_users = mysql_num_rows($users);



?>
<style>

.userlist {
	display:block;
	width:250px;
	cursor: pointer;	
}

.userlist li{
	display:block;
	width:inherit;
	margin-bottom:1px;
	padding:4px;
	text-decoration:none;
	list-style:none;
	text-indent:0;
	margin:0;
	background-color:#CCC;
}

.userlist li:hover {
	background-color:#666;
}

	

</style>

<p><a onclick="">add user</a></p><div id="adduserform">
<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
<table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Username:</td>
      <td><input type="text" name="user_name" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Password:</td>
      <td><input type="text" name="user_pword" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">First name:</td>
      <td><input type="text" name="user_fname" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Last name:</td>
      <td><input type="text" name="user_lname" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email:</td>
      <td><input type="text" name="user_email" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_level:</td>
      <td><select name="user_level">
        <option value="General" <?php if (!(strcmp("General", ""))) {echo "SELECTED";} ?>>General User</option>
        <option value="Administrator" <?php if (!(strcmp("Administrator", ""))) {echo "SELECTED";} ?>>Administrator</option> <option value="Guest" <?php if (!(strcmp("Guest", ""))) {echo "SELECTED";} ?>>Guest</option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Phone #:</td>
      <td><input type="text" name="user_phone" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">B day:</td>
      <td><input type="text" name="user_bday" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Phone IP:</td>
      <td><input type="text" name="user_phone_ip" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Add User" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />
</form>
</div>
<hr />
<div>
  <p><strong>User List (click to edit)</strong></p>
  <ul class="userlist">
   <?php
	$prev = "";
	do { 
      if ( $row_users['user_level'] != $prev){
	  echo "<b>".$row_users['user_level']."</b>";
	  $prev= $row_users['user_level']; 
	  }
     ?> <li><a>
        <?php echo $row_users['user_fname'];?> <?php echo $row_users['user_lname'];?> </a>
      </li>
      <div id="e<?php echo $row_users['user_id']; ?>"></div>
      <?php } while ($row_users = mysql_fetch_assoc($users)); ?>
  </ul>
</div>

<?php
mysql_free_result($users);
?>
