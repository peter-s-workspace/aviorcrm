<?php require_once('Connections/CRMconnection.php'); ?>
<?php
require_once('includes/classes/notes.class.php');
require_once('includes/classes/user.class.php');
require_once('includes/classes/hipchat.class.php');

if (!isset($_SESSION)) {
  session_start();
}

$hipchat_instance   = new hipchat();

$MM_authorizedUsers = "Administrator";
$MM_donotCheckaccess = "false";

$userObj = new User();

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
$colname_onclientlist = "none";

if( !isset($_GET['cnt_Id']) || !is_numeric($_GET['cnt_Id']) ) {
	header('location: adminmain.php');
	exit();
}





//function to make thumbnail/resize a input image
function make_thumb($src, $dest, $desired_width) {

	/* read the source image */
	$source_image = imagecreatefromjpeg($src);
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = floor($height * ($desired_width / $width));
	
	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	
	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	
	/* create the physical thumbnail image to its destination */
	imagejpeg($virtual_image, $dest);
}

/* File drop fall-back*/
if ((isset($_FILES["Filedata"]))){

	if (!empty($_FILES) && isset($_GET['cnt_Id']) && isset($_FILES["Filedata"]['name']) && isset($_FILES["Filedata"]['tmp_name'] ) &&  $_FILES["Filedata"]['size'] > 0 ) {
		
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = 'uploads/clientp/'.$_GET['cnt_Id'].'/';
		$targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];
		
		//make directory if it doens not yet exist		
		if(!is_dir($targetPath)) {
			mkdir($targetPath, 0777);
		} 
		
		move_uploaded_file($tempFile,$targetFile);
		include("js/resize-class.php");
		
		//resize the file quickly
		$width = 150;
		$height = 150;
		// *** 1) Initialize / load image  
		$resizeObj = new resize($targetFile);  
		
		// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)  
		$resizeObj -> resizeImage($width,$height, 'auto');  
		
		// *** 3) Save image  
		$resizeObj -> saveImage($targetPath."1.jpg", 100);  
		//remove original
		unlink($targetFile);
}

	
//now we enter all the details into the database  - whoopdidoo basil
/*
 $insertSQL = sprintf("UPDATE tblcnt SET cpict=%s WHERE cnt_Id=%s",
                       GetSQLValueString($ran3 , "text") ,
                       GetSQLValueString($_POST['cnt_Id'], "int"));

mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());
*/

}


$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$currentPage = $_SERVER["PHP_SELF"];

$colname_reminders = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_reminders = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_reminders = sprintf("SELECT * FROM reminders WHERE user_id = %s ORDER BY remind_id DESC", GetSQLValueString($colname_reminders, "text"));
$reminders = mysql_query($query_reminders, $CRMconnection) or die(mysql_error());
$row_reminders = mysql_fetch_assoc($reminders);
$totalRows_reminders = mysql_num_rows($reminders);


$contact_id = "-1";
if (isset($_GET['contact_id'])) {
  $contact_id = $_GET['contact_id']; /* Client Id */
}
/**
* Get Notes by client ID.
*/
$notesData = array();
$notesDataSize = 0;

$colname_userdet = "-1";
if(isset($_GET['cnt_Id'])){
    $colname_userdet = $_GET['cnt_Id'];
}

if ($colname_userdet > 0){
	$notesObj = new Notes();
	$notesData = $notesObj->getUserNotes($colname_userdet);
	$notesDataSize = sizeof($notesData);
}

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userdet = sprintf("SELECT *,contacts.portal_access as 'user_portal_access', contacts.webview_print as 'user_webview_print', tblcompanylist.portal_access as 'company_portal_access', tblcompanylist.webview_print as 'company_webview_print' FROM contacts, tblcompanylist,tblcompanytier  WHERE contacts.id = %s AND tblcompanylist.comp_id = contacts.company_id AND tblcompanytier.id_companytier = tblcompanylist.id_companytier", GetSQLValueString($colname_userdet, "int"));
$userdet = mysql_query($query_userdet, $CRMconnection) or die(mysql_error());
$row_userdet = mysql_fetch_assoc($userdet);
$totalRows_userdet = mysql_num_rows($userdet);

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userdrm = sprintf("SELECT drm_status FROM contacts WHERE contacts.id = %s ", GetSQLValueString($colname_userdet, "int"));
$userdrm = mysql_query($query_userdrm, $CRMconnection) or die(mysql_error());
$row_userdrm = mysql_fetch_assoc($userdrm);
$totalRows_userdrm = mysql_num_rows($userdrm);

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_complist = "SELECT * FROM tblcompanylist ORDER BY Company ASC";
$complist = mysql_query($query_complist, $CRMconnection) or die(mysql_error());
$row_complist = mysql_fetch_assoc($complist);
$totalRows_complist = mysql_num_rows($complist);

$colname_responsib = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_responsib = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_responsib = sprintf("SELECT responsiblist.response_desc, responsiblist.respons_id, cntresponsibility.idcntresponsibility FROM cntresponsibility, responsiblist WHERE cntresponsibility.cnt_id=%s AND responsiblist.respons_id=cntresponsibility.respons_id ORDER BY responsiblist.response_desc", GetSQLValueString($colname_responsib, "int"));
$responsib = mysql_query($query_responsib, $CRMconnection) or die(mysql_error());
$row_responsib = mysql_fetch_assoc($responsib);
$totalRows_responsib = mysql_num_rows($responsib);

$colname_resplist = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_resplist = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_resplist = sprintf("SELECT * FROM responsiblist WHERE responsiblist.respons_id NOT IN ( SELECT respons_id FROM cntresponsibility WHERE cntresponsibility.cnt_id=%s) ORDER BY responsiblist.response_desc", GetSQLValueString($colname_resplist, "int"));
$resplist = mysql_query($query_resplist, $CRMconnection) or die(mysql_error());
$row_resplist = mysql_fetch_assoc($resplist);
$totalRows_resplist = mysql_num_rows($resplist);

$maxRows_rscontacts = 6;
$pageNum_rscontacts = 0;
if (isset($_GET['pageNum_rscontacts'])) {
  $pageNum_rscontacts = $_GET['pageNum_rscontacts'];
}

$startRow_rscontacts = $pageNum_rscontacts * $maxRows_rscontacts;

$colname_rscontacts = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rscontacts = $_GET['cnt_Id'];
}
/*
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rscontacts = sprintf("SELECT * FROM tblnotes WHERE note_cnt_Id = %s ORDER BY note_date DESC", GetSQLValueString($colname_rscontacts, "double"));
$query_limit_rscontacts = sprintf("%s LIMIT %d, %d", $query_rscontacts, $startRow_rscontacts, $maxRows_rscontacts);
$rscontacts = mysql_query($query_limit_rscontacts, $CRMconnection) or die(mysql_error());
$row_rscontacts = mysql_fetch_assoc($rscontacts);

if (isset($_GET['totalRows_rscontacts'])) {
  $totalRows_rscontacts = $_GET['totalRows_rscontacts'];
} else {
  $all_rscontacts = mysql_query($query_rscontacts);
  $totalRows_rscontacts = mysql_num_rows($all_rscontacts);
}
$totalPages_rscontacts = ceil($totalRows_rscontacts/$maxRows_rscontacts)-1;
*/
$colname_rsemail = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rsemail = $_GET['cnt_Id'];
}

mysql_select_db($database_CRMconnection, $CRMconnection);

$query_rsemail = sprintf("
			SELECT 
				DISTINCT contacts_interests.*, 
				interests.*, 
				IF (ISNULL(contacts.drm_status), tblcompanylist.drm_status, contacts.drm_status) as drm_status, 
				tblcompanylist.drm_status as company_drm_status, 
				contacts.drm_status as drm_status_override, 
				IF (ISNULL(contacts.webview_print), tblcompanylist.webview_print, contacts.webview_print) as webview_print, 
				tblcompanylist.webview_print as company_webview_print,
				contacts.webview_print as webview_print_override 
			FROM 
				contacts_interests, 
				interests, 
				contacts, 
				tblcompanylist
			WHERE 
				contacts_interests.contact_id = %s 
        AND contacts.id = contacts_interests.contact_id
      	AND contacts.company_id = tblcompanylist.comp_id
				AND contacts_interests.interest_id = interests.id 
			ORDER BY 
				interests.description ASC"
			, GetSQLValueString($colname_rsemail, "int"));


$rsemail = mysql_query($query_rsemail, $CRMconnection) or die(mysql_error());
$row_rsemail = mysql_fetch_assoc($rsemail);
$totalRows_rsemail = mysql_num_rows($rsemail);

$colname_rsnotemail = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rsnotemail = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsnotemail = sprintf("SELECT * FROM interests
							WHERE description NOT IN (SELECT interests.description FROM interests , contacts_interests
							WHERE contacts_interests.contact_id = %s AND contacts_interests.interest_id = interests.id)
							ORDER BY interests.description", GetSQLValueString($colname_rsnotemail, "int"));
$rsnotemail = mysql_query($query_rsnotemail, $CRMconnection) or die(mysql_error());
$row_rsnotemail = mysql_fetch_assoc($rsnotemail);
$totalRows_rsnotemail = mysql_num_rows($rsnotemail);

$colname_userd = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userd = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userd = sprintf("SELECT * FROM tbluser WHERE tbluser.user_name = %s", GetSQLValueString($colname_userd, "text"));
$userd = mysql_query($query_userd, $CRMconnection) or die(mysql_error());
$row_userd = mysql_fetch_assoc($userd);
$totalRows_userd = mysql_num_rows($userd);

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsCtype = "SELECT * FROM tblcontacttype ORDER BY ContactType ASC";
$rsCtype = mysql_query($query_rsCtype, $CRMconnection) or die(mysql_error());
$row_rsCtype = mysql_fetch_assoc($rsCtype);
$totalRows_rsCtype = mysql_num_rows($rsCtype);


if (isset($_SESSION["MM_Username"])) {
  $colname_onclientlist = $_SESSION["MM_Username"];
}
$colname2_onclientlist = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname2_onclientlist = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_onclientlist = sprintf("SELECT tblfavourites.tblFav_id, tblfavourites.fav_Username, tblfavourites.tbl_cnt_Name_Desc, tblfavourites.tbl_cnt_Id, tblfavourites.tbl_Tiering, tblfavourites.fav_nick, tblfavourites.listid, cnt_listnames.listname, cnt_listnames.ownerusername, cnt_listnames.shared, cnt_listnames.editablebyothers FROM tblfavourites, cnt_listnames WHERE cnt_listnames.listid=tblfavourites.listid AND ( tblfavourites.fav_Username= %s OR cnt_listnames.shared=1) AND tblfavourites.tbl_cnt_Id=%s", GetSQLValueString($colname_onclientlist, "text"),GetSQLValueString($colname2_onclientlist, "int"));
$onclientlist = mysql_query($query_onclientlist, $CRMconnection) or die(mysql_error());
$row_onclientlist = mysql_fetch_assoc($onclientlist);
$totalRows_onclientlist = mysql_num_rows($onclientlist);

$colname_availlist = "none";
if (isset($_SESSION['MM_Username'])) {
  $colname_availlist = $_SESSION['MM_Username'];
}
$colname2_availlist = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname2_availlist = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_availlist = sprintf("SELECT * FROM cnt_listnames WHERE cnt_listnames.listid NOT IN ( SELECT tblfavourites.listid FROM tblfavourites WHERE tblfavourites.fav_Username=%s AND tblfavourites.tbl_cnt_Id = %s ) AND  (cnt_listnames.ownerusername= %s OR cnt_listnames.listid=1 OR (cnt_listnames.shared= 1 AND cnt_listnames.editablebyothers=1))  ORDER BY cnt_listnames.listname ASC", GetSQLValueString($colname_availlist, "text"),GetSQLValueString($colname2_availlist, "int"),GetSQLValueString($colname_availlist, "text"));
$availlist = mysql_query($query_availlist, $CRMconnection) or die(mysql_error());
$row_availlist = mysql_fetch_assoc($availlist);
$totalRows_availlist = mysql_num_rows($availlist);

$colname_rsaddphone = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rsaddphone = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsaddphone = sprintf("SELECT * FROM addphone WHERE cnt_id = %s", GetSQLValueString($colname_rsaddphone, "int"));
$rsaddphone = mysql_query($query_rsaddphone, $CRMconnection) or die(mysql_error());
$row_rsaddphone = mysql_fetch_assoc($rsaddphone);
$totalRows_rsaddphone = mysql_num_rows($rsaddphone);
$additionalPhones = array();
for($x=0; $x < $totalRows_rsaddphone; $x++) {
	$additionalPhones[] = $row_rsaddphone;
	$row_rsaddphone = mysql_fetch_assoc($rsaddphone);
}

$get_user_hipchat_rooms = "SELECT * FROM tblclient_hipchatrooms where client_id = ".$_GET['cnt_Id']."";

mysql_select_db($database_CRMconnection);

$run_client_query       = mysql_query($get_user_hipchat_rooms, $CRMconnection) or  die(mysql_error());
$client_chat_rooms      = array();
while ($row             = mysql_fetch_array($run_client_query)) {
  $client_chat_rooms [] = $row['hipchat_room_id'];
}
$client_rooms           = "".implode(",", $client_chat_rooms)."";
/*
$queryString_rscontacts = "";	
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rscontacts") == false && 
        stristr($param, "totalRows_rscontacts") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rscontacts = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rscontacts = sprintf("&totalRows_rscontacts=%d%s", $totalRows_rscontacts, $queryString_rscontacts);
*/



########################### Get Analyst Data #############
 $note_admins_allowed = array('Administrator','NoteAdmin');

  if (in_array($_SESSION['MM_UserGroup'], $note_admins_allowed)) {
      $show_analyst_dropdown = "";
  } else {
      $show_analyst_dropdown = "none";
  }

  $analyst_query      = "SELECT CONCAT(u.user_fname,' ',u.user_lname) as full_name, u.user_name FROM avcrm.tbluser u 
                     WHERE u.user_level <> 'Administrator' OR u.user_level <> 'NoteAdmin' ORDER BY full_name ASC;";

 $run_analyst_query   = mysql_query($analyst_query, $CRMconnection);
  
 if ($run_analyst_query) {
    $analyst_select         = "<select name=analyst_list id=analyst_list>";

    $analyst_select        .= "<option selected value=''> -- Select Analyst -- </option>";
    while ($analyst_row     = mysql_fetch_array($run_analyst_query)) {
     $analyst_option_name   = urlencode($analyst_row['full_name']);
     $analyst_display_name  = $analyst_row['full_name'];
     $analyst_username      = $analyst_row['user_name'];
     ### Spaces have an issue when setting them as option values need to be encoded ##########
     $analyst_select       .= "<option value=$analyst_username>".$analyst_display_name."</option>";
    }
    $analyst_select        .= "</select>";
 }   

########################### End Analyst Data #############


?>
<?php require_once('includes/sitevars.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<link type="text/css" href="css/kdes1/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<link type="text/css" href="css/datatables/dataTables.twitter.bootstrap.css" rel="stylesheet" />
<link type="text/css" href="https://cdn.datatables.net/1.10.5/css/jquery.dataTables.css" rel="stylesheet" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<link type="text/css" href="css/skipjack.css" rel="stylesheet" />
<link type="text/css" href="css/dropzone.css" rel="stylesheet" />
<script src="js/jquery-1.10.2.js"></script>
<script src="js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/dropzone.js"></script>
<script type="text/javascript" src="js/jquery.linkify.min.js"></script>
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/mobile-device-sync.js"></script>
<script type="text/javascript" src="js/jquery.validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/clientnotes.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript">

			$(function(){

        var isKeyAccManager = false;
        var accCompanyNames = Array();
        //alert(selected_value);
       // $('.js-example-basic-multiple').select2({}).select2('val',['3891319','3898278']);
        $('.js-example-basic-multiple').select2().val([<?php echo $client_rooms;?>]).trigger("change");
				//hide id's on initial load;
				$('#addnoteform').hide();
				$('#addreminderform').hide();
				$('#cancelnotebutton').hide();
				$('#newulist').hide();
				$('#removeulist').hide();
				$('#modulist').hide();
				
				// Accordion
				$("#accordion").accordion({ header: "h3" });
				
				// Tabs
				$('#tabs1').tabs();
				$('#tabs2').tabs();
				
				// Dialog Link
				$('#dialog_link').click(function(){
					$('#dialog').dialog('open');
					return false;
				});
				
				// Datepicker
				$('#note_date').datepicker({
					dateFormat: 'yy-mm-dd'
				});
				
				//hide add reminder box
				$('#addreminder').hide();
				
				$('.itemDelete').live('click', function  () {
					$(this).closest('li').remove();
					//$(this).closest('li').
				});
				//reminder button -open form
				$('#btnaddreminder').click(function(){
					$('#addreminder').show('fold');
					return false;
				});
				//close reminder form
				$('#btnCancelRemind').click(function(){
					$('#addreminder').hide('fold');
					return false;
				});
				
				
				
				//hide add notes
				$('#addnotebutton').click( function(){
					$('#addnotebutton').hide();
					$('#addnoteform').show('fold');
					$('#cancelnotebutton').show('fold');
					return false;
				});
				
				$('#cadremin').click(function(){
					$('#addreminderform').show();
					$('#cancelnotebutton').show();
				});
				
				
				//user list hide/show
				$('#btnnewulist').click( function(){
					$('#removeulist').hide();
					$('#modulist').hide();
					$('#newulist').show('fold');
				});
				
				$('#btnremoveulist').click( function(){
					$('#newulist').hide();
					$('#modulist').hide();
					$('#removeulist').show('fold');
				});
				
				$('#btnmodulist').click( function(){
					$('#newulist').hide();
					$('#removeulist').hide();
					$('#modulist').show('fold');
					
				});
				
				//cancel buttons in user list operation boxes
				$('#btncancelnewulist').click( function(){
					$('#newulist').hide('fold');
				});
				
				$('#btncancelremoveulist').click( function(){
					$('#removeulist').hide('fold');
				});
				$('#btncancelmodulist').click( function(){
					$('#modulist').hide('fold');
				});
					
				//operations for user lists
				//new user list
				$('#confaddulist').click(function (){
				// do this
					//get variables from frmaddnewlist
					var tmp =$('#frmaddnewlist').serialize();
				
				//do query and load into #avvaillist div
					///show loader
					
					$('#availlistspan').empty().html('<img src="images/ajax-loader.gif" />');
					
					$('#availlistspan').load('userclientlistavaillist.php?'+tmp);
					$('#newulist').hide('fold');
					
				});
				
				//cancel new note button operations
				$('#cancelnotebutton').click( function(){
					$('#addnoteform').hide();
					$('#addreminderform').hide();
					$('#cancelnotebutton').hide();
					$('#addnotebutton').show();
					$('#cadremin').show();
					return false;
				});
				
				$('#savenewnotebutton').click(function(){
					//$('#cnotes').load('clientdetailsajaxops.php?'+ $('#addnoteform').serialize());
					$('#addnoteform').hide('fold');
					$('#cancelnotebutton').hide('fold');
					$('#addnotebutton').show('fold');				
				});
				
				
				var client_image = $('#client_image').val();
				var client_imagesize = $('#client_imagesize').val();
				var file_dropped = 0;
				
				var myDropzone = new Dropzone("div#picturedrop", 
					{ url: "js/uploadifyclientp.php?folder=/uploads/clientp/<?php echo $colname_userdet; ?>",
					addRemoveLinks :true,
					acceptedFiles: 'image/*',
					forceFallback: false,
					paramName:'Filedata',
					dictFallbackText: 'Please use the fallback form below to upload your files',
					dictRemoveFile: 'Clear Picture',
					maxFiles:1
					});
					
				
					if($('.dz-browser-not-supported').length == 0 ) {
						
						myDropzone.on("removedfile", function(file) {
							
							  if (!file.name) {  return; } // The file hasn't been uploaded
								 myDropzone.removeAllFiles(true);
								 myDropzone.options.maxFiles = 1;
								 $('.my-drop-message').show();
								 $(".dz-error, .error").remove();
								 $.post("js/dropzone-remove-file.php",{id:'<?php echo $colname_userdet; ?>'}); 
							}).on("drop",function(file,error){
							
							  $('#cpicture').append('<div class="notice error"><em>Note: Images need to be saved to desktop first before dropping</em></div>');
							  

						});
		
						if( client_image != '' ) {
							$('.my-drop-message').hide();
							var mockFile = { name: "1.jpg", size: client_imagesize };
							myDropzone.emit("addedfile", mockFile);
							myDropzone.emit("thumbnail", mockFile, client_image);
							myDropzone.emit("complete", mockFile);
							var existingFileCount = 1; 
							myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
							
						}
						myDropzone.on("error", function(file,error) {

							$(".error").remove();
							$(".dz-error").hide();

							if( error == 'You can not upload any more files.' ) {
								$('#cpicture').append('<div class="error">You can not upload any more files. <br />Please clear image first</div>');
							} else {
								$('#cpicture').append('<div class="error">'+error+'</div>');
							}
						});
						
						myDropzone.on("processing", function(file){
							 $('.notice').remove();
							   $('.my-drop-message').hide();
						});
						

					} else {
					$('.my-drop-message').hide();
					$('.dz-browser-not-supported').append('<div id="fallback-preview"></div>');
						if( client_image != '' ) {
							$('#fallback-preview').html('<img src="'+client_image+'" alt="Preview" /><br /><a id="fallback-removefile" href="#">Remove File</a>');
							$('.dz-browser-not-supported form').hide();
						}
						$('.dz-browser-not-supported form').submit(function(){
							$('#picturedrop form').attr('action','clientdetails-edit.php?cnt_Id=<?php echo $colname_userdet; ?>');
							//var filedata = $('#picturedrop form').serialize();
							//return false;
						});	
						
						$('#fallback-removefile').click(function(){
							$('.dz-browser-not-supported form').show();
							$.post("js/dropzone-remove-file.php",{id:'<?php echo $colname_userdet; ?>'}); 
							$('#fallback-preview img, #fallback-removefile').hide();
							return false;
						});
					}

                var clientId = location.search.split('cnt_Id=')[1];

                var request = $.ajax({
                    type: 'POST',
                    url: 'keyAccManagerCheck.php',
                    data: { client_id : clientId },
                    dataType: 'json'
                });

                request.done(function(msg){
                    if(msg.length > 0){
                        isKeyAccManager = true;
                        accCompanyNames = msg;
                    }
                });

                var initialActiveState = $('input[name="cnt_active"]:checked').val();

                $("#btnUpdateRecord").on('click' , function(){						

                    var currentActiveState = $('input[name="cnt_active"]:checked').val();

                    if(initialActiveState == 1 && currentActiveState == 0 && isKeyAccManager){

                        var companies = "";

                        for(i in accCompanyNames){
                            companies += "- " + accCompanyNames[i]['Company'] + "\n";
                        }

                        var msg = "This client is the Key Account Manager for the following companies: \n\n" + companies + "\n Continue to deactivate?";
                        var r = confirm(msg);

                        if(r == true){
                            ajaxformget('#clientupdate' , 'clientdetailsajaxops.php' , '#updresp' , true);
                        }

                    } else {
                        ajaxformget('#clientupdate' , 'clientdetailsajaxops.php' , '#updresp' , false);
                    }

                });

			});
		

		// applies email subscription change
		function doemailsubchange(contact_id,interest_id, targetid){
		 	var loadline= "clientmailsubajax.php?contact_id=" + contact_id + "&MM_addmailsub=donow&interest_id="+interest_id;
		 	// loadline += $(valinid).val(); //gets value from select box
			$(targetid).load(loadline);
		}
				
		//function to do loader more ubiquities
		function showloader(targetid){
		$(targetid).empty().html('<img src="images/ajax-loader.gif" />');
				}	
				
				//function to retrieve form values from a specific form  - identified by id and put them into a line for GET

function noteupdate(cnt_id,targetid){
	var queryline =  "clientdetailsajaxops.php?MM_noteupdate=updateusernote&cnt_Id=" + cnt_id + "&cnt_note=" ;
	queryline +=	escape($('#cnt_note').val()) ;
	$(targetid).load(queryline);
}

function loadColleagues(cnt_id) {
	var queryline =  "ajaxClientColleagues.php?cnt_Id=" + cnt_id ;
	$("#tabs2-2").load(queryline);
}
	
	//gets form data from formid, puts it into url data, GET's results from targetURL, and resturns results to resultsout div tag
	function ajaxformget(formid, targeturl, resultsout , deactivateKam) {
		//gets form inputs from specified form		
		if( $('#clientupdate').valid()) {
			var tt = $(formid).serialize();

            if(deactivateKam){
                tt += "&deactivateKam=true";
            }

			$.ajax({
				dataType: "html",
				url: targeturl,
				data: tt,
				success: function(data){
					var result = data.replace(/<\/?[^>]+>/gi, '');
          var clientId  = $(formid+' input[name="cnt_Id"]').val();          
					if( $.trim(result) == 'Updated') {
						if(clientId != '') {
							window.location.href = 'clientdetails.php?cnt_Id='+clientId;
						}
					} else {
              alert(data);
              window.location.href = 'clientdetails.php?cnt_Id='+clientId;
          }
				}
			});
			//$(resultsout).load(turl);
		}
	}
</script>
<script type="text/javascript">
	$(document).ready(function(){
    //get the value on load
      var default_value = $("#cnt_override_drm option:selected").val();
      if (default_value != 3 || default_value == " ") {
           $("#webviewerwithprint").css("display", "none");
      } else  {
          $("#webviewerwithprint").css("display", "");
      } 

      $("#cnt_override_drm").on('change',function(){
       
        var value = $(this).val();
        if (value != 3 || value == " ") {
          $("#webviewerwithprint").css("display", "none");
        } else if (value == 3) {
          $("#webviewerwithprint").css("display", "");
        }
    });

		var emailAddress = $('input[name="cnt_email"]').val(),  activeMonthMax  = 6, apiUrl = "https://aviorlib.co.za/api/devices?";
		
		addDeviceSync(apiUrl, emailAddress, activeMonthMax);

		$('#ipad-activity').click(function(){
			$('#tabs2').tabs({active: 2});
		});
		
		$('#android-activity').click(function(){
			$('#tabs2').tabs({active: 3});
		});
		
		 $('#clientupdate').validate({
            errorClass:'validate-error',
            errorPlacement: function(error, element) {		
                error.appendTo( element.parents('tr').find("span.validation-error").addClass('error') );													
            },
            rules: {
				cnt_Fname: {required: true },
				cnt_Lastname: {required: true },
                cnt_email: { required: false, email: true }
            },
            messages: {		
				cnt_Fname: { required: 'Please enter First name' },
				cnt_Lastname: { required: 'Please enter Last name' },
                cnt_email: { email: 'Please enter a valid email address' }
            }
        });
		
		$('#addMoreNumbers').click(function(){
			var phoneInput = '<span class="phone-field"><p><input type="text" name="phone_num[]" size="24" value="" /> <a href="javascript://" title="Click to remove phone" class="remove-phone">Remove</a></p></span>'
			$('#other-phone-numbers').append(phoneInput);
			return false;
		});
		$('.remove-phone').live('click',function(){
			$(this).parent().remove();
		});
	});
	
</script>


<!-- InstanceEndEditable -->
<script language="javascript">
	$(function(){
		
		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					}
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);
				
					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
	})



</script>	
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			<div>
				<fieldset>
					<Legend>Quick Search</Legend>
					<input type="text" id="quicksearch" size="16" />
				</fieldset>
			</div> 
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<h2>Client Details : <?php echo $row_userdet['first_name']; ?> <?php echo $row_userdet['surname']; ?></h2>

<div class="content40">
<div id="tabs1">
      <ul>
        <li><a href="#tabs1-1">Details</a></li>
        <li><a href="#tabs1-2">Responsibility List</a></li>
        <li><a href="#tabs1-3">Email subscriptions</a></li>
      </ul>
      <div id="tabs1-1">
      <div id="ajaxt2" style="border:1px solid #CCC;">
        <fieldset>
		  <legend>Edit Client details</legend><form name="clientupdate"  id="clientupdate">
		  <div class="device-icon" id="android-activity-image"><a href="javascript://" id="android-activity" title="Click to view android activity"><img src="images/android_phone_avior2.png" width="40" align="Android" border="0" /></a></div>
		  <div class="device-icon" id="ipad-activity-image"><a href="javascript://" id="ipad-activity" title="Click to view ipad activity"><img src="images/ipad_avior2.png" width="40" align="Ipad" border="0" /></a></div>
		  
			  <table align="left">
			<tr>
			  <td nowrap="nowrap" align="right">Firstname:</td>
			  <td><input type="text" name="cnt_Fname" value="<?php echo htmlentities($row_userdet['first_name'], ENT_COMPAT, 'utf-8'); ?>" size="32" />
			  	<span class="validation-error"></span>
			  </td>
        <input type="hidden" name="old_user_name" value ="<?php echo htmlentities($row_userdet['first_name'], ENT_COMPAT, 'utf-8'); ?>" />
			</tr>
			<tr>
			  <td nowrap="nowrap" align="right">Surname</td>
			  <td><input type="text" name="cnt_Lastname" value="<?php echo htmlentities($row_userdet['surname'], ENT_COMPAT, 'utf-8'); ?>" size="32" />
			  <span class="validation-error"></span>
			  </td>
        <input type="hidden" name="old_user_surname" value ="<?php echo htmlentities($row_userdet['surname'], ENT_COMPAT, 'utf-8'); ?>" />
			</tr>
			<tr>
			  <td align="right" valign="top" nowrap="nowrap">Phone:</td>
        <input type="hidden" name="old_user_number" value ="<?php echo htmlentities($row_userdet['landline'], ENT_COMPAT, 'utf-8'); ?>" />
			   <td><input type="text" name="cnt_phone" value="<?php echo htmlentities($row_userdet['landline'], ENT_COMPAT, 'utf-8'); ?>" size="24" />
			   <a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userdet['landline']; ?>')"> <img src="images/phoneicon.png" width="20" height="20" align="absmiddle" /></a><br />
			   <p><a href="jvascript://" id="addMoreNumbers" title="Add another phone number">Add more</a></p>
			   <div id="other-phone-numbers">
			   <?php if (sizeof($additionalPhones) > 0) {
			   	for($x=0; $x< sizeof($additionalPhones); $x++ ) { ?>
			   		<span class="phone-field"><p><input type="text" name="phone_num[]" size="24" value="<?php echo $additionalPhones[$x]['phone_num']; ?>" /> <a href="javascript://" title="Click to remove phone" class="remove-phone">Remove</a></p></span>
			   <?php } 
			   }?>
			   </div>
			  </td>
			</tr>
			<tr>
			  <td nowrap="nowrap" align="right">Cell:</td>
			  <td><input type="text" name="cnt_cell" value="<?php echo htmlentities($row_userdet['mobile'], ENT_COMPAT, 'utf-8'); ?>" size="24" /><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userdet['mobile']; ?>')"> <img src="images/mobile_icon.png" width="20" height="20" align="absmiddle" /></a>
			  <span class="validation-error"></span>
			  </td>
			</tr>
			<tr>
			  <td nowrap="nowrap" align="right">Email:</td>
			  <td><input type="text" name="cnt_email" id="cnt_email" value="<?php echo htmlentities($row_userdet['email'], ENT_COMPAT, 'utf-8'); ?>" size="24" />
			   <a href="mailto:<?php echo htmlentities($row_userdet['email'], ENT_COMPAT, 'utf-8'); ?>"> <img src="images/email-icon24.png" width="16" height="16" align="absmiddle" /></a>
			   <span class="validation-error"></span>
			   </td>
			</tr>
			<!--
			<tr>
			  <td nowrap="nowrap" align="right">Birthday:</td>
			  <td><input type="text" name="cnt_bdate" readonly="readonly" id="cnt_bdate" value="<?php echo htmlentities($row_userdet['dob'], ENT_COMPAT, 'utf-8'); ?>" size="24" /></td>
			</tr>
			-->
			<tr>
			  <td nowrap="nowrap" align="right">Institution:</td>
			  <td><select name="cnt_company" style="max-width:300px">
				<?php
		do {  
		?>
				<option value="<?php echo $row_complist['comp_id']?>"<?php if (!(strcmp($row_complist['Company'], $row_userdet['Company']))) {echo "selected=\"selected\"";} ?>><?php echo $row_complist['Company']?></option>
				<?php
		} while ($row_complist = mysql_fetch_assoc($complist));
		  $rows = mysql_num_rows($complist);
		  if($rows > 0) {
			  mysql_data_seek($complist, 0);
			  $row_complist = mysql_fetch_assoc($complist);
		  }
		?>
			  </select>
			  <?php  
			  $outcolor="";
			  $tve= $row_userdet['companytier'] ;
			  switch ($tve){ 
			  case 'Platinum':
				$outcolor= "#E5E4E2";
				break;
			  case 'Gold' :
				$outcolor= "#B8860B";	
				break;
			  case 'Silver' :
				$outcolor= "#C0C0C0";	
				break;
			  case 'Blue' :
				$outcolor= "#0A238C";	
				break;
			case 'Bronze' :
				$outcolor= "#f4a460";	
				break;
				case 'N/A' :
				case 'Not Assigned' :
				$outcolor= "#fff";	
				break;
			  default :
				$outcolor= "#CE1620";
			  }
			  ?>
			  <span style="width:100%;display:block; text-indent:5px;  font-size:1em; background:<?php echo $outcolor; ?>; padding:4px; border-radius:3px;"><?php echo $row_userdet['companytier']; ?></span>
			  <span class="validation-error"></span>
			  </td>
			</tr>
		  
      <tr>
          <td nowrap="nowrap" align="right">Hipchat Room:</td>
          <td>
            <?php 
                echo $hipchat_instance->get_hipchat_rooms();
             ?> 
          </td>
      </tr>
			
      <tr>
			<td nowrap="nowrap" align="left" colspan="2">
				<?php if(file_exists("uploads/clientp/".$colname_userdet."/1.jpg")) { ?>
				<input type="hidden" name="client_image" id="client_image" value="<?php if(file_exists("uploads/clientp/".$colname_userdet."/1.jpg"))  echo "uploads/clientp/".$colname_userdet."/1.jpg"; ?>" >
				<input type="hidden" name="client_imagesize" id="client_imagesize" value="<?php if(file_exists("uploads/clientp/".$colname_userdet."/1.jpg"))  echo filesize("uploads/clientp/".$colname_userdet."/1.jpg"); ?>" >
				<?php }  else { ?>
				<img style="padding:10px 0;" src="<?php echo $userObj->getGravatar(htmlentities($row_userdet['email'], ENT_COMPAT, 'utf-8'));?>" />
				<?php } ?>
				<div id="cpicture_client" style="text-align:center; padding-bottom:3px;">
				  <div id="picturedrop" class="picturedrop">
					  <p class="my-drop-message dz-message">Drop or Click<br />here<br />add picture</p>
				  </div>
				</div>
			</td>
			</tr>
			 <tr><td colspan="2">
				 <fieldset>
				<legend>Notes</legend>
				  
					<textarea name="cnt_note" id="cnt_note" onblur="noteupdate(<?php echo $row_userdet['id']; ?>,'#noteupresult');" cols="45" rows="5"><?php echo $row_userdet['note']; ?></textarea>
				
			  <p><small>Notes and keywords added here are searchable from the quicksearch box</small></p>
				  <div id="noteupresult"></div>
				</p>
			  </fieldset>
			
			</td></tr>
			<tr>
			  <td nowrap="nowrap" align="right">Jobtitle</td>
			  <td><input type="text" name="cnt_jobtitle" value="<?php echo htmlentities($row_userdet['jobtitle'], ENT_COMPAT, 'utf-8'); ?>" size="32" />
			  <span class="validation-error"></span>
			  </td>
			</tr>
			<tr>
			  <td colspan="2">
			  <fieldset>
				<legend>Bloomberg User?</legend>
			  <?php 
			  
				  $bloombergUserYes = $row_userdet['bloomberg_status'] == '1' ? 'selected="selected"' : '';
				  $bloombergUserNo = $row_userdet['bloomberg_status'] == '0' ? 'selected="selected"' : '';
				  $bloombergUserNotSet = ($bloombergUserYes == '' && $bloombergUserNo == '' ) ? 'selected="selected"' : '';
			  ?>
				<select name="bloomberguser_status" id="bloomberguser_status">
					<option value="" <?php echo $bloombergUserNotSet; ?>></option>
					<option value="0" <?php echo $bloombergUserNo; ?>>No</option>
					<option value="1" <?php echo $bloombergUserYes; ?>>Yes</option>
				</select>
				</fieldset>
			  </td>
			</tr>
      <tr>
        <td colspan="2">
        <fieldset>
        <legend>Protection Type</legend>

        <?php 

            #First check if the company status has not been set this is the overriding status regarrdless of the client status #

          
            if (isset($row_rsemail['drm_status_override'])) {
                $WebProt    = $row_rsemail['drm_status_override'] == '3' ? 'selected="selected"' : '';
                $DRMProt    = $row_rsemail['drm_status_override'] == '2' ? 'selected="selected"' : '';
                $NormalProt = $row_rsemail['drm_status_override'] == '1' ? 'selected="selected"' : '';
                $DRMNotSet  = $row_rsemail['drm_status_override'] == '' ? 'selected="selected"' : '';
            } 
						
						switch ($row_rsemail['company_drm_status']) {
							case '3':
								# code...
								if ($row_rsemail['company_webview_print'])
									$company_status = "Web Viewer with Print";
								else 
									$company_status = "Web Viewer";
								break;
							case '2':
								$company_status = "DRM";
								break;
							case '1':
								$company_status = "Watermark";
								break; 
							default: 
								break;
						}
				
        ?>
        
        <select name="cnt_override_drm" id="cnt_override_drm">
                    <option value="" <?php echo  $DRMNotSet; ?>>Company Protection (<?php echo $company_status;?>) </option>
                    <option value="1" <?php echo $NormalProt; ?>>Watermark</option>
                    <option value="2" <?php echo $DRMProt; ?>>DRM</option>
                    <option value="3" <?php echo $WebProt; ?>>Web Viewer</option>
                 
        </select>
              <div id="show_status"></div>
              <div id="webviewerwithprint" class="<?php if( $row_userdet['drm_status_override'] == '3' ) echo 'show'; else echo 'hide' ?>">              
              	<br />
        
								<?php $selected_webview_print = isset($row_userdet['user_webview_print']) ? $row_userdet['user_webview_print'] : '0'; ?>   

              	<label><input type="checkbox" id="webview_print" name="webview_print" value="1" <?php if( $selected_webview_print == 1 ) echo 'checked="checked"'; ?> /> Web Viewer Printing Allowed?</label>
							</div>                                              
        </fieldset>        
        </td>
      </tr>
      <tr>
        <td colspan="2">
        <fieldset>
        <legend>Portal Access</legend>

        <?php 
          $WebProt = $row_userdrm['drm_status'] == '3' ? 'selected="selected"' : '';
          $DRMProt = $row_userdrm['drm_status'] == '2' ? 'selected="selected"' : '';
          $NormalProt = $row_userdrm['drm_status'] == '1' ? 'selected="selected"' : '';
          $DRMNotSet = $row_userdrm['drm_status'] == '' ? 'selected="selected"' : '';
					
					$companyPortalAccessSetting = 'Disabled';
					if ($row_userdet['company_portal_access'] == '1') {
						$companyPortalAccessSetting = 'Enabled';
					}
					
        ?>        
        
        <select name="portal_access" id="portal_access">
                    <option value="">Company Setting [<?php echo $companyPortalAccessSetting;?>]</option>
                    <option value="1" <?php if($row_userdet['user_portal_access'] == '1') echo 'selected="selected"'; ?>>Enabled</option>
                    <option value="0" <?php if($row_userdet['user_portal_access'] == '0') echo 'selected="selected"'; ?>>Disabled</option>                    
        </select>

        </fieldset>        
        </td>
      </tr>      
			<tr style="display:none"> 
			  <td nowrap="nowrap" align="right">BB Username:</td>
			  <td><input type="text" name="cnt_blmuser" value="<?php echo htmlentities($row_userdet['bloomberg_user'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
			</tr>
			<tr> <td colspan="2"><fieldset><legend>Active client ?</legend> Active <input type="radio" name="cnt_active" value="1"  <?php if ($row_userdet['active']==1) {echo 'checked';} ?> />
			<input type="radio" name="cnt_active" value="0" <?php if ($row_userdet['active']==0) {echo 'checked';} ?> >Inactive</fieldset>
			</td></tr>
			<tr style="height:60px;">
			  <td nowrap="nowrap" align="right">&nbsp;</td>
        <br />
			  <td style="height:50px;"> <input id="btnUpdateRecord" type="button" value="Update record" />
			  <span id="updresp" style="height:50px;"> </span></font></td>
			</tr>
		  </table>
  <input type="hidden" name="cnt_Id" value="<?php echo $row_userdet['id']; ?>" />
  <input type="hidden" name="MM_update" value="updateuserdet" />
 </form>
 </fieldset>
   <div>
 <fieldset>
 <legend>Contact List Management</legend>
 <input type="hidden" name="MM_insert" value="addnewlist" />
 <table width="100%">
<tr style="vertical-align:top"><td width="50%"><fieldset><legend>On Lists</legend>
<span id="onlistspan">
<?php do { ?>
  <a id="onli<?php echo $row_onclientlist['tblFav_id'] ?>" href="#" onclick="$('#availlistspan').empty().html('<img src=images/ajax-loader.gif />');
$('#availlistspan').load('<?php echo("userclientlistavaillist.php?tblFav_id=".$row_onclientlist['tblFav_id']."&MM_delete=removefromlist&cnt_Id=".$row_userdet['id']."&sourcepage=clientdetails.php&MM_Username=".$_SESSION['MM_Username']) ?>'); $('#onli<?php echo $row_onclientlist['tblFav_id'] ?>').remove();" class="clicklink"><?php echo $row_onclientlist['listname']; ?><?php if($row_onclientlist['shared']==1){?><img src="images/redbox.png" width="5" height="5" border="0" /><?php } ?></a>
  <?php } while ($row_onclientlist = mysql_fetch_assoc($onclientlist)); ?>
</span>
</fieldset>
<img src="images/redbox.png" width="5" height="5" />= shared with other users</td>
  <td><fieldset><legend>Available lists</legend>
    <span id="availlistspan">
    <?php do { ?>
      <a id="avls<?php echo $row_availlist['listid']; ?>" href="#" onclick="$('#onlistspan').empty().html('<img src=images/ajax-loader.gif />'); $('#onlistspan').load('<?php echo("userclientlistlistajax.php?cnt_Id=".$row_userdet['id']."&fav_Username=".$_SESSION['MM_Username']."&listid=".$row_availlist['listid']."&tbl_cnt_Name_Desc=". urlencode($row_userdet['first_name']." ".$row_userdet['surname'])."&tbl_Tiering=1&fav_nick=none&MM_insert=addtolist") ?>'); $('#avls<?php echo $row_availlist['listid']; ?>').remove()" class="clicklink"><?php echo $row_availlist['listname']; ?><?php if($row_availlist['shared']==1){?><img src="images/redbox.png" width="5" height="5" border="0" /><?php } ?></a>
      <?php } while ($row_availlist = mysql_fetch_assoc($availlist)); ?>
    </span>
</fieldset>
<input type="button" value="Add" id="btnnewulist" /><input type="button" value="Remove"  id="btnremoveulist"/><input type="button" value="Modify"  id="btnmodulist"/>
<fieldset id="newulist">
<legend>Add new list</legend>
<form  id="frmaddnewlist" name="frmaddnewlist">
<span style="text-align:right;">
Listname: <input type="text" name="listname" value="" size="24" /><br />
	  <input type="checkbox" name="shared" value="1" style="display:none;"/> <br />
      <input type="checkbox" name="editablebyothers" value="1" style="display:none;" checked /><br /><input type="button" value="Cancel" id="btncancelnewulist" /><input type="button" value="Add" id="confaddulist" /></span><input type="hidden" name="MM_Username" value="<?php echo $colname_onclientlist ?>"  />
      <input type="hidden" name="MM_insert" value="frmaddnewlist"  />
      <input type="hidden" name="cnt_Id" value="<?php echo $colname_rscontacts ?>" />
      <input type="hidden" name="sourcepage" value="clientdetails.php" />
</form>
</fieldset>
<fieldset id="removeulist">
<legend>Remove list</legend>
<input type="button" value="Cancel" id="btncancelremoveulist" />
</fieldset>
<fieldset id="modulist">
<legend>Modify list</legend>
<input type="button" value="Cancel" id="btncancelmodulist" />
</fieldset>
</td></tr>
</table> 
 </fieldset>
 </div>
 </div>
</div>
      <div id="tabs1-2">
      <fieldset id="isresp" style="display:block; border:#CCC thin solid">
      <legend>Responsibilities</legend>

      <?php do { ?>
        <a href="#" id="r<?php echo $row_responsib['idcntresponsibility']; ?>" onclick="$('#r<?php echo $row_responsib['idcntresponsibility']; ?>').remove();showloader('#notresp'); $('#notresp').load('clientresponsibajax.php?cnt_Id=<?php echo $row_userdet['id']; ?>&idcntresponsibility=<?php echo $row_responsib['idcntresponsibility']; ?>&MM_removeresp=yes')" class="clicklink"><?php echo $row_responsib['response_desc']; ?></a>
        <?php } while ($row_responsib = mysql_fetch_assoc($responsib)); ?>
      </fieldset>
      <fieldset id="notresp" style="display:block; border:#CCC thin solid"><legend>Available List</legend> <?php do { ?>
        <a href="#" id="nr<?php echo $row_resplist['respons_id']; ?>" onclick="$('#nr<?php echo $row_resplist['respons_id']; ?>').remove();showloader('#isresp'); $('#isresp').load('clientresponsibajax.php?cnt_Id=<?php echo $row_userdet['id']; ?>&MM_addresp=donow&respons_id=<?php echo $row_resplist['respons_id']; ?>')" class="clicklink"><?php echo $row_resplist['response_desc']; ?></a>
        <?php } while ($row_resplist = mysql_fetch_assoc($resplist)); ?>
      </fieldset>
      </div>
      <div id="tabs1-3">
      <fieldset id="ismailsub" style="display:block; border:#CCC thin solid">
         <?php $isadmin = isset($_SESSION['MM_Username']) && (isAuthorized("x","Administrator", $_SESSION['MM_Username'], $_SESSION['MM_UserGroup']));?>
      <legend>Subscriptions></legend>
       <?php do { ?>
       <?php if($isadmin) { ?>
         <div id="ismailsub<?php echo $row_rsemail['id']; ?>" class="clicklink" ><a href="#"  onclick="$('#ismailsub<?php echo $row_rsemail['id']; ?>').remove();showloader('#isnotmailsub');$('#isnotmailsub').load('clientmailsubajax.php?contact_id=<?php echo $row_userdet['id']; ?>&interest_id=<?php echo $row_rsemail['id']; ?>&removemail=yes')" ><?php echo $row_rsemail['description']; ?></a><span class="superscript"><?php if($row_rsemail['drm_status'] == "2"){ echo "DRM" ; } else { echo "" ;}  ?></span></div>
       <?php }else { ?> 
       <div id="ismailsub<?php echo $row_rsemail['id']; ?>" class="clicklink" ><?php echo $row_rsemail['description']; ?><span class="superscript"><?php if($row_rsemail['drm_status'] == "2"){ echo "DRM" ; } else { echo "" ;}  ?></span></div>
       <?php } ?>
         <?php } while ($row_rsemail = mysql_fetch_assoc($rsemail)); ?>
      </fieldset>
      <fieldset id="isnotmailsub" style="display:block; border:#CCC thin solid">
      <legend>Other Available</legend>
      <?php do { ?>
        <?php if($isadmin) { ?>
              <div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>" class="clicklink"> <a href="#"  onclick="$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>i').toggle();"> <?php echo $row_rsnotemail['description']; ?></a><div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>i" style="display:inline;">&nbsp;<button onclick="showloader('#ismailsub');doemailsubchange('<?php echo $row_userdet['id']; ?>','<?php echo urlencode($row_rsnotemail['id']); ?>', '#ismailsub' );$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>').remove()">Add</button></div><script language="javascript">$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>i').hide();</script></div>
        <?php }else { ?> 
              <div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>" class="clicklink"> <?php echo $row_rsnotemail['description']; ?><div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>i" style="display:inline;">&nbsp;<button onclick="showloader('#ismailsub');doemailsubchange('<?php echo $row_userdet['id']; ?>','<?php echo urlencode($row_rsnotemail['id']); ?>', '#ismailsub' );$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>').remove()">Add</button></div><script language="javascript">$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>i').hide();</script></div>
        <?php } ?>
        <?php } while ($row_rsnotemail = mysql_fetch_assoc($rsnotemail)); ?>
      </fieldset>
      </div>
  </div>
</div>
<div class="content40">
<div id="tabs2">
      <ul>
        <li><a href="#tabs2-1">Recent Contact</a></li>
        <li><a href="#tabs2-2" onclick="loadColleagues(<?php echo $row_userdet['id']; ?>)">Colleagues</a></li>
		<li id="ipad-activity-tab"><a href="#tabs2-4">iOS Activity</a></li>
		<li id="android-activity-tab"><a href="#tabs2-5">Android Activity</a></li>
      </ul>
      <div id="tabs2-1">
		  <fieldset>
		  <legend>Notes and reminders</legend>
		  <input type="button" id="addnotebutton" value="Add Note" /><input type="button" value="Add reminder" id="cadremin" />
		  <input type="button" id="cancelnotebutton" value="Cancel" />
		  <form id="addreminderform" style="background:#ccc;" >
		  <table>
		  <tr><td>Reminder:<br />
		  <input name="remtext" type="text" size="30" /><input type="button" value="Save" /></td></tr>
		  </table>
		  </form>
		  <form id="addnoteform" style="background:#ccc;"><table>
		  <tr><td valign="top">Note:<br /> 
        <textarea name="note_txt" rows="4"></textarea></td><td valign="top">Date:<br /> 
			<input name="note_date" type="text" id="note_date" value="<?php echo date("Y-m-d"); ?>" /><br />
      Time<br />
                <input type="time" name="note_time" id="note_time" value=""><br />
      Type<br /> <select name="note_type" id="note_type">
						  <?php
		do {  
		?>
						  <option value="<?php echo $row_rsCtype['ContactType']?>"><?php echo $row_rsCtype['ContactType']?></option>
						  <?php
		} while ($row_rsCtype = mysql_fetch_assoc($rsCtype));
		  $rows = mysql_num_rows($rsCtype);
		  if($rows > 0) {
			  mysql_data_seek($rsCtype, 0);
			  $row_rsCtype = mysql_fetch_assoc($rsCtype);
		  }
		?>
						</select>
            <div style="display: <?php echo $show_analyst_dropdown;?>">
                    Note By<br/>
                    <?php 
                      echo $analyst_select;
                    ?>
            </div>
          </td><td><input type="button" id="savenewnotebutton" value="Save" /><input type="hidden" name="MM_noteadd" value="addnote" />
						<input type="hidden" name="note_by" value="<?php echo $_SESSION['MM_Username']; ?>" />
						<input type="hidden" name="notebyNick" value="<?php echo $row_userd['user_fname']; ?>" />
						<input type="hidden" name="note_cnt_Id" value="<?php echo $row_userdet['id']; ?>" />
						<input type="hidden" name="note_cnt_name" value="<?php echo $row_userdet['first_name']; ?> <?php echo $row_userdet['surname']; ?>" />
						<input type="hidden" name="cntCompany" value="<?php echo $row_userdet['company_id']; ?>" />
					   </td></tr>
		
		  </table>
		  </form>
		  </fieldset>
		 <fieldset>
		 	<legend>Contact notes</legend>
			<div id="notes-edit-form" title="Edit Note">
				<form id="update-notes-form" method="post" action="">
					<label for="editnote">Note:</label><br />
					 <textarea id="editnote" name="editnote" rows="10" cols="45"></textarea>
					 <input type="hidden" id="editnoteId" name="editnoteId" value="" />
					 <input type="hidden" id="editRowIdx" name="editRowIdx" value="" />
				</form>
			</div>
			<div id="confirm-delete" title="Delete note?">
				<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you would like to delete this note?</p>
				<input type="hidden" id="deleteNoteId" name="deleteNoteId" value=""  />
				<input type="hidden" id="delRowIndx" name="delRowIndx" value=""  />
				
			</div>
				 <div id="cnotes">
			 	<table border="0" width="100%" id="contact-list-table" class="table table-striped dataTable" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th>Date</th>
							<th>Note</th>
							<th>Type</th>
							<th>By</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php if ($notesDataSize > 0) {
							for($x=0; $x < $notesDataSize; $x++ ){
						 ?>
						<tr>
							<td><?php echo $notesData[$x]['note_date']; ?><br /> <?php echo $notesData[$x]['note_time']; ?></td>
							<td><?php echo $notesData[$x]['note_txt']; ?></td>
							<td><?php echo $notesData[$x]['note_type']; ?></td>
							<td><?php echo $notesData[$x]['note_by']; ?></td>
							<td><a href="javascript://" class="edit-button" data-id="<?php echo $notesData[$x]['note_Id']; ?>" title="Click to edit"><small>Edit</small></a><br /><br />
								<a href="javascript://" class="delete-button" data-id="<?php echo $notesData[$x]['note_Id']; ?>" title="Click to delete"><small>Delete</small></a>
							</td>
						</tr>
						<?php } 
						 }
						?>
					</tbody>
				</table>
				<p>&nbsp;</p>

			 </div>
		   </fieldset>
 			 </div>
      <div id="tabs2-2"></div>
<!--      <div id="tabs2-3"></div> -->
	  <div id="tabs2-4">
	  		<h3>iOS activity</h3>
			<h4>Last Sync</h4>
			<table class="sync-table" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<th>Device Name</th>
						<th>Device Model</th>
						<th>OS Version</th>
						<th>Time</th>
						<th>Date</th>
				</thead>
				<tbody id="ios-sync-data">
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</tbody>
			</table>
		
	  </div>
	  <div id="tabs2-5">
	  		<h3>Android activity</h3>
			<h4>Last Sync</h4>
			<table class="sync-table" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<th>Device Name</th>
						<th>Device Model</th>
						<th>OS Version</th>
						<th>Time</th>
						<th>Date</th>
				</thead>
				<tbody id="android-sync-data">
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</tbody>
			</table>
	  </div>
  </div>
</div><div id="phonebox"></div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php

mysql_free_result($reminders);
mysql_free_result($userdet);
mysql_free_result($complist);
mysql_free_result($responsib);
mysql_free_result($resplist);
//mysql_free_result($rscontacts);
mysql_free_result($rsemail);
mysql_free_result($rsnotemail);
mysql_free_result($userd);
mysql_free_result($rsCtype);
mysql_free_result($onclientlist);
mysql_free_result($availlist);
mysql_free_result($rsaddphone);
?>