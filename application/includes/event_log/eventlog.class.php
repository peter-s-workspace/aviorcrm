<?php
//Prevent the script from timing out when processing the export
ini_set('max_execution_time', 180);
/**
* Class is used for logging user events
* @version 1.1
* Usage:
* <code>
* <?php
*  $eventLogObj->save(array(
*  	'event' => 'EVENT_ALIAS',
*	'user' => 'Username', 
*	'affected_user' => 'AffectedUser Fullname',
*	'affected_type' => 'user'
*	));
* ?>
* </code>
*/
class EventLog{

	const OFFSET = 10000;
	/**
	* @param string tableName Name of the table to log to.
	*/
	public $tableName = "tblevent_logs";
	
	/**
	* @param array events Holds the events aliases to be used for logging throughout the site.
	*/
	public $events  = array();

	/**
	* The following events are stored in the tblevents table. We are using the event_alias for readability purposes. This also makes it easier
	* to change the description for each alias. These aliases should not be changed. The numbers represent the associated IDs in the database.
	**/	
	function __construct() {
		
		$this->events = array(
			"ADD_USER" => 1,
			"UPDATE_USER" => 2,
			"DELETE_USER" => 3,
			"UPDATE_USER_PWD" => 4,
			"ADD_CLIENT" => 5,
			"UPDATE_CLIENT_DETAILS" => 6,
			"UPDATE_CLIENT_INTERESTS" => 7,
			"UPDATE_CLIENT_PIC" => 8,
			"DELETE_CLIENT" => 9,
			"REMOVE_CLIENT_MAILINGLIST" => 10,
			"ADD_CLIENT_TO_MAILINGLIST" => 11,
			"ADD_CLIENT_RESPONSIBILITY" => 12,
			"REMOVE_CLIENT_RESPONSIBILITY" => 13,
			"UPDATE_CLIENT_STATUS" => 14
		);
	}

	/**
	* Stores the Event in the database
	* @param array eventData
	*/

	public function save( $eventData ){
	
		include('Connections/CRMconnection.php');
		
		if( is_array( $eventData ) && sizeof( $eventData ) > 0 ) {
		
			$event = isset($eventData['event']) ? $eventData['event']: null;
			$event_id = array_key_exists($event,$this->events) ? $this->events[$event] : '';
			$user = isset($eventData['user']) ? $eventData['user']: null;
			$affected_user = isset($eventData['affected_user']) ? $eventData['affected_user']: null;
			$affected_usertype = isset($eventData['affected_type']) ? $eventData['affected_type']: null;
			$company = isset($eventData['company']) ? $eventData['company']: null;
			$before = isset($eventData['before']) ? $eventData['before']: null;
			$after = isset($eventData['after']) ? $eventData['after']: null;
			$date_created = date('Y-m-d H:i:s');
			$insertSQL = sprintf("INSERT INTO %s (event_id, user, affected_user, affected_usertype,company,before_values, after_values, date_created) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
					   $this->tableName,
					   $this->GetSQLValueString($event_id, "int"),
                       $this->GetSQLValueString($user, "text"),
                       $this->GetSQLValueString($affected_user, "text"),
					   $this->GetSQLValueString($affected_usertype, "text"),
					   $this->GetSQLValueString($company, "text"),
					   $this->GetSQLValueString($before, "text"),
					   $this->GetSQLValueString($after, "text"),
                       $this->GetSQLValueString($date_created, "text"));

		  mysql_select_db($database_CRMconnection, $CRMconnection);
		  $result = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());

		}
	}
		
	/**
	* Events log table does not have client ID, it uses firstname and lastname for affected user. 
	* Function returns a list of clients and the company they each belong to.
	*/
	protected function GetClientCompany()
	{
		require_once('Connections/CRMconnection.php');
		$sql = "SELECT 
					CONCAT(contacts.first_name,' ',contacts.surname) AS 'user',
					tblcompanylist.Company AS 'cnt_company'
				FROM 
					contacts , tblcompanylist
			    WHERE
			        contacts.company_id = tblcompanylist.comp_id";
		mysql_select_db($database_CRMconnection, $CRMconnection);		
		$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
		$recordSet = array();
		$returnData = array();

		while ($row = mysql_fetch_assoc($result)) {
			$recordSet[] = $row;
		}
		for ($x=0; $x < sizeof($recordSet); $x++) {
			$returnData[$recordSet[$x]['user']] = $recordSet[$x]['cnt_company'];
		}
		
		return $returnData;
	}
	/**
	* Exports the file in CSV format.
	* @version 1.1 Updated query to use username for User, if the user gets deleted in the user table.
	**/
	public function ExportLog(){

			$headers = array(
								'user'				=>  'User',
								'action' 			=>  'Action',
								'affected_user' 	=>  'Affected User',
	                            'affected_type' 	=>  'Affected User Type',
	                            'company' 			=>  'Company',
								'before' 			=>  'Before',
								'after' 			=>  'After',
								'date_created' 		=>  'Date Created'
							);
			
			if (ob_get_level()) 
			{
        		ob_end_clean();
    		}
    		
    		ob_implicit_flush();
    		$filename = "Avior-CRM-Event-log-".date('Y-m-d');
    		# Set the header information
			header('Content-type: application/csv');
			header('Content-Disposition: attachment; filename="'. $filename .'.csv"');
			header( "Pragma: no-cache" );
            header( "Expires: 0" );
            flush(); #send headers immediately so it doesn't appear to be hanging download

            $fp = fopen('php://output', 'w');
    		
            fputcsv($fp, $headers);
            //flush();

            $x = 0;
       
            do {
            	$export_logs = $this->fetch_data($x);
            	$x = $x + 1;
            	
            	foreach ($export_logs as $key => $log) {
            		
            		$log_array = [];
            		$log_array[0] = $log['User'];
            		$log_array[1] = $log['Action'];
            		$log_array[2] = $log['Affected User'];
            		$log_array[3] = $log['Affected User Type'];
            		$log_array[4] = $log['Company'];
            		$log_array[5] = $log['Before'];
            		$log_array[6] = $log['After'];
            		$log_array[7] = $log['Date Created'];
            		
            		fputcsv($fp,$log_array);
        			flush();	
            	}
            	
            	
            } while ($export_logs);
          	  exit;
          	  fclose($fp);
	}
	
	protected function fetch_data($x) {
		include('Connections/CRMconnection.php');
		$log_results 			= array();
		$offset 				= $x * EventLog::OFFSET;
		$log_query				= "SELECT DISTINCT
									IFNULL(CONCAT(tbluser.user_fname,' ',tbluser.user_lname),tblevent_logs.user) AS 'User',
									tblevents.event_description AS 'Action',
									tblevent_logs.affected_user AS 'Affected User',
									tblevent_logs.affected_usertype AS 'Affected User Type',
									tblevent_logs.company AS 'Company',
									tblevent_logs.before_values AS 'Before',
									tblevent_logs.after_values AS 'After',
									tblevent_logs.date_created AS 'Date Created'
								FROM
									tblevent_logs INNER JOIN tblevents ON tblevents.event_id = tblevent_logs.event_id
									LEFT OUTER JOIN tbluser ON tbluser.user_name = tblevent_logs.user
									
								ORDER BY
									tblevent_logs.date_created ASC limit ".EventLog::OFFSET." OFFSET $offset";
		mysql_select_db($database_CRMconnection, $CRMconnection);
		$run_log_query 			= mysql_query($log_query,$CRMconnection) or die (mysql_error());

		if ($run_log_query) {
			while ($row  = mysql_fetch_array($run_log_query)) 
			{
				$log_results [] = $row;
			}	
		} else {
			die("Error Running the query");
		}

		return $log_results;		
	}
	
	function GetSQLValueString($theValue, $theType ){

	  if (PHP_VERSION < 6) {
		$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
	  }
	  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
	  
	  switch ($theType) {
		case "text":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;    
		case "long":
		case "int":
		  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
		  break;
		case "double":
		  $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
		  break;
		case "date":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
	  }
	  return $theValue;
	}
	
	/*
	protected function array_to_scv($array, $header_row = true, $col_sep = ",", $row_sep = "\n", $qut = '"') {
		if (!is_array($array) or !is_array($array[0])) return false;

		if (ob_get_level()) {
			ob_end_clean();
		}

		$filename = "Avior-CRM-Event-log-".date('Y-m-d').".csv";
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=".$filename);
		header("Pragma: no-cache");
		header("Expires: 0");
				
		$fh =  fopen('php://output','w');
		$keys_array = array();
		$data_values = array();
		if ($header_row) {
			foreach ($array[0] as $key => $val) {
				$keys_array [] = $key;	
			}
		}
		
		fputcsv($fh, $keys_array);
		for ($i=0; $i < count($array); $i++) {
			$data_values [] = $array[$i];
		}

		/*
			This cleans the output that is send into the csv file some weird characters come through sometimes this hides any bad characters coming there
		

		ob_start();
		for ($i=0; $i <count($data_values); $i++) {
			fputcsv($fh, array_values($data_values[$i]));
			if ($i % 100 == 0) {
				flush();
			}
		}
		fclose($fh);
		exit;
		}
	
}
*/
}
?>