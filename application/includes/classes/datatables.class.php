<?php
/**
 * Class Datatables 
 * @author Skipjack IT Solutions CC < www.skipjack.co.za >
 * @category Skipjack
 * @package Skipjack backend
 *
 * @copyright Copyright (c) 2010 Skipjack IT Solutions CC @
 * NOTE: This code is intellectual property of
 * Skipjack IT Solutions CC and may not be
 * reproduced or used without prior permission.
 */
	
	
class Datatables{

	/**
	 * @var $limit_clause
	 * @var $order_clause
	 * @var $table_header
	 */
	var $limit_clause = array();
	var $order_clause = array();
	var $table_header = '';
	
	/**
	 *
	 */
	function __construct() {

	}
	
	/**
	 * Sets the columns that should be used.
	 * @param string $table_header
	 */	
	public function useColumns($table_header)
	{
		$this->table_header = $table_header;
	}
	
	/**
	 * Prepare data table sql
	 * @param string $sql
	 * @return $returnSql
	 */	
	public function PrepareDataTableSql($sql)
	{
		
		$returnSql = "";
		if( '' != trim($sql)){

			$returnSql = " SELECT SQL_CALC_FOUND_ROWS *  FROM( ".$sql.") all_data ";
			$returnSql .=" ".$this->SearchTable( 'listtable' );		
			$returnSql .= " ".$this->GetColumnOrder( 'listtable' );
			$returnSql .= " ".$this->GetPagingQuery( 'listtable' );	
		}

		return $returnSql;	
	}
	/**
	 * Gets a paging query
	 * @see PrepareDataTableSql()
	 * @return string $tablename
	 */	
	protected function GetPagingQuery($tablename)
	{
		
		$return_query = "";
		if (isset( $_POST['iDisplayStart'] ) && is_numeric( $_POST['iDisplayStart'] ) && $_POST['iDisplayLength'] != '-1') {			
			$return_query = "LIMIT ".addslashes($_POST['iDisplayStart']).", ".addslashes($_POST['iDisplayLength']);
		}
		$this->limit_clause[ $tablename ] = $return_query;
	   return $this->limit_clause[ $tablename ];

	}
	/**
	 * Gets the column order
	 * @see PrepareDataTableSql()
	 * @param string $tablename
	 * @return string $sOrder
	 */	
	protected function GetColumnOrder( $tablename ) {

		$aColumns = explode(',',$this->table_header);
		$sOrder = "";
		if ( isset( $_POST['iSortCol_0'] ) ) {
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_POST['iSortingCols'] ) ; $i++ ) {
				if ( $_POST[ 'bSortable_'.intval($_POST['iSortCol_'.$i]) ] == "true" ) {
					$sOrder .=  $aColumns[intval( $_POST['iSortCol_'.$i])]." ".addslashes( $_POST['sSortDir_'.$i] ) .", ";

				}
			}
			$sOrder = substr_replace( $sOrder, "", -2 );

			if ( trim($sOrder) == "ORDER BY" ) {
				$sOrder = "";
			}
		}
		$this->order_clause[ $tablename ] = $sOrder;

		return $sOrder;	
	}
	/**
	 * search table
	 * @see PrepareDataTableSql()
	 * @return $where_clause
	 */	
	public function SearchTable( $tablename ) {
		
		$aColumns = explode(',',$this->table_header);
		$number_of_columns = count( $aColumns );
		$where_clause = '';
		$search_term = '';
		
		for( $x=0; $x < $number_of_columns; $x++ ) {
			$search_term = '';
			$search_terms = array();			
			if( array_key_exists('bSearchable_'.$x,$_POST ) &&  'true' == $_POST['bSearchable_'.$x] &&  '' != $_POST['sSearch_'.$x]  ) {
			

				if ( $where_clause == "" ) {
					$where_clause = " WHERE ";
				} else {
					// $where_clause .= " AND ";
					$where_clause .= " OR ";					
				}
				$search_term = trim($_POST['sSearch_'.$x]);
				$search_terms = explode(',',$search_term);
				if( sizeof($search_terms) > 1 ) {
					$where_clause .= '(';
				}
				for( $k=0; $k < sizeof($search_terms); $k++ ) {					

					if (!strpos('s__',$search_terms[$k]) === false) {
						$text_begins_with = false;
					} else {
						$text_begins_with = true;
						$search_terms[$k] = str_replace('s__','',$search_terms[$k]);
					}
					if( true == $text_begins_with ) {
						$where_clause .= " ".$aColumns[$x]." LIKE '%".addslashes($search_terms[$k])."%' ";	
					}else{
						$where_clause .= " ".$aColumns[$x]." LIKE '%".addslashes($search_terms[$k])."%' ";	
					}
					if(($k+1) != sizeof( $search_terms ) ) {
						$where_clause .= " OR ";
					}
				}
				if( sizeof($search_terms) > 1 ) {
					$where_clause .= ')';
				}

			} else if ( array_key_exists('bSearchable_'.$x,$_POST ) &&  'true' == $_POST['bSearchable_'.$x] && '' != $_POST['sSearch']  ) {

				if ( $where_clause == "" ) {
					$where_clause = " WHERE ";
				} else {
					$where_clause .= " OR ";
				}
				$where_clause .= " ".$aColumns[$x]." LIKE '%".addslashes( trim($_POST['sSearch']) )."%' ";
			} else if ( isset($_POST['sSearch']) && '' != $_POST['sSearch']  ) {				
				
				for( $x=0; $x < $number_of_columns; $x++ ) {
					
					if ( $where_clause == "" ) {
						$where_clause = " WHERE ";
					} else {
						$where_clause .= " OR ";
					}				
									
					$where_clause .= " ".$aColumns[$x]." LIKE '%".addslashes($_POST['sSearch'])."%' ";						
				}
			
			}
		}
		return $where_clause;
	}
	/**
	 * This function will be in charge of returning the json string to javascript.
	 * @param array $returnData
	 * @param int $totalNumberOfRecords
	 * @param string $tablename
	 * @param int $found_rows
	 * @param array $list_headers
	 * @return JSON $output
	 */		
	function PrepareReturnData( $returnData, $totalNumberOfRecords, $tablename, $found_rows ) {
	

		$aColumns = explode(',',$this->table_header);
		if( $found_rows == '' ) $found_rows = 0;
		
		$sEcho = isset($_POST['sEcho']) ? intval($_POST['sEcho']) : null;
		if ($sEcho == null) {
			$sEcho = isset($_GET['sEcho']) ? intval($_GET['sEcho']) : null;		
		}
		$output = array(
			"sEcho" => $sEcho,
			"iTotalRecords" => $totalNumberOfRecords,
			"iTotalDisplayRecords" => $found_rows,
			"aaData" => array()
		);		
		
		$returnDataSize = sizeof( $returnData );

		for( $x=0; $x < $returnDataSize; $x++ ) {
			$row = array();			
			for ( $i=0 ; $i < count($aColumns) ; $i++ ) {
				
				if ( $aColumns[$i] == "version" ) {
					/**
					 * Special output formatting for 'version' column 
					 */
					$row[] = ($returnData[$x][ $aColumns[$i] ]=="0") ? '-' : $returnData[$x][ $aColumns[$i] ];
					
				} else if ( $aColumns[$i] != '' &&  array_key_exists( $aColumns[$i], $returnData[$x])) {
					/**
					* General output
					 */
					$row[] = $returnData[$x][ $aColumns[$i] ];
				}
			}
			
			$output['aaData'][] = $row;
		}
	
		$jsonoutput = json_encode( $output,JSON_NUMERIC_CHECK );
		
		return $jsonoutput;
		
	 }
	 
}
