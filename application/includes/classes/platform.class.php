<?php


class Platform{

	private $database_CRMconnection;
	private $CRMconnection;
	
	public function __construct()
	{
	
		include('Connections/CRMconnection.php');		

		$this->database_CRMconnection = $database_CRMconnection;
		$this->CRMconnection = $CRMconnection;
	}
	
	public function getEnvironment()
	{
		$returnResult = '';
		$sql = "SELECT 
					config.value
				FROM 
					config  
				WHERE 
					config.key = 'platform'
				LIMIT 0,1";

		$returnData = array();
		$db_connected =  mysql_select_db($this->database_CRMconnection, $this->CRMconnection);		
		if ($db_connected) {
			$result = mysql_query($sql, $this->CRMconnection);
			if ($result) {
				$row = mysql_fetch_assoc($result);
				$returnResult = $row['value'];
			}
			mysql_free_result($result);
		}
		return $returnResult;
	}
	
	public function getSMTPDetails()
	{
		$returnData = array();
		$sql = "SELECT 
					config.key  AS 'key',
					config.value AS 'value'
				FROM
					config
				WHERE
					config.key != 'platform'";

		$returnData = array();
		$db_connected =  mysql_select_db($this->database_CRMconnection, $this->CRMconnection);		
		if ($db_connected) {
			
			$result = mysql_query($sql, $this->CRMconnection);
			
			if ($result) {
				while ($row = mysql_fetch_assoc($result)) {
					$returnData[$row['key']] = $row['value'];
				}
			}
			mysql_free_result($result);
		}

		return $returnData;
	}
	
	
	
	
}
