<?php
class Mitel
{
	private $mitel_jar_path;
	private $mitel_extension;
	private $number_to_dial;
	private $mitel_server;
	private $mitel_app_name;
	private $mitel_app_password;
	private $mitel_local_password;
	private $mitel_company_name;
	private $mitel_icp_address;
	private $session_id;
	private $curl_type;
	private $mitel_database_id;
	private $request_type;
	private $check_session_id_status = false;
	private $full_name;
	private $old_full_name;
	private $old_phone_number;
	private $new_number;

	static function create($db_schema, $connection) {
        try {
            return new Mitel($db_schema, $connection);
        }
        catch( Exception $e ) {
			error_log($e->getMessage(), 3, "error_log");
            //trigger_error($e->getMessage(), E_USER_ERROR);
            return null;
        }
    }

	public function __construct($db_schema, $connection) {
		$this->database_CRMconnection 	= $db_schema;
		$this->CRMconnection 			= $connection;
		######## Set and configure all the mitel parameters and data #######
		$this->get_mitel_config_data();
		
		$this->session_id 				= $this->mitel_session_id();
		
		if (is_null($this->session_id)) {
			throw new Exception("Error can not get the session id from mitel server");
		}
		else if (is_numeric($this->session_id)) {
			$this->check_session_id_status = true;
		}
		
	}

	public function mitel_session_id() {
		if (!empty($this->mitel_jar_path) && !empty($this->mitel_server) && !empty($this->mitel_app_name) && !empty($this->mitel_company_name) && !empty($this->mitel_app_password) && !empty($this->mitel_local_password)) {

			// java -cp OIGJavaCallControlSample.jar oigjavacallcontrolsample/Login 196.50.239.156 ACM_CRM_Integration 'Avior Capital Markets (Pty) Ltd' AviorCRM09091 AviorCRM09091
			exec($this->mitel_jar_path.'/Login' ." ". $this->mitel_server ." ". $this->mitel_app_name ." '". $this->mitel_company_name ."' ". $this->mitel_app_password ." ".$this->mitel_local_password, $response, $response_message);
			
			#### Return the response from the mitel server should be the session id or an error message ########
			return  $response[0];
		} else {
			return null;
		}
	}

	public function initiate_call ($extension, $phone_number) {
		
		######### pass the session id status check if there is an error the string is returned else the session id which is numeric will be returned, stop making a call if the server failed #########
		if (!empty($extension) && !empty($phone_number) && $this->check_session_id_status) {
				$this->mitel_extension 			= filter_var($extension, FILTER_SANITIZE_STRING);
				$this->number_to_dial 		 	= filter_var($phone_number, FILTER_SANITIZE_STRING);
				
				exec($this->mitel_jar_path."/MitelBridge" ." ".$this->mitel_extension ." "."0". $this->number_to_dial ."#"." ".$this->session_id ." ".$this->mitel_icp_address ." " .$this->mitel_server, $response, $response_message);
				########## This should return true of false depending on whether the call has been successful or not #######
				
				return $response[0];

		} else if (is_string($this->session_id)) {
				die("Can not initiate a call " . $this->session_id);
		} else {
				die("Can not initiate a call either extension/phone number is required");
		}

	}
	
	public function  get_mitel_config_data() {
		$mitel_jar_path_query  	= "SELECT * from avcrm.config;";
		mysql_select_db($this->database_CRMconnection);
		$run_query 				= mysql_query($mitel_jar_path_query, $this->CRMconnection) or die("Error Running the query");
	
		while ($response = mysql_fetch_array($run_query)) {
			switch ($response['key']) {
				case 'mitel_server':
					$this->mitel_server 			= $response['value'];
					break;
				case 'java_path':
					$this->mitel_jar_path 			= $response['value'];
					break;
				case 'mitel_app_name':
					$this->mitel_app_name 			= $response['value'];
					break;	
				case 'mitel_company_name':
					$this->mitel_company_name 		= $response['value'];
					break;	
				case 'mitel_local_password':
					$this->mitel_local_password		= $response['value'];	
					break;
				case 'mitel_icp_address':
					$this->mitel_icp_address 		= $response['value'];	
					break;
				case 'mitel_database_id':
				    $this->mitel_database_id 		= $response['value'];
				    break;				
				case 'mitel_app_password':
					$this->mitel_app_password 		= $response['value'];
					break;   
				default:
					#### dont do nothing here #########
					break;
			}

		}		
	}

	public function  format_number ($phone_number) {
		if (substr($phone_number,0,2) == '00') {
			return preg_replace('/00/', '', $phone_number,1);
	
		}  else if (substr($phone_number, 0,1) == '0') {
			return preg_replace('/0/', '27', $phone_number,1);
			
		} else {
			return $phone_number;
		}
	}

	public function insert_update_mitel_telephone_directory($action_type, array $user_data) {
		############ This Needs proper Testing first ###############
			######### Check if the session has been set successfully before running the curl ########

			if (!$this->check_session_id_status) {
				return array('status' => false, 'message' => "There was an error updating the telephone directory from mitel server");				
			}
			
			$this->full_name 					= rawurlencode($user_data['last_name'] .','. $user_data['first_name']); 
			$this->new_number 					= $this->format_number($user_data['user_phone']);

			switch ($action_type) {
				case 'insert':
					$this->request_type 		= "POST";
					$this->curl_type 			= "http://".$this->mitel_server."/mitel/oig/rest/resources/v1/databases/views/telephonedirectory?databaseId=".$this->mitel_database_id."&number=0~".$this->new_number."&name=".$this->full_name."&primeName=0&privacy=0";
					break;
				case 'update':
					###### only when updating you gonna need the old first/last name #######
					$this->old_full_name 		= $user_data['old_user_last_name'] .','.$user_data['old_user_first_name'];
					$this->old_phone_number 	= $this->format_number($user_data['old_user_phone']);

					$this->request_type 		= "PUT";	
					$this->curl_type 			= "http://".$this->mitel_server."/mitel/oig/rest/resources/v1/databases/views/telephonedirectory?databaseId=".$this->mitel_database_id."&number=0~".$this->old_phone_number."&name=".$this->old_full_name."&primeName=0&privacy=0&newName=".$this->full_name."&newNumber=0~".$this->new_number."";
					break;
				default:
					die("Action type is invalid");
					break;
			}

			############################################
			error_log($this->curl_type, 3, "error.log");
			####### Curl code to be called here ########
			
			$curl = curl_init();
			curl_setopt_array($curl, array(
				  CURLOPT_URL 				=> $this->curl_type,
				  CURLOPT_RETURNTRANSFER 	=> true,
				  CURLOPT_ENCODING	 		=> "",
				  CURLOPT_MAXREDIRS 		=> 10,
				  CURLOPT_TIMEOUT 			=> 30,
				  CURLOPT_HTTP_VERSION 		=> CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST 	=> $this->request_type,
				  CURLOPT_HTTPHEADER 		=> array(
				    "authorization:" .$this->session_id,
				    "cache-control: no-cache",
				    "content-type: application/x-www-form-urlencoded",
				    "postman-token: 579c9578-2f0a-1e42-4b3a-644c7e860432"
				  ),
				));

			$response 	= curl_exec($curl);
			$err 		= curl_error($curl);
			
			curl_close($curl);
		
			if ($err) {
			  ############ handle the error back to the update view  return an array parameters ########
			  error_log($err, 3, "error.log");	
			  return array('status'=> false, 'message' => $err);
			  //return array('status' => true, 'message' => json_encode(array('newRecord'=>array('name'=>$this->full_name,'number'=> $user_data['user_phone']))));

			} else {

			  error_log($response, 3 , "error.log");		
			  return array('status' => true, 'message' => $response);
			}	
			
	
	}
}
?>