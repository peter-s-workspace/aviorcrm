<?php
##############################################
/* 
	Hipchat class that can be extended alot can be improved in the code and maybe increase,
	The security, this class should be added as a plugin function into the core code, which
	should make maintainance much easier
	
	The hard coded token keys can be set and renewed via the admin portal if possible aswell
*/
##############################################
class hipchat
{
	private $auth_key;
	private $client;
	private $rooms;
	private $roomAPI;
	private $authorize;
	private $selected_options;
	private $add_room_response;
	private $model_room;
	private $model_message;
	private $message_status;
	private $users;
	private $userApi;
	private $schema;
	private $database_CRMconnection;
	private $read_room_key;
	private $get_users_key;
	private $add_room_key;
	private $send_message_key;

	public function __construct($default_key) {
		//load all the dependencies of the hipchat api
		require_once ('vendor/autoload.php');
		include('Connections/CRMconnection.php');
		$this->schema 			= $database_CRMconnection;
		$this->database_CRMconnection = $CRMconnection;
		$this->config_connection();

		if (empty($default_key)) {
			$default_key = $this->read_room_key;
		} 

		$this->authorize 		= new GorkaLaucirica\HipchatAPIv2Client\Auth\OAuth2($default_key);
		$this->client 			= new GorkaLaucirica\HipchatAPIv2Client\Client($this->authorize, null, "https://avior.hipchat.com");
		$this->roomAPI 			= new GorkaLaucirica\HipchatAPIv2Client\API\RoomAPI($this->client);
		$this->model_room 		= new GorkaLaucirica\HipchatAPIv2Client\Model\Room();
		$this->model_message 	= new GorkaLaucirica\HipchatAPIv2Client\Model\Message();
		$this->userApi			= new GorkaLaucirica\HipchatAPIv2Client\API\UserAPI($this->client);
	}


	########### This is suppose to be changed rather return the array of data back and construct the selection type in the current context instead of building its here ##################################
	public function get_hipchat_rooms() {
		$this->rooms 			 	 = $this->roomAPI->getRooms();
		$this->selected_options	 	 = "<select class='js-example-basic-multiple'  id='hipchat_rooms' name='hipchat_rooms[]'  multiple='multiple'>";
	
		foreach ($this->rooms as $key => $room) {
			$this->selected_options .= "<option value=".$room->getId().">".$room->getName()."</option>";
		}
		$this->selected_options 	.= "</select>";
		
		return $this->selected_options;
	}
	public function config_connection () {
			$get_config_data = "SELECT * FROM avcrm.config";
			mysql_select_db($this->schema, $this->database_CRMconnection);

			$run_query 		= mysql_query($get_config_data, $this->database_CRMconnection);
			while ($row 	= mysql_fetch_array($run_query)) {
				   $key  	= $row['key'];
				   switch ($key) {
				   	case 'add_room_key':
				   		$this->add_room_key  	  	= $row['value'];
				   		break;
				   	case 'send_message_key':
				   		$this->send_message_key   	= $row['value'];
				   	  	break;
				   	 case 'get_users_key':
				   	 	$this->get_users_key 		= $row['value']; 
				   	 	break;		
				   	 case 'read_room_key':
				   	 	$this->read_room_key 		= $row['value'];
				   	 	break;		
				   	default:
				   		# code...
				   		break;
				   }
			}
	}

	public function get_all_rooms () {
		$this->rooms 				= $this->roomAPI->getRooms();
		if (!empty($this->rooms)) {
			return $this->rooms;
		} else {
			return false;
		}
	}

	public function get_all_users() {
		$this->__construct($this->get_users_key);
		$this->users 				= $this->userApi->getAllUsers();
		if (!empty($this->users)) {
			return $this->users;
		} else {
			return false;
		}
	}

	public function add_hipchat_room($chat_room_name)  {
		$this->__construct($this->add_room_key);
		try {
			$this->add_room_response 	= $this->roomAPI->createRoom($this->model_room->setName($chat_room_name));
		} catch (Exception $e) {
			############## save the room into the database and its id ###########################
			return array('status' => false, 'message'=> $e->getMessage());
		}
		return $this->add_room_response;
	}

	public function send_hipchat_message ($roomId, $user_send_from, $user_message) {
		$this->__construct($this->send_message_key);
		$this->message_status = $this->roomAPI->sendRoomNotification($roomId,$this->model_message->setMessage($user_message),$this->model_message->setFrom($user_send_from), $this->model_message->setNotify('true'));

		return $this->message_status;
	}
}
?>