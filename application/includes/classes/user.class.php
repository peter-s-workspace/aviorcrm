<?php
/**
* Class is used for getting the user profile, whether user is client or actual user.
* @version 1.0 
**/
class User{


	/**
	* @param userType can either be user or client.
	*/
	protected $userType;
	
	/**
	* @param userFields an array containing all the user fields that are needed together with their labels.
	*/
	protected $userFields = array();
	
	/**
	* @param userFields an array containing all the client fields that are needed together with their labels.
	*/
	protected $clientFields = array();

	/**
	* Set the userType to default to user. set all required userfiilds and client fields.
	* 
	**/	
	function __construct() {
		
		$this->userType = 'user';
		
		$this->userFields = array(
						"user_id" => "User ID",
						"user_name" => "User Name",
						"user_fname" => "First Name",
						"user_lname" => "Last Name",
						"user_email" => "Email",
						"user_level" => "User Level",
						"user_phone" => "Phone",
						"user_nick" => "Nick Name",
						"user_bday" => "Birthday",
						"user_clist" => "Client List",
						"user_coverage" => "Coverage",
						"popbox" => "PopBox",
						"user_phone_ip" => "User Phone IP");

		$this->clientFields = array(
						"id" => "Client ID",
						"first_name" => "First Name",
						"surname" => "Last Name",
						"landline" => "Phone Number",
						"fax" => "Fax",
						"mobile" => "Cellphone",
						"email" => "Email",
						"company" => "Company",
						"jobtitle" => "Job Title",
						"bloomberg_user" => "Bloomberg User",
						"note" => "Note",
						"favourite" => "Favourite",
						"dob" => "Birthday",
						"active" => "Active");
	}
	
	function Logout()
	{
		unset($_SESSION['MM_Username'],$_SESSION['MM_UserGroup'],$_SESSION['PrevUrl']);
		header('Location: login.php');
		exit();
	}

	/**
	* Returns the form fields in a readable format.
	* @param array formData. POST/GET form data.
	* @return string returnData json encoded string with the field labels and data. Only returning values that are available in the array.
	*/
	public function GetReadableFields( $formData ){
		
		$returnData = '';
		$newFormData = array();
		
		if( is_array( $formData ) && sizeof( $formData ) > 0 ) {
			
				switch( $this->userType ) {
					case 'user':
					foreach( $formData as $key => $value ) {	
						$new_key = isset($this->userFields[$key]) ? $this->userFields[$key] : null;
						if( isset($new_key )) $newFormData[$new_key] = $value;
					}
					break;
					case 'client':
					foreach( $formData as $key => $value ) {	
						$new_key = isset($this->clientFields[$key]) ? $this->clientFields[$key] : null;
						if( isset($new_key )) $newFormData[$new_key] = $value;
					}
					break;
				}
				
				if( sizeof($newFormData) > 0 ) {
					$returnData = json_encode($newFormData,true);
				}
		}
		return $returnData;
	}
	
	/**
	* Set the user Type. If the user type is not set, then the default value, or the last set value will be used.
	*/
	public function setType( $userType ){
	
		if( $userType != '' ) {
			$this->userType = $userType;
		}
		return $this;
	}	
	/**
	* Get User Profile. Uses the usertype
	* @see setType()
	* @param int userID could be client or user ID.
	* @returns string returnData json encoded string with values user/client details that are stored in the database.
	**/
	public function GetProfile( $userID ){
	
		include('Connections/CRMconnection.php');
		
		$returnData = '';
		switch ( $this->userType ) {
		
			case 'user':
				$sql = sprintf("SELECT
							   tbluser.user_id AS 'User ID',
							   tbluser.user_name AS 'User Name',
							   tbluser.user_fname AS 'First Name',
							   tbluser.user_lname AS 'Last Name',
							   tbluser.user_email AS 'Email',
							   tbluser.user_level AS 'User Level',
							   tbluser.user_phone AS 'Phone',
							   tbluser.user_nick AS 'Nick Name',
							   tbluser.user_bday AS 'Birthday',
							   tbluser.user_clist AS 'Client List',
							   tbluser.user_coverage AS 'Coverage',
							   tbluser.popbox AS 'PopBox',
							   tbluser.user_phone_ip AS 'User Phone IP'
						FROM 
							tbluser
						WHERE
							tbluser.user_id=%s
						LIMIT 0,1", $this->GetSQLValueString($userID, "int") );

					mysql_select_db($database_CRMconnection, $CRMconnection);		
					$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
					$recordSet = array();	
					break;

			case 'client':

				$sql = sprintf("SELECT
						   contacts.id AS 'Client ID',
						   contacts.first_name AS 'First Name',
						   contacts.surname AS 'Last Name',
						   contacts.landline AS 'Phone Number',
						   contacts.fax AS 'Fax',
						   contacts.mobile AS 'Cellphone',
						   contacts.email AS 'Email',
						   tblcompanylist.Company AS 'Company',
						   contacts.jobtitle AS 'Job Title',
						   contacts.bloomberg_user AS 'blm User',
						   contacts.note AS 'Note',
						   contacts.favourite AS 'Favourite',
						   contacts.dob AS 'Birthday',
						   contacts.active AS 'Active'
					FROM 
						contacts , tblcompanylist
					WHERE
					    tblcompanylist.comp_id = contacts.company_id
					AND
						 contacts.id=%s
					LIMIT 0,1", $this->GetSQLValueString($userID, "int") );

				mysql_select_db($database_CRMconnection, $CRMconnection);
				$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
				$recordSet = array();		
				break;	
		}
		while ($row = mysql_fetch_assoc($result)) {
			$recordSet[] = $row;
		}

		if( sizeof( $recordSet )  > 0 ) {
			$returnData = json_encode($recordSet[0], true);
		}
		return $returnData;
	}
	
	/**
	* @return Json encoded Client Interests.
	*/
	public function GetClientInterests( $clientID ) {
	
		include('Connections/CRMconnection.php');
		
		$returnData = '';
		
		if( $clientID > 0 ) {
		
		$sql = sprintf("SELECT 
							GROUP_CONCAT(DISTINCT interests.description ORDER BY interests.id ASC SEPARATOR ', ' ) AS 'cntinterests'
						FROM 
							interests , contacts_interests
						WHERE 
							contacts_interests.contact_id = %s AND interests.id = contacts_interests.interest_id", $this->GetSQLValueString($clientID, "int") );

		
			mysql_select_db($database_CRMconnection, $CRMconnection);
			$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
			if( $result ) {
				$returnedInterests = $result ? mysql_fetch_assoc($result) : array();
				if( sizeof($returnedInterests) > 0 ) {
					$returnData = json_encode(explode(', ',$returnedInterests['cntinterests']), true);
				}
			}
		}
		return $returnData;
	}
	
	public function GetClientList(){
		
		include('Connections/CRMconnection.php');
		$returnData = array();
		
		$sql = "SELECT DISTINCT
					contacts.id,
					contacts.first_name,
					contacts.surname
				FROM 
					contacts
				ORDER BY
					contacts.first_name ASC, contacts.surname ASC";
		
		mysql_select_db($database_CRMconnection, $CRMconnection);
		$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
		if( $result ) {
				while ($row = mysql_fetch_assoc($result)) {
					$returnData[] = $row;
				}
		}
		
		return $returnData;
	}

    //Surely this is the same as getEmployees()
	public function GetClientListByCompanyName($companyName)
	{
		include('Connections/CRMconnection.php');
		$recordSet = array();
		
		if( $companyName != '' ){
		
			$sql = sprintf("SELECT DISTINCT
								contacts.id,
								contacts.first_name,
								contacts.surname
							FROM 
								contacts, tblcompanylist
							WHERE
								tblcompanylist.Company = %s
							AND
							    tblcompanylist.comp_id = contacts.company_id
							AND
								contacts.active = 1
							ORDER BY
								contacts.first_name ASC, contacts.surname ASC", $this->GetSQLValueString($companyName, "text") );

			mysql_select_db($database_CRMconnection, $CRMconnection);
			$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
			$recordSet = array();
			//print_r($sql);
			while ($row = mysql_fetch_assoc($result)) {
				$recordSet[] = $row;
			}
	
			return $recordSet;
		}
	}
	
	public function GetClientKAM( $clientID ) {
	
		include('Connections/CRMconnection.php');
		$recordSet = array();
		
		$sql =  sprintf("SELECT 
					tblcompanylist.id_kam1 AS 'id_kam1',
					tblcompanylist.id_kam2 AS 'id_kam2',
					CONCAT(kam1.first_name,' ',kam1.surname) AS 'kam1',
					CONCAT(kam2.first_name,' ',kam2.surname) AS 'kam2'
				FROM 
					contacts INNER JOIN  tblcompanylist ON tblcompanylist.comp_id=contacts.company_id
					LEFT OUTER JOIN contacts AS kam1 ON kam1.id = tblcompanylist.id_kam1
					LEFT OUTER JOIN contacts AS kam2 ON kam2.id = tblcompanylist.id_kam2
				WHERE
					contacts.id = %s", $this->GetSQLValueString($clientID, "int") );
		
		mysql_select_db($database_CRMconnection, $CRMconnection);
		$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
		$recordSet = array();
		
		while ($row = mysql_fetch_assoc($result)) {
			$recordSet[] = $row;
		}

		return $recordSet;		
	}
	
	public function GetClientDealers( $clientID ) {
		include('Connections/CRMconnection.php');
		$recordSet = array();
		
		$sql =  sprintf("SELECT 
					tblcompanylist.id_dealer1 AS 'id_dealer1',
					tblcompanylist.id_dealer2 AS 'id_dealer2',
					CONCAT(dealer1.first_name,' ',dealer1.surname) AS 'dealer1',
					CONCAT(dealer2.first_name,' ',dealer2.surname) AS 'dealer2'
				FROM 
					contacts INNER JOIN  tblcompanylist ON tblcompanylist.comp_id=contacts.company_id
					LEFT OUTER JOIN contacts AS dealer1 ON dealer1.id = tblcompanylist.id_dealer1
					LEFT OUTER JOIN contacts AS dealer2 ON dealer2.id = tblcompanylist.id_dealer2
				WHERE
					contacts.id = %s", $this->GetSQLValueString($clientID, "int") );
		
		mysql_select_db($database_CRMconnection, $CRMconnection);
		$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
		$recordSet = array();
		
		while ($row = mysql_fetch_assoc($result)) {
			$recordSet[] = $row;
		}
		if (sizeof($recordSet) > 0) {
				if ($recordSet[0]['id_dealer1'] == -1) {
					$recordSet[0]['dealer1'] = "CSA Only";
				}
		}

		return $recordSet;
	}
	
	/**
	* Updates usernames that are used in other tables.
	*/
	public function updateUserNameReferences($oldUserName, $newUserName)
	{
		if ($oldUserName != '' && $newUserName != '') {
			include('Connections/CRMconnection.php');
			///Update Notes.
			$sql = sprintf("UPDATE tblnotes SET tblnotes.note_by = %s WHERE tblnotes.note_by = %s", 
							 $this->GetSQLValueString($newUserName, "text"),
							 $this->GetSQLValueString($oldUserName, "text"));

			mysql_select_db($database_CRMconnection, $CRMconnection);
			$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
			
			//Client List Name
			$sql = sprintf("UPDATE cnt_listnames SET cnt_listnames.ownerusername = %s WHERE cnt_listnames.ownerusername = %s", 
							 $this->GetSQLValueString($newUserName, "text"),
							 $this->GetSQLValueString($oldUserName, "text"));
			$result = mysql_query($sql, $CRMconnection) or die(mysql_error());		
			
			//Client Favourites
			$sql = sprintf("UPDATE tblfavourites SET tblfavourites.fav_Username = %s WHERE tblfavourites.fav_Username  = %s", 
							 $this->GetSQLValueString($newUserName, "text"),
							 $this->GetSQLValueString($oldUserName, "text"));
			$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
			
			
			//Events log
			$sql = sprintf("UPDATE tblevent_logs SET tblevent_logs.user = %s WHERE tblevent_logs.user = %s", 
							 $this->GetSQLValueString($newUserName, "text"),
							 $this->GetSQLValueString($oldUserName, "text"));

			$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
			
		} 
	}
	/**
	* Update Client additional numbers.
	*/
	public function addClientAdditionalNumbers($clientId, $additionalPhoneNumbers)
	{
		if( $clientId > 0) {
			include('Connections/CRMconnection.php');
			//First remove all additional numbers associated with Client ID.
			$deleteSql = sprintf("DELETE FROM addphone WHERE addphone.cnt_id = %s",$this->GetSQLValueString($clientId, "int"));
			$result = mysql_query($deleteSql, $CRMconnection) or die(mysql_error());
			//Add new number of additional number array has any values
			if (sizeof($additionalPhoneNumbers) > 0) {
				//Get unique numbers, and update array keys..
				$clientPhoneNumbers = array_values(array_unique($additionalPhoneNumbers));
				for($x=0; $x < sizeof($clientPhoneNumbers); $x++ ){
					$sql = sprintf("INSERT INTO addphone(cnt_id,phone_num,phone_desc) VALUES (%s,%s,NULL)",
									$this->GetSQLValueString($clientId, "int"),
									 $this->GetSQLValueString($additionalPhoneNumbers[$x], "text"));
							$insertResult = mysql_query($sql, $CRMconnection) or die(mysql_error());
				}
			}
		}
		
	}
	
	/**
	* Send User Password 
	*/
	public function emailUserPassword($emailAddress, $password)
	{
		require_once 'includes/classes/platform.class.php';
		require_once "vendor/PHPMailer/PHPMailer/class.phpmailer.php";

		$send_result = "notsent";
		$send_message = '';
		
		if( $password != '' && $emailAddress != '') {
				
				$platformObj = new PlatForm();
				$smptDetails = $platformObj->getSMTPDetails();
					
				if (sizeof($smptDetails) > 0) {
					$smtpHost = isset($smptDetails['smtp_host']) ? $smptDetails['smtp_host'] : 'localhost';
					$smtpPort = isset($smptDetails['smtp_port']) ? $smptDetails['smtp_port'] : 25;
					$smtpUsername = isset($smptDetails['smtp_username']) ? trim($smptDetails['smtp_username']) : '';
					$smptPassword = isset($smptDetails['smpt_password']) ? trim($smptDetails['smpt_password']) : '';
					$emailFrom = isset($smptDetails['email_from']) ? trim($smptDetails['email_from']) : '';
					$subject  = "CRM Password Reset";
					$emailContent = "Your CRM password has been reset successfully. Your new password is: ".$password;
					try { 
						$mail = new PHPMailer(true);		
						$mail->IsSMTP(true); 
						$mail->SMTPDebug = false;
						
						$mail->SMTPAuth   = true;
						if ($smtpHost === "smtp.gmail.com")
							$mail->SMTPSecure = "tls";
						
						$mail->IsHTML(true);
						$mail->Port  = $smtpPort;
						$mail->Host = $smtpHost;
						if($smtpUsername != '' && $smtpUsername != '') {
							$mail->Username = $smtpUsername;
							$mail->Password = $smptPassword;
						}
						$mail->ClearAddresses();
						$mail->AddAddress($emailAddress);
						if( $emailFrom  != '') {
							$mail->SetFrom($emailFrom, 'Avior CRM');
						}
						$mail->Subject = $subject;
						$mail->Body =  $this->setHTML($subject,"<p>".$emailContent."</p>");
						$mail->AltBody = $emailContent;
						if($mail->Send()) {
							$send_result = "sent";
						}
						else {
							$send_result = $mail->ErrorInfo;
						}
						
					} catch (phpmailerException $e) {
						$send_message = $e->errorMessage(); 
					} catch (Exception $e) {
						$send_message = $e->getMessage(); 
					}
			}
		}
		if ($send_message != '')
			return $send_result." Error: ".$send_message;
		else
			return $send_result;		
	}
	/**
	*
	**/
	function ExportAllClient(){
	
		include('Connections/CRMconnection.php');
		
		$sql = "SELECT DISTINCT
					   contacts.id AS 'Client ID',
					   contacts.first_name AS 'First Name',
					   contacts.surname AS 'Last Name',
					   contacts.landline AS 'Phone',
					   contacts.fax AS 'Fax',
					   contacts.mobile AS 'Cellphone',
					   contacts.email AS 'Email',
					   tblcompanylist.Company AS 'Company',
					   GROUP_CONCAT(DISTINCT tblcompanytier.companytier) AS 'Client Tier',
					   contacts.jobtitle AS 'Job Title',
					   contacts.bloomberg_user AS 'Bloomberg user',
					   contacts.note AS 'Notes',
					   contacts.favourite AS 'Favourite',
					   contacts.dob AS 'Birthday',
					   contacts.active AS 'Active Client',

					   IF (ISNULL(contacts.drm_status), 'Use Company Setting', 
					   IF (contacts.drm_status=1, 'Normal Protection', 
					   IF (contacts.drm_status=2, 'DRM Protection', 
					   IF (contacts.drm_status=3, 'Web Viewer', 
					   'Error')))) as 'DRM Status',

					   IF (tblcompanylist.drm_status=2, 'DRM Protection', 
					   IF (tblcompanylist.drm_status=3, 'Web Viewer',	'Normal Protection')) as 'Company DRM Setting',

					   IF (ISNULL(contacts.drm_status), (IF (tblcompanylist.drm_status=2, 'DRM Protection', IF (contacts.drm_status=3, 'Web Viewer', 'Normal Protection')) ), 
					   IF (contacts.drm_status=1, 'Normal Protection', 
					   IF (contacts.drm_status=2, 'DRM Protection', 
					   IF (contacts.drm_status=3, 'Web Viewer', 
					   'Error')))) as 'DRM Status Applied',

					   CONCAT('\"',GROUP_CONCAT(DISTINCT interests.description ORDER BY interests.id ASC SEPARATOR '; '),'\"') AS 'Subscription Info',
					   CONCAT('\"',GROUP_CONCAT(DISTINCT responsiblist.response_desc SEPARATOR '; '),'\"') AS 'Responsibility'
					   
					   
				FROM 
					contacts LEFT OUTER JOIN contacts_interests ON contacts_interests.contact_id = contacts.id
					LEFT OUTER JOIN interests ON interests.id = contacts_interests.interest_id
					LEFT OUTER JOIN cntresponsibility ON cntresponsibility.cnt_id = contacts.id
					LEFT OUTER JOIN responsiblist ON responsiblist.respons_id = cntresponsibility.respons_id
					LEFT OUTER JOIN tblcompanylist ON tblcompanylist.comp_id = contacts.company_id
					LEFT OUTER JOIN tblcompanytier ON tblcompanytier.id_companytier = tblcompanylist.id_companytier
				GROUP BY
					contacts.id
				ORDER BY
					contacts.id ASC";

		mysql_select_db($database_CRMconnection, $CRMconnection);
		$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
		$recordSet = array();
		if( $result ) {
			while ($row = mysql_fetch_assoc($result)) {
				$recordSet[] = $row;
			}

            //Add 'Profile Picture' info
            for($i = 0; $i < count($recordSet); $i++){
                $id = $recordSet[$i]['Client ID'];
                $hasProfilePic = 0;
                if(file_exists('uploads/clientp/' . $id . '/1.jpg')) $hasProfilePic = 1;

                $recordSet[$i]['Profile Picture'] = $hasProfilePic;
            }

//            var_dump($recordSet);

			if(sizeof( $recordSet ) > 0 ){
			
                $fileData = $this->getCSV($recordSet);
				$fileData = html_entity_decode($fileData,ENT_QUOTES,"UTF-8");
				$fileData = utf8_decode($fileData);
				
				if( $fileData != '' ) {
					$filename = "Avior-CRM-Client-extract-".date('Y-m-d').".csv";
					header("Content-type: text/csv");
					header("Content-Disposition: attachment; filename=".$filename);
					header("Pragma: no-cache");
					header("Expires: 0");
					echo $fileData;
					exit();
				}
			}
		}
	}

	/**
	* Helper function
	*/
	private function setHTML($title,$content)
	{
		
		return "<html><head><title>".$title."</title></head><body>".$content."</body></html>";
	}
	function GetSQLValueString($theValue, $theType ){

	  if (PHP_VERSION < 6) {
		$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
	  }
	  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
	  
	  switch ($theType) {
		case "text":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;    
		case "long":
		case "int":
		  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
		  break;
		case "double":
		  $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
		  break;
		case "date":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
	  }
	  return $theValue;
	}
	
	protected function array_to_scv($array, $header_row = true, $col_sep = ",", $row_sep = "\n", $qut = '"') {
		
		if (!is_array($array) or !is_array($array[0])) return false;
		
		/**
		 * Header row.
		 */
		 $output = '';
		if ($header_row) {
			foreach ($array[0] as $key => $val) {
				/**
				 * Escaping quotes.
				 */
				$key = str_replace($qut, "$qut$qut", $key);
				$output .= "$col_sep$qut$key$qut";
			}
			$output = substr($output, 1)."\n";
		}
		/**
		 * Data rows.
		 */
		foreach ($array as $key => $val) {
			$tmp = '';
			foreach ($val as $cell_key => $cell_val) {
				/**
				 * Escaping quotes.
				 */
				$cell_val = str_replace($qut, "$qut$qut", $cell_val);
				$tmp .= "$col_sep$qut$cell_val$qut";
			}
			$output .= substr($tmp, 1).$row_sep;
		}
		
		return $output;
	}

    protected function getCSV($array){

        if(!is_array($array)) return false;

        $data = '';

        //Headers
        $headers = array();
        foreach($array[0] as $key => $val){
            array_push($headers , $key);
        }
        $data .= implode("," , $headers);
        $data .= "\n";

        //Content
        foreach($array as $key => $val){
            $lineArray = array();

            foreach($val as $cell_key => $cell_val){

                $cell_val = trim($cell_val);
                if(!ctype_alnum($cell_val)) $cell_val = '"' . $cell_val . '"';

                array_push($lineArray , $cell_val);
            }

            $data .= implode("," , $lineArray);
            $data .= "\n";
        }

        return $data;
		}
	
/**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string $email The email address
 * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
 * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
 * @param boole $img True to return a complete IMG tag False for just the URL
 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
 * @return String containing either just a URL or a complete image tag
 * @source https://gravatar.com/site/implement/images/php/
 */
	function getGravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";
    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
	}
	
}

?>