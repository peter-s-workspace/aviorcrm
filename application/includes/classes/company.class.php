<?php

class Company{

	private $id;
	private $database_CRMconnection;
	private $CRMconnection;

	public function __construct($id = 0)
	{
		$this->id = $id;
		include('Connections/CRMconnection.php');
		require_once('hipchat.class.php');
		$this->database_CRMconnection = $database_CRMconnection;
		$this->CRMconnection = $CRMconnection;

	}

	/**
	* Returns the name of the Company
	*/
	public function getName()
	{
		$companyName = '';
		if ($this->id > 0) {
			$sql = sprintf("SELECT

							tblcompanylist.Company AS 'company'
						FROM
							tblcompanylist
						WHERE
							tblcompanylist.comp_id = %s
						LIMIT 0,1", $this->GetSQLValueString($this->id, "int") );

			$returnData = $this->fetchResults( $sql );
			if ( sizeof($returnData) > 0) {
				$companyName = $returnData[0]['company'];
			}
		}
		return $companyName;
	}

	public function ExportAllRooms() {

			$get_company_hipchat_room_id 	= "SELECT hr.hipchat_room_id , cl.Company, CONCAT(ct.companytier,' ','Clients') AS companytier FROM tblcompanylist cl
											   LEFT JOIN tblcompany_hipchatrooms hr ON cl.comp_id = hr.company_id
											   INNER JOIN tblcompanytier ct ON cl.id_companytier = ct.id_companytier
											   ORDER by cl.Company ASC
												";

										

			$new_hipchat_instance = new hipchat();
								
			mysql_select_db($this->database_CRMconnection);
			$run_query 						= mysql_query($get_company_hipchat_room_id, $this->CRMconnection) or die('Error running the query ' . mysql_error());
			$company_hipchat_id 			= array();

			

			while ($row 					= mysql_fetch_array($run_query)) {
				$company					= $row['Company'];
					if ($company 	== $prev_company) {
						$company_hipchat_id[$company][] = $row['hipchat_room_id'];
						$company_hipchat_id[$company][] = $row['companytier'];
					} else {
						$company_hipchat_id[$company][] = $row['hipchat_room_id'];
						$company_hipchat_id[$company][] = $row['companytier'];
					}
				$prev_company 				= $company;
			}

			$get_client_hipchat_room_id 	= "SELECT hr.hipchat_room_id , CONCAT(c.first_name,' ', c.surname) 
											   AS full_name , CONCAT(ct.companytier ,' ','Clients') as companytier FROM contacts c
											   LEFT JOIN tblclient_hipchatrooms hr ON  c.id = hr.client_id
											   INNER JOIN tblcompanylist cl ON c.company_id = cl.comp_id
											   INNER JOIN tblcompanytier ct ON cl.id_companytier = ct.id_companytier
											   ORDER by c.id ASC
												";
			

			mysql_select_db($this->database_CRMconnection);
			$run_client_query 						= mysql_query($get_client_hipchat_room_id, $this->CRMconnection) or die('Error running the query ' . mysql_error());
			$client_hipchat_id 				= array();

			while ($row 					= mysql_fetch_array($run_client_query)) {
				$client_name			    = $row['full_name'];
					if ($client_name 		== $prev_client) {
						$client_hipchat_id[$client_name][]  = $row['hipchat_room_id'];
						$client_hipchat_id[$client_name][]  = $row['companytier'];
					} else {
						$client_hipchat_id[$client_name][] = $row['hipchat_room_id'];
						$client_hipchat_id[$client_name][]  = $row['companytier'];
					}
				$prev_client 				= $client_name;
			}			
			

 			$available_rooms = $new_hipchat_instance->get_all_rooms();

			$company_data_array 	= array();
			foreach ($company_hipchat_id as $key1 => $value1) {
				foreach ($value1 as $key => $value) {
					foreach ($available_rooms as $room_key => $room_name) {
 							if ($room_name->getId() == $value || strtolower($room_name->getName()) == strtolower($value)) 
 							{
 								$company_data_array[$room_name->getName()][] = $key1;
 							} else {
 								if ($room_name->getName() != $old_key) {
 									
 									$company_data_array [$room_name->getName()][]= $room_name->getName();
 								}
 								$old_key 	= $room_name->getName();
 							}
 							
 						}
 					
				}
			}


			$client_data_array = array();
			foreach ($client_hipchat_id as $key1 => $value1) {
				foreach ($value1 as $key => $value) {
					foreach ($available_rooms as $room_key => $room_name) {

 							if ($room_name->getId() == $value || strtolower($room_name->getName()) == strtolower($value)) 
 							{
 								$client_data_array[$room_name->getName()][] = $key1;
 							} else {
 								if ($room_name->getName() != $old_key) {
 									
 									$client_data_array [$room_name->getName()][]= $room_name->getName();
 								}
 								$old_key 	= $room_name->getName();
 							}
 							
 						}
 					
				}
			}

			$headers = array(
								'hipchat_room'				=>  'Hipchat room',
								'company' 					=>  'Company',
								'client'					=> 'Client'
							);
			
			if (ob_get_level()) 
			{
        		ob_end_clean();
    		}
    		

    		ob_implicit_flush();
    		$filename = "Avior-CRM-Hipchat-".date('Y-m-d');
    		# Set the header information
			header('Content-type: application/csv');
			header('Content-Disposition: attachment; filename="'. $filename .'.csv"');
			header( "Pragma: no-cache" );
            header( "Expires: 0" );
            flush(); #send headers immediately so it doesn't appear to be hanging download

            $fp = fopen('php://output', 'w');
    		
            fputcsv($fp, $headers);
			
			$csv_array = array();
			
			foreach ($company_data_array as $key => $value) {
				
					if (count(array_unique($value)) == 1 && count(array_unique($client_data_array[$key])) == 1) {
						$csv_array [0] = $key;
						$csv_array [1] = "";
						$csv_array [2] = "";
						fputcsv($fp, $csv_array);
						flush();
					} else {
						$format_company_data = str_replace($key,',', implode(",",array_unique($value)));
						$companys 	         = preg_replace('/,{2,}/', ',', trim($format_company_data, ','));
						
						$format_client_data  =  str_replace($key,',', implode(",",array_unique($client_data_array[$key])));
						$client 			 =  preg_replace('/,{2,}/', ',', trim($format_client_data, ','));
						
						$csv_array [0] = $key;
						$csv_array [1] = $companys;
						$csv_array [2] = $client;

						
						fputcsv($fp, $csv_array);
						flush();
					}
				
			}

			
			fclose($fp);
			exit;
	}
	public function getCompanyDetails()
	{
		$returnData = array();

		$sql = sprintf("SELECT
							tblcompanylist.comp_id AS 'id',
							tblcompanylist.Company AS 'company',
							tblcompanylist.id_geography AS 'id_geography',
							tblcompanylist.drm_status AS 'drm_status',
							tblcompanylist.company_rating AS 'company_rating',
							tblcompanylist.id_companytier AS 'id_companytier',
							tblcompanylist.id_companystatus AS 'id_companystatus',
							tblcompanylist.id_companytype AS 'id_companytype',
							tblcompanylist.id_kam1 AS 'id_kam1',
							tblcompanylist.id_kam2 AS 'id_kam2',
							tblcompanylist.id_dealer1 AS 'id_dealer1',
							tblcompanylist.id_dealer2 AS 'id_dealer2',
							tblcompanylist.address_1 AS 'address_1',
							tblcompanylist.address_2 AS 'address_2',
							tblcompanylist.address_3 AS 'address_3',
							tblcompanylist.address_4 AS 'address_4',
							tblcompanylist.switchboard_1 AS 'switchboard_1',
							tblcompanylist.switchboard_2 AS 'switchboard_2',
							tblcompanylist.website AS 'website',
							tblcompanylist.social_fb AS 'social_fb',
							tblcompanylist.social_twitter AS 'social_twitter',
							tblcompanylist.social_linkedin AS 'social_linkedin',
							tblcompanylist.portal_access AS 'portal_access',
							tblcompanylist.webview_print AS 'webview_print'														
						FROM
							tblcompanylist
						WHERE
							tblcompanylist.comp_id = %s
						LIMIT 0,1", $this->GetSQLValueString($this->id, "int") );

		$returnData = $this->fetchResults( $sql );

		return $returnData;
	}

	public function getCompanyProfile()
	{
			$returnData = array();

		$sql = sprintf("SELECT
							tblcompanylist.comp_id AS 'id',
							tblcompanylist.Company AS 'company',
							tblcompanygeography.name AS 'geography',
							tblcompanylist.drm_status AS 'drm_status',
							tblcompanylist.company_rating AS 'company_rating',
							tblcompanytier.companytier AS 'companytier',
							tblcompanystatus.company_status AS 'company_status',
							tblcompanytype.companytype AS 'companytype',
							tblcompanylist.id_kam1 AS 'id_kam1',
							CONCAT(kam1.first_name,' ',kam1.surname) AS 'kam1_name',
							tblcompanylist.id_kam2 AS 'id_kam2',
							CONCAT(kam2.first_name,' ',kam2.surname) AS 'kam2_name',
							tblcompanylist.id_dealer1 AS 'id_dealer1',
							CONCAT(dealer1.first_name,' ',dealer1.surname) AS 'dealer1_name',
							tblcompanylist.id_dealer2 AS 'id_dealer2',
							CONCAT(dealer2.first_name,' ',dealer2.surname) AS 'dealer2_name',
							tblcompanylist.address_1 AS 'address_1',
							tblcompanylist.address_2 AS 'address_2',
							tblcompanylist.address_3 AS 'address_3',
							tblcompanylist.address_4 AS 'address_4',
							tblcompanylist.switchboard_1 AS 'switchboard_1',
							tblcompanylist.switchboard_2 AS 'switchboard_2',
							tblcompanylist.website AS 'website',
							tblcompanylist.social_fb AS 'social_fb',
							tblcompanylist.social_twitter AS 'social_twitter',
							tblcompanylist.social_linkedin AS 'social_linkedin',
							tblcompanylist.portal_access AS 'portal_access',
							tblcompanylist.webview_print AS 'webview_print'														
						FROM
							tblcompanylist LEFT OUTER JOIN tblcompanystatus ON tblcompanystatus.id_companystatus = tblcompanylist.id_companystatus
							LEFT OUTER JOIN tblcompanygeography ON tblcompanygeography.id_geography =
							tblcompanylist.id_geography
							LEFT OUTER JOIN tblcompanytier ON tblcompanytier.id_companytier  = tblcompanylist.id_companytier
							LEFT OUTER JOIN tblcompanytype ON tblcompanytype.id_companytype = tblcompanylist.id_companytype
							LEFT OUTER JOIN contacts AS kam1 ON kam1.id = tblcompanylist.id_kam1
							LEFT OUTER JOIN contacts AS kam2 ON kam2.id = tblcompanylist.id_kam2
							LEFT OUTER JOIN contacts AS dealer1 ON dealer1.id = tblcompanylist.id_dealer1
							LEFT OUTER JOIN contacts AS dealer2 ON dealer2.id = tblcompanylist.id_dealer2
						WHERE
							tblcompanylist.comp_id  = %s
						LIMIT 0,1", $this->GetSQLValueString($this->id, "int") );

		$returnData = $this->fetchResults( $sql );

		return $returnData;
	}

	public function ExportAllCompany()
	{
		include_once('Connections/CRMconnection.php');

		$sql = "SELECT DISTINCT
							tblcompanylist.comp_id AS 'id',
							tblcompanylist.Company AS 'company',
							-- tblcompanylist.drm_status AS 'drm_status',
							CASE tblcompanylist.drm_status WHEN '1' THEN 'Non DRM' WHEN '2' THEN 'DRM' ELSE 'Not Set' END AS 'drm_status',
							tblcompanytier.companytier AS 'companytier',
							tblcompanystatus.company_status AS 'company_status',
							tblcompanytype.companytype AS 'companytype',
							CONCAT(kam1.first_name,' ',kam1.surname) AS 'kam1_name',
							CONCAT(kam2.first_name,' ',kam2.surname) AS 'kam2_name',
							CONCAT(dealer1.first_name,' ',dealer1.surname) AS 'dealer1_name',
							CONCAT(dealer2.first_name,' ',dealer2.surname) AS 'dealer2_name',
							tblcompanylist.address_1 AS 'address_1',
							tblcompanylist.address_2 AS 'address_2',
							tblcompanylist.address_3 AS 'address_3',
							tblcompanylist.address_4 AS 'address_4',
							tblcompanylist.switchboard_1 AS 'switchboard_1',
							tblcompanylist.switchboard_2 AS 'switchboard_2',
							tblcompanylist.website AS 'website',
							tblcompanylist.social_fb AS 'social_fb',
							tblcompanylist.social_twitter AS 'social_twitter',
							tblcompanylist.social_linkedin AS 'social_linkedin'
						FROM
							tblcompanylist LEFT OUTER JOIN tblcompanystatus ON tblcompanystatus.id_companystatus = tblcompanylist.id_companystatus
							LEFT OUTER JOIN tblcompanytier ON tblcompanytier.id_companytier  = tblcompanylist.id_companytier
							LEFT OUTER JOIN tblcompanytype ON tblcompanytype.id_companytype = tblcompanylist.id_companytype
							LEFT OUTER JOIN contacts AS kam1 ON kam1.id = tblcompanylist.id_kam1
							LEFT OUTER JOIN contacts AS kam2 ON kam2.id = tblcompanylist.id_kam2
							LEFT OUTER JOIN contacts AS dealer1 ON dealer1.id = tblcompanylist.id_dealer1
							LEFT OUTER JOIN contacts AS dealer2 ON dealer2.id = tblcompanylist.id_dealer2
						GROUP BY
							tblcompanylist.Company
						ORDER BY
							tblcompanylist.Company ASC";


		mysql_select_db($database_CRMconnection, $CRMconnection);
		$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
		$recordSet = array();
		if( $result ) {
			while ($row = mysql_fetch_assoc($result)) {
				$recordSet[] = $row;
			}

            // //Add 'Profile Picture' info
            // for($i = 0; $i < count($recordSet); $i++){
            //     $id = $recordSet[$i]['Client ID'];
            //     $hasProfilePic = 0;
            //     if(file_exists('uploads/clientp/' . $id . '/1.jpg')) $hasProfilePic = 1;

            //     $recordSet[$i]['Profile Picture'] = $hasProfilePic;
            // }

//            var_dump($recordSet);

			if(sizeof( $recordSet ) > 0 ){

                $fileData = $this->getCSV($recordSet);
				$fileData = html_entity_decode($fileData,ENT_QUOTES,"UTF-8");
				$fileData = utf8_decode($fileData);

				if( $fileData != '' ) {
					$filename = "Avior-CRM-FullInstitution-extract-".date('Y-m-d').".csv";
					header("Content-type: text/csv");
					header("Content-Disposition: attachment; filename=".$filename);
					header("Pragma: no-cache");
					header("Expires: 0");
					echo $fileData;
					exit();
				}
			}
		}
	}




	public function ExportKAM()
	{

		include_once('Connections/CRMconnection.php');

		$sql = "SELECT
				Company, concat(A.first_name,' ', A.surname) as KAM1_name, concat(B.first_name,' ', B.surname) as KAM2_name 
				FROM tblcompanylist
				LEFT JOIN contacts as A ON tblcompanylist.id_kam1 = A.id
				LEFT JOIN contacts as B ON tblcompanylist.id_kam2 = B.id
				ORDER BY Company ASC";


		mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
		$result = mysql_query($sql, $this->CRMconnection) or die(mysql_error());
		$recordSet = array();
		if( $result ) {
			while ($row = mysql_fetch_assoc($result)) {
				$recordSet[] = $row;
			}
			


			if(sizeof( $recordSet ) > 0 ){

                $fileData = $this->getCSV($recordSet);
				$fileData = html_entity_decode($fileData,ENT_QUOTES,"UTF-8");
				$fileData = utf8_decode($fileData);

				if( $fileData != '' ) {
					$filename = "Avior-CRM-KAM-extract-".date('Y-m-d').".csv";
					header("Content-type: text/csv");
					header("Content-Disposition: attachment; filename=".$filename);
					header("Pragma: no-cache");
					header("Expires: 0");
					echo $fileData;
					exit();
				}
			}
		}
	}

	public function getCompanyTypes()
	{
		$returnData = array();

		$sql = "SELECT DISTINCT
					tblcompanytype.id_companytype,
					tblcompanytype.companytype
				FROM
					tblcompanytype
				WHERE
					tblcompanytype.companytype_status = 1
				ORDER BY tblcompanytype.display_order ASC";

		$returnData = $this->fetchResults( $sql );

		return $returnData;
	}
	
	public function getNoteTypes()
	{
		$returnData = array();

		$sql = "SELECT DISTINCT 
			tblnotes.note_type
			FROM tblnotes
			ORDER BY 
			tblnotes.note_type ASC";

		$data = $this->fetchResults( $sql );
		
		foreach($data as $key => $value) {
			$returnData[] = $value['note_type'];
		}
		$returnData[] = 'Webview open';
		sort($returnData);


		return $returnData;
	}	

	public function getCompanyGeographyTypes() 
	{
		$returnData = array();
		$sql  = "SELECT *  FROM 
					tblcompanygeography
				 ORDER BY name ASC	
				 ";
		$returnData = $this->fetchResults($sql);
		return $returnData; 
	}

	public function getCompanyStatuses()
	{
		$returnData = array();
		$sql = "SELECT DISTINCT
					tblcompanystatus.id_companystatus,
					tblcompanystatus.company_status
				FROM
					tblcompanystatus
				ORDER BY
					tblcompanystatus.display_order ASC";

		$returnData = $this->fetchResults( $sql );
		return $returnData;
	}
	/**
	* A list of distinct institutions
	*/
	public function getInstitutions()
	{
		$returnData = array();
		$sql = "SELECT DISTINCT tblcompanylist.comp_id, tblcompanylist.Company FROM tblcompanylist ORDER BY tblcompanylist.Company ASC";

		$returnData = $this->fetchResults( $sql );
		return $returnData;
	}
	
	/**
	* A list of distinct individuals
	*/
	public function getIndividuals()
	{
		$returnData = array();
		$sql = "SELECT C.id, CONCAT(C.first_name,' ', C.surname) AS  'Individual' FROM contacts C ORDER BY C.first_name, C.surname";

		$companyNoteArray = array();
		$companyNoteArray[] = array(
			'id' => 'Company Note',
			'Individual' => 'Company Note'
		);

		$returnData = $this->fetchResults( $sql );		
		
		$returnData = array_merge($companyNoteArray, $returnData);
		
		return $returnData;
	}	
	
	
		
	/**
	* A list of activated company tier
	*/
	public function getCompanyTiers()
	{
		$returnData = array();
		$sql = "SELECT DISTINCT
						tblcompanytier.id_companytier,
						tblcompanytier.companytier
					FROM
						tblcompanytier
					WHERE
						tblcompanytier.companytier_status = 1
					ORDER BY
						tblcompanytier.display_order ASC";

		$returnData = $this->fetchResults( $sql );
		return $returnData;
	}

	/**
	 * A list of Locations - Geography
	 *
	 **/
	public function getLocation()
	{
		$sql = "SELECT DISTINCT
					tblcompanygeography.id_geography,
					tblcompanygeography.name
				FROM
					tblcompanygeography
				WHERE
					tblcompanygeography.display_status = 1
				ORDER BY
					tblcompanygeography.name ASC";

		return $this->fetchResults($sql);
	}


	/**
	* Updates the company details, DRM setting, and all references of the company name - if the name has been changed.
	* @param array $companyData data to update
	*/
	public function updateCompany( $companyData )
	{

		if( is_array($companyData) && sizeof($companyData) > 0  && $this->id > 0 ) {

			$company_details = $this->getCompanyDetails();
			$companyID = isset($company_details[0]['id']) ? trim($company_details[0]['id']) : '';
            $old_companyname = isset($company_details[0]['company']) ? trim($company_details[0]['company']) : '';
			$old_companytier = isset($company_details[0]['drm_status']) ? $company_details[0]['drm_status'] : '';
			
			$old_portal_access = isset($company_details[0]['portal_access']) ? $company_details[0]['portal_access'] : '';
			$old_webview_print = isset($company_details[0]['webview_print']) ? $company_details[0]['webview_print'] : '';						

			$company_name = isset($companyData['company_name']) ? trim($companyData['company_name']) : '';
			$id_companytier = isset($companyData['company_tier']) ? $companyData['company_tier'] : '';

			$id_companystatus = isset($companyData['company_status']) ? $companyData['company_status'] : '';
			$id_geography = isset($companyData['id_geography']) ? $companyData['id_geography'] : '';
			$id_companytype = isset($companyData['company_type']) ? $companyData['company_type'] : '';

			$id_kam1 = isset($companyData['company_kam1']) ? $companyData['company_kam1']: null;
			$id_kam2 = isset($companyData['company_kam2']) ? $companyData['company_kam2']: null;

			$id_dealer1 = isset($companyData['company_dealer1']) ? $companyData['company_dealer1']: null;
			$id_dealer2 = isset($companyData['company_dealer2']) ? $companyData['company_dealer2']: null;

			$address_1 = isset($companyData['address_1']) ? trim($companyData['address_1']) : null;
			$address_2 = isset($companyData['address_2']) ? trim($companyData['address_2']) : null;
			$address_3 = isset($companyData['address_3']) ? trim($companyData['address_3']) : null;
			$address_4 = isset($companyData['address_4']) ? trim($companyData['address_4']) : null;

			$switchboard_1 = isset($companyData['switchboard_1']) ? trim($companyData['switchboard_1']) : null;
			$switchboard_2 = isset($companyData['switchboard_2']) ? trim($companyData['switchboard_2']) : null;
			$website = isset($companyData['company_website']) ? trim( $companyData['company_website'] ): null;

			$social_fb = isset($companyData['social_fb']) ? trim( $companyData['social_fb'] ): null;
			$social_twitter = isset($companyData['social_twitter']) ? trim( $companyData['social_twitter'] ): null;
			$social_linkedin = isset($companyData['social_linkedin']) ? trim( $companyData['social_linkedin'] ): null;

			$drm_setting = isset($companyData['company_drm_setting']) ? $companyData['company_drm_setting']: null;
			
			$portal_access = isset($companyData['portal_access']) ? $companyData['portal_access']: '0';
			$webview_print = isset($companyData['webview_print']) ? $companyData['webview_print']: '0';												

			$modified_date = date('Y-m-d H:i:s');

			if( $company_name != '' && $id_companytier > 0 && $id_companystatus > 0 && $id_companytype > 0 ) {

				$sql = sprintf("UPDATE  tblcompanylist SET
								tblcompanylist.id_companytier = %s,
								tblcompanylist.id_geography = %s,
								tblcompanylist.id_companystatus = %s,
								tblcompanylist.id_companytype = %s,
								tblcompanylist.id_kam1 = %s,
								tblcompanylist.id_kam2 = %s,
								tblcompanylist.id_dealer1 = %s,
								tblcompanylist.id_dealer2 = %s,
								tblcompanylist.Company = %s,
								tblcompanylist.address_1 = %s,
								tblcompanylist.address_2 = %s,
								tblcompanylist.address_3 = %s,
								tblcompanylist.address_4 = %s,
								tblcompanylist.switchboard_1 = %s,
								tblcompanylist.switchboard_2 = %s,
								tblcompanylist.website = %s,
								tblcompanylist.social_fb = %s,
								tblcompanylist.social_twitter = %s,
								tblcompanylist.social_linkedin = %s,
								tblcompanylist.modified_date = %s
							WHERE
								tblcompanylist.comp_id = %s",
								$this->GetSQLValueString($id_companytier, "int"),
								$this->GetSQLValueString($id_geography, "int"),
								$this->GetSQLValueString($id_companystatus, "int"),
								$this->GetSQLValueString($id_companytype, "int"),
								$this->GetSQLValueString($id_kam1, "int"),
								$this->GetSQLValueString($id_kam2, "int"),
								$this->GetSQLValueString($id_dealer1, "int"),
								$this->GetSQLValueString($id_dealer2, "int"),
								$this->GetSQLValueString($company_name, "text"),
								$this->GetSQLValueString($address_1, "text"),
								$this->GetSQLValueString($address_2, "text"),
								$this->GetSQLValueString($address_3, "text"),
								$this->GetSQLValueString($address_4, "text"),
								$this->GetSQLValueString($switchboard_1, "text"),
								$this->GetSQLValueString($switchboard_2, "text"),
								$this->GetSQLValueString($website, "text"),
								$this->GetSQLValueString($social_fb, "text"),
								$this->GetSQLValueString($social_twitter, "text"),
								$this->GetSQLValueString($social_linkedin, "text"),
								$this->GetSQLValueString($modified_date, "text"),
								$this->GetSQLValueString($this->id, "int"));

				$db_connected = mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
				if( $db_connected ) {
					$result = mysql_query($sql, $this->CRMconnection);;
					if( $result ) {
						########### hipchat logic here ############################
						$hipchat_rooms 		= $companyData['hipchat_rooms'];

						if (!empty($hipchat_rooms)) {
							$delete_existing_rooms 	= "DELETE FROM tblcompany_hipchatrooms WHERE company_id = ". $companyData['comp_id']."";
							$run_delete_query 		= mysql_query($delete_existing_rooms, $this->CRMconnection);
							if (!$run_delete_query) {
								die("Error deleting the query ". mysql_error());
							}
						}

						foreach ($hipchat_rooms as $key => $value) {
							$insert_company_rooms 	= "INSERT INTO tblcompany_hipchatrooms (company_id,hipchat_room_id) VALUES (".$companyData['comp_id'].", $value)";
							$run_insert_query 		= mysql_query($insert_company_rooms, $this->CRMconnection);
							if (!$run_insert_query) {
								die("Error inserting the rooms " . mysql_error());
							}
						}

						if(isset($drm_setting) && $drm_setting !== $old_companytier) {
							//DRM setting changes the previously set company tier. if these values are not the same, then they need to be changed.
							// We haven't changed the company name references yet, so we're going to update the DRM using the old name.
							$this->UpdateCompanyDRM( $companyID, $drm_setting);
						}
						
						if(isset($portal_access) && $portal_access !== $old_portal_access) {
							//Portal access setting changes the previously set company tier. if these values are not the same, then they need to be changed.
							$this->UpdateCompanyPortalAccess( $companyID, $portal_access);
						}
						
						if(isset($webview_print) && $webview_print !== $old_webview_print) {
							//Webview print setting changes the previously set company tier. if these values are not the same, then they need to be changed.
							$this->UpdateCompanyWebviewPrintStatus( $companyID, $webview_print);
						}												

						return "success";

					} else{
						return "db-error";
					}
				} else {
					return "db-error";
				}
			} else{
				return "data-missing";
			}
		}

		return "data-missing";
	}
	/**
	* Returns a list of employees working for the company
	*
	*/
	public function Employees($id)
	{
		$returnData = array();

		if ($id != '') {

			$sql = sprintf("SELECT DISTINCT
								contacts.id,
								CONCAT(contacts.first_name,' ',contacts.surname) AS 'client_name',
								contacts.jobtitle AS 'job_title',
								contacts.active AS 'active',
								contacts.email AS 'email',
								COUNT(DISTINCT contacts_interests.interest_id) AS 'mailing_lists'
							FROM
								contacts
								LEFT OUTER JOIN contacts_interests ON contacts_interests.contact_id = contacts.id
							WHERE
								contacts.company_id = %s
							GROUP BY
								contacts.id
							ORDER BY contacts.active DESC, contacts.first_name ASC, contacts.surname ASC",
							$this->GetSQLValueString($id, "int"));

			$returnData = $this->fetchResults($sql);
		}
		return $returnData;
	}

	/*
	* Creates company directory
	* return @void
	*/
	public function createSharedDirectory()
	{

		if ($this->id > 0) {
			$uploaddir = 'uploads/public/company/'.sha1(md5($this->id));
			if (!is_dir('uploads/public/company')) {
				mkdir('uploads/public/company', 0777);
			}
			//make directory if it doens not yet exist
			if(!is_dir($uploaddir)) {
				mkdir($uploaddir, 0777);
			}
		}
	}

	/**
	* Returns the Company Responsibility Files
	*/
	public function sharedFiles()
	{
		$fileData = array();
		$returnData = array();
		if ($this->id > 0) {
			$dir = 'uploads/public/company/'.sha1(md5($this->id));
			if (is_dir($dir)) {
				if ($dh = opendir($dir)) {
					while (($file = readdir($dh)) !== false) {
						if (strlen($file)>3){
							$fileData[] = $dir."/".rawurlencode($file);
						}
					}
				}
				closedir($dh);
				if(sizeof($fileData) > 0 ) {
					usort($fileData, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
				}

				$returnData = array_map(function($file){
								return array( 'path' => $file,'name' => basename(urldecode($file)));
						},$fileData);


			}
		}
		return $returnData;
	}

	/**
	* Changes as per queries found in adminmain.php (For when changing DRM Setting).
	* @param string $companyName - The company name is used to update the
	*/
	protected function UpdateCompanyDRM( $id, $drmStatus)
	{

		if( isset($this->id) && isset($drmStatus)) {
			//change the company Tier
			$updateSQL = sprintf("UPDATE tblcompanylist SET drm_status= %s WHERE comp_id =%s",
								$this->GetSQLValueString($drmStatus, "int"), $this->GetSQLValueString($this->id, "int"));
			mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
			$Result1 = mysql_query($updateSQL, $this->CRMconnection) or die(mysql_error());

		}
	}
	
	/**
	* Changes as per queries found in adminmain.php (For when changing Portal Access Setting).
	* @param string $companyName - The company name is used to update the
	*/
	protected function UpdateCompanyPortalAccess( $id, $portalAccessStatus)
	{

		if( isset($this->id) && isset($portalAccessStatus)) {
			//change the company Tier
			$updateSQL = sprintf("UPDATE tblcompanylist SET portal_access= %s WHERE comp_id =%s",
								$this->GetSQLValueString($portalAccessStatus, "int"), $this->GetSQLValueString($this->id, "int"));
			mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
			$Result1 = mysql_query($updateSQL, $this->CRMconnection) or die(mysql_error());

		}
	}
	
	/**
	* Changes as per queries found in adminmain.php (For when changing Webview print Setting).
	* @param string $companyName - The company name is used to update the
	*/
	protected function UpdateCompanyWebviewPrintStatus( $id, $webviewPrintStatus)
	{

		if( isset($this->id) && isset($webviewPrintStatus)) {
			//change the company Tier
			$updateSQL = sprintf("UPDATE tblcompanylist SET webview_print= %s WHERE comp_id =%s",
								$this->GetSQLValueString($webviewPrintStatus, "int"), $this->GetSQLValueString($this->id, "int"));
			mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
			$Result1 = mysql_query($updateSQL, $this->CRMconnection) or die(mysql_error());

		}
	}		

	protected function UpdateCompanyNameReferences($oldCompanyName, $newCompanyName)
	{
		if( $newCompanyName != '' && $oldCompanyName != ''  ) {
			//now also update all the clients in the database:
			$insertSQL = sprintf("UPDATE contacts SET company = %s WHERE company =%s",
			$this->GetSQLValueString($newCompanyName, "text"),
			$this->GetSQLValueString($oldCompanyName, "text"));

			mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
			$Result1 = mysql_query($insertSQL, $this->CRMconnection) or die(mysql_error());
		}
	}

	public function UpdateCompanyTier($id_companytier)
	{
		$result = '';
		if(isset($this->id) && $id_companytier != ''){

			$updateSQL = sprintf("UPDATE tblcompanylist SET tblcompanylist.id_companytier = %s WHERE tblcompanylist.comp_id = %s",
			$this->GetSQLValueString($id_companytier, "int"),
			$this->GetSQLValueString($this->id, "int"));

			mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
			$Result1 = mysql_query($updateSQL, $this->CRMconnection) or die(mysql_error());
			if($Result1) {
				$result = "updated";
			}
		}
		return $result;
	}

	public function UpdateCompanyStatus($id_companystatus)
	{
		$result = '';
		if(isset($this->id) && $id_companystatus != ''){

			$updateSQL = sprintf("UPDATE tblcompanylist SET tblcompanylist.id_companystatus = %s WHERE tblcompanylist.comp_id = %s",
			$this->GetSQLValueString($id_companystatus, "int"),
			$this->GetSQLValueString($this->id, "int"));

			mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
			$Result1 = mysql_query($updateSQL, $this->CRMconnection) or die(mysql_error());
			if($Result1) {
				$result = "updated";
			}
		}
		return $result;
	}

	public function UpdateCompanyType($id_companytype)
	{
		$result = '';
		if(isset($this->id) && $id_companytype != ''){

			$updateSQL = sprintf("UPDATE tblcompanylist SET tblcompanylist.id_companytype = %s WHERE tblcompanylist.comp_id = %s",
			$this->GetSQLValueString($id_companytype, "int"),
			$this->GetSQLValueString($this->id, "int"));

			mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
			$Result1 = mysql_query($updateSQL, $this->CRMconnection) or die(mysql_error());
			if($Result1) {
				$result = "updated";
			}
		}
		return $result;
	}

	public function UpdateCompanyLocation($id_location)
	{
		$result = '';
		if(isset($this->id) && $id_location != ''){

			$updateSQL = sprintf("UPDATE tblcompanylist SET tblcompanylist.id_geography = %s WHERE tblcompanylist.comp_id = %s",
			$this->GetSQLValueString($id_location, "int"),
			$this->GetSQLValueString($this->id, "int"));

			mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
			$Result1 = mysql_query($updateSQL, $this->CRMconnection) or die(mysql_error());
			if($Result1) {
				$result = "updated";
			}
		}
		return $result;
	}

	public function ExportCompanyCodes(){

		$returnData = array();

		$sql = "SELECT DISTINCT
					tblcompanylist.Company AS 'Company Name',
					tblcompanygeography.name AS 'Geography',
					tblcompanytier.companytier AS 'Tier',
					tblcompanystatus.company_status AS 'Status',
					tblcompanytype.companytype AS 'Type'

				FROM
					tblcompanylist LEFT OUTER JOIN tblcompanytier ON tblcompanytier.id_companytier = tblcompanylist.id_companytier
					LEFT OUTER JOIN tblcompanytype ON tblcompanytype.id_companytype = tblcompanylist.id_companytype
					LEFT OUTER JOIN tblcompanystatus ON tblcompanystatus.id_companystatus = tblcompanylist.id_companystatus
					LEFT OUTER JOIN tblcompanygeography ON tblcompanygeography.id_geography =
					tblcompanylist.id_geography
				ORDER BY
					tblcompanylist.Company ASC";

		$returnData = $this->fetchResults( $sql );
		if( sizeof($returnData) > 0 ) {

			$addHeader = true;
			$csvFileData = $this->array_to_scv($returnData, $addHeader);
			$csvFileData = html_entity_decode($csvFileData,ENT_QUOTES,"UTF-8");
			$csvFileData = utf8_decode($csvFileData);

			if( $csvFileData != '' ) {
				$filename = "Avior-CRM-Company-Colour-Codes-".date('Y-m-d').".csv";
				header("Content-type: text/csv");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				echo $csvFileData;
				exit();
			}
		}
	}

	public function getAllCompanyProperties()
	{
		$returnData = array();

		$sql = "SELECT DISTINCT
					tblcompanylist.comp_id,
					tblcompanylist.id_companytier,
					tblcompanylist.id_companystatus,
					tblcompanylist.id_companytype,
					tblcompanylist.id_geography,
					tblcompanylist.Company,
					tblcompanystatus.company_status,
					tblcompanytier.companytier,
					tblcompanytype.companytype,
					tblcompanygeography.name AS 'geography_name'
				FROM
					tblcompanylist
					LEFT OUTER JOIN tblcompanytier ON tblcompanytier.id_companytier = tblcompanylist.id_companytier
					LEFT OUTER JOIN tblcompanystatus ON tblcompanystatus.id_companystatus = tblcompanylist.id_companystatus
					LEFT OUTER JOIN tblcompanytype ON tblcompanytype.id_companytype = tblcompanylist.id_companytype
					LEFT OUTER JOIN tblcompanygeography ON tblcompanygeography.id_geography =
					tblcompanylist.id_geography
				ORDER BY
					tblcompanylist.Company ASC";

		$returnData = $this->fetchResults( $sql );

		return $returnData;
	}
	/**
	* Helper function
	*/
	function GetSQLValueString($theValue, $theType )
	{

	  if (PHP_VERSION < 6) {
		$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
	  }
	  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

	  switch ($theType) {
		case "text":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
		case "long":
		case "int":
		  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
		  break;
		case "double":
		  $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
		  break;
		case "date":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
	  }
	  return $theValue;
	}

	private function fetchResults( $sql )
	{
		$returnData = array();
		$db_connected =  mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
		if( $db_connected ) {
			$result = mysql_query($sql, $this->CRMconnection);
			if( $result ) {
				while ($row = mysql_fetch_assoc($result)) {
					$returnData[] = $row;
				}
				mysql_free_result($result);
			}			
		}
		return $returnData;
	}

	protected function array_to_scv($array, $header_row = true, $col_sep = ",", $row_sep = "\n", $qut = '"') {

		if (!is_array($array) or !is_array($array[0])) return false;

		/**
		 * Header row.
		 */
		 $output = '';
		if ($header_row) {
			foreach ($array[0] as $key => $val) {
				/**
				 * Escaping quotes.
				 */
				$key = str_replace($qut, "$qut$qut", $key);
				$output .= "$col_sep$qut$key$qut";
			}
			$output = substr($output, 1)."\n";
		}
		/**
		 * Data rows.
		 */
		foreach ($array as $key => $val) {
			$tmp = '';
			foreach ($val as $cell_key => $cell_val) {
				/**
				 * Escaping quotes.
				 */
				$cell_val = str_replace($qut, "$qut$qut", $cell_val);
				$tmp .= "$col_sep$qut$cell_val$qut";
			}
			$output .= substr($tmp, 1).$row_sep;
		}

		return $output;
	}


	protected function getCSV($array){

        if(!is_array($array)) return false;

        $data = '';

        //Headers
        $headers = array();
        foreach($array[0] as $key => $val){
            array_push($headers , $key);
        }
        $data .= implode("," , $headers);
        $data .= "\n";

        //Content
        foreach($array as $key => $val){
            $lineArray = array();

            foreach($val as $cell_key => $cell_val){

                $cell_val = trim($cell_val);
                if(!ctype_alnum($cell_val)) $cell_val = '"' . $cell_val . '"';

                array_push($lineArray , $cell_val);
            }

            $data .= implode("," , $lineArray);
            $data .= "\n";
        }

        return $data;
    }
		
	/**
	 * Function to get type data for note contact notes list
	 */		
	public function getTypeData() {
		
		$returnData = array();

		$sql = sprintf("SELECT DISTINCT
							tblnotes.note_type as 'type'
						FROM
							tblnotes INNER JOIN contacts ON contacts.id = tblnotes.note_cnt_Id
							LEFT OUTER JOIN tbluser ON tbluser.user_name = tblnotes.note_by
						WHERE
							contacts.company_id = %s
						ORDER BY
							tblnotes.note_type ASC", $this->GetSQLValueString($this->id, "int") );
							

		$returnData = $this->fetchResults( $sql );
		
		//Add Email read
		$returnData[] = array('type' => 'Email read');
		//Add Webview open
		$returnData[] = array('type' => 'Webview open');		
		
		//Sort
		$newData = [];
		for($t=0;$t<sizeof($returnData);$t++) {
			$newData[] = $returnData[$t]['type'];
		}
						
		sort($newData);		
		$returnData = [];
		for($t=0;$t<sizeof($newData);$t++) {
			$returnData[] = array('type' => $newData[$t]);
		}

		return $returnData;		
	}
	
	/**
	 * Function to get by / analyst data for note contact notes list
	 */	
	public function getAnalystData() {
		
		$returnData = array();

		$sql = sprintf("SELECT DISTINCT
							-- CONCAT(tbluser.user_fname,' ',SUBSTRING(TRIM(tbluser.user_lname),1,1)) AS 'analyst'
							-- CONCAT(tbluser.user_fname,' ', tbluser.user_lname) AS 'analyst'							
							IFNULL(tbluser.user_nick, CONCAT(tbluser.user_fname, ' ', SUBSTRING(TRIM(tbluser.user_lname),1,1))) AS 'analyst'
						FROM
							tblnotes INNER JOIN contacts ON contacts.id = tblnotes.note_cnt_Id
							INNER JOIN tbluser ON tbluser.user_name = tblnotes.note_by
						WHERE
							contacts.company_id = %s
						ORDER BY
							CONCAT(tbluser.user_fname,' ',SUBSTRING(TRIM(tbluser.user_lname),1,1)) ASC", $this->GetSQLValueString($this->id, "int") );

		$returnData = $this->fetchResults( $sql );

		return $returnData;		
	}	
	
	/**
	 * Function to get client data for note contact notes list
	 */	
	public function getClientData() {
		
		$returnData = array();

		$sql = sprintf("SELECT DISTINCT
							tblnotes.note_cnt_name AS 'client'
						FROM
							tblnotes INNER JOIN contacts ON contacts.id = tblnotes.note_cnt_Id
							INNER JOIN tbluser ON tbluser.user_name = tblnotes.note_by
						WHERE
							contacts.company_id = %s
						ORDER BY
							tblnotes.note_cnt_name ASC", $this->GetSQLValueString($this->id, "int") );


		$companyNoteArray = array();
		$companyNoteArray[] = array(
			'client' => 'Company Note',
		);
		
		$returnData = $this->fetchResults( $sql );				
		
		$returnData = array_merge($companyNoteArray, $returnData);
				
		return $returnData;		
	}		


}
