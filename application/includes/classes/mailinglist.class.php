<?php
/**
* Class is used for adding and removing mailing lists
* @version 1.0 
**/
class MailingList{
	
	function __construct() {
	}
	
	/**
	* Check if the section name is unique
	* @param String $sectionName
	* @returns boolean $returnValue
	*/	
	protected function IsUnique( $sectionName ) {
	
		include('Connections/CRMconnection.php');	
		
		$returnValue = true;
		
		if($sectionName != '' ) {
		
			$sql = sprintf("SELECT tblsectors.sect_id FROM tblsectors WHERE tblsectors.sect_Name = %s LIMIT 0,1",$this->GetSQLValueString($sectionName, "text") );
			mysql_select_db($database_CRMconnection, $CRMconnection);		
			$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
			$row = mysql_fetch_assoc($result);
			
			if(is_array( $row ) && sizeof( $row ) > 0 ) {
				$returnValue = false;
			}
		}
		return $returnValue;
	}
	
	/**
	* Save the mailing list and client interesets
	* @param array $mailingListArray
	* @returns String $returnData
	*/
	public function Save( $mailingListArray ){
		
		include('Connections/CRMconnection.php');	

		$mailingListName = (isset($mailingListArray['mailing_list_name']) && $mailingListArray['mailing_list_name'] != '' ) ? trim($mailingListArray['mailing_list_name']) : '';
		//it seems like sec_Desc is not being used for anything at present. Still check if it exists.
		$sect_Desc = (isset($mailingListArray['sect_Desc']) && $mailingListArray['sect_Desc'] != '' ) ? trim($mailingListArray['sect_Desc']) : '';
		$mergeLists = (isset($mailingListArray['mergeLists']) && $mailingListArray['mergeLists'] != '' ) ? $mailingListArray['mergeLists'] : '';
		
		$returnData = '';
		
		if( $mailingListName != '' ) {

				if( $this->IsUnique( $mailingListName ) ) {
					$sql = "INSERT INTO interests(description) VALUES('" . $mailingListName . "')";
					mysql_select_db($database_CRMconnection, $CRMconnection);
					$insertResult = mysql_query($sql, $CRMconnection) or die(mysql_error());
					
					# The id of the just created Interest must be passed to addClientInterest
					$mailingListIdentifier="";
					if( $insertResult ) {
						$sqlret = sprintf('SELECT
									interests.id
								FROM
									interests
								WHERE 
									interests.description = "%s"',$mailingListName);
						
						$result = mysql_query($sqlret, $CRMconnection) or die(mysql_error());
						$row = mysql_fetch_assoc($result);
						if( $row['id'] != '' ) {
							$mailingListIdentifier=$row['id'];
						}
						else{
							return "Could not add new list.";
						}
					}

					if( $insertResult ) {
						if( $mergeLists != '') {
							$clientInterestAdded = $this->AddClientInterests( $mergeLists, $mailingListIdentifier );
						}
						$returnData = "New List added.";
					} else {
						$returnData = "Could not add new list.";
					}
				} else {
					$returnData = "List already exists. Please enter new Mailing List.";
				}
		}
		
		return $returnData;
	}
	
	/**
	* Removes item from Mailing list.
	* @param string $itemsToRemove A comma separated list of items to remove for list and iterests.
	* @returns string $result 
	*/
	public function RemoveMailingList( $itemsToRemove ) {
	
		include('Connections/CRMconnection.php');

		$result = '';
		
		if( $itemsToRemove != '' ) {

			$listItems = explode(',',$itemsToRemove);

			// Make sure that we're not deleting ALL items.
			if( $itemsToRemove != '' && !in_array('All',$listItems ) ) {
				//First delete from the join table.
				$deleteSql = "DELETE FROM contacts_interests WHERE interest_id IN ($itemsToRemove)";
				mysql_select_db($database_CRMconnection, $CRMconnection);
				$delete_sectors_results = mysql_query($deleteSql, $CRMconnection) or die(mysql_error());
				if( $delete_sectors_results ) {

					$result = 'Success';
					//Delete from interests table.
					$deleteInterestSql = "DELETE FROM interests WHERE id IN ($itemsToRemove)";
					mysql_select_db($database_CRMconnection, $CRMconnection);
					$delete_interest_results = mysql_query($deleteInterestSql, $CRMconnection) or die(mysql_error());	
				} else {
					$result = 'Could not remove: '.$itemsToRemove." ";
				}
			} else if( in_array('All',$listItems ) ) {
				$result = 'You are trying to delete ALL items';
			}
		}
		return $result;
	}
	
	/**
	* Adds Client Interests from other lists. If ALL is part of the list, then all other items in the list gets ignored. We then 
	* get a distinct list of clients and add the new list name.
	* @param string $mergeList A comma separated list of items to merge from.
	* @param string $newListName the new name of the list being added
	* @returns boolean $clientsAdded.
	*/
	protected function AddClientInterests( $mergeList, $newListName ){
		
		include('Connections/CRMconnection.php');	
		$insertResults = array();
		$clientsAdded = false;
		
		if( trim( $mergeList ) !=  '' ) {
		
			$listItems = explode(',',$mergeList);
			$newListArray = array();
			for( $x=0; $x < sizeof($listItems);$x++ ) {
				$newListArray[] = $this->GetSQLValueString($listItems[$x], "text");
			}

			$newList = implode(',',$newListArray);
			
			if (!in_array('All',$listItems)) {
			
				$sql = sprintf("SELECT DISTINCT
								contacts_interests.contact_id
							FROM
								contacts_interests, contacts, tblcompanylist
							WHERE 
								contacts.id = contacts_interests.contact_id 
								AND contacts.company_id = tblcompanylist.comp_id 
								AND contacts_interests.interest_id IN (%s)",$newList);
			} else {
				/*Get All Distinct client data*/
				$sql = "SELECT DISTINCT
								contacts_interests.contact_id
							FROM
								contacts_interests, contacts, tblcompanylist
							WHERE
								contacts.id = contacts_interests.contact_id 
								AND contacts.company_id = tblcompanylist.comp_id ";
			}

			mysql_select_db($database_CRMconnection, $CRMconnection);
			$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
			$recordSet = array();
			while ($row = mysql_fetch_assoc($result)) {
				if( $row['contact_id'] != '' ) {
					// Add new item name as Cnt Interest
					$recordSet[] = array(
						'contact_id' => trim($row['contact_id']),
						'interest_id' => $newListName
					);
				}
			}
			$recordSetSize = sizeof( $recordSet );
			if( $recordSetSize > 0 ) {
				
				for( $x=0; $x < $recordSetSize; $x++ ) {
					$sql = sprintf("INSERT INTO contacts_interests (contact_id, interest_id) VALUES (%s,%s)",
						$this->GetSQLValueString($recordSet[$x]['contact_id'], "int"),
						$this->GetSQLValueString($recordSet[$x]['interest_id'], "int"));
						mysql_select_db($database_CRMconnection, $CRMconnection);
						$result = mysql_query($sql, $CRMconnection) or die(mysql_error());
						$insertResults[] = $result;
				}
				
				if( !in_array(false,$insertResults) ) {
					$clientsAdded = true;
				}
			}	
		}
		return $clientsAdded;
	}
	
	/**
	* Helper function
	*/
	function GetSQLValueString($theValue, $theType ){

	  if (PHP_VERSION < 6) {
		$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
	  }
	  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
	  
	  switch ($theType) {
		case "text":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;    
		case "long":
		case "int":
		  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
		  break;
		case "double":
		  $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
		  break;
		case "date":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
	  }
	  return $theValue;
	}
	
	
	
}

?>