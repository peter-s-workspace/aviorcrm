<?php

require_once('includes/classes/platform.class.php');

ini_set('max_execution_time', 300); //300 seconds = 5 minutes

class Notes {

	private $database_CRMconnection;
	private $CRMconnection;
	private $useDatatables; 
	private $datatablesObj;
	
	public function __construct($useDatatables = false)
	{   
		include('Connections/CRMconnection.php');		
		$this->database_CRMconnection = $database_CRMconnection;
		$this->CRMconnection = $CRMconnection;
		$this->useDatatables = $useDatatables;
		if($this->useDatatables) {
			include('includes/classes/datatables.class.php');
			$this->datatablesObj = new Datatables();
		}
		
	}
	
	/**
	* Function to check if a user belongs to 'Avior Capital Markets (Pty) Ltd'
	**/
	public function doesContactBelongToAvior($userId)	{
		
		$doesBelongToAvior = false;
		
		//Set company id based on environment: dev / production
		$aviorCompId = 81;
		$platform = new Platform();
    $currentEnv = $platform->getEnvironment();
		if ($currentEnv == 'dev') {
			$aviorCompId = 81;
		}
		
		if ($userId > 0) {
			$sql = sprintf("SELECT contacts.id
						FROM 
								contacts
						WHERE 
								contacts.id = %s 
								AND contacts.company_id = %s", $this->GetSQLValueString($userId, "int"), $this->GetSQLValueString($aviorCompId, "int") );

			$returnData = $this->fetchResults( $sql );		
			
			if (sizeof($returnData) > 0 ) {
				$doesBelongToAvior = true;
			}
		}
		
		return $doesBelongToAvior;
	}
	
	/**
	* Get All user Notes
	**/
	public function getUserNotes($userId)
	{
		$returnData = array();
		
		$sql = $this->getUserNotesSqlWithAviorLibData($userId);
		$returnData = $this->fetchResults( $sql );
			
		return $returnData;
	}
	
	/**
	* Get Avior user Notes
	**/
	public function getAviorUserNotes($userId)
	{
		$returnData = array();
		if ($userId > 0) {						
						
			$sql = sprintf("SELECT DISTINCT N.note_Id,
					N.notebyNick,
					N.note_cnt_name,					
					IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) AS 'note_by',
					N.cntCompany,
					N.note_txt,
					N.note_type,
					concat(N.note_date,' ',IfNull(N.note_time,'00:00:00')) as note_date,
					N.note_time
			FROM tblnotes N
			JOIN  tbluser U
				ON U.user_name = N.note_by
			JOIN contacts C
				ON C.email = U.user_email
			WHERE  C.id = %s
				AND N.note_date > DATE_SUB(CURRENT_DATE(), INTERVAL 2 YEAR)	
			ORDER BY  N.note_date DESC, N.note_time DESC, N.note_Id DESC", $this->GetSQLValueString($userId, "int") );									

			$returnData = $this->fetchResults( $sql );
			
		}
		return $returnData;
	}	
	
	/**
	* Function to generate query for company notes (without Avior lib data)
	*/	
	public function getCompanyNotesSql($id, $filterData) {
		$sql = '';
		
		if($id != '') {									

			$sql = sprintf("SELECT DISTINCT N.note_Id
					, N.notebyNick
					, N.note_cnt_name
					, CONCAT(U.user_fname,' ',SUBSTRING(TRIM(U.user_lname),1,1)) AS 'note_by'
					, N.cntCompany
					, N.note_txt
					, N.note_type
					, REPLACE(N.note_date, ' ', '<br>') as note_date
					FROM (
						-- This query is more accurate from a company point of view, but its joining on the name of the company, string. 
						SELECT N.note_Id
							, N.notebyNick
            	, N.note_by
							, N.note_cnt_name
							, N.cntCompany
							, N.note_txt
							, N.note_type
							, concat(N.note_date,' ',IfNull(N.note_time,'00:00:00')) as note_date
						FROM tblnotes N
						JOIN contacts C
							ON C.id = N.note_cnt_Id
						JOIN tblcompanylist P
							ON N.cntCompany = P.Company
						WHERE P.comp_id = %s

					UNION
					
						-- This query gets all results missed above and joins on the contacts details.
						SELECT N.note_Id
									, N.notebyNick
														, N.note_by
									, N.note_cnt_name
									, N.cntCompany
									, N.note_txt
									, N.note_type
									, concat(N.note_date,' ',IfNull(N.note_time,'00:00:00')) as note_date
						FROM tblnotes N
						JOIN contacts C
							ON C.id = N.note_cnt_Id
						LEFT JOIN tblcompanylist P
							ON N.cntCompany = P.Company
						WHERE P.comp_id IS NULL
							 AND C.company_id = %s
					) N
				LEFT JOIN tbluser U
					ON U.user_name = N.note_by
				-- add date filter
				WHERE 
					N.note_date > DATE_SUB(CURRENT_DATE(), INTERVAL 2 YEAR)", $this->GetSQLValueString($id, "int"), $this->GetSQLValueString($id, "int") );											
						
			if ($filterData['maxDate'] != null && $filterData['minDate'] != null) {
				
				$sql = $sql . sprintf(" AND N.note_date >= %s AND N.note_date <= %s", 
					$this->GetSQLValueString($filterData['minDate'], "date"),
					$this->GetSQLValueString($filterData['maxDate'], "date")
					);				
			}
			
			if ($filterData['note'] != null) {
				$sql = $sql . sprintf(" AND N.note_txt LIKE '%%%s%%'", 
					$this->GetSQLValueString($filterData['note'], "string")
					);								
			}
			
			if ($filterData['type'] != null) {
				$sql = $sql . sprintf(" AND N.note_type = '%s'", 
					$this->GetSQLValueString($filterData['type'], "string")
					);								
			}			
			
			if ($filterData['analyst'] != null) {
				
				$sql = $sql . sprintf(" AND CONCAT(U.user_fname,' ',SUBSTRING(TRIM(U.user_lname),1,1)) = '%s'", 
					$this->GetSQLValueString($filterData['analyst'], "string")
					);				
			}			
						
			if ($filterData['client'] != null) {
				$sql = $sql . sprintf(" AND N.note_cnt_name = '%s'", 
					$this->GetSQLValueString($filterData['client'], "string")
					);								
			}				
						
			$sqlPostfix = " ORDER BY N.note_date DESC";
								
			$sql = $sql . $sqlPostfix;			
	
		}
		return $sql;
	}
	
	/**
	* Function to generate query for company notes (WITH Avior lib data)
	*/		
	public function getCompanyNotesSqlWithAviorLibData($id, $filterData) {
		
		$sql = '';
		
		if($id != '') {									
			
			$dateRange = " DATE_SUB(CURRENT_DATE(), INTERVAL 2 YEAR) ";
			
			$sqlString = "SELECT 
				REPLACE(N.note_date, ' ', '<br>') as note_date				
				, IF(N.note_type = \"Webview open\" OR N.note_type = \"Email read\",
					CONCAT(
						N.note_txt, 
						' <span class=\"badge count count_', N.viewNo, 
						'\" data-notetype=\"', REPLACE(LOWER(N.note_type), ' ', ''), 
						'\" data-taskid=\"', N.task_id, 
						'\" data-reportid=\"', N.report_id, 
						'\" data-count=\"', N.viewNo ,
						'\" data-contact=\"', N.note_cnt_name ,						
						'\" data-rticker=\"', N.report_ticker ,												
						'\" data-rtitle=\"', N.report_title ,							
						'\" data-email=\"', N.Contact_email ,																							
						'\" >', N.viewNo, '</span>'
					), 
					N.note_txt) AS note_txt
				, N.note_type
				, N.viewNo
				-- , IFNULL(CONCAT(U.user_fname, ' ', U.user_lname), N.notebyNick) AS 'note_by'
				, IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) AS 'note_by'
				-- for display purposes in main view and modal
				, N.note_cnt_name
				-- for email read mod
				-- , N.task_id				
				-- webview modal
				, N.Contact_email
				, N.report_ticker
				, N.report_title
	
			FROM (
				-- Notes as found in the CRM and the current view
				
					SELECT DISTINCT
										N.note_by
									, N.notebyNick
									, C.first_name			AS 'Contact_fname'
									, C.surname			AS 'Contact_lname'
									, N.cntCompany
									, N.note_txt
									, N.note_type
									, concat(N.note_date,' ',IfNull(N.note_time,'00:00:00')) as note_date
									, 1 AS 'viewNo'
									-- Extra info for modal
										, N.note_Id AS task_id
										, C.email	AS 'Contact_email'
										, ' '				AS 'report_ticker'
										, ' '				AS 'report_title'
										, 0 AS 'report_id'
										, IfNull(CONCAT(C.first_name,' ', C.surname), 'Company Note' ) AS  'note_cnt_name'
					
								FROM tblnotes N 
									JOIN tblcompanylist P ON N.cntCompany = P.Company
									LEFT JOIN contacts C ON C.id = N.note_cnt_Id
								WHERE
									P.comp_id = %s 
									AND N.note_date > ".$dateRange."						
				UNION 
			 
					-- event_logs from AviorLib
					SELECT  
						 R.username
						, R.first_name		AS 'notebyNick'
						, E.first_name		AS 'Contact_fname'
						, E.surname			AS 'Contact_lname'
						, E.Company
						, CONCAT_WS(' | ', E.report_ticker, E.report_title)  AS 'note_txt'
						, 'Webview open' AS 'note_type'
						, E.note_date
						, E.count AS 'viewNo'
					-- Extra info for modal
						, Event_id			AS 'task_id'
						, E.user				AS 'Contact_email'
						, E.report_ticker
						, E.report_title
						, R.id AS 'report_id'
						, CONCAT(E.first_name,' ', E.surname) AS  'note_cnt_name'
					FROM (
						SELECT 
								MAX(E.id)					AS Event_id
							, E.report_id
							, E.user
							, C.first_name
							, C.surname
							, P.Company
							, E.report_title
							, E.report_ticker
							, MAX(E.date_created) AS 'note_date'
							, COUNT(DISTINCT UNIX_TIMESTAMP(E.date_created) DIV 300) AS 'count'
						FROM  
							contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
							JOIN `avior`.event_logs E ON C.email = E.user
						WHERE 
							P.comp_id = %s 
							AND E.description = 'Webview Document'
							AND E.date_created > ".$dateRange."
						GROUP BY 
							E.report_id
							, E.user
							, E.report_title
							, E.report_ticker
							, C.first_name
							, C.surname, P.Company
						) E
						JOIN (
							SELECT 
									R.id
								, R.title
								, R.primary_analyst
								, U.username, U.first_name
								, GROUP_CONCAT(T.Name ORDER BY T.Name) AS 'Tickers'
							FROM `avior`.reports R 
								JOIN `avior`.reports_tickers RT ON R.id = RT.report_id
								JOIN `avior`.tickers T ON RT.ticker_id = T.id
								JOIN `avior`.users U ON R.primary_analyst = U.id
							GROUP BY 
								R.id
								, R.title
								, R.primary_analyst
								, U.username
							) R 
							 ON E.report_id = R.id
			
					UNION 
			 
						SELECT 
								U.username
								, U.first_name
								, C.first_name			AS 'Contact_fname'
								, C.surname			AS 'Contact_lname'
								, MT.company
								, CONCAT_WS(' | ', ER.report_ticker, ER.report_title)	AS 'note_txt'
								, 'Email read' AS 'Note_type'
								, MAX(ER.date_created) AS 'note_date'
								, COUNT(DISTINCT UNIX_TIMESTAMP(ER.date_created) DIV 300) AS 'viewNo'
									-- Extra info for modal
									, MT.id						AS 'task_id'
									, MT.email				AS 'Contact_email'
									, ER.report_ticker
									, ER.report_title
									, 0 AS 'report_id'
									, CONCAT(C.first_name,' ', C.surname) AS  'note_cnt_name'
							FROM 
								contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
								JOIN `avior`.mailertasks MT ON C.email = MT.email
								JOIN `avior`.email_reads ER ON MT.id = ER.task_id 
								JOIN `avior`.jobs J ON MT.job_id = J.id
								JOIN `avior`.users U ON J.user_id = U.id
							 WHERE 
								P.comp_id = %s
								AND ER.date_created > ".$dateRange."
							GROUP BY
								U.username
								, U.first_name
								, C.first_name
								, C.surname
								, MT.id
								, MT.company
								, ER.report_title
								, ER.report_ticker
						) N
			LEFT JOIN tbluser U ON U.user_name = N.note_by";
			
							
			$sql = sprintf($sqlString, $this->GetSQLValueString($id, "int"), $this->GetSQLValueString($id, "int"), $this->GetSQLValueString($id, "int") );											
		
			
			$sqlWhereClause = '';
						
			if ($filterData['maxDate'] != null && $filterData['minDate'] != null) {
				
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_date >= %s AND N.note_date <= %s", 
					$this->GetSQLValueString($filterData['minDate'], "date"),
					$this->GetSQLValueString($filterData['maxDate'], "date")
					);				
			}
			
			if ($filterData['note'] != null) {
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_txt LIKE '%%%s%%'", 
					$this->GetSQLValueString($filterData['note'], "string")
					);								
			}
			
			if ($filterData['type'] != null) {
				
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_type = '%s'", 
					$this->GetSQLValueString($filterData['type'], "string")
					);								
			}			
			
			if ($filterData['analyst'] != null) {
				
				/*
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND CONCAT(U.user_fname,' ',U.user_lname) = '%s'", 
					$this->GetSQLValueString($filterData['analyst'], "string")
					);				
				*/
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) = '%s'", 
					$this->GetSQLValueString($filterData['analyst'], "string")
				);								
				
			}			
						
			if ($filterData['client'] != null) {
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_cnt_name = '%s'", 
					$this->GetSQLValueString($filterData['client'], "string")
					);								
			}			

			$prefix = " AND";							
			if (substr($sqlWhereClause, 0, strlen($prefix)) == $prefix) {
					$sqlWhereClause = " WHERE ".substr($sqlWhereClause, strlen($prefix));
			}			
						
			$sqlPostfix = " ".$sqlWhereClause." ORDER BY N.note_date DESC";
								
			$sql = $sql . $sqlPostfix;
			
			//echo $sql;
			//exit();								
			
		}
		return $sql;
	}	

	public function getCompanyNotesSqlWithAviorLibDataBeforeReportId($id, $filterData) {
		
		$sql = '';
		
		if($id != '') {									
			
			$dateRange = " DATE_SUB(CURRENT_DATE(), INTERVAL 2 YEAR) ";
			
			$sqlString = "SELECT 
				REPLACE(N.note_date, ' ', '<br>') as note_date				
				, IF(N.note_type = \"Webview open\" OR N.note_type = \"Email read\",
					CONCAT(
						N.note_txt, 
						' <span class=\"badge count count_', N.viewNo, 
						'\" data-notetype=\"', REPLACE(LOWER(N.note_type), ' ', ''), 
						'\" data-taskid=\"', N.task_id, 
						'\" data-reportid=\"', N.task_id, 
						'\" data-count=\"', N.viewNo ,
						'\" data-contact=\"', N.note_cnt_name ,						
						'\" data-rticker=\"', N.report_ticker ,												
						'\" data-rtitle=\"', N.report_title ,							
						'\" data-email=\"', N.Contact_email ,																							
						'\" >', N.viewNo, '</span>'
					), 
					N.note_txt) AS note_txt
				, N.note_type
				, N.viewNo
				-- , IFNULL(CONCAT(U.user_fname, ' ', U.user_lname), N.notebyNick) AS 'note_by'
				, IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) AS 'note_by'
				-- for display purposes in main view and modal
				, N.note_cnt_name
				-- for email read mod
				-- , N.task_id				
				-- webview modal
				, N.Contact_email
				, N.report_ticker
				, N.report_title
	
			FROM (
				-- Notes as found in the CRM and the current view
				
					SELECT DISTINCT
										N.note_by
									, N.notebyNick
									, C.first_name			AS 'Contact_fname'
									, C.surname			AS 'Contact_lname'
									, N.cntCompany
									, N.note_txt
									, N.note_type
									, concat(N.note_date,' ',IfNull(N.note_time,'00:00:00')) as note_date
									, 1 AS 'viewNo'
									-- Extra info for modal
										, N.note_Id AS task_id
										, C.email	AS 'Contact_email'
										, ' '				AS 'report_ticker'
										, ' '				AS 'report_title'
										, IfNull(CONCAT(C.first_name,' ', C.surname), 'Company Note' )AS  'note_cnt_name'
					
								FROM tblnotes N 
									JOIN tblcompanylist P ON N.cntCompany = P.Company
									LEFT JOIN contacts C ON C.id = N.note_cnt_Id
								WHERE
									P.comp_id = %s 
									AND N.note_date > ".$dateRange."						
				UNION 
			 
					-- event_logs from AviorLib
					SELECT  
						 R.username
						, R.first_name		AS 'notebyNick'
						, E.first_name		AS 'Contact_fname'
						, E.surname			AS 'Contact_lname'
						, E.Company
						, CONCAT_WS(' | ', E.report_ticker, E.report_title)  AS 'note_txt'
						, 'Webview open' AS 'note_type'
						, E.note_date
						, E.count AS 'viewNo'
					-- Extra info for modal
						, Event_id			AS 'task_id'
						, E.user				AS 'Contact_email'
						, E.report_ticker
						, E.report_title
						, CONCAT(E.first_name,' ', E.surname) AS  'note_cnt_name'
					FROM (
						SELECT 
								MAX(E.id)					AS Event_id
							, E.report_id
							, E.user
							, C.first_name
							, C.surname
							, P.Company
							, E.report_title
							, E.report_ticker
							, MAX(E.date_created) AS 'note_date'
							, COUNT(DISTINCT UNIX_TIMESTAMP(E.date_created) DIV 300) AS 'count'
						FROM  
							contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
							JOIN `avior`.event_logs E ON C.email = E.user
						WHERE 
							P.comp_id = %s 
							AND E.description = 'Webview Document'
							AND E.date_created > ".$dateRange."
						GROUP BY 
							E.report_id
							, E.user
							, E.report_title
							, E.report_ticker
							, C.first_name
							, C.surname, P.Company
						) E
						JOIN (
							SELECT 
									R.id
								, R.title
								, R.primary_analyst
								, U.username, U.first_name
								, GROUP_CONCAT(T.Name ORDER BY T.Name) AS 'Tickers'
							FROM `avior`.reports R 
								JOIN `avior`.reports_tickers RT ON R.id = RT.report_id
								JOIN `avior`.tickers T ON RT.ticker_id = T.id
								JOIN `avior`.users U ON R.primary_analyst = U.id
							GROUP BY 
								R.id
								, R.title
								, R.primary_analyst
								, U.username
							) R 
							 ON E.report_id = R.id
			
					UNION 
			 
						SELECT 
								U.username
								, U.first_name
								, C.first_name			AS 'Contact_fname'
								, C.surname			AS 'Contact_lname'
								, MT.company
								, CONCAT_WS(' | ', ER.report_ticker, ER.report_title)	AS 'note_txt'
								, 'Email read' AS 'Note_type'
								, MAX(ER.date_created) AS 'note_date'
								, COUNT(DISTINCT UNIX_TIMESTAMP(ER.date_created) DIV 300) AS 'viewNo'
									-- Extra info for modal
									, MT.id						AS 'task_id'
									, MT.email				AS 'Contact_email'
									, ER.report_ticker
									, ER.report_title
															, CONCAT(C.first_name,' ', C.surname) AS  'note_cnt_name'
							FROM 
								contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
								JOIN `avior`.mailertasks MT ON C.email = MT.email
								JOIN `avior`.email_reads ER ON MT.id = ER.task_id 
								JOIN `avior`.jobs J ON MT.job_id = J.id
								JOIN `avior`.users U ON J.user_id = U.id
							 WHERE 
								P.comp_id = %s
								AND ER.date_created > ".$dateRange."
							GROUP BY
								U.username
								, U.first_name
								, C.first_name
								, C.surname
								, MT.id
								, MT.company
								, ER.report_title
								, ER.report_ticker
						) N
			LEFT JOIN tbluser U ON U.user_name = N.note_by";
			
							
			$sql = sprintf($sqlString, $this->GetSQLValueString($id, "int"), $this->GetSQLValueString($id, "int"), $this->GetSQLValueString($id, "int") );											
		
			
			$sqlWhereClause = '';
						
			if ($filterData['maxDate'] != null && $filterData['minDate'] != null) {
				
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_date >= %s AND N.note_date <= %s", 
					$this->GetSQLValueString($filterData['minDate'], "date"),
					$this->GetSQLValueString($filterData['maxDate'], "date")
					);				
			}
			
			if ($filterData['note'] != null) {
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_txt LIKE '%%%s%%'", 
					$this->GetSQLValueString($filterData['note'], "string")
					);								
			}
			
			if ($filterData['type'] != null) {
				
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_type = '%s'", 
					$this->GetSQLValueString($filterData['type'], "string")
					);								
			}			
			
			if ($filterData['analyst'] != null) {
				
				/*
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND CONCAT(U.user_fname,' ',U.user_lname) = '%s'", 
					$this->GetSQLValueString($filterData['analyst'], "string")
					);				
				*/
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) = '%s'", 
					$this->GetSQLValueString($filterData['analyst'], "string")
				);								
				
			}			
						
			if ($filterData['client'] != null) {
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_cnt_name = '%s'", 
					$this->GetSQLValueString($filterData['client'], "string")
					);								
			}			

			$prefix = " AND";							
			if (substr($sqlWhereClause, 0, strlen($prefix)) == $prefix) {
					$sqlWhereClause = " WHERE ".substr($sqlWhereClause, strlen($prefix));
			}			
						
			$sqlPostfix = " ".$sqlWhereClause." ORDER BY N.note_date DESC";
								
			$sql = $sql . $sqlPostfix;
			
			//echo $sql;
			//exit();								
			
		}
		return $sql;
	}		
	
	/**
	* Function to generate query for user notes (WITH Avior lib data)
	*/		
	public function getUserNotesSqlWithAviorLibData($id, $filterData = []) {
		
		$sql = '';
		
		if($id != '') {									
			
			$dateRange = " DATE_SUB(CURRENT_DATE(), INTERVAL 2 YEAR) ";
			
			$sqlString = "SELECT 
				N.note_id as note_Id
				,REPLACE(N.note_date, ' ', '<br>') as note_date				
				, IF(N.note_type = \"Webview open\" OR N.note_type = \"Email read\",
					CONCAT(
						N.note_txt, 
						' <span class=\"badge count count_', N.viewNo, 
						'\" data-notetype=\"', REPLACE(LOWER(N.note_type), ' ', ''), 
						'\" data-taskid=\"', N.task_id, 
						'\" data-reportid=\"', N.report_id, 
						'\" data-count=\"', N.viewNo ,
						'\" data-contact=\"', N.note_cnt_name ,						
						'\" data-rticker=\"', N.report_ticker ,												
						'\" data-rtitle=\"', N.report_title ,							
						'\" data-email=\"', N.Contact_email ,																							
						'\" >', N.viewNo, '</span>'
					), 
					N.note_txt) AS note_txt
				, N.note_type
				, N.viewNo				
				-- , IFNULL(CONCAT(U.user_fname, ' ', U.user_lname), N.notebyNick) AS 'note_by'
				, IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) AS 'note_by'
				-- for display purposes in main view and modal
				, N.note_cnt_name
				-- for email read mod
				-- , N.task_id				
				-- webview modal
				, N.Contact_email
				, N.report_ticker
				, N.report_title
	
			FROM (
				-- Notes as found in the CRM and the current view
				
					SELECT DISTINCT 
							N.note_Id
							, N.note_by
							, N.notebyNick
							, C.first_name			AS 'Contact_fname'
							, C.surname			AS 'Contact_lname'
							, N.cntCompany
							, N.note_txt
							, N.note_type
							, concat(N.note_date,' ', ifnull(N.note_time, '00:00:00')) as note_date
							, 1 AS 'viewNo'
							-- Extra info for modal
								, N.note_Id AS task_id
								, C.email	AS 'Contact_email'
								, ' ' AS 'report_ticker'
								, ' '	AS 'report_title'
								, ' '	AS 'report_id'
							, CONCAT(C.first_name,' ', C.surname) AS  'note_cnt_name'
								
						FROM 
							tblnotes N JOIN contacts C ON C.id = N.note_cnt_Id
							JOIN tblcompanylist P ON N.cntCompany = P.Company
						WHERE 
							C.id = %s 
							AND N.note_date > ".$dateRange."					
					
				UNION 
			 
					-- event_logs from AviorLib
					SELECT  
						'0' AS 'note_Id'
						, R.username
						, R.first_name		AS 'notebyNick'
						, E.first_name		AS 'Contact_fname'
						, E.surname			AS 'Contact_lname'
						, E.Company
						, CONCAT_WS(' | ', E.report_ticker, E.report_title)  AS 'note_txt'
						, 'Webview open' AS 'note_type'
						, E.note_date
						, E.count AS 'viewNo'
					-- Extra info for modal
						, Event_id			AS 'task_id'
						, E.user				AS 'Contact_email'
						, E.report_ticker
						, E.report_title
						, E.report_id
						, CONCAT(E.first_name,' ', E.surname) AS  'note_cnt_name'
					FROM (
						SELECT 
								MAX(E.id)					AS Event_id
							, E.user
							, C.first_name
							, C.surname
							, P.Company
							, E.report_title
							, E.report_ticker
							, E.report_id
							, MAX(E.date_created) AS 'note_date'
							, COUNT(DISTINCT UNIX_TIMESTAMP(E.date_created) DIV 300) AS 'count'
						FROM  
							contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
							JOIN `avior`.event_logs E ON C.email = E.user
						WHERE 
							C.id = %s 
							AND E.description = 'Webview Document'
							AND E.date_created > ".$dateRange."
						GROUP BY 
							E.report_id
							, E.user
							, E.report_title
							, E.report_ticker
							, C.first_name
							, C.surname, P.Company
						) E
						JOIN (
							SELECT 
									R.id
								, R.title
								, R.primary_analyst
								, U.username, U.first_name
								, GROUP_CONCAT(T.Name ORDER BY T.Name) AS 'Tickers'
							FROM `avior`.reports R 
								JOIN `avior`.reports_tickers RT ON R.id = RT.report_id
								JOIN `avior`.tickers T ON RT.ticker_id = T.id
								JOIN `avior`.users U ON R.primary_analyst = U.id
							GROUP BY 
								R.id
								, R.title
								, R.primary_analyst
								, U.username
							) R 
							 ON E.report_id = R.id
			
					UNION 
			 
						SELECT
								'0' AS 'note_Id' 
								, U.username
								, U.first_name
								, C.first_name			AS 'Contact_fname'
								, C.surname			AS 'Contact_lname'
								, MT.company
								, CONCAT_WS(' | ', ER.report_ticker, ER.report_title)	AS 'note_txt'
								, 'Email read' AS 'Note_type'
								, MAX(ER.date_created) AS 'note_date'
								, COUNT(DISTINCT UNIX_TIMESTAMP(ER.date_created) DIV 300) AS 'viewNo'
									-- Extra info for modal
									, MT.id						AS 'task_id'
									, MT.email				AS 'Contact_email'
									, ER.report_ticker
									, ER.report_title
									, '' AS report_id
									, CONCAT(C.first_name,' ', C.surname) AS  'note_cnt_name'
							FROM 
								contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
								JOIN `avior`.mailertasks MT ON C.email = MT.email
								JOIN `avior`.email_reads ER ON MT.id = ER.task_id 
								JOIN `avior`.jobs J ON MT.job_id = J.id
								JOIN `avior`.users U ON J.user_id = U.id
							 WHERE 
								C.id = %s
								AND ER.date_created > ".$dateRange."
							GROUP BY
								U.username
								, U.first_name
								, C.first_name
								, C.surname
								, MT.id
								, MT.company
								, ER.report_title
								, ER.report_ticker
						) N
			LEFT JOIN tbluser U ON U.user_name = N.note_by";
			
							
			$sql = sprintf($sqlString, $this->GetSQLValueString($id, "int"), $this->GetSQLValueString($id, "int"), $this->GetSQLValueString($id, "int") );
			
			$sqlWhereClause = '';
			
			if(sizeof($filterData) > 0) {
						
				if ($filterData['maxDate'] != null && $filterData['minDate'] != null) {
					
					$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_date >= %s AND N.note_date <= %s", 
						$this->GetSQLValueString($filterData['minDate'], "date"),
						$this->GetSQLValueString($filterData['maxDate'], "date")
						);				
				}
				
				if ($filterData['note'] != null) {
					$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_txt LIKE '%%%s%%'", 
						$this->GetSQLValueString($filterData['note'], "string")
						);								
				}
				
				if ($filterData['type'] != null) {
					
					$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_type = '%s'", 
						$this->GetSQLValueString($filterData['type'], "string")
						);								
				}			
				
				if ($filterData['analyst'] != null) {
					
					$sqlWhereClause = $sqlWhereClause . sprintf(" AND CONCAT(U.user_fname,' ',SUBSTRING(TRIM(U.user_lname),1,1)) = '%s'", 
						$this->GetSQLValueString($filterData['analyst'], "string")
						);				
				}			
							
				if ($filterData['client'] != null) {
					$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_cnt_name = '%s'", 
						$this->GetSQLValueString($filterData['client'], "string")
						);								
				}			
	
				$prefix = " AND";							
				if (substr($sqlWhereClause, 0, strlen($prefix)) == $prefix) {
						$sqlWhereClause = " WHERE ".substr($sqlWhereClause, strlen($prefix));
				}			
			}
			
			$sqlPostfix = " ".$sqlWhereClause." ORDER BY N.note_date DESC";
								
			$sql = $sql . $sqlPostfix;			
			
		}
		return $sql;
	}		
	
	/**
	* Function to generate query for company notes (WITH Avior lib data)
	*/		
	public function getCompanyNotesSqlWithAviorLibDataForExport($id, $filterData, $getData) {
		
		$sql = '';
		
		if($id != '') {									
			
			$dateRange = " DATE_SUB(CURRENT_DATE(), INTERVAL 2 YEAR) ";
			

		$sqlString = "
			SELECT		
				N.task_id AS 'note_id'
				, U.user_fname
				, U.user_lname
				-- , N.notebyNick
				, IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) AS 'notebyNick'
				, N.note_cnt_name
				, N.cntCompany
				, N.note_type
				, N.note_txt
				, N.note_date
					
			FROM (
				-- Notes as found in the CRM and the current view
			
					SELECT DISTINCT
								N.note_by
							, N.notebyNick
							, C.first_name			AS 'Contact_fname'
							, C.surname			AS 'Contact_lname'
							, N.cntCompany
							, N.note_txt
							, N.note_type
							, concat(N.note_date,' ',IfNull(N.note_time,'00:00:00')) as note_date
							-- Extra info for modal
								, N.note_Id AS task_id
								, C.email	AS 'Contact_email'
								, ' '				AS 'report_ticker'
								, ' '				AS 'report_title'
								, IfNull(CONCAT(C.first_name,' ', C.surname), 'Company Note' )AS  'note_cnt_name'
			
						FROM
							tblnotes N JOIN tblcompanylist P ON N.cntCompany = P.Company
							LEFT JOIN contacts C ON C.id = N.note_cnt_Id
						WHERE
							P.comp_id = %s
							AND N.note_date > ".$dateRange."	
			
				UNION
			
					-- event_logs from AviorLib
					SELECT
						 R.username
						, R.first_name		AS 'notebyNick'
						, E.first_name		AS 'Contact_fname'
						, E.surname			AS 'Contact_lname'
						, E.Company
						, CONCAT_WS(' | ', E.report_ticker, E.report_title)  AS 'note_txt'
						, 'Webview open' AS 'note_type'
						, E.note_date
					-- Extra info for modal
						, Event_id			AS 'task_id'
						, E.user				AS 'Contact_email'
						, E.report_ticker
						, E.report_title
						, CONCAT(E.first_name,' ', E.surname) AS  'note_cnt_name'
					FROM (
						SELECT
								MAX(E.id)					AS Event_id
							, E.report_id
							, E.user
							, C.first_name
							, C.surname
							, P.Company
							, E.report_title
							, E.report_ticker
							, MAX(E.date_created) AS 'note_date'
						FROM
							contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
							JOIN `avior`.event_logs E ON C.email = E.user
						WHERE
							P.comp_id = %s
							AND E.description = 'Webview Document'
							AND E.date_created > ".$dateRange."	
						GROUP BY
							E.report_id
							, E.user
							, E.report_title
							, E.report_ticker
							, C.first_name
							, C.surname, P.Company
											, UNIX_TIMESTAMP(E.date_created) DIV 300
						) E
						JOIN (
							SELECT
									R.id
								, R.title
								, R.primary_analyst
								, U.username, U.first_name
								, GROUP_CONCAT(T.Name ORDER BY T.Name) AS 'Tickers'
							FROM `avior`.reports R
								JOIN `avior`.reports_tickers RT ON R.id = RT.report_id
								JOIN `avior`.tickers T ON RT.ticker_id = T.id
								JOIN `avior`.users U ON R.primary_analyst = U.id
							GROUP BY
								R.id
								, R.title
								, R.primary_analyst
								, U.username
							) R
							 ON E.report_id = R.id
			
					UNION
			
						SELECT
								U.username
								, U.first_name
								, C.first_name			AS 'Contact_fname'
								, C.surname			AS 'Contact_lname'
								, MT.company
								, CONCAT_WS(' | ', ER.report_ticker, ER.report_title)	AS 'note_txt'
								, 'Email read' AS 'Note_type'
								, MAX(ER.date_created) AS 'note_date'
									-- Extra info for modal
									, MT.id						AS 'task_id'
									, MT.email				AS 'Contact_email'
									, ER.report_ticker
									, ER.report_title
									, CONCAT(C.first_name,' ', C.surname) AS  'note_cnt_name'
							FROM
								contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
								JOIN `avior`.mailertasks MT ON C.email = MT.email
								JOIN `avior`.email_reads ER ON MT.id = ER.task_id
								JOIN `avior`.jobs J ON MT.job_id = J.id
								JOIN `avior`.users U ON J.user_id = U.id
							 WHERE
								P.comp_id = %s
								AND ER.date_created > ".$dateRange."	
							GROUP BY
								U.username
								, U.first_name
								, C.first_name
								, C.surname
								, MT.id
								, MT.company
								, ER.report_title
								, ER.report_ticker
													, UNIX_TIMESTAMP(ER.date_created) DIV 300
						) N
			LEFT JOIN tbluser U ON U.user_name = N.note_by";					
			
							
			$sql = sprintf($sqlString, $this->GetSQLValueString($id, "int"), $this->GetSQLValueString($id, "int"), $this->GetSQLValueString($id, "int") );											
			
			$sqlWhereClause = '';			
						
			if ($filterData['maxDate'] != null && $filterData['minDate'] != null) {
				
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_date >= %s AND N.note_date <= %s", 
					$this->GetSQLValueString($filterData['minDate'], "date"),
					$this->GetSQLValueString($filterData['maxDate'], "date")
					);				
			}
			
			if ($filterData['note'] != null) {
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_txt LIKE '%%%s%%'", 
					$this->GetSQLValueString($filterData['note'], "string")
					);								
			}
			
			if ($filterData['type'] != null) {				
				
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_type = '%s'", 
					$this->GetSQLValueString($filterData['type'], "string")
					);								
			}			
			
			if ($filterData['analyst'] != null) {
				

				// $sqlWhereClause = $sqlWhereClause . sprintf(" AND CONCAT(U.user_fname,' ',SUBSTRING(TRIM(U.user_lname),1,1)) = '%s'", 
				// 	$this->GetSQLValueString($filterData['analyst'], "string")
				// 	);				
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) = '%s'", 
					$this->GetSQLValueString($filterData['analyst'], "string")
				);								
				
			}			
						
			if ($filterData['client'] != null) {
				$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_cnt_name = '%s'", 
					$this->GetSQLValueString($filterData['client'], "string")
					);								
			}			

			$prefix = " AND";							
			if (substr($sqlWhereClause, 0, strlen($prefix)) == $prefix) {
					$sqlWhereClause = " WHERE ".substr($sqlWhereClause, strlen($prefix));
			}			
						
			$sqlPostfix = " ".$sqlWhereClause;
			$sql = $sql . $sqlPostfix;			
			
			//." ORDER BY N.note_date DESC";
			
			//Map GET sorting filters to columns
			$sortMapping = [
				'0' => 'N.note_date',
				'1' => 'N.note_txt',
				'2' => 'N.note_type',
				'3' => "CONCAT(U.user_fname,' ',SUBSTRING(TRIM(U.user_lname),1,1))",
				'4' => 'N.note_cnt_name'
			];

			$orderClause = '';			
			$numSortColumns = isset($getData['iSortingCols']) ? intval($getData['iSortingCols']) : 0;
			
			for($t=0;$t<$numSortColumns;$t++) {
				if (isset($getData['iSortCol_'.$t])) {
					
					$columnIndex = $getData['iSortCol_'.$t];
					$sortDirection = $getData['sSortDir_'.$t];
				
					$orderClause .= ", ".$sortMapping[$columnIndex]. " ". $sortDirection;
				}
			}					
			
			$orderClause = trim($orderClause, ',');
			if ($orderClause != '') {
				$orderClause = " ORDER BY ".$orderClause;
			}
								
			$sql = $sql . $orderClause;																					
			
		}

		return $sql;
		
	}		
	
	/**
	* Returns read emails by task id
	*/
	public function getReadEmailsByTaskId($taskId)	
	{
		$returnData = [];
		
		if (intval($taskId) > 0 ) {
		
			$sql = sprintf("SELECT MAX(ER.date_created)	AS 'date_created'
				, ER.ip_address
				, ER.reader
				, ER.report_ticker
				, ER.host_name				
				, ER.report_title
			FROM
				`avior`.email_reads ER
			WHERE
				ER.task_id = %s
			GROUP BY
				ER.reader
				, ER.report_ticker
				, ER.report_title
				, UNIX_TIMESTAMP(ER.date_created) DIV 300", $this->GetSQLValueString($taskId, "int") );									

			$returnData = $this->fetchResults( $sql );		
			
			$returnData = json_encode($returnData);
			
		}			
		
		return $returnData;
	}
	
	/**
	* Returns webview opens
	*/
	public function getWebviewOpens($email, $reportTitle, $reportTicker, $contactName)
	{
		$returnData = [];				

		if ($email != '' && $reportTitle != '' && $reportTicker != '' && $contactName != '') {
		
			$sql = sprintf("SELECT 
					MAX(E.date_created) AS 'date_created'
					, E.affected_user AS 'ip_address'
					, %s AS 'reader'
					, E.report_ticker,
					E.report_title
				FROM  `avior`.event_logs E
				WHERE E.description = 'Webview Document'
					AND E.user = %s
					AND E.report_title = %s
					AND E.report_ticker = %s
				GROUP BY 
					E.user
					, E.report_title
					, E.report_ticker
					, UNIX_TIMESTAMP(E.date_created) DIV 300
				ORDER BY
					E.date_created  DESC",
					
				$this->GetSQLValueString($contactName, "text"),
				$this->GetSQLValueString($email, "text"),
				$this->GetSQLValueString($reportTitle, "text"),
				$this->GetSQLValueString($reportTicker, "text")
					
			);							

			$returnData = $this->fetchResults( $sql );		
			
			$returnData = json_encode($returnData);
			
		}			
		
		return $returnData;
	}	

	/**
	* Get Note Webview Open Meta Data
	*/
	public function getWebViewOpenMetaData($reportId, $email)
	{
		$metaData = [];

		/*
		echo "getWebViewOpenMetaData\r\n";
		echo "reportId: $reportId\r\n";
		echo "email: $email\r\n";
		exit();
		*/

		if (intval($reportId) > 0 && $email != '') {

			$sql = sprintf("CALL  sp_getWebViewOpenMetaData(%s , '%s')", $this->GetSQLValueString($reportId, "int"), $this->GetSQLValueString($email, "string"));

			$returnData = $this->fetchResults( $sql );	

			$metaData = [
				'reportid' => $reportId,
				'email' => $email,
				'opened' => 0,
				'pages' => 0,
				'duration' => $this->_formatSeconds(0)
			];

			if( sizeof($returnData) > 0 ) {

				$returnDataSize = sizeof($returnData);

				//Find data where result = 1;
				for($y=0;$y<$returnDataSize;$y++) {
					if(isset($returnData[$y]['Result']) && intval($returnData[$y]['Result']) == 1) {

						$metaData = [
							'reportid' => $returnData[$y]['report_id'],
							'email' => $returnData[$y]['user'],
							'opened' => $returnData[$y]['Open_No'],
							'pages' => $returnData[$y]['No_Pages'],
							'duration' => $this->_formatSeconds($returnData[$y]['Total_Time_sec']),
							'secs' => $returnData[$y]['Total_Time_sec']
						];
						break;
					}
				}
			}


			$metaData = json_encode($metaData, JSON_FORCE_OBJECT);
		}		
		return $metaData;
	}

	private function _formatSeconds($totalSeconds) {

		$timeString = '';

		if (intval($totalSeconds) > 0) {
			$hours = floor($totalSeconds / 3600);
			$minutes = floor(($totalSeconds / 60) % 60);
			$seconds = $totalSeconds % 60;

			if ($hours>0) {
				$timeString = $hours."h".$minutes."m".$seconds."s";
			} else {
				$timeString = $minutes."m".$seconds."s";
			}
		} else {
			$timeString = '0s';
		}			
		return $timeString;
	}
		
	
	/**
	* Returns Company Notes
	*/
	public function getCompanyNotes($id, $filterData)
	{
		$returnData = array();
					
		//$sql = $this->getCompanyNotesSql($id, $filterData);										
		$sql = $this->getCompanyNotesSqlWithAviorLibData($id, $filterData);						
							
		if( $this->useDatatables == true) {
		
		
			$this->datatablesObj->useColumns( "note_date,note_txt,note_type,note_by,note_cnt_name");

			$dataTablesql = $this->datatablesObj->PrepareDataTableSql($sql);			
			
			$recordSet = $this->fetchResults($dataTablesql);			

			$numberOfRows = $this->GetFoundRows();
			$totalNumberOfRecords = $this->GetTotalNumberOfRecords($sql); 				
			$returnData = $this->datatablesObj->PrepareReturnData($recordSet, $totalNumberOfRecords, 'listtable', $numberOfRows);				

		} else {						
			$returnData = $this->fetchResults( $sql );
		}
		
		return $returnData;
	}
	
	/**
	* Returns Company Notes
	*/
	public function getCompanyNotesExportData($id, $filterData, $getData)
	{
		$returnData = array();
		
		//$sql = $this->getCompanyNotesSql($id, $filterData);		
		$sql = $this->getCompanyNotesSqlWithAviorLibDataForExport($id, $filterData, $getData);																				
			
		//$this->datatablesObj->useColumns( "note_date,note_txt,note_type,user_fname,user_lname,notebyNick,note_cnt_name");
		$dataTablesql = $this->datatablesObj->PrepareDataTableSql($sql);								
		
		$returnData = $this->fetchResults($dataTablesql);
		
		return $returnData;
	}	
	
	/**
	* Returns user note by ID.
	*/
	public function getUserNote($noteId)
	{
		$returnData = array();
		
		if($noteId > 0) {
			$sql = sprintf("SELECT DISTINCT
							tblnotes.note_Id AS 'note_id',
							tblnotes.note_cnt_name,
							CONCAT(tbluser.user_fname,' ',SUBSTRING(TRIM(tbluser.user_lname),1,1)) AS 'notebyNick',
							tblnotes.cntCompany,
							tblnotes.note_txt AS 'note_text',
							tblnotes.note_type,
							tblnotes.note_date,
							tblnotes.note_time
						FROM 
								tblnotes LEFT OUTER JOIN tbluser ON tbluser.user_name = tblnotes.note_by
						WHERE 
								tblnotes.note_Id = %s
						ORDER BY 
								tblnotes.note_date DESC", $this->GetSQLValueString($noteId, "int") );

			$tempReturnData = $this->fetchResults( $sql );	
			if( $tempReturnData > 0 ) $returnData = $tempReturnData[0];
		} 
		
		return $returnData;
	}
	
	
	/**
	 * Gets found rows
	 * @return $found_rows
	 */
	protected function GetFoundRows(){
		
		$found_rows  = 0;
		$sql = "SELECT FOUND_ROWS() AS 'found_rows'";
		$recordSet = $this->fetchResults( $sql );
		
		if (sizeof($recordSet) > 0) {

			$found_rows = $recordSet[0]['found_rows'];	
		}
		return $found_rows;
	}
	
	/**
	 * Gets the total number of records returned by a query
	 * @param string $sql
	 * @param array $parameters
	 * @return int $numberOfRecords
	 */
	public function GetTotalNumberOfRecords($sql)
	{
		
		$numberOfRecords = 0;
		if( '' != trim( $sql ) ){
			$countSql = " SELECT COUNT(*) AS 'number_of_records' FROM( ".$sql.")all_records";
			$recordSet = $this->fetchResults( $countSql );
			$numberOfRecords = isset($recordSet[0]['number_of_records']) ? $recordSet[0]['number_of_records'] : 0;
		}
		return $numberOfRecords;	
	}
	
	/**
	* Insert new notes
	**/
	public function add($noteData)
	{
		$returnData = array();
		if (is_array($noteData) && sizeof($noteData) > 0) {
			$insertSQL = sprintf("INSERT INTO tblnotes (note_by, notebyNick, note_cnt_Id, note_cnt_name, cntCompany, note_txt, note_type, note_date, note_time) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       $this->GetSQLValueString($noteData['note_by'], "text"),
                       $this->GetSQLValueString($noteData['notebyNick'], "text"),
                       $this->GetSQLValueString($noteData['note_cnt_Id'], "int"),
                       $this->GetSQLValueString($noteData['note_cnt_name'], "text"),
                       $this->GetSQLValueString($noteData['cntCompany'], "text"),
                       $this->GetSQLValueString($noteData['note_text'], "text"),
                       $this->GetSQLValueString($noteData['note_type'], "text"),
                       $this->GetSQLValueString($noteData['note_date'], "date"),
                       $this->GetSQLValueString($noteData['note_time'], "text"),
                       $this->GetSQLValueString($noteData['analyst_list'], "text"));
			
		  mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
		  $Result = mysql_query($insertSQL, $this->CRMconnection) or die(mysql_error());
		  if ($Result) {
			$noteId =  mysql_insert_id($this->CRMconnection);
			if($noteId > 0) {
				$returnData['note_id'] = $noteId;
			}
		  }
		}
		return $returnData;
	}
	
	/*
	* Deletes Note
	*/
	public function remove($noteId)
	{
		$returnResult = false;
		if ($noteId > 0) {
			$deleteSQL = sprintf("DELETE FROM tblnotes WHERE note_Id=%s", $this->GetSQLValueString($noteId, "int"));
			mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
			$Result = mysql_query($deleteSQL, $this->CRMconnection) or die(mysql_error());
			if ($Result) {
				$returnResult = true;
			}
		}
		return $returnResult;
	}
	
	/**
	* Update Notes
	*/
	public function update($noteId, $noteText)
	{
		$returnResults = false;
		if ($noteId > 0 && trim($noteText) != '') {
			$updateSQL = sprintf("UPDATE tblnotes SET note_txt=%s WHERE note_Id=%s", $this->GetSQLValueString($noteText, "text"), $this->GetSQLValueString($noteId, "int"));
		  	mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
 		    $Result = mysql_query($updateSQL, $this->CRMconnection);
		   if ($Result) {
		   		$returnResults = true;
		   }
		}
		return $returnResults;
	}
	
	/**
	* 
	**/
	public function updateClientNote($clientId, $noteText)
	{
		$returnResults = false;
		if($clientId > 0 &&  trim($noteText) != '') {
			 $updateSQL = sprintf("UPDATE contacts SET note=%s WHERE id=%s",$this->GetSQLValueString($noteText, "text"), $this->GetSQLValueString($clientId, "int"));
			  mysql_select_db($this->database_CRMconnection, $this->CRMconnection);
			  $Result = mysql_query($updateSQL, $this->CRMconnection);
			  if ($Result) {
				$returnResults = true;
			  }
		}
		return $returnResults;
	}
	/**
	* Not used anymore.
	**/
	protected function convertLinks($text)
	{
		$text = preg_replace(
					  "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/",
					  '<a href="$1" target="_blank">$1</a>',
					  $text
				);

		return $text;
	}
	
	/**
	* Helper function
	*/
	function GetSQLValueString($theValue, $theType )
	{

	  if (PHP_VERSION < 6) {
		$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
	  }
	  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
	  
	  switch ($theType) {
		case "text":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;    
		case "long":
		case "int":
		  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
		  break;
		case "double":
		  $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
		  break;
		case "date":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
	  }
	  return $theValue;
	}
	
	private function fetchResults( $sql )
	{
		$returnData = array();
		$db_connected =  mysql_select_db($this->database_CRMconnection, $this->CRMconnection);		
		if( $db_connected ) {
			$result = mysql_query($sql, $this->CRMconnection);
			if( $result ) {
				while ($row = mysql_fetch_assoc($result)) {
					$returnData[] = $row;
				}
			mysql_free_result($result);	
			}
			
		}
		return $returnData;
	}	
	
	/**
	 * Function to get type data for note contact notes list
	 */		
	public function getTypeData($userId) {
		
		$returnData = array();

		$sql = sprintf("SELECT DISTINCT
							tblnotes.note_type as 'type'
						FROM
							tblnotes LEFT OUTER JOIN tbluser ON tbluser.user_name = tblnotes.note_by
						WHERE
							tblnotes.note_cnt_Id = %s 
						ORDER BY
							tblnotes.note_type ASC", $this->GetSQLValueString($userId, "int") );

		$returnData = $this->fetchResults( $sql );

		return $returnData;		
	}	
	
	/**
	* Get All user Notes
	**/
	public function getAnalystData($userId)
	{
		$returnData = array();
		if ($userId > 0) {
			$sql = sprintf("SELECT DISTINCT
							CONCAT(tbluser.user_fname,' ',SUBSTRING(TRIM(tbluser.user_lname),1,1)) AS 'analyst'
						FROM 
								tblnotes INNER JOIN tbluser ON tbluser.user_name = tblnotes.note_by
						WHERE 
								tblnotes.note_cnt_Id = %s 
						ORDER BY 
								CONCAT(tbluser.user_fname,' ',SUBSTRING(TRIM(tbluser.user_lname),1,1)) ASC", $this->GetSQLValueString($userId, "int") );

			$returnData = $this->fetchResults( $sql );
			
		}
		return $returnData;
	}	
	
	/**
	* Get All user Notes
	**/
	public function getUserNotesDataForExport($userId, $filterData, $sortNames, $sortValues)
	{
		
		$returnData = array();
		if ($userId > 0) {
			$sql = sprintf("SELECT DISTINCT			
							tblnotes.note_Id as 'note_id',
							tbluser.user_fname as 'user_fname',
							tbluser.user_lname as 'user_lname',
							tblnotes.notebyNick as 'notebyNick',
							tblnotes.note_cnt_name as 'note_cnt_name',
							tblnotes.cntCompany as 'cntCompany',
							tblnotes.note_type as 'note_type',							
							tblnotes.note_txt as 'note_txt',
							tblnotes.note_date as 'note_date',
							tblnotes.note_time as 'note_time'							
					FROM 
						tblnotes INNER JOIN contacts ON contacts.id = tblnotes.note_cnt_Id
						LEFT OUTER JOIN tbluser ON tbluser.user_name = tblnotes.note_by									
						WHERE 
								tblnotes.note_cnt_Id = %s", $this->GetSQLValueString($userId, "int") );
								
			if (isset($filterData['maxDate']) && isset($filterData['minDate']) && $filterData['maxDate'] != null && $filterData['minDate'] != null) {
				
				$sql = $sql . sprintf(" AND tblnotes.note_date >= %s AND tblnotes.note_date <= %s", 
					$this->GetSQLValueString($filterData['minDate'], "date"),
					$this->GetSQLValueString($filterData['maxDate'], "date")
					);				
			}
			
			if (isset($filterData['note']) && $filterData['note'] != null) {
				$sql = $sql . sprintf(" AND note_txt LIKE '%%%s%%'", 
					$this->GetSQLValueString($filterData['note'], "string")
					);								
			}
			
			if (isset($filterData['type']) && $filterData['type'] != null) {
				$sql = $sql . sprintf(" AND note_type = '%s'", 
					$this->GetSQLValueString($filterData['type'], "string")
					);								
			}			
			
			if (isset($filterData['analyst']) && $filterData['analyst'] != null) {
				
				$sql = $sql . sprintf(" AND CONCAT(tbluser.user_fname,' ',SUBSTRING(TRIM(tbluser.user_lname),1,1)) = '%s'", 
					$this->GetSQLValueString($filterData['analyst'], "string")
					);				
			}
			
			$sqlPostfix = "";
			for($t=0;$t<sizeof($sortNames);$t++) {
				$sortName = isset($sortNames[$t]) && trim($sortNames[$t] != '') ? $sortNames[$t] : null;								
				$sortValue = isset($sortValues[$t]) && trim($sortValues[$t] != '') ? $sortValues[$t] : null;				
				
				if ($sortName !== null && $sortValue !== null && ($sortValue == 'desc' || $sortValue == 'asc' )) {
					
					if ($sortName == 'date') {						
						$sqlPostfix .= " tblnotes.note_date ".addslashes($sortValue). ",";					
					} elseif ($sortName == 'note') {						
						$sqlPostfix .= " tblnotes.note_txt ".addslashes($sortValue). ",";					
					} elseif ($sortName == 'type') {						
						$sqlPostfix .= " tblnotes.note_type ".addslashes($sortValue). ",";					
					} elseif ($sortName == 'by') {						
						$sqlPostfix .= " CONCAT(tbluser.user_fname,' ',SUBSTRING(TRIM(tbluser.user_lname),1,1)) ".addslashes($sortValue). ",";					
					}					
					
				}
			}
			
			if ($sqlPostfix != "") {
				$sqlPostfix = trim($sqlPostfix, ",");						
								
				$sql = $sql . " ORDER BY ".$sqlPostfix;					
			}								

			$returnData = $this->fetchResults( $sql );
			
		}
		return $returnData;
	}	
	
	/**
	* Get All user Notes WithAviorLibData
	**/	
	//schulz
	public function getUserNotesWithAviorLibDataForExport($userId, $filterData, $sortNames, $sortValues)
	{
		
		$returnData = array();
		if ($userId > 0) {
			$sql = sprintf("SELECT DISTINCT			
							tblnotes.note_Id as 'note_id',
							tbluser.user_fname as 'user_fname',
							tbluser.user_lname as 'user_lname',
							tblnotes.notebyNick as 'notebyNick',
							tblnotes.note_cnt_name as 'note_cnt_name',
							tblnotes.cntCompany as 'cntCompany',
							tblnotes.note_type as 'note_type',							
							tblnotes.note_txt as 'note_txt',
							tblnotes.note_date as 'note_date',
							tblnotes.note_time as 'note_time'							
					FROM 
						tblnotes INNER JOIN contacts ON contacts.id = tblnotes.note_cnt_Id
						LEFT OUTER JOIN tbluser ON tbluser.user_name = tblnotes.note_by									
						WHERE 
								tblnotes.note_cnt_Id = %s", $this->GetSQLValueString($userId, "int") );
							
			$dateRange = " DATE_SUB(CURRENT_DATE(), INTERVAL 2 YEAR) ";
											
			$sqlString = "SELECT 
				REPLACE(N.note_date, ' ', '<br>') as note_date				
				, IF(N.note_type = \"Webview open\" OR N.note_type = \"Email read\",
					CONCAT(
						N.note_txt, 
						' <span class=\"badge count count_', N.viewNo, 
						'\" data-notetype=\"', REPLACE(LOWER(N.note_type), ' ', ''), 
						'\" data-taskid=\"', N.task_id, 
						'\" data-count=\"', N.viewNo ,
						'\" data-contact=\"', N.note_cnt_name ,						
						'\" data-rticker=\"', N.report_ticker ,												
						'\" data-rtitle=\"', N.report_title ,							
						'\" data-email=\"', N.Contact_email ,																							
						'\" >', N.viewNo, '</span>'
					), 
					N.note_txt) AS note_txt
				, N.note_type
				, N.viewNo
				, IFNULL(U.user_nick, N.notebyNick) AS 'note_by'				
				-- for display purposes in main view and modal
				, N.note_cnt_name
				-- for email read mod
				-- , N.task_id				
				-- webview modal
				, N.Contact_email
				, N.report_ticker
				, N.report_title
	
			FROM (
				-- Notes as found in the CRM and the current view
				
					SELECT DISTINCT 
								N.note_by
							, N.notebyNick
							, C.first_name			AS 'Contact_fname'
							, C.surname			AS 'Contact_lname'
							, N.cntCompany
							, N.note_txt
							, N.note_type
							, concat(N.note_date,' ',IfNull(N.note_time,'00:00:00')) as note_date
							, 1 AS 'viewNo'
							-- Extra info for modal
								, N.note_Id AS task_id
								, C.email	AS 'Contact_email'
								, ' ' AS 'report_ticker'
								, ' '	AS 'report_title'
							, CONCAT(C.first_name,' ', C.surname) AS  'note_cnt_name'
								
						FROM 
							tblnotes N JOIN contacts C ON C.id = N.note_cnt_Id
							JOIN tblcompanylist P ON N.cntCompany = P.Company
						WHERE 
							C.id = %s 
							AND N.note_date > ".$dateRange."					
					
				UNION 
			 
					-- event_logs from AviorLib
					SELECT  
						 R.username
						, R.first_name		AS 'notebyNick'
						, E.first_name		AS 'Contact_fname'
						, E.surname			AS 'Contact_lname'
						, E.Company
						, CONCAT_WS(' | ', E.report_ticker, E.report_title)  AS 'note_txt'
						, 'Webview open' AS 'note_type'
						, E.note_date
						, E.count AS 'viewNo'
					-- Extra info for modal
						, Event_id			AS 'task_id'
						, E.user				AS 'Contact_email'
						, E.report_ticker
						, E.report_title
						, CONCAT(E.first_name,' ', E.surname) AS  'note_cnt_name'
					FROM (
						SELECT 
								MAX(E.id)					AS Event_id
							, E.report_id
							, E.user
							, C.first_name
							, C.surname
							, P.Company
							, E.report_title
							, E.report_ticker
							, MAX(E.date_created) AS 'note_date'
							, COUNT(DISTINCT UNIX_TIMESTAMP(E.date_created) DIV 300) AS 'count'
						FROM  
							contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
							JOIN `avior`.event_logs E ON C.email = E.user
						WHERE 
							C.id = %s 
							AND E.description = 'Webview Document'
							AND E.date_created > ".$dateRange."
						GROUP BY 
							E.report_id
							, E.user
							, E.report_title
							, E.report_ticker
							, C.first_name
							, C.surname, P.Company
						) E
						JOIN (
							SELECT 
									R.id
								, R.title
								, R.primary_analyst
								, U.username, U.first_name
								, GROUP_CONCAT(T.Name ORDER BY T.Name) AS 'Tickers'
							FROM `avior`.reports R 
								JOIN `avior`.reports_tickers RT ON R.id = RT.report_id
								JOIN `avior`.tickers T ON RT.ticker_id = T.id
								JOIN `avior`.users U ON R.primary_analyst = U.id
							GROUP BY 
								R.id
								, R.title
								, R.primary_analyst
								, U.username
							) R 
							 ON E.report_id = R.id
			
					UNION 
			 
						SELECT 
								U.username
								, U.first_name
								, C.first_name			AS 'Contact_fname'
								, C.surname			AS 'Contact_lname'
								, MT.company
								, CONCAT_WS(' | ', ER.report_ticker, ER.report_title)	AS 'note_txt'
								, 'Email read' AS 'Note_type'
								, MAX(ER.date_created) AS 'note_date'
								, COUNT(DISTINCT UNIX_TIMESTAMP(ER.date_created) DIV 300) AS 'viewNo'
									-- Extra info for modal
									, MT.id						AS 'task_id'
									, MT.email				AS 'Contact_email'
									, ER.report_ticker
									, ER.report_title
															, CONCAT(C.first_name,' ', C.surname) AS  'note_cnt_name'
							FROM 
								contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
								JOIN `avior`.mailertasks MT ON C.email = MT.email
								JOIN `avior`.email_reads ER ON MT.id = ER.task_id 
								JOIN `avior`.jobs J ON MT.job_id = J.id
								JOIN `avior`.users U ON J.user_id = U.id
							 WHERE 
								C.id = %s
								AND ER.date_created > ".$dateRange."
							GROUP BY
								U.username
								, U.first_name
								, C.first_name
								, C.surname
								, MT.id
								, MT.company
								, ER.report_title
								, ER.report_ticker
						) N
			LEFT JOIN tbluser U ON U.user_name = N.note_by";
			
							
			$sql = sprintf($sqlString, $this->GetSQLValueString($userId, "int"), $this->GetSQLValueString($userId, "int"), $this->GetSQLValueString($userId, "int") );											
			
		$sqlString = "
			SELECT		
				N.task_id AS 'note_id'
				, U.user_fname
				, U.user_lname
				-- , N.notebyNick
				, IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) AS 'notebyNick'
				, N.note_cnt_name
				, N.cntCompany
				, N.note_type
				, N.note_txt
				, N.note_date
					
			FROM (
				-- Notes as found in the CRM and the current view
			
					SELECT DISTINCT
								N.note_by
							, N.notebyNick
							, C.first_name			AS 'Contact_fname'
							, C.surname			AS 'Contact_lname'
							, N.cntCompany
							, N.note_txt
							, N.note_type
							, concat(N.note_date,' ',IfNull(N.note_time,'00:00:00')) as note_date
							-- Extra info for modal
								, N.note_Id AS task_id
								, C.email	AS 'Contact_email'
								, ' '				AS 'report_ticker'
								, ' '				AS 'report_title'
								, CONCAT(C.first_name,' ', C.surname) AS  'note_cnt_name'
			
						FROM
							tblnotes N JOIN contacts C ON C.id = N.note_cnt_Id
							JOIN tblcompanylist P ON N.cntCompany = P.Company
						WHERE
							C.id = %s
							AND N.note_date > ".$dateRange."	
			
				UNION
			
					-- event_logs from AviorLib
					SELECT
						 R.username
						, R.first_name		AS 'notebyNick'
						, E.first_name		AS 'Contact_fname'
						, E.surname			AS 'Contact_lname'
						, E.Company
						, CONCAT_WS(' | ', E.report_ticker, E.report_title)  AS 'note_txt'
						, 'Webview open' AS 'note_type'
						, E.note_date
					-- Extra info for modal
						, Event_id			AS 'task_id'
						, E.user				AS 'Contact_email'
						, E.report_ticker
						, E.report_title
						, CONCAT(E.first_name,' ', E.surname) AS  'note_cnt_name'
					FROM (
						SELECT
								MAX(E.id)					AS Event_id
							, E.report_id
							, E.user
							, C.first_name
							, C.surname
							, P.Company
							, E.report_title
							, E.report_ticker
							, MAX(E.date_created) AS 'note_date'
						FROM
							contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
							JOIN `avior`.event_logs E ON C.email = E.user
						WHERE
							C.id = %s
							AND E.description = 'Webview Document'
							AND E.date_created > ".$dateRange."	
						GROUP BY
							E.report_id
							, E.user
							, E.report_title
							, E.report_ticker
							, C.first_name
							, C.surname, P.Company
											, UNIX_TIMESTAMP(E.date_created) DIV 300
						) E
						JOIN (
							SELECT
									R.id
								, R.title
								, R.primary_analyst
								, U.username, U.first_name
								, GROUP_CONCAT(T.Name ORDER BY T.Name) AS 'Tickers'
							FROM `avior`.reports R
								JOIN `avior`.reports_tickers RT ON R.id = RT.report_id
								JOIN `avior`.tickers T ON RT.ticker_id = T.id
								JOIN `avior`.users U ON R.primary_analyst = U.id
							GROUP BY
								R.id
								, R.title
								, R.primary_analyst
								, U.username
							) R
							 ON E.report_id = R.id
			
					UNION
			
						SELECT
								U.username
								, U.first_name
								, C.first_name			AS 'Contact_fname'
								, C.surname			AS 'Contact_lname'
								, MT.company
								, CONCAT_WS(' | ', ER.report_ticker, ER.report_title)	AS 'note_txt'
								, 'Email read' AS 'Note_type'
								, MAX(ER.date_created) AS 'note_date'
									-- Extra info for modal
									, MT.id						AS 'task_id'
									, MT.email				AS 'Contact_email'
									, ER.report_ticker
									, ER.report_title
									, CONCAT(C.first_name,' ', C.surname) AS  'note_cnt_name'
							FROM
								contacts C JOIN tblcompanylist P ON C.company_id = P.comp_id
								JOIN `avior`.mailertasks MT ON C.email = MT.email
								JOIN `avior`.email_reads ER ON MT.id = ER.task_id
								JOIN `avior`.jobs J ON MT.job_id = J.id
								JOIN `avior`.users U ON J.user_id = U.id
							 WHERE
								C.id = %s
								AND ER.date_created > ".$dateRange."	
							GROUP BY
								U.username
								, U.first_name
								, C.first_name
								, C.surname
								, MT.id
								, MT.company
								, ER.report_title
								, ER.report_ticker
													, UNIX_TIMESTAMP(ER.date_created) DIV 300
						) N
			LEFT JOIN tbluser U ON U.user_name = N.note_by";				
			
			$sql = sprintf($sqlString, $this->GetSQLValueString($userId, "int"), $this->GetSQLValueString($userId, "int"), $this->GetSQLValueString($userId, "int") );														
		
			
			$sqlWhereClause = '';
			
			if(sizeof($filterData) > 0) {
						
				if ($filterData['maxDate'] != null && $filterData['minDate'] != null) {
					
					$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_date >= %s AND N.note_date <= %s", 
						$this->GetSQLValueString($filterData['minDate'], "date"),
						$this->GetSQLValueString($filterData['maxDate'], "date")
						);				
				} elseif ($filterData['minDate'] != null) {
					
					$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_date >= %s", 
						$this->GetSQLValueString($filterData['minDate'], "date")
					);				
				} elseif ($filterData['maxDate'] != null) {
					
					$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_date <= %s", 
						$this->GetSQLValueString($filterData['maxDate'], "date")
					);				
				}								
				
				if ($filterData['note'] != null) {
					$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_txt LIKE '%%%s%%'", 
						$this->GetSQLValueString($filterData['note'], "string")
						);								
				}
				
				if ($filterData['type'] != null) {
					
					$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_type = '%s'", 
						$this->GetSQLValueString($filterData['type'], "string")
						);								
				}			
				
				if ($filterData['analyst'] != null) {
					
					//$sqlWhereClause = $sqlWhereClause . sprintf(" AND CONCAT(U.user_fname,' ',SUBSTRING(TRIM(U.user_lname),1,1)) = '%s'", 					
					// $sqlWhereClause = $sqlWhereClause . sprintf(" AND N.notebyNick = '%s'", 					
					// 	$this->GetSQLValueString($filterData['analyst'], "string")
					// 	);				

					$sqlWhereClause = $sqlWhereClause . sprintf(" AND IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) = '%s'", 					
						$this->GetSQLValueString($filterData['analyst'], "string")
					);					

						
				}			
				
				/*			
				if ($filterData['client'] != null) {
					$sqlWhereClause = $sqlWhereClause . sprintf(" AND N.note_cnt_name = '%s'", 
						$this->GetSQLValueString($filterData['client'], "string")
						);								
				}	
				*/		
	
				$prefix = " AND";							
				if (substr($sqlWhereClause, 0, strlen($prefix)) == $prefix) {
						$sqlWhereClause = " WHERE ".substr($sqlWhereClause, strlen($prefix));
				}			
			}
			
			$sqlPostfix = " ".$sqlWhereClause." ";
								
			$sql = $sql . $sqlPostfix;									
			
			$sqlPostfix = "";
			for($t=0;$t<sizeof($sortNames);$t++) {
				$sortName = isset($sortNames[$t]) && trim($sortNames[$t] != '') ? $sortNames[$t] : null;								
				$sortValue = isset($sortValues[$t]) && trim($sortValues[$t] != '') ? $sortValues[$t] : null;				
				
				if ($sortName !== null && $sortValue !== null && ($sortValue == 'desc' || $sortValue == 'asc' )) {
					
					if ($sortName == 'date') {						
						$sqlPostfix .= " N.note_date ".addslashes($sortValue). ",";					
					} elseif ($sortName == 'note') {						
						$sqlPostfix .= " N.note_txt ".addslashes($sortValue). ",";					
					} elseif ($sortName == 'type') {						
						$sqlPostfix .= " N.note_type ".addslashes($sortValue). ",";					
					} elseif ($sortName == 'by') {						
						$sqlPostfix .= " N.notebyNick ".addslashes($sortValue). ",";					
					}					
					
				}
			}
			
			if ($sqlPostfix != "") {
				$sqlPostfix = trim($sqlPostfix, ",");						
								
				$sql = $sql . " ORDER BY ".$sqlPostfix;					
			}
			
			$returnData = $this->fetchResults( $sql );
			
		}
		return $returnData;
	}		
	
	/**
	* Get All user Notes
	**/
	public function getAviorUserNotesDataForExport($userId, $filterData, $sortNames, $sortValues)
	{
		
		$returnData = array();
		if ($userId > 0) {
			$sql = sprintf("SELECT DISTINCT
					tblnotes.note_Id as 'note_id',
					tbluser.user_fname as 'user_fname',
					tbluser.user_lname as 'user_lname',
					tblnotes.notebyNick as 'notebyNick',
					tblnotes.note_cnt_name as 'note_cnt_name',
					tblnotes.cntCompany as 'cntCompany',
					tblnotes.note_type as 'note_type',							
					tblnotes.note_txt as 'note_txt',
					tblnotes.note_date as 'note_date',
					tblnotes.note_time as 'note_time'							
				FROM 
						tblnotes INNER JOIN contacts ON tblnotes.note_by = contacts.email
						LEFT OUTER JOIN tbluser ON tbluser.user_name = tblnotes.note_by
				WHERE 
						contacts.id = %s", $this->GetSQLValueString($userId, "int") );
						
			$sql = sprintf("SELECT DISTINCT
					N.note_Id as 'note_id',
					U.user_fname as 'user_fname',
					U.user_lname as 'user_lname',
					-- N.notebyNick as 'notebyNick',
					IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) AS 'notebyNick',
					N.note_cnt_name as 'note_cnt_name',
					N.cntCompany as 'cntCompany',
					N.note_type as 'note_type',							
					N.note_txt as 'note_txt',
					concat(N.note_date,' ',IfNull(N.note_time,'00:00:00')) as 'note_date'
					N.note_time as 'note_time'		
				FROM tblnotes N
				JOIN  tbluser U
					ON U.user_name = N.note_by
				JOIN contacts C
					ON C.email = U.user_email
				WHERE  C.id = %s
					AND N.note_date > DATE_SUB(CURRENT_DATE(), INTERVAL 2 YEAR)", $this->GetSQLValueString($userId, "int") );						
								
			if (isset($filterData['maxDate']) && isset($filterData['minDate']) && $filterData['maxDate'] != null && $filterData['minDate'] != null) {
				
				$sql = $sql . sprintf(" AND N.note_date >= %s AND N.note_date <= %s", 
					$this->GetSQLValueString($filterData['minDate'], "date"),
					$this->GetSQLValueString($filterData['maxDate'], "date")
					);				
			}
			
			if (isset($filterData['note']) && $filterData['note'] != null) {
				$sql = $sql . sprintf(" AND N.note_txt LIKE '%%%s%%'", 
					$this->GetSQLValueString($filterData['note'], "string")
					);								
			}
			
			if (isset($filterData['type']) && $filterData['type'] != null) {
				$sql = $sql . sprintf(" AND N.note_type = '%s'", 
					$this->GetSQLValueString($filterData['type'], "string")
					);								
			}			
			
			if (isset($filterData['analyst']) && $filterData['analyst'] != null) {
				
				// $sql = $sql . sprintf(" AND CONCAT(U.user_fname,' ',SUBSTRING(TRIM(U.user_lname),1,1)) = '%s'", 
				// 	$this->GetSQLValueString($filterData['analyst'], "string")
				// );				

				$sql = $sql . sprintf(" AND IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) = '%s'", 
					$this->GetSQLValueString($filterData['analyst'], "string")
				);								
					
			}
			
			$sqlPostfix = "";
			for($t=0;$t<sizeof($sortNames);$t++) {
				$sortName = isset($sortNames[$t]) && trim($sortNames[$t] != '') ? $sortNames[$t] : null;								
				$sortValue = isset($sortValues[$t]) && trim($sortValues[$t] != '') ? $sortValues[$t] : null;				
				
				if ($sortName !== null && $sortValue !== null && ($sortValue == 'desc' || $sortValue == 'asc' )) {
					
					if ($sortName == 'date') {						
						$sqlPostfix .= " N.note_date ".addslashes($sortValue). ",";					
					} elseif ($sortName == 'note') {						
						$sqlPostfix .= " N.note_txt ".addslashes($sortValue). ",";					
					} elseif ($sortName == 'type') {						
						$sqlPostfix .= " N.note_type ".addslashes($sortValue). ",";					
					} elseif ($sortName == 'by') {						
						$sqlPostfix .= " CONCAT(U.user_fname,' ',SUBSTRING(TRIM(U.user_lname),1,1)) ".addslashes($sortValue). ",";					
					}					
					
				}
			}
			
			if ($sqlPostfix != "") {
				$sqlPostfix = trim($sqlPostfix, ",");						
								
				$sql = $sql . " ORDER BY ".$sqlPostfix;					
			}																

			$returnData = $this->fetchResults( $sql );
			
		}
		return $returnData;
	}		
	
		/**
		* Function to export data: send data headers and CSV data
		*/	 
	 function exportData($entityAlias, $data) {

		 $filename = "Avior_data_export_" . date("Y-m-d") . ".csv";		 		 
		 
		 if ($entityAlias == 'company-contact-notes') {
			 
		 	$filename = "Avior_".$entityAlias."_data_export_" . date("Y-m-d") . ".csv";			 
			 
			$columns[] = 'note_id';
			$columns[] = 'user_fname';
			$columns[] = 'user_lname';
			$columns[] = 'notebyNick';
			$columns[] = 'note_cnt_name';
			$columns[] = 'cntCompany';
			$columns[] = 'note_type';
			$columns[] = 'note_txt'; 
			$columns[] = 'note_date';
			 
		 } elseif ($entityAlias == 'user-contact-list') {			 			 
			 
		 	$filename = "Avior_".$entityAlias."_data_export_" . date("Y-m-d") . ".csv";			 
			 
			$columns[] = 'note_id';
			$columns[] = 'user_fname';
			$columns[] = 'user_lname';
			$columns[] = 'notebyNick';
			$columns[] = 'note_cnt_name';
			$columns[] = 'cntCompany';
			$columns[] = 'note_type';
			$columns[] = 'note_txt'; 
			$columns[] = 'note_date';
		 }		 
		 
		 
		 	
		// disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
		
		echo $this->array2csv($columns, $data);
		exit();
					
	 }
	 
	/**
	* Function to convert and array to CSV
	*/		 
	function array2csv($columns, array &$array)
	{
		 if (count($array) == 0) {
			 return null;
		 }
		 ob_start();
		 $df = fopen("php://output", 'w');
		 
		 fputs($df, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

		 fputcsv($df, $columns);
		 
		 foreach ($array as $row) {
		 	$row=str_replace("\r", "<br>", $row);
		 	$row=str_replace("\n", "", $row);
			fputcsv($df, $row);
		 }
		 fclose($df);
		 return ob_get_clean();
	}	 
	
	
	
}