<?php 
	require_once('Connections/CRMconnection.php');
	require_once('includes/classes/hipchat.class.php');
 ?>
<?php

if (!isset($_SESSION)) {
  session_start();
}

$hipchat_instance = new hipchat();
/*==============================
	Developer Note:
	References to institution refer to company/companies in the db
 ===============================
*/

$MM_authorizedUsers = "Administrator";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
  // For security, start by assuming the visitor is NOT authorized.
  $isValid = False;

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
  // Therefore, we know that a user is NOT logged in if that Session variable is blank.
  if (!empty($UserName)) {
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
    // Parse the strings into arrays.
    $arrUsers = Explode(",", $strUsers);
    $arrGroups = Explode(",", $strGroups);
    if (in_array($UserName, $arrUsers)) {
      $isValid = true;
    }
    // Or, you may restrict access to only certain users based on their username.
    if (in_array($UserGroup, $arrGroups)) {
      $isValid = true;
    }
    if (($strUsers == "") && false) {
      $isValid = true;
    }
  }
  return $isValid;
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0)
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo);
  exit;
}

require_once('includes/sitevars.php');
require_once('includes/classes/company.class.php');
require_once('includes/classes/user.class.php');
require_once('includes/classes/notes.class.php');

$companyId = isset( $_GET['id'] ) ? $_GET['id'] : 0;
$formSuccessfullyUpdated = (isset($_GET['updated']) && $_GET['updated'] == 'success') ? true : false;

if( $companyId == 0 ) { //Go back to admin main if there's no ID.
	header('Location: adminmain.php');
	exit();
}

$companyObj = new Company($companyId);
if( isset( $_POST['comp_update'] ) ) {

	$formUpdateResult = $companyObj->updateCompany( $_POST );
	if( $formUpdateResult == 'success' ) {
		header("location: companydetails-edit.php?id=".$companyId."&updated=success");
		exit;
	}
}

if( !is_dir('uploads/companyp') ) {
	mkdir('uploads/companyp', 0777);
}

/* File drop fall-back*/
if ((isset($_FILES["Filedata"]))){

	if (!empty($_FILES) && isset($_GET['id']) && isset($_FILES["Filedata"]['name']) && isset($_FILES["Filedata"]['tmp_name'] ) &&  $_FILES["Filedata"]['size'] > 0 ) {

		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = 'uploads/companyp/'.$_GET['id'].'/';
		$targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];


		//make directory if it doens not yet exist
		if(!is_dir($targetPath)) {
			mkdir($targetPath, 0777);
		}

		move_uploaded_file($tempFile,$targetFile);
		include("js/resize-class.php");

		//resize the file quickly
		$width = 150;
		$height = 150;
		// *** 1) Initialize / load image
		$resizeObj = new resize($targetFile);

		// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
		$resizeObj -> resizeImage($width,$height, 'auto');

		// *** 3) Save image
		$resizeObj -> saveImage($targetPath."1.jpg", 100);
		//remove original
		unlink($targetFile);
	}
}

$companyDetails = $companyObj->getCompanyDetails();
$companyTypes = $companyObj->getCompanyTypes();
$companyStatus = $companyObj->getCompanyStatuses();
$companyTiers = $companyObj->getCompanyTiers();
$geography = $companyObj->getLocation();

$userObj = new User();
$clientList = $userObj->getClientList();
$clientListSize = sizeof($clientList);

$aviorCompanyName = "Avior Capital Markets (Pty) Ltd";
$aviorClientList = $userObj->GetClientListByCompanyName($aviorCompanyName);
$aviorClientListSize = sizeof($aviorClientList);

$notesObj = new Notes();
$companyName = isset($companyDetails[0]['company']) ? $companyDetails[0]['company'] : '';
$employees = array();
$companyNotes = array();
if($companyName != '') {
	$employees = $companyObj->employees($companyDetails[0]['id']);
}

/**
* Create company folder using Company ID. This will be where the public shared company files are stored.
*/
$companyObj->createSharedDirectory();
############################## hipchat logic here ################################
$get_user_hipchat_rooms = "SELECT * FROM tblcompany_hipchatrooms where company_id = ".$_GET['id']."";

mysql_select_db($database_CRMconnection);

$run_company_query      = mysql_query($get_user_hipchat_rooms, $CRMconnection) or  die(mysql_error());
$company_chat_rooms     = array();
while ($row             = mysql_fetch_array($run_company_query)) {
  $company_chat_rooms[]	= $row['hipchat_room_id'];
}
$company_chatrooms           = "".implode(",", $company_chat_rooms)."";

############################ end logic here ###############################
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
<link rel="manifest" href="favicons/manifest.json">
<link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<meta name="theme-color" content="#ffffff">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<link type="text/css" href="css/kdes1/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<link type="text/css" href="css/datatables/dataTables.twitter.bootstrap.css" rel="stylesheet" />
<link type="text/css" href="https://cdn.datatables.net/1.10.5/css/jquery.dataTables.css" rel="stylesheet" />
<link type="text/css" href="css/skipjack.css" rel="stylesheet" />
<link type="text/css" href="css/dropzone.css" rel="stylesheet" />
<link type="text/css" href="css/uploadify.css" rel="stylesheet" />
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/swfobject.js"></script>
<script type="text/javascript" src="js/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="js/dropzone.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<!-- InstanceEndEditable -->
<script language="javascript">
	$(function(){
		$('.js-example-basic-multiple').select2().val([<?php echo $company_chatrooms;?>]).trigger("change");

		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					},
					appendTo: '.quick-search-container'
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);

					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
	})
</script>
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			<div class="quick-search-container">
					<input type="text" id="quicksearch" size="16" />
			</div>
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<h2>Edit Institution Details : <?php echo $companyDetails[0]['company']; ?></h2>
<div class="content40" style="position:relative;">
<div id="tabs1">
		<ul>
			<li><a href="#tabs1-1">Details</a></li>
			<li><a href="#tabs1-2">People</a></li>
			<li><a href="#tabs1-3">Responsibility List Files</a></li>
		</ul>
		<div id="tabs1-1">
			<fieldset>
				<legend>View Institution details</legend>
				<?php if( $_SESSION['MM_UserGroup'] == 'Administrator' ) { ?>
					<div class="static-edit-link"><a href="companydetails-view.php?id=<?php  echo $companyId;?>">View</a></div>
				 <?php } ?>
				 <?php
					if( isset($formUpdateResult) || isset($formSuccessfullyUpdated) ) {
						if(isset($formSuccessfullyUpdated) && $formSuccessfullyUpdated == true) { ?>
						 <p><font color="red">Successfully Updated Institution Details.</font></p>
				<?php	} else if( isset($formUpdateResult) && ($formUpdateResult == "data-missing" || $formUpdateResult == "db-error") ) { ?>
						<p><font color="red">Form could not be updated. Please check if you have provided the following, and try again: </font><br />
							<ul>
								<li>Institution Name</li>
								<li>Tier</li>
								<li>Institution Status</li>
								<li>Institution Type</li>
						</p>
				<?php 	}
				}?>
				 <form method="post" id="editCompany" name="editCompany" class="edit-form" action="">
				  	<div id="editCompanyResponse"></div>
					  <table class="edit-table">
						<tr>
							<td class="label">Name:</td>
							<td><input type="text" id="company_name" name="company_name" value="<?php echo $companyDetails[0]['company']; ?>" />
								<span class="validation-error"></span>
							</td>
						</tr>
						<tr>
							<td class="label">Geography:</td>
							<td>
								<select id="id_geography" name="id_geography">
									<option value="">Select Geography</option>
									<?php for($x=0; $x < sizeof($geography); $x++ ) {
										$selected_location = isset($companyDetails[0]['id_geography']) && $companyDetails[0]['id_geography'] == $geography[$x]['id_geography'] ? 'selected="selected"': ""; ?>
										<option value="<?php echo $geography[$x]['id_geography']; ?>" <?php echo $selected_location; ?>><?php echo $geography[$x]['name']; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="label">Institution Type:</td>
							<td>
								<select id="company_type" name="company_type">
									<option value="">Select Institution Type</option>
									<?php for($x=0; $x < sizeof($companyTypes); $x++ ) {
										$selected_type = isset($companyDetails[0]['id_companytype']) && $companyDetails[0]['id_companytype'] == $companyTypes[$x]['id_companytype'] ? 'selected="selected"': ""; ?>
										<option value="<?php echo $companyTypes[$x]['id_companytype']; ?>" <?php echo $selected_type; ?>><?php echo $companyTypes[$x]['companytype']; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="label">Status:</td>
							<td>
								<?php for($x=0; $x < sizeof($companyStatus); $x++ ) {

									// Default is set on add
									// New dbs entries will never run the else if
									// Used to handle legacy nulls in the db
									// Suggested to be removed at later stage.
									$selected_status = '';
									if(isset($companyDetails[0]['id_companystatus']) && $companyDetails[0]['id_companystatus'] == $companyStatus[$x]['id_companystatus'])
									{
										$selected_status = 'checked="checked"';
									}
									else if (!isset($companyDetails[0]['id_companystatus']) && $companyStatus[$x]['company_status']=="Prospect" )
									{
										//Set the default of status to Prospect
										$selected_status = 'checked="checked"';
									}
									else
									{
										$selected_status = '';
									}
									?>
									<label><input type="radio" id="company_status" name="company_status" value="<?php echo $companyStatus[$x]['id_companystatus'];?>" <?php echo $selected_status ?> />&nbsp;<?php echo $companyStatus[$x]['company_status'];?></label><br />
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td class="label">Tier:</td>
							<td>
								<select id="company_tier" name="company_tier">
									<option value="">Select Institution Tier</option>
									<?php for($x=0; $x < sizeof($companyTiers); $x++ ){
										$selected_tier = isset($companyDetails[0]['id_companytier']) && $companyDetails[0]['id_companytier'] == $companyTiers[$x]['id_companytier'] ? 'selected="selected"': "";?>
										<option value="<?php echo $companyTiers[$x]['id_companytier'];?>" <?php echo $selected_tier; ?>><?php echo $companyTiers[$x]['companytier'];?></option>
									<?php }?>
								</select>
							</td>
						</tr>

						<tr>
				          <td class="label">Hipchat Room:</td>
				          <td>
				        
					            <?php 
					             	echo $hipchat_instance->get_hipchat_rooms();
					             ?>
				          </td>
				      	</tr>

						<tr>
							<td class="label">Address 1:</td>
							<td><input type="text" id="address_1" name="address_1" value="<?php echo $companyDetails[0]['address_1']; ?>" /></td>
						</tr>
						<tr>
							<td class="label">Address 2:</td>
							<td><input type="text" id="address_2" name="address_2" value="<?php echo $companyDetails[0]['address_2']; ?>" /></td>
						</tr>
						<tr>
							<td class="label">Address 3:</td>
							<td><input type="text" id="address_3" name="address_3" value="<?php echo $companyDetails[0]['address_3']; ?>" /></td>
						</tr>
						<tr>
							<td class="label">Address 4:</td>
							<td><input type="text" id="address_4" name="address_4" value="<?php echo $companyDetails[0]['address_4']; ?>" /></td>
						</tr>
						<tr>
							<td nowrap="nowrap" align="left" colspan="2" width="200px">
								<input type="hidden" name="company_image" id="company_image" value="<?php if(file_exists("uploads/companyp/".$companyDetails[0]['id']."/1.jpg"))  echo "uploads/companyp/".$companyDetails[0]['id']."/1.jpg"; ?>" >
								<input type="hidden" name="company_imagesize" id="company_imagesize" value="<?php if(file_exists("uploads/companyp/".$companyDetails[0]['id']."/1.jpg"))  echo filesize("uploads/companyp/".$companyDetails[0]['id']."/1.jpg"); ?>" >
								<div id="cpicture" style="text-align:center; padding-bottom:3px;">
								  <div id="picturedrop" class="picturedrop">
									  <p class="my-drop-message dz-message">Drop or Click<br />here<br />add picture</p>
								  </div>
								</div>
							</td>
						</tr>
						<tr>
							<td class="label">Switchboard 1</td>
							<td><input type="text" id="switchboard_1" name="switchboard_1" value="<?php echo $companyDetails[0]['switchboard_1']; ?>" /></td>
						</tr>
						<tr>
							<td class="label">Switchboard 2</td>
							<td><input type="text" id="switchboard_2" name="switchboard_2" value="<?php echo $companyDetails[0]['switchboard_2']; ?>" /></td>
						</tr>
						<tr>
							<td class="label">Website:</td>
							<td><input type="text" id="company_website" name="company_website" value="<?php echo $companyDetails[0]['website']; ?>" />
							<span class="validation-error"></span>
							</td>
						</tr>
						<tr>
							<td class="label">Facebook:</td>
							<td><input type="text" id="social_fb" name="social_fb" value="<?php echo $companyDetails[0]['social_fb']; ?>" />
								<span class="validation-error"></span>
							</td>
						</tr>
						<tr>
							<td class="label">Twitter:</td>
							<td><input type="text" id="social_twitter" name="social_twitter" value="<?php echo $companyDetails[0]['social_twitter']; ?>" />
							<span class="validation-error"></span>
							</td>
						</tr>
						<tr>
							<td class="label">LinkedIn:</td>
							<td><input type="text" id="social_linkedin" name="social_linkedin" value="<?php echo $companyDetails[0]['social_linkedin']; ?>" />
							<span class="validation-error"></span>
							</td>
						</tr>
						<?php if( $aviorClientListSize > 0 ) { ?>
						<tr>
							<td class="label">KAM 1:</td>
							<td>
								<select id="company_kam1" name="company_kam1">
									<option value="">Select KAM 1</option>
									<?php for( $x=0; $x < $aviorClientListSize; $x++ ){
										$selected_kam1 = isset($companyDetails[0]['id_kam1']) && $companyDetails[0]['id_kam1'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?>
										<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_kam1; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="label">KAM 2:</td>
							<td>
								<select id="company_kam2" name="company_kam2">
									<option value="">Select KAM 2</option>
									<?php for( $x=0; $x < $aviorClientListSize; $x++ ){
										$selected_kam2 = isset($companyDetails[0]['id_kam2']) && $companyDetails[0]['id_kam2'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?>
										<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_kam2; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>
									<?php } ?>
								</select></td>
						</tr>
						<tr>
							<td class="label">Dealer 1:</td>
							<td>
								<select id="company_dealer1" name="company_dealer1">
									<option value="">Select Dealer 1</option>
									<?php
										$selected_csa_only = isset($companyDetails[0]['id_dealer1']) && $companyDetails[0]['id_dealer1'] == -1 ? 'selected="selected"': "";
									?>
									<option value="-1" <?php echo $selected_csa_only; ?>>CSA Only</option>
									<?php for( $x=0; $x < $aviorClientListSize; $x++ ){
										$selected_delear1 = isset($companyDetails[0]['id_dealer1']) && $companyDetails[0]['id_dealer1'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?>
										<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_delear1; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>
									<?php } ?>


								</select>
							</td>
						</tr>
						<tr>
							<td class="label">Dealer 2:</td>
							<td>
								<select id="company_dealer2" name="company_dealer2">
									<option value="">Select Dealer 2</option>
									<?php for( $x=0; $x < $aviorClientListSize; $x++ ){
										$selected_delear2 = isset($companyDetails[0]['id_dealer2']) && $companyDetails[0]['id_dealer2'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?>
										<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_delear2; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
						<?php } else { ?>
						<tr>
							<td colspan="2">Please <a href="adminmain.php" title="" >add</a> Avior Employees.</td>
						</tr>
						<?php } ?>
						<tr>
							<td class="label" style="vertical-align:top;">Select Protection Type:</td>
							<td>
								<select id="company_drm_setting" name="company_drm_setting">
									<?php $selected_drm = isset($companyDetails[0]['drm_status']) ? $companyDetails[0]['drm_status'] : ''; ?>
									<option value="1" <?php if( $selected_drm == 1 ) echo 'selected="selected"'; ?> >Watermark</option>
									<option value="2" <?php if( $selected_drm == 2 ) echo 'selected="selected"'; ?>>DRM</option>
									<option value="3" <?php if( $selected_drm == 3 ) echo 'selected="selected"'; ?>>Web Viewer</option>
								</select>
							<br />
							<small style="background-color: #ffffaa"> Please note - changing the protection here will change the e-mail protection for all clients on all mailing lists at this specific institution - use with caution!</small>
              
              <div id="webviewerwithprint" class="<?php if( $selected_drm == 3 ) echo 'show'; else echo 'hide' ?>">
              	<br />
								<?php $selected_webview_print = isset($companyDetails[0]['webview_print']) ? $companyDetails[0]['webview_print'] : '0'; ?>                
              	<label><input type="checkbox" id="webview_print" name="webview_print" value="1" <?php if( $selected_webview_print == 1 ) echo 'checked="checked"'; ?> /> Web Viewer with Print?</label>
							</div>                
							</td>
						</tr>
            
						<tr>
							<td class="label" style="vertical-align:top;">Select Portal Access:</td>
							<td>
								<?php $selected_portal_access = isset($companyDetails[0]['portal_access']) ? $companyDetails[0]['portal_access'] : '0'; ?>                
              	<label><input type="checkbox" id="portal_access" name="portal_access" value="1" <?php if( $selected_portal_access == 1 ) echo 'checked="checked"'; ?> /> Portal Access Enabled?</label>
							</td>
						</tr>            
						<tr>
							<td colspan="2">
								<input type="hidden" id="comp_id" name="comp_id" value="<?php echo $companyId;?>" />
								<p><input type="submit" id="save-company-edit-changes" name="save-company-edit-changes" value="Save Changes"  /></p>
							</td>
						</tr>
					  </table>
				  <input type="hidden" name="comp_update" id="comp_update" value="true" />
			</form>
			</fieldset>
		</div>
		<div id="tabs1-2">
			<fieldset>
					<legend>View People</legend>
					  <table class="table table-striped dataTable" id="people-list" width="100%" >
						<thead>
							<tr>
								<th>Name</th>
								<th>Designation</th>
								<th># of lists</th>
							</tr>
						</thead>
						<tbody>
							<?php if (sizeof($employees) > 0) {
								for ($x=0; $x < sizeof($employees); $x++) {
								$inactiveClass = $employees[$x]['active'] == 0 ? 'style="color:#666666;"' : "";
							?>
								<tr>
									<td><a href="clientdetails.php?cnt_Id=<?php echo $employees[$x]['id'];?>" <?php echo $inactiveClass; ?> title="Click to view profile"><?php echo $employees[$x]['client_name']; ?></a></td>

									<td><?php echo $employees[$x]['job_title']; ?></td>
									<td class="center"><?php echo $employees[$x]['mailing_lists']; ?></td>
								</tr>
							<?php }// end for
							} ?>
						</tbody>
					  </table>
				</fieldset>
		</div>
		<div id="tabs1-3">
			<fieldset>
					<legend>Upload Shared files</legend>
					<div id="pubfileUpload">You have a problem with your javascript</div>
			</fieldset>
			<fieldset>
					<legend>Shared files</legend>
					<div id="shared-files"></div>
			</fieldset>
		</div>
	</div>
</div>
<div class="content40">
<div id="tabs2">
		<ul>
			<li><a href="#tabs2-1">Recent Contact</a></li>
			<li><a href="#tabs2-2">News</a></li>
			<li><a href="#tabs2-3">Social</a></li>
		</ul>
		<div id="tabs2-1">
			<fieldset>
				<legend>Contact Notes</legend>
				<table class="table table-striped dataTable" id="company-notes" width="100%" >
						<thead>
							<tr>
								<th>Date</th>
								<th>Note</th>
								<th>Type</th>
								<th>By</th>
								<th>Client</th>
							</tr>
						</thead>
			  </table>
			</fieldset>
		</div>
		<div id="tabs2-2">
			<fieldset>
					<legend>News</legend>
					  <p>Coming Soon</p>
				</fieldset>
		</div>
		<div id="tabs2-3">
			<fieldset>
					<legend>Social</legend>
					<p>Coming Soon</p>
				</fieldset>
		</div>
	</div>

</div>
</div>
</div>

<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script src="js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery.linkify.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="js/companydetails.js"></script>
<script type="text/javascript" src="js/jquery.validate/jquery.validate.min.js"></script>

<script type="text/javascript">

	function showHideWebviewPrint() {
		if($('#company_drm_setting').val() == '3') {
			$('#webviewerwithprint').show()
		} else {				
			$('#webviewerwithprint').hide()
		}
	}

	$(document).ready(function(){

		$('#tabs1, #tabs2').tabs();
		$('.website').linkify();

		var cid = "<?php echo $companyDetails[0]['id']; ?>";
		var encId= "<?php echo sha1(md5($companyDetails[0]['id'])); ?>";
		//shared file uploads
		var compDetails = new companyDetails(cid, encId);

		compDetails.peopleToDatatable();
		compDetails.setResponsibilityUploader();
		compDetails.displayResponsibilityFiles();
		compDetails.getCompanyNotes();

		showHideWebviewPrint();

		$('#company_drm_setting').on('change',function(){
			showHideWebviewPrint();	
			if($('#company_drm_setting').val() != '3')
				$('#webview_print').val(0);					
		});

		 $('#editCompany').validate({
            errorClass:'validate-error',
            errorPlacement: function(error, element) {
                error.appendTo( element.parents('tr').find("span.validation-error").addClass('error') );
            },
            rules: {
				company_name: {required: true },
				company_website: { required: false, url: true },
                social_fb: { required: false, url: true },
				social_twitter: { required: false, url: true },
				social_linkedin: { required: false, url: true }
            },
            messages: {
				company_name: { required: 'Please enter institution name' },
				company_website: { url: 'Please enter a valid url starting with http(s)://' },
                social_fb: { url: 'Please enter a valid url starting with http(s)://' },
				social_twitter: { url: 'Please enter a valid url starting with http(s)://' },
				social_linkedin: { url: 'Please enter a valid url starting with http(s)://' }

            }
        });

		var company_image = $('#company_image').val();
		var company_imagesize = $('#company_imagesize').val();
		var file_dropped = 0;

		var myDropzone = new Dropzone("div#picturedrop",
			{ url: "js/uploadifyclientp.php?folder=/uploads/companyp/<?php echo $companyDetails[0]['id']; ?>",
			addRemoveLinks :true,
			acceptedFiles: 'image/*',
			forceFallback: false,
			paramName:'Filedata',
			dictFallbackText: 'Please use the fallback form below to upload your files',
			dictRemoveFile: 'Clear Picture',
			maxFiles:1
			});

		if($('.dz-browser-not-supported').length == 0) {

				myDropzone.on("removedfile", function(file) {

					  if (!file.name) { return; }
						 myDropzone.removeAllFiles(true);
						 myDropzone.options.maxFiles = 1;
						 $('.my-drop-message').show();
						 $(".dz-error, .error").remove();
						 $.post("js/dropzone-remove-file.php",{cid:'<?php echo $companyDetails[0]['id']; ?>'});
					}).on("drop",function(file,error){
						  $('#cpicture').append('<div class="notice error"><em>Note: Images need to be saved to desktop first before dropping</em></div>');
						});

				if( company_image != '' ) {
					$('.my-drop-message').hide();
					var mockFile = { name: "1.jpg", size: company_imagesize };
					myDropzone.emit("addedfile", mockFile);
					myDropzone.emit("thumbnail", mockFile, company_image);
					myDropzone.emit("complete", mockFile);
					var existingFileCount = 1;
					myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;

				}
				myDropzone.on("error", function(file,error) {

					$(".error").remove();
					$(".dz-error").hide();

					if( error == 'You can not upload any more files.' ) {
						$('#cpicture').append('<div class="error">You can not upload any more files. <br />Please clear image first</div>');
					} else {
						$('#cpicture').append('<div class="error">'+error+'</div>');
					}
				});

				myDropzone.on("processing", function(file){
					 $('.notice').remove();
					   $('.my-drop-message').hide();
				});
			}  else {
					$('.my-drop-message').hide();
					$('.dz-browser-not-supported').append('<div id="fallback-preview"></div>');
						if( company_image != '' ) {
							$('#fallback-preview').html('<img src="'+company_image+'" alt="Preview" /><br /><a id="fallback-removefile" href="#">Remove File</a>');
							$('.dz-browser-not-supported form').hide();
						}
						$('.dz-browser-not-supported form').submit(function(){
							$('#picturedrop form').attr('action','companydetails-edit.php?id=<?php echo $companyDetails[0]['id']; ?>');
							//var filedata = $('#picturedrop form').serialize();
							//return false;
						});

						$('#fallback-removefile').click(function(){
							$('.dz-browser-not-supported form').show();
							$.post("js/dropzone-remove-file.php",{cid:'<?php echo $companyDetails[0]['id']; ?>'});
							$('#fallback-preview img, #fallback-removefile').hide();
							return false;
						});
					}

	});
</script>

<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
