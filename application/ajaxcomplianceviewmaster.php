<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET["MM_insert"])) && ($_GET["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO aviorsharetrades (userid, share_code, share_desc, director, natureofholding, orderdate, datefilled, initpos, actiontype, numsharesplanned, closeoutpos) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_GET['userid'], "int"),
                       GetSQLValueString($_GET['share_code'], "text"),
                       GetSQLValueString($_GET['share_desc'], "text"),
                       GetSQLValueString(isset($_GET['director']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_GET['natureofholding'], "text"),
                       GetSQLValueString($_GET['orderdate'], "date"),
                       GetSQLValueString($_GET['datefilled'], "date"),
                       GetSQLValueString(isset($_GET['initpos']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_GET['actiontype'], "text"),
                       GetSQLValueString($_GET['numsharesplanned'], "double"),
                       GetSQLValueString(isset($_GET['closeoutpos']) ? "true" : "", "defined","1","0"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());
}

if ((isset($_GET["MM_update"])) && ($_GET["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE aviorsharetrades SET userid=%s, share_code=%s, share_desc=%s, director=%s, natureofholding=%s, orderdate=%s, datefilled=%s, initpos=%s, actiontype=%s, numsharesplanned=%s, closeoutpos=%s WHERE idaviorsharetrades=%s",
                       GetSQLValueString($_GET['userid'], "int"),
                       GetSQLValueString($_GET['share_code'], "text"),
                       GetSQLValueString($_GET['share_desc'], "text"),
                       GetSQLValueString(isset($_GET['director']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_GET['natureofholding'], "text"),
                       GetSQLValueString($_GET['orderdate'], "date"),
                       GetSQLValueString($_GET['datefilled'], "date"),
                       GetSQLValueString(isset($_GET['initpos']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_GET['actiontype'], "text"),
                       GetSQLValueString($_GET['numsharesplanned'], "double"),
                       GetSQLValueString(isset($_GET['closeoutpos']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_GET['idaviorsharetrades'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($updateSQL, $CRMconnection) or die(mysql_error());
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_Recordset1 = 30;
$pageNum_Recordset1 = 0;
if (isset($_GET['pageNum_Recordset1'])) {
  $pageNum_Recordset1 = $_GET['pageNum_Recordset1'];
}
$startRow_Recordset1 = $pageNum_Recordset1 * $maxRows_Recordset1;

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_Recordset1 = "SELECT * FROM aviorsharetrades ORDER BY idaviorsharetrades DESC";
$query_limit_Recordset1 = sprintf("%s LIMIT %d, %d", $query_Recordset1, $startRow_Recordset1, $maxRows_Recordset1);
$Recordset1 = mysql_query($query_limit_Recordset1, $CRMconnection) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);

if (isset($_GET['totalRows_Recordset1'])) {
  $totalRows_Recordset1 = $_GET['totalRows_Recordset1'];
} else {
  $all_Recordset1 = mysql_query($query_Recordset1);
  $totalRows_Recordset1 = mysql_num_rows($all_Recordset1);
}
$totalPages_Recordset1 = ceil($totalRows_Recordset1/$maxRows_Recordset1)-1;$maxRows_viewmlist = 30;
$pageNum_viewmlist = 0;
if (isset($_GET['pageNum_viewmlist'])) {
  $pageNum_viewmlist = $_GET['pageNum_viewmlist'];
}
$startRow_viewmlist = $pageNum_viewmlist * $maxRows_viewmlist;

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_viewmlist = "SELECT * FROM aviorsharetrades ORDER BY idaviorsharetrades DESC";
$query_limit_viewmlist = sprintf("%s LIMIT %d, %d", $query_viewmlist, $startRow_viewmlist, $maxRows_viewmlist);
$viewmlist = mysql_query($query_limit_viewmlist, $CRMconnection) or die(mysql_error());
$row_viewmlist = mysql_fetch_assoc($viewmlist);

if (isset($_GET['totalRows_viewmlist'])) {
  $totalRows_viewmlist = $_GET['totalRows_viewmlist'];
} else {
  $all_viewmlist = mysql_query($query_viewmlist);
  $totalRows_viewmlist = mysql_num_rows($all_viewmlist);
}
$totalPages_viewmlist = ceil($totalRows_viewmlist/$maxRows_viewmlist)-1;

$queryString_Recordset1 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Recordset1") == false && 
        stristr($param, "totalRows_Recordset1") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Recordset1 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_Recordset1 = sprintf("&totalRows_Recordset1=%d%s", $totalRows_Recordset1, $queryString_Recordset1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="1" cellpadding="1" cellspacing="2" class="stripeMe">
  <thead>
  <tr>
    <td>idaviorsharetrades</td>
    <td>userid</td>
    <td>share_code</td>
    <td>share_desc</td>
    <td>orderdate</td>
    <td>actiontype</td>
  </tr>
  </thead>
  <tbody>
  <?php do { ?>
    <tr>
      <td><a href="#" onclick="$('#showmasterlist').load('ajaxcomplianceedittrade.php?idaviorsharetrades=<?php echo $row_viewmlist['idaviorsharetrades']; ?>')"><?php echo $row_viewmlist['idaviorsharetrades']; ?></a></td>
      <td><?php echo $row_viewmlist['userid']; ?></td>
      <td><?php echo $row_viewmlist['share_code']; ?></td>
      <td><?php echo $row_viewmlist['share_desc']; ?></td>
      <td><?php echo $row_viewmlist['orderdate']; ?></td>
      <td><?php echo $row_viewmlist['actiontype']; ?></td>
    </tr>
    <?php } while ($row_viewmlist = mysql_fetch_assoc($viewmlist)); ?>
    </tbody>
</table>

<table border="0" align="center">
  <tr>
    <td><?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, 0, $queryString_Recordset1); ?>">First</a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, max(0, $pageNum_Recordset1 - 1), $queryString_Recordset1); ?>">Previous</a>
        <?php } // Show if not first page ?></td>
    <td><?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, min($totalPages_Recordset1, $pageNum_Recordset1 + 1), $queryString_Recordset1); ?>">Next</a>
        <?php } // Show if not last page ?></td>
    <td><?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, $totalPages_Recordset1, $queryString_Recordset1); ?>">Last</a>
        <?php } // Show if not last page ?></td>
  </tr>
</table>
</body>
</html>
<?php
mysql_free_result($viewmlist);
?>
