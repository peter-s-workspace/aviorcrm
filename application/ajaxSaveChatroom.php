<?php  
if (!isset($_SESSION)) {
  session_start();
}
require_once('Connections/CRMconnection.php'); 
require_once('includes/event_log/eventlog.class.php'); 
require_once('includes/classes/company.class.php'); 
require_once('includes/classes/hipchat.class.php');

if(isset($_SESSION['MM_Username']) && isset( $_SESSION['MM_UserGroup'] ) &&  $_SESSION['MM_UserGroup'] == 'Administrator' ) {
	$hipchat_response 		= null;
	if (isset($_POST) && !empty($_POST)) {
		########## Hipchat API different token's are used for different actions #################
		$hipchat_instance 	= new hipchat();

		$post_type 			= filter_var($_POST['post_type'], FILTER_SANITIZE_STRING);
		$chat_room_name 	= trim(filter_var($_POST['chat_room_name'], FILTER_SANITIZE_STRING));
		
		
		
		mysql_select_db($database_CRMconnection, $CRMconnection);
		if ($post_type == 'insert') {
			$query 		= "INSERT INTO avcrm.tbl_chatrooms (chat_room_name) VALUES('$chat_room_name')";
		} 
		
		$run_query					= mysql_query($query, $CRMconnection);
		if (!$run_query) {
			echo json_encode(array('status'=> false, 'message' => mysql_error($CRMconnection)));	
		} else {
			$added_room_id 			= mysql_insert_id();
			$hipchat_response 		= $hipchat_instance->add_hipchat_room($chat_room_name);

			if (is_numeric($hipchat_response)) {
				$update_query 		= "UPDATE avcrm.tbl_chatrooms set chat_room_id = $hipchat_response WHERE id = $added_room_id";
				$run_update_query 	= mysql_query($update_query, $CRMconnection);

				if (!$run_update_query) {
				    echo json_encode(array('status'=> false,'message'=>'Error Updating chatroom'));
				} else {
					echo json_encode(array('status'=> true,'message'=>'Successfully Added'));
				}
			}
			
		}
	}
}
?>