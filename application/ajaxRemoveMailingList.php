<?php

require_once('includes/classes/mailinglist.class.php');
if (!isset($_SESSION)) {
  session_start();
}

//Check if Administrator.
if(isset($_SESSION['MM_Username']) && isset( $_SESSION['MM_UserGroup'] ) &&  $_SESSION['MM_UserGroup'] == 'Administrator' ) {

	$itemsToRemove = (isset( $_POST['listItem'] ) && trim($_POST['listItem'])) ? trim($_POST['listItem']) : '';
	
	if( $itemsToRemove != '' ) {
		$mailingListObj = new MailingList();
		$returnValue = $mailingListObj->RemoveMailingList($itemsToRemove);
		echo $returnValue;
	
	} else {
		echo "Please select at least 1 item.";
	}
}
exit();
?>