<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}

require_once 'includes/classes/user.class.php';
require_once 'includes/classes/company.class.php';
require_once 'includes/classes/notes.class.php';

if( isset($_GET['logout']) && $_GET['logout'] == 1 ) {
	$userObj = new User();
	$userObj->Logout();
}

/*==============================
	Developer Note:
	References to institution refer to company/companies in the db
 ===============================
*/

$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";


// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
  // For security, start by assuming the visitor is NOT authorized.
  $isValid = False;

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
  // Therefore, we know that a user is NOT logged in if that Session variable is blank.
  if (!empty($UserName)) {
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
    // Parse the strings into arrays.
    $arrUsers = Explode(",", $strUsers);
    $arrGroups = Explode(",", $strGroups);
    if (in_array($UserName, $arrUsers)) {
      $isValid = true;
    }
    // Or, you may restrict access to only certain users based on their username.
    if (in_array($UserGroup, $arrGroups)) {
      $isValid = true;
    }
    if (($strUsers == "") && true) {
      $isValid = true;
    }
  }
  return $isValid;
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0)
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo);
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_onclientlist="";
//user plain dtails
$colname_userd ="none";
if (isset($_SESSION['MM_Username'])) {
  $colname_userd = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userd = sprintf("SELECT * FROM tbluser WHERE tbluser.user_name = %s ", GetSQLValueString($colname_userd, "text"));
$userd = mysql_query($query_userd, $CRMconnection) or die(mysql_error());
$row_userd = mysql_fetch_assoc($userd);
$totalRows_userd = mysql_num_rows($userd);

//check if user has google login details saved:

//

$colname_userlists = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userlists = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userlists = sprintf("SELECT * FROM tblfavourites, contacts WHERE tblfavourites.fav_Username = %s AND contacts.id =tblfavourites.tbl_cnt_Id AND tblfavourites.listid =1 ORDER BY contacts.first_name ASC", GetSQLValueString($colname_userlists, "text"));
$userlists = mysql_query($query_userlists, $CRMconnection) or die(mysql_error());
$row_userlists = mysql_fetch_assoc($userlists);
$totalRows_userlists = mysql_num_rows($userlists);

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsresplists = "SELECT * FROM responsiblist ORDER BY response_desc ASC";
$rsresplists = mysql_query($query_rsresplists, $CRMconnection) or die(mysql_error());
$row_rsresplists = mysql_fetch_assoc($rsresplists);
$totalRows_rsresplists = mysql_num_rows($rsresplists);

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_specresplists = "SELECT cntresponsibility.*,contacts.*, tblcompanylist.comp_id ,  tblcompanylist.Company FROM cntresponsibility, contacts, tblcompanylist WHERE cntresponsibility.respons_id= (SELECT respons_id FROM responsiblist ORDER BY response_desc ASC LIMIT 1) AND contacts.id=cntresponsibility.cnt_id AND tblcompanylist.comp_id = contacts.company_id";
$specresplists = mysql_query($query_specresplists, $CRMconnection) or die(mysql_error());
$row_specresplists = mysql_fetch_assoc($specresplists);
$totalRows_specresplists = mysql_num_rows($specresplists);

$colname_availlists = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_availlists = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$sql = "";
$sql = "SELECT * FROM cnt_listnames ORDER BY cnt_listnames.listname ASC";

$query_availlists = sprintf($sql, GetSQLValueString($colname_availlists, "text"));
$availlists = mysql_query($query_availlists, $CRMconnection) or die(mysql_error());
$row_availlists = mysql_fetch_assoc($availlists);
$totalRows_availlists = mysql_num_rows($availlists);

$colname_lastnote = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_lastnote = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_lastnote = sprintf("SELECT * FROM tblnotes WHERE note_by = %s ORDER BY note_date , note_Id DESC LIMIT 1", GetSQLValueString($colname_lastnote, "text"));
$lastnote = mysql_query($query_lastnote, $CRMconnection) or die(mysql_error());
$row_lastnote = mysql_fetch_assoc($lastnote);
$totalRows_lastnote = mysql_num_rows($lastnote);

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsCtype = "SELECT * FROM tblcontacttype ORDER BY ContactType ASC";
$rsCtype = mysql_query($query_rsCtype, $CRMconnection) or die(mysql_error());
$row_rsCtype = mysql_fetch_assoc($rsCtype);
$totalRows_rsCtype = mysql_num_rows($rsCtype);

$maxRows_rsRecCompCons = 100;
$pageNum_rsRecCompCons = 0;
if (isset($_GET['pageNum_rsRecCompCons'])) {
  $pageNum_rsRecCompCons = $_GET['pageNum_rsRecCompCons'];
}
$startRow_rsRecCompCons = $pageNum_rsRecCompCons * $maxRows_rsRecCompCons;

mysql_select_db($database_CRMconnection, $CRMconnection);

//Get search filter data
$selectedContactAnalyst = isset($_GET['recent_contact_analyst'])
                    && $_GET['recent_contact_analyst'] != ''
                    && $_GET['recent_contact_analyst'] != 'All'
                    ? GetSQLValueString($_GET['recent_contact_analyst'], "text") : '';

$selectedGeographyId = isset($_GET['geography']) && $_GET['geography'] != ''
                    ? GetSQLValueString($_GET['geography'], "int") : '';

$exportFilteredData = isset($_GET['export']) && $_GET['export'] == 'true' ? true : false;
$filterStartDate = isset($_GET['start_date']) && $_GET['start_date'] != '' ? GetSQLValueString($_GET['start_date'], "text") : '';
$filterEndDate = isset($_GET['end_date']) && $_GET['end_date'] != '' ? GetSQLValueString($_GET['end_date'], "text") : '';
$filterInstitution = isset($_GET['institution']) && $_GET['institution'] != '' ? GetSQLValueString($_GET['institution'], "text") : '';
$filterIndividual = isset($_GET['individual']) && $_GET['individual'] != '' ?  GetSQLValueString($_GET['individual'], "text") : '';

$filterCompanyStatus = isset($_GET['company_status']) && $_GET['company_status'] != '' ?  GetSQLValueString($_GET['company_status'], "int") : '';
$filterCompanyTier = isset($_GET['company_tier']) && $_GET['company_tier'] != '' ?  GetSQLValueString($_GET['company_tier'], "int") : '';
$filterCompanyType = isset($_GET['company_type']) && $_GET['company_type'] != '' ?  GetSQLValueString($_GET['company_type'], "int") : '';

// TODO: ACRM-380 we need to apply a default filter for the $filterNoteType when $exportFilteredData == false
$filterNoteType = isset($_GET['note_type']) && $_GET['note_type'] != '' && $_GET['note_type'] != '-webviewopen' ?  GetSQLValueString($_GET['note_type'], "text") : '-webviewopen';

$advancedFilterEnabled = false;
if ($exportFilteredData == false) {


	$filterSqlColumns = "
					N.note_Id,
					U.user_fname,
					U.user_lname,
					IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) AS 'note_by_nick',
					N.note_cnt_name,
					N.Company as 'cnt_company',
					N.note_type,
					N.note_txt,
					N.note_date";

	$filterSqlColumns = "
					N.Company AS 'Institution'
					,IFNULL(CONCAT(Contact_fname,' ', Contact_lname), 'Company Note' ) AS 'Individual'
					,Company_id AS 'Company_id'
					,IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) AS 'Note_by'	
					,N.note_type
					,N.note_txt
					,N.note_date
					,Contact_id AS 'Note_by_id'";					

	$dateRange = " DATE_SUB(CURRENT_DATE(), INTERVAL 2 YEAR) ";

	$recentContactListQuery = "SELECT ".$filterSqlColumns."
				FROM (
		-- Notes as found in the CRM and the current view

			SELECT
						N.note_Id
					, N.note_by
					, N.notebyNick
					, C.first_name			AS 'Contact_fname'
					, C.surname			AS 'Contact_lname'
					, N.note_txt
					, N.note_type
					, N.note_date
					/*---- Filter columns ---*/
					, C.id							AS 'Contact_id'
					, IfNull(CONCAT(C.first_name,' ', C.surname), 'Company Note' )AS  'note_cnt_name'
					, P.comp_id					AS 'Company_id'
					, P.Company
					, P.id_companytier
					, P.id_geography
					, P.id_companystatus
					, P.id_companytype

				FROM
					tblnotes N JOIN tblcompanylist P ON N.cntCompany = P.Company
					LEFT JOIN contacts C ON C.id = N.note_cnt_Id
				WHERE N.note_date > ".$dateRange."

		UNION

				SELECT
							E.note_id
						, R.username
						, R.first_name			AS 'notebyNick'
						, C.first_name			AS 'Contact_fname'
						, C.surname				AS 'Contact_lname'
						, CONCAT_WS(' | ', E.report_ticker, E.report_title)  AS 'note_txt'
						, 'Webview open' 	AS 'note_type'
						, E.note_date
						, C.id						AS 'Contact_id'
						, IfNull(CONCAT(C.first_name,' ', C.surname), 'Company Note' )AS  'note_cnt_name'
						, P.comp_id
						, P.Company
						, P.id_companytier
						, P.id_geography
						, P.id_companystatus
						, P.id_companytype
				FROM (
								SELECT
									MAX(E.id) AS 'note_id'
									, E.report_id		-- TODO: ACRM-352 added
									, E.user
									, E.report_title
									, E.report_ticker
									, E.description
									, MAX(E.date_created) AS 'note_date'
								FROM `avior`.event_logs E
								WHERE  E.date_created > ".$dateRange."
									AND  E.description = 'Webview Document'
								-- Test values
								GROUP BY
									E.report_id		-- TODO: ACRM-352 added
									, E.user
									, E.report_title
									, E.report_ticker
									, E.description
									, UNIX_TIMESTAMP(E.date_created) DIV 300
								) E
					JOIN (
								SELECT
										R.id
									, R.title
									, R.primary_analyst
									, U.username, U.first_name
									, GROUP_CONCAT(T.Name ORDER BY T.Name) AS 'Tickers'
								FROM `avior`.reports R
									JOIN `avior`.reports_tickers RT ON R.id = RT.report_id
									JOIN `avior`.tickers T ON RT.ticker_id = T.id
									JOIN `avior`.users U ON R.primary_analyst = U.id
								GROUP BY
									R.id
									, R.title
									, R.primary_analyst
									, U.username
							) R
						ON E.report_id = R.id	-- TODO: ACRM-352 replaced: ON E.report_title = R.title AND E.report_ticker = R.Tickers
				JOIN contacts C
					 ON C.email = E.user
				JOIN tblcompanylist P
					ON C.company_id = P.comp_id
				) N
	LEFT JOIN tbluser U ON U.user_name = N.note_by";

	// If a filter is aplied
	if ($selectedContactAnalyst != '' || $selectedGeographyId != '' || $filterStartDate != '' || $filterEndDate != ''
			|| $filterInstitution != '' || $filterIndividual != ''
			|| $filterCompanyStatus != '' || $filterCompanyTier != '' || $filterCompanyType != '' || $filterNoteType != ''
	) {


			if ($filterStartDate != '' || $filterEndDate != ''
					|| $filterInstitution != '' || $filterIndividual != ''
					|| $filterCompanyStatus != '' || $filterCompanyTier != '' || $filterCompanyType != '' || $filterNoteType != ''
			) {
				$advancedFilterEnabled = true;
			}



			$whereParts = '';
			if ($selectedContactAnalyst != '' ) {
					//$whereParts = sprintf($whereParts . " AND IFNULL(U.user_nick, N.notebyNick) = %s ", $selectedContactAnalyst);
					$whereParts = sprintf($whereParts . " AND U.user_id = %s ", $selectedContactAnalyst);
			}
			if ($selectedGeographyId != '' ) {
					$whereParts = sprintf($whereParts . " AND N.id_geography = %s ", $selectedGeographyId);
			}

			if ($filterStartDate != '' && $filterEndDate != '') {
					$whereParts = sprintf($whereParts . " AND (N.note_date >= %s AND N.note_date <= %s) ", $filterStartDate, $filterEndDate);
			} elseif ($filterStartDate != '') {
					$whereParts = sprintf($whereParts . " AND N.note_date >= %s ", $filterStartDate);
			}	elseif ($filterEndDate != '') {
					$whereParts = sprintf($whereParts . " AND N.note_date <= %s ", $filterEndDate);
			}

			if ($filterInstitution != '') {
					$whereParts = sprintf($whereParts . " AND N.Company_id = %s ", $filterInstitution);
			}

			if ($filterIndividual != '') {
				if( $filterIndividual == "'Company Note'") {
					$whereParts = sprintf($whereParts . " AND IFNULL(N.note_cnt_name, 'Company Note' ) = %s ", $filterIndividual);
				} else {
					$whereParts = sprintf($whereParts . " AND N.Contact_id = %s ", $filterIndividual);
				}
			}

			if ($filterCompanyStatus != '') {
					$whereParts = sprintf($whereParts . " AND N.id_companystatus = %s ", $filterCompanyStatus);
			}
			if ($filterCompanyTier != '') {
					$whereParts = sprintf($whereParts . " AND N.id_companytier = %s ", $filterCompanyTier);
			}
			if ($filterCompanyType != '') {
					$whereParts = sprintf($whereParts . " AND N.id_companytype = %s ", $filterCompanyType);
			}
			if ($filterNoteType != '') {
					
					if($filterNoteType == '-webviewopen') {
						$whereParts = sprintf($whereParts . " AND N.note_type <> 'Webview open'");
					} else {
						$whereParts = sprintf($whereParts . " AND N.note_type = %s ", $filterNoteType);
					}	
			}

			if ($whereParts != '') {
				$prefix = " AND";
				if (substr($whereParts, 0, strlen($prefix)) == $prefix) {
  			  $whereParts = substr($whereParts, strlen($prefix));
				}
				$recentContactListQuery .= " WHERE ".$whereParts;
			}

	}	

	$recentContactListQuery .= " ORDER BY note_date DESC, note_by,  note_cnt_name";

	$recentContactListQueryWitLimit = sprintf("%s LIMIT %d, %d", $recentContactListQuery, $startRow_rsRecCompCons, $maxRows_rsRecCompCons);
	//echo $recentContactListQueryWitLimit;
	//exit();

	$rsRecCompCons = mysql_query($recentContactListQueryWitLimit, $CRMconnection) or die(mysql_error());
	$row_rsRecCompCons = mysql_fetch_assoc($rsRecCompCons);



} elseif ($exportFilteredData == true) {

	$filterSqlColumns = "
					N.note_Id,
					U.user_fname,
					U.user_lname,
					IFNULL(U.user_nick, CONCAT(U.user_fname, ' ', SUBSTRING(TRIM(U.user_lname),1,1))) AS 'note_by_nick',
					N.note_cnt_name,
					N.Company as 'cnt_company',
					N.note_type,
					N.note_txt,
					N.note_date";

	$dateRange = " DATE_SUB(CURRENT_DATE(), INTERVAL 2 YEAR) ";

	$recentContactListQuery = "SELECT ".$filterSqlColumns."
				FROM (
		-- Notes as found in the CRM and the current view

			SELECT
						N.note_Id
					, N.note_by
					, N.notebyNick
					, C.first_name			AS 'Contact_fname'
					, C.surname			AS 'Contact_lname'
					, N.note_txt
					, N.note_type
					, N.note_date
					/*---- Filter columns ---*/
					, C.id							AS 'Contact_id'
					, IfNull(CONCAT(C.first_name,' ', C.surname), 'Company Note' )AS  'note_cnt_name'
					, P.comp_id					AS 'Company_id'
					, P.Company
					, P.id_companytier
					, P.id_geography
					, P.id_companystatus
					, P.id_companytype

				FROM
					tblnotes N JOIN tblcompanylist P ON N.cntCompany = P.Company
					LEFT JOIN contacts C ON C.id = N.note_cnt_Id
				WHERE N.note_date > ".$dateRange."

		UNION

				SELECT
							E.note_id
						, R.username
						, R.first_name			AS 'notebyNick'
						, C.first_name			AS 'Contact_fname'
						, C.surname				AS 'Contact_lname'
						, CONCAT_WS(' | ', E.report_ticker, E.report_title)  AS 'note_txt'
						, 'Webview open' 	AS 'note_type'
						, E.note_date
						, C.id						AS 'Contact_id'
						, IfNull(CONCAT(C.first_name,' ', C.surname), 'Company Note' )AS  'note_cnt_name'
						, P.comp_id
						, P.Company
						, P.id_companytier
						, P.id_geography
						, P.id_companystatus
						, P.id_companytype
				FROM (
								SELECT
									MAX(E.id) AS 'note_id'
									,  E.user
									, E.report_id
									, E.report_title
									, E.report_ticker
									, E.description
									, MAX(E.date_created) AS 'note_date'
								FROM `avior`.event_logs E
								WHERE  E.date_created > ".$dateRange."
									AND  E.description = 'Webview Document'
								-- Test values
								GROUP BY
									E.user
									, E.report_id
									, E.report_title
									, E.report_ticker
									, E.description
									, UNIX_TIMESTAMP(E.date_created) DIV 300
								) E
					JOIN (
								SELECT
										R.id
									, R.title
									, R.primary_analyst
									, U.username, U.first_name
									, GROUP_CONCAT(T.Name ORDER BY T.Name) AS 'Tickers'
								FROM `avior`.reports R
									JOIN `avior`.reports_tickers RT ON R.id = RT.report_id
									JOIN `avior`.tickers T ON RT.ticker_id = T.id
									JOIN `avior`.users U ON R.primary_analyst = U.id
								GROUP BY
									R.id
									, R.title
									, R.primary_analyst
									, U.username
							) R						
						ON  E.report_id = R.id
				JOIN contacts C
					 ON C.email = E.user
				JOIN tblcompanylist P
					ON C.company_id = P.comp_id
				) N
	LEFT JOIN tbluser U ON U.user_name = N.note_by";

	if ($selectedContactAnalyst != '' || $selectedGeographyId != '' || $filterStartDate != '' || $filterEndDate != ''
			|| $filterInstitution != '' || $filterIndividual != ''
			|| $filterCompanyStatus != '' || $filterCompanyTier != '' || $filterCompanyType != '' || $filterNoteType != ''
	) {


			if ($filterStartDate != '' || $filterEndDate != ''
					|| $filterInstitution != '' || $filterIndividual != ''
					|| $filterCompanyStatus != '' || $filterCompanyTier != '' || $filterCompanyType != '' || $filterNoteType != ''
			) {
				$advancedFilterEnabled = true;
			}



			$whereParts = '';
			if ($selectedContactAnalyst != '' ) {
					//$whereParts = sprintf($whereParts . " AND IFNULL(U.user_nick, N.notebyNick) = %s ", $selectedContactAnalyst);
					$whereParts = sprintf($whereParts . " AND U.user_id = %s ", $selectedContactAnalyst);
			}
			if ($selectedGeographyId != '' ) {
					$whereParts = sprintf($whereParts . " AND N.id_geography = %s ", $selectedGeographyId);
			}

			if ($filterStartDate != '' && $filterEndDate != '') {
					$whereParts = sprintf($whereParts . " AND (N.note_date >= %s AND N.note_date <= %s) ", $filterStartDate, $filterEndDate);
			} elseif ($filterStartDate != '') {
					$whereParts = sprintf($whereParts . " AND N.note_date >= %s ", $filterStartDate);
			}	elseif ($filterEndDate != '') {
					$whereParts = sprintf($whereParts . " AND N.note_date <= %s ", $filterEndDate);
			}

			if ($filterInstitution != '') {
					$whereParts = sprintf($whereParts . " AND N.Company_id = %s ", $filterInstitution);
			}

			if ($filterIndividual != '') {
				if( $filterIndividual == "'Company Note'") {
					$whereParts = sprintf($whereParts . " AND IFNULL(N.note_cnt_name, 'Company Note' ) = %s ", $filterIndividual);
				} else {
					$whereParts = sprintf($whereParts . " AND N.Contact_id = %s ", $filterIndividual);
				}
			}

			if ($filterCompanyStatus != '') {
					$whereParts = sprintf($whereParts . " AND N.id_companystatus = %s ", $filterCompanyStatus);
			}
			if ($filterCompanyTier != '') {
					$whereParts = sprintf($whereParts . " AND N.id_companytier = %s ", $filterCompanyTier);
			}
			if ($filterCompanyType != '') {
					$whereParts = sprintf($whereParts . " AND N.id_companytype = %s ", $filterCompanyType);
			}
			if($filterNoteType != '-webviewopen') {
				$whereParts = sprintf($whereParts . " AND N.note_type = %s ", $filterNoteType);
			}				

			if ($whereParts != '') {
				$prefix = " AND";
				if (substr($whereParts, 0, strlen($prefix)) == $prefix) {
  			  $whereParts = substr($whereParts, strlen($prefix));
				}
				$recentContactListQuery .= " WHERE ".$whereParts;
			}

	}

	$recentContactListQuery .= " ORDER BY note_date DESC, note_by,  note_cnt_name";

	/*
	echo "<pre>";
	echo $recentContactListQuery;
	echo "</pre>";
	exit();
	*/
	


	$rsRecCompCons = mysql_query($recentContactListQuery, $CRMconnection) or die(mysql_error());
	$row_rsRecCompCons = mysql_fetch_assoc($rsRecCompCons);

	$exporData = [];
	while( $row = mysql_fetch_assoc($rsRecCompCons)){
	$exporData[] = $row; // Inside while loop
	}

	$notesObj = new Notes(false);
	$notesObj->exportData('company-contact-notes',$exporData);
}

//Fetch filter data: analyst
//$distinctNoteNickQuery = "SELECT DISTINCT tblnotes.notebyNick, tblnotes.note_cnt_id FROM tblnotes ORDER BY tblnotes.notebyNick ASC";
$distinctNoteNickQuery = "SELECT DISTINCT
		U.user_id AS 'Analyst_id'
		, CONCAT(U.user_fname, ' ', U.user_lname)	AS 'Analyst_name'
	FROM
		tblnotes N JOIN contacts C ON C.id = N.note_cnt_Id
		JOIN tblcompanylist P ON N.cntCompany = P.Company
		LEFT JOIN tbluser U ON U.user_name = N.note_by
	WHERE
		U.user_id IS NOT NULL
	ORDER BY
		Analyst_name ASC";
$distinctNoteNickResult = mysql_query($distinctNoteNickQuery , $CRMconnection) or die(mysql_error());

if (isset($_GET['totalRows_rsRecCompCons'])) {
  $totalRows_rsRecCompCons = $_GET['totalRows_rsRecCompCons'];
} else {
  $all_rsRecCompCons = mysql_query($recentContactListQueryWitLimit);
  $totalRows_rsRecCompCons = mysql_num_rows($all_rsRecCompCons);
}
$totalPages_rsRecCompCons = ceil($totalRows_rsRecCompCons/$maxRows_rsRecCompCons)-1;

// code for reminders
$colname_reminders = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_reminders = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_reminders = sprintf("SELECT * FROM reminders WHERE user_id = %s ORDER BY remind_id DESC", GetSQLValueString($colname_reminders, "text"));
$reminders = mysql_query($query_reminders, $CRMconnection) or die(mysql_error());
$row_reminders = mysql_fetch_assoc($reminders);
$totalRows_reminders = mysql_num_rows($reminders);
//end code for reminders

$companyObj = new Company();

//Fetch filter data: geography
$allLocations = $companyObj->getLocation();


$_SESSION['user_extension'] = isset($row_userd['user_extension']) ? $row_userd['user_extension'] : '';

// #### add the logic to check whether the user has the extension or not ######
/*<td><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userlists['landline']; ?>'); $('#phonebox').dialog('open');"><?php echo $row_userlists['landline']; ?></a></td>
                      <td align="right"><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userlists['mobile']; ?>')"><?php echo $row_userlists['mobile']; ?></a></td> */

//Fetch filter data: institution
$institutionData = $companyObj->getInstitutions();

//Fetch filter data: individual
$individualData = $companyObj->getIndividuals();

//Fetch filter data: company tier, company status, company type
$companyTiers = $companyObj->getCompanyTiers();
$companyTiersSize = sizeof($companyTiers);
$companyStatus = $companyObj->getCompanyStatuses();
$companyStatusSize = sizeof($companyStatus);
$companyType = $companyObj->getCompanyTypes();
$companyTypeSize = sizeof($companyType);
$noteType = $companyObj->getNoteTypes();
$noteTypeSize = sizeof($noteType);



?>
<?php require_once('includes/sitevars.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
<link rel="manifest" href="favicons/manifest.json">
<link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<style type="text/css">
	.timer {
		visibility: hidden;
	}
</style>
<link type="text/css" href="css/kdes1/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<link type="text/css" href="css/skipjack.css" rel="stylesheet" />
<script src="js/jquery-1.10.2.js"></script>
<script src="js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="js/mitel-call.js"></script>
<script type="text/javascript" src="js/jquery.simple.timer.js"></script>
<script type="text/javascript" src="js/jquery.quicksearch.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){


	$('#advanced-filter-link').show();
	$('#simple-filter-link').hide();
	$('.advanced-filter-element').hide();

	<?php if($advancedFilterEnabled == true) {?>
		$('#advanced-filter-link').hide();
		$('#simple-filter-link').show();
		$('.advanced-filter-element').show();
	<?php } ?>

	if (window.matchMedia("(min-width: 1200px)").matches) {
		$('#advanced-filter-link').hide();
		$('#simple-filter-link').show();
		$('.advanced-filter-element').show();
		$('#simple-filter-link').hide();
		$('#advanced-filter-link').hide();
	}

	$('#advanced-filter-link').click(function(){
		$('#advanced-filter-link').hide();
		$('#simple-filter-link').show();
		$('.advanced-filter-element').show();
	});
	$('#simple-filter-link').click(function(){
		$('.advanced-filter-element').hide();
		$('#advanced-filter-link').show();
		$('#simple-filter-link').hide();
	});

	$('.select2').select2();
	$("#mindate").datepicker({
		dateFormat: 'yy-mm-dd',
		onSelect: function () {

				var minDate = $("#mindate").val();
				var maxDate = $("#maxdate").val();

				var d1 = Date.parse(minDate);
				var d2 = Date.parse(maxDate);

				if (d1 > d2) {
					$("#maxdate").val(minDate);
				}
		},
		changeMonth: true,
		changeYear: true
	});

	$("#maxdate").datepicker({
		dateFormat: 'yy-mm-dd',
		onSelect: function () {

				var minDate = $("#mindate").val();
				var maxDate = $("#maxdate").val();

				var d1 = Date.parse(minDate);
				var d2 = Date.parse(maxDate);

				if (d2 < d1) {
					$("#mindate").val(maxDate);
				}
		},
		changeMonth: true,
		changeYear: true
	});
});
</script>


		<script type="text/javascript">

		//function to do loader more ubiquities
		function showloader(targetid){
		$(targetid).empty().html('<img src="images/ajax-loader.gif" />');
				}
		function getcal(iduser, username, pass){
			$('#gcal').load('ajaxcalendardisplay.php?userid='+iduser +'&googuser='+ username +'&googpass='+pass );

		}


			$(function(){
				// Accordion
				$("#accordion").accordion({ header: "h3" });
				// Tabs
				$('#tabs1').tabs();
				$('#tabs2').tabs();

				//hide add reminder box
				$('#addreminder').hide();
				//reminder button -open form
				$('#btnaddreminder').click(function(){
					$('#addreminder').show('fold');
					return false;
				});
				//close reminder form
				$('#btnCancelRemind').click(function(){
					$('#addreminder').hide('fold');
					return false;
				});

				//stripe tables
				$('.stripeMe tr:even').addClass('alt');
				//quick serach boxes
				$('#filterulist').quicksearch('#userlist tbody tr');
				//for resplist
				$('#filterrlist').quicksearch('#responsiblist tbody tr');
				//hide list manager on initial load
				$('#listmanager').hide('fold');

				//show manager
				$('#btnmanagelists').click(function() {
					$('#listmanager').show('fold');
					$('#btndonemanagelist').show();
				});

				//hide manager when click done
				$('#btndonemanagelist').click(function(){
					$('#btndonemanagelist').hide();
					$('#listmanager').hide('fold');
				});

			//for loading custom client lists
				$('#selectulist').change( function () {
  				var tmp =$('#selectulist').val();
				showloader('#ajaxt2');
				$('#ajaxt2').load('filterulistindex.php?cusername=<?php echo urlencode($colname_userlists) ?>&listid='+tmp , function() {

                        $('.stripeMe tr:even').addClass('alt');
                        //quick serach boxes
                        $('#filterulist').quicksearch('#userlist tbody tr');
                        //for resplist
                        $('#filterrlist').quicksearch('#responsiblist tbody tr');

                    });
                });

				$('#searchtext').keyup(function(e) {

				//alert(e.keyCode);

				if(e.keyCode == 13) {
				//send value of search to search page - load resuts in search result div

					$('#searchresult').load("searchres.php?phone_ip=<?php echo $row_userd['user_phone_ip']; ?>&"+($('#searchtext').serialize()));

				}

});
				$("#mitel_pop_up").dialog({
					autoOpen 		: false,
					modal			: true,
					resizable 		: false,
					height 			: 100,
					closeOnEscape 	: false,
					open 			: function(event, ui){
						$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
					}

				});

				//phone box hider
				$("#phonebox").hide();
				$("#phonebox").dialog({
				autoOpen: false,
				resizable: false}
				)

				$('.itemDelete').live('click', function() {
    $(this).closest('li').remove();
});

				//quicksearch function - execute search on enter
				$('#searchlistuser').keyup(function(e) {

				///alert(e.keyCode);

				if(e.keyCode == 13) {
				///send value of search to search page - load resuts in search result div

					$('#searchlistuserresult').load("quicksearchulist.php?searchterm="+($('#searchlistuser').val()));

				}

});

				// Datepicker
				$('#note_date').datepicker({
					dateFormat: 'yy-mm-dd'
				});

				///code for user list management
				//hide id's on initial load;
				$('#addnoteform').hide();
				$('#cancelnotebutton').hide();
				$('#newulist').hide();
				$('#removeulist').hide();
				$('#modulist').hide();
				$('#btndonemanagelist').hide();


				//hide add notes
				$('#addnotebutton').click( function(){
					$('#addnotebutton').hide();
					$('#addnoteform').show('fold');
					$('#cancelnotebutton').show('fold');
					return false;
				});

				//user list hide/show
				$('#btnnewulist').click( function(){
					$('#removeulist').hide();
					$('#modulist').hide();
					$('#newulist').show('fold');
				});

				$('#btnremoveulist').click( function(){
					$('#newulist').hide();
					$('#modulist').hide();
					$('#removeulist').show('fold');
				});

				// Display the modify sub form
				$('#btnmodulist').click( function(){
					$('#newulist').hide();
					$('#removeulist').hide();
					$('#modulist').show('fold');

					// Set up links so the form can be edited
					var listname_var = document.getElementById("edit_listname_id");
					var shared_var = document.getElementById("edit_shared_id");
					var editable_var = document.getElementById("edit_editablebyothers_id");

					// The Call List selected
					var selected_list_id= $('#selectulist').val();

					// Previous db call already defined the call lists
					// Loop until correct entry found and display.
					<?php
					do {

					?>
					  if (selected_list_id== <?php echo $row_availlists['listid']?>)
					  {
					  		listname_var.value="<?php echo $row_availlists['listname']?>";

					  		if (<?php echo $row_availlists['shared']?>)
					  		{
					  			shared_var.checked="checked";
					  		}
					  		if (<?php echo $row_availlists['editablebyothers']?>)
					  		{
					  			editable_var.checked="checked";
					  		}
					  }

					  <?php
					} while ($row_availlists = mysql_fetch_assoc($availlists));
					  $rows = mysql_num_rows($availlists);
					  if($rows > 0) {
					      mysql_data_seek($availlists, 0);
						  $row_availlists = mysql_fetch_assoc($availlists);
					  }
					?>
				});



				// Process the Modify sub form
				$('#conf_edit_ulist').click( function()
				{
					// Update the db
					var tmp = $('#edit_call_list').serialize() + "&selectulistd="+$('#selectulist').val();

					$('#searchlistuserresult').empty();
					 $('#selectulist').load('userclientlistavaillist.php?'+tmp);
					$('#ajaxt2').load('filterulistindex.php?cusername=<?php echo $colname_userlists ?>&listid='+$('#selectulist').val());
					$('#modulist').hide();
				});



				//cancel buttons in user list operation boxes
				$('#btncancelnewulist').click( function(){
					$('#newulist').hide('fold');
				});

				$('#btncancelremoveulist').click( function(){
					$('#removeulist').hide('fold');
				});

				$('#btndoremoveulist').click( function(){

					// Add the id of the call list of which you want to delete
					// Userclientlistavaillist.php holds the controller code to delete the call list completely
					var tmp = $('#deletelist').serialize() + "&selectulistd="+$('#selectulist').val();

					$('#refreshdellist').empty().html('<a class="clicklink" href="#" onclick="$(&#39;#refreshdellist&#39;).load(&#39;userclientlistavaillist.php?sourcepage=index.phpd&#39;)">Click to refresh available list</a>');
					$('#removeulist').hide('fold');
					$('#searchlistuserresult').empty();
					$('#selectulist').load('userclientlistavaillist.php?'+tmp);
					$('#ajaxt2').load('filterulistindex.php?cusername=<?php echo $colname_userlists ?>&listid=1');


				});


				$('#btncancelmodulist').click( function(){
					$('#modulist').hide('fold');
				});

				//operations for user lists
				//new user list
				$('#confaddulist').click(function (){
				// do this
					//get variables from frmaddnewlist
					var tmp =$('#frmaddnewlist').serialize();
					$('#selectulist').load('userclientlistavaillist.php?'+tmp);
					location.reload();
				})
				//// completed user list management code

				//goodle username add related code
				$('#addgdatasend').click(function(){
					$('#gcal').load('ajaxcalendardisplay.php?' + ($('#addgdataform').serialize()));
				})

			});

function dopoptransfer(notebynickera,cntnoteCompanya,cntNoteNameDesca,cntNoteidvala,cntNotebyUsera) {

	popb = document.getElementById('cnoteadd2') ;
popb.notebynicker.value = notebynickera ;
popb.cntNoteCompany.value = cntnoteCompanya;
popb.cntNoteNameDesc.value = cntNoteNameDesca;
popb.cntNoteidval.value = cntNoteidvala;
popb.cntNoteByUser.value= cntNotebyUsera;

			}
		</script>

<!-- InstanceEndEditable -->
<script language="javascript">
	$(function(){

		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					},
					appendTo: '.quick-search-container'
				}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("ui-autocomplete-item", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);

					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};

        $("#clear-filter").on('click' , function(){
            window.location = 'index.php';
        });

        $("#apply-filter, #export-data").on('click' , function(){

            var exportData = false;
            if($(this).attr('id') == 'export-data') {
              exportData = true;
            }

            var parameters = {},
            selectedGeography = $('#filter-geography :selected').val(),
            selectedAnalyst = $("#filter-analyst :selected").val(),
            startDate = $("#mindate").val(),
            endDate = $("#maxdate").val(),
            institution = $("#filter-institution").val(),
            individual = $("#filter-individual").val(),
            companyStatus = $("#filter-company-status").val(),
            companyTier = $("#filter-company-tier").val(),
            companyType = $("#filter-company-type").val();
            noteType = $("#filter-note-type").val();

            parameters['export'] = 'false';
            if (exportData == true) {
                parameters['export'] = 'true';
            }

            if (selectedGeography != '') {
                parameters['geography'] = selectedGeography;
            }
            if(selectedAnalyst != '') {
                parameters['recent_contact_analyst'] = selectedAnalyst;
            }
            if(startDate != '') {
                parameters['start_date'] = startDate;
            }
            if(endDate != '') {
                parameters['end_date'] = endDate;
            }
            if(institution != '') {
                parameters['institution'] = institution;
            }
            if(individual != '') {
                parameters['individual'] = individual;
            }
            if(companyStatus != '') {
                parameters['company_status'] = companyStatus;
            }
            if(companyTier != '') {
                parameters['company_tier'] = companyTier;
            }
            if(companyType != '') {
                parameters['company_type'] = companyType;
            }
            if(noteType != '') {
                parameters['note_type'] = noteType;
            }
            if (parameters) {
                var queryString = $.param(parameters);
                window.location = 'index.php?'+queryString;
            }
        });

	})
</script>
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			<div class="quick-search-container">
					<input type="text" id="quicksearch" size="16" />
			</div>
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<div class="content40">
<div id="tabs1">
      <ul>
        <li><a href="#tabs1-1"><?php echo $row_userd['user_fname']; ?>'s Lists</a></li>
        <li><a href="#tabs1-2">Responsibility List</a>

        <li><a href="#tabs1-3">Search</a></li>
      </ul>
    <div id="tabs1-1" style="height: 600px; overflow: scroll;"><fieldset>


Current List: <select name="selectulist" id="selectulist">
  <?php
do {
?>
  <option name="<?php echo $row_availlists['listname']?>" value="<?php echo $row_availlists['listid']?>"<?php if (!(strcmp($row_availlists['listid'], 1))) {echo "selected=\"selected\"";} ?>><?php echo $row_availlists['listname']?><?php if( $row_availlists['shared']==1) {?>*<?php } ?> </option>
  <?php
} while ($row_availlists = mysql_fetch_assoc($availlists));
  $rows = mysql_num_rows($availlists);
  if($rows > 0) {
      mysql_data_seek($availlists, 0);
	  $row_availlists = mysql_fetch_assoc($availlists);
  }
?>
</select>

       <input type="button" value="Manage Lists" id="btnmanagelists"  />
       *= shared</fieldset>

      <fieldset id="listmanager"><legend>List Manager</legend>
       <table width="100%" border="0" cellpadding="0" cellspacing="3"><tr><td valign="top" width="50%">
       <fieldset>
       <legend> Users and lists</legend>
       Search for user:<input type="text" id="searchlistuser" name="searchlistuser" /><br />
       <span id="searchlistuserresult"></span>
       </fieldset>

       </td><td width="50%" valign="top">
       <fieldset><legend>Manage Lists</legend>

       	<?php if(isset($_SESSION['MM_Username']) && isset( $_SESSION['MM_UserGroup'] ) &&  $_SESSION['MM_UserGroup'] == 'Administrator' ) {
       	?>
       			<input type="button" value="Add" id="btnnewulist" /><input type="button" value="Remove"  id="btnremoveulist"/><input type="button" value="Modify"  id="btnmodulist"/>
   		<?php
   	    }
   		else
   		{ ?>
   				Admin permissions required.
   		<?php } ?>


<fieldset id="newulist">
<legend>Add new list</legend>
<form id="frmaddnewlist" name="frmaddnewlist">
<span style="text-align:right;">
Listname: <input type="text" name="listname" value="" size="24" /><br />
	  <input type="checkbox" name="shared" value="1" style="display:none;" checked/> <br />
      <input type="checkbox" name="editablebyothers" value="1" style="display:none;" /><br /><input type="button" value="Cancel" id="btncancelnewulist" /><input type="button" value="Add" id="confaddulist" /></span><input type="hidden" name="MM_Username" value="<?php echo $colname_onclientlist ?>"  />
      <input type="hidden" name="MM_insert" value="frmaddnewlist"  />
      <input type="hidden" name="sourcepage" value="index.phpd" />
</form>
</fieldset>
<fieldset id="removeulist">
<legend>Remove list</legend>
	<form id="deletelist">
		<span id="refreshdellist"><a class="clicklink" href="#" onclick="$('#refreshdellist').load('userclientlistavaillist.php?sourcepage=index.phpd')">Click to refresh available list</a></span>
		<input type="hidden" name="sourcepage" value="index.php" />
		<input type="hidden" name="deletefullist"  value="deletefullist"/>
		<input type="button" name="btndoremoveulist" id="btndoremoveulist" value="Remove List" />
		<input type="button" value="Cancel" id="btncancelremoveulist" />
		<br /> * Please note - default list cannot be removed.
	</form>
</fieldset>
<fieldset id="modulist">
<legend>Modify list</legend>
	<form id="edit_call_list">
		  Listname: <input type="text" name="edit_listname" id="edit_listname_id" size="24" />
		  <input type="checkbox" name="edit_shared" id="edit_shared_id" value='1' style="display:none;" checked/> <br />
	      <input type="checkbox" name="edit_editablebyothers" id ="edit_editablebyothers_id" value='1' style="display:none;"/><br />
	      <input type="hidden" name="sourcepage" value="index.php" />


	      <input type="hidden" name="edit_call_list"  value="edit_call_list"/>
	      <input type="button" value="Edit" name="conf_edit_ulist" id="conf_edit_ulist" /></span>

		  <input type="button" value="Cancel" id="btncancelmodulist" />
	</form>
</fieldset>

</fieldset>
</td></tr></table>
   <input type="button" value="Done" id="btndonemanagelist" />
     </fieldset>
    <fieldset>
    <div id="ajaxt2">
    Quick Filter: <input name="filterulist" id="filterulist" type="text" />
    <table cellpadding="2" class="stripeMe" id="userlist" style="width:100%">
	<thead>
                <tr class="tabbhead">
                  <td>Name</td>
                  <td>Institution</td>
                  <td>Phone</td>
                 <!-- <td>Phone</td> !-->
                  <td>CellX</td>
                  <td></td>
                   </tr>
    </thead>
                <tbody>
                  <?php do { ?>
                    <tr>
                      <td><a href="clientdetails.php?cnt_Id=<?php echo $row_userlists['id']; ?>"><?php echo $row_userlists['first_name']; ?> <?php echo $row_userlists['surname']; ?></a></td>
                      <td><?php echo $row_userlists['cnt_company']; ?></td>
                      <!--<td><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userlists['landline']; ?>'); $('#phonebox').dialog('open');"><?php echo $row_userlists['landline']; ?></a></td>
                      !-->
                 	 <?php if (isset($row_userd['user_extension'])) { ?>
                      <td><a href="javascript:void(0);" onclick="callMitel('<?php echo $row_userd['user_extension'];?>','<?php echo $row_userlists['landline'];?>','<?php echo $row_userlists['first_name']; ?>',' <?php echo $row_userlists['surname']; ?>');$('#mitel_pop_up').dialog('open');"><?php echo $row_userlists['landline']; ?></a></td>

                       <td><a href="javascript:void(0);" onclick="callMitel('<?php echo $row_userd['user_extension'];?>','<?php echo $row_userlists['mobile'];?>','<?php echo $row_userlists['first_name']; ?>',' <?php echo $row_userlists['surname']; ?>');"><?php echo $row_userlists['mobile']; ?></a></td>
                       <?php } else { ?>
                       		<td><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userlists['landline']; ?>'); $('#phonebox').dialog('open');"><?php echo $row_userlists['landline']; ?></a></td>
                      		<td align="right"><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userlists['mobile']; ?>')"><?php echo $row_userlists['mobile']; ?></a></td>
                      	<?php } ?>
                      <td><span style="text-align:right;"><a href="mailto:<?php echo $row_userlists['email']; ?>"><img border="0" src="images/email-icon-16.gif" width="16" height="16" align="right" /></a></span></td>
                  </tr>
                    <?php } while ($row_userlists = mysql_fetch_assoc($userlists)); ?>
                </tbody>
  </table>
  </div>
  </fieldset>
</div>
      <div id="tabs1-2">
      <fieldset>
      Please select one:<select name="selresp" onchange="$('#idrespload').load('loadresplist.php?respons_id='+ this.options[this.options.selectedIndex].value+'&user_phone_ip=<?php echo $row_userd['user_phone_ip']; ?>')">
        <?php
do {
?>
        <option value="<?php echo $row_rsresplists['respons_id']?>"><?php echo $row_rsresplists['response_desc']?></option>
        <?php
} while ($row_rsresplists = mysql_fetch_assoc($rsresplists));
  $rows = mysql_num_rows($rsresplists);
  if($rows > 0) {
      mysql_data_seek($rsresplists, 0);
	  $row_rsresplists = mysql_fetch_assoc($rsresplists);
  }

?>
      </select>
      </fieldset>

      <fieldset>Quick Filter: <input name="filterrlist" id="filterrlist" type="text" />
      <div id="idrespload">
<table cellpadding="2" class="stripeMe" id="responsiblist" style="width:100%">
	<thead>
                <tr class="tabbhead">
                  <td>Name</td>
                  <td>Institution</td>
                  <td>Phone</td>
                  <td>Cell</td>
      </tr>
    </thead>
                <tbody>
                  <?php do { ?>
                    <tr>
                      <td><?php echo $row_specresplists['first_name']; ?> <?php echo $row_specresplists['surname']; ?></td>
                      <td><a href="companydetails-view.php?id=<?php echo $row_specresplists['comp_id']; ?>" title="Click to view Institution"><?php echo $row_specresplists['Company']; ?></a></td>
                      <?php if(isset($row_userd['user_extension'])) { ?>
                      		<td><a href="javascript:void(0);" onclick="callMitel('<?php echo $row_userd['user_extension'];?>','<?php echo $row_specresplists['landline'];?>');$('#phonebox').dialog('open');"><?php echo $row_specresplists['landline']; ?></a></td>
                      		 <td><a href="javascript:void(0);" onclick="callMitel('<?php echo $row_userd['user_extension'];?>','<?php echo $row_specresplists['mobile'];?>');"><?php echo $row_specresplists['mobile']; ?></a></td>
                      <?php } else { ?>
	                      <td><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_specresplists['landline']; ?>')" ><?php echo $row_specresplists['landline']; ?></a></td>
	                      <td align="right"><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_specresplists['mobile']; ?>')"><?php echo $row_specresplists['mobile']; ?> </a></td>
	                  <?php } ?>

                  </tr>
                    <?php } while ($row_specresplists = mysql_fetch_assoc($specresplists)); ?>
                </tbody>
</table>
      </div>
      </fieldset></div>
      <div id="tabs1-3"><fieldset>
      <legend>Search</legend><input type="text" id="searchtext" name="searchterm" onfocus="this.value=''" value="search text here"  />
      </fieldset>
      <fieldset>
      <div id="searchresult"></div>
      </fieldset>
      </div>
  </div>
 </div>
 <div class="timer" data-seconds-left ='7'></div>
<div class="content40">
<div id="tabs2">
      <ul>
        <li><a href="#tabs2-1">Recent Contacts</a></li>
        <li><a href="#tabs2-2">Calendar</a></li>
        <li><a href="#tabs2-3">Institution Browser</a></li>
      </ul>
      <div id="tabs2-1" style="height: 600px; overflow: scroll;">
      <fieldset>
      <p>Filter By:</p>
       <table border="0" cellspacing="0" cellpadding="0" style="margin:10px 0; width:100%;">
            <tbody>
                <tr>
                    <td style="padding-left:0;">Analyst:</td>
                    <td>
                      <select id="filter-analyst" style="min-width:153px;" class="select2">
                        <option value="All">-- All Analysts --</option><? while($analyst = mysql_fetch_assoc($distinctNoteNickResult)) { ?> <option value="<? echo $analyst['Analyst_id'];?>" <? if(isset($_GET['recent_contact_analyst']) && $_GET['recent_contact_analyst'] == $analyst['Analyst_id']){?> selected<?}?> ><? echo $analyst['Analyst_name'];?></option> <? }?>
                      </select>
                    </td>
                    <td style="text-align:right;"><input type="button" id="export-data" value="Export Data" /></td>
								</tr>
                <tr>
                    <td style="padding-left:0; width:100px;">Geography:</td>
                    <td>
                      <select id="filter-geography" name="filter-geography" style="min-width:153px;" class="select2">
                              <option value="">--All--</option>
                              <?php
                              for ($x=0; $x < sizeof($allLocations); $x++) {
                                  $selectedGeography = isset($_GET['geography'])
                                                      && $_GET['geography'] == $allLocations[$x]['id_geography']
                                                      ? 'selected="selected"' : '';
                               ?><option value="<?php echo $allLocations[$x]['id_geography']; ?>" <?php echo $selectedGeography; ?>>
                                      <?php echo $allLocations[$x]['name']; ?></option>
                              <?php } ?>
                      </select>
                    </td>
                    <td style="text-align:right;"><input id="apply-filter" type="button" value="Apply Filter"/> <input id="clear-filter" type="button" value="Clear Filters"/></td>
								</tr>

                <tr>
                	<td colspan="3">
                  <a href="javascript: void(0);" id="advanced-filter-link" style="display:none;">Advanced Filter</a>
                  <a href="javascript: void(0);" id="simple-filter-link" style="display:none;">Simple Filter</a>
                  </td>
                </tr>
                <tr class="advanced-filter-element">
                	<td colspan="3">
                  <hr />
                  </td>
                </tr>
                <tr class="advanced-filter-element">
                    <td style="padding-left:0;">Start Date:</td>
                    <td><input name="mindate" id="mindate" type="text" value="<?php if(isset($_GET['start_date'])) {echo $_GET['start_date'];}?>"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="advanced-filter-element">
                    <td style="padding-left:0;">End Date:</td>
                    <td><input name="maxdate" id="maxdate" type="text" value="<?php if(isset($_GET['end_date'])) {echo $_GET['end_date'];}?>"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="advanced-filter-element">
                    <td style="padding-left:0;">Institution:</td>
                    <td colspan="2">
                      <select id="filter-institution" name="filter-institution" style="width:300px;" class="select2">
                        <option value="">-- All Institutions --</option>
                        <?php
												foreach ($institutionData as $institution) {?>
                        <option value="<? echo $institution['comp_id'];?>" <? if(isset($_GET['institution']) && $_GET['institution'] == $institution['comp_id']){?> selected<?}?> ><? echo $institution['Company'];?></option>
                        <?php } ?>
												<? while($institution = mysql_fetch_assoc($distinctNoteInstitutionResult)) { ?> <option value="<? echo $institution['comp_id'];?>" <? if(isset($_GET['institution']) && $_GET['institution'] == $institution['comp_id']){?> selected<?}?> ><? echo $institution['Company'];?></option> <? }?>

                      </select>
                    </td>

                </tr>
                <tr class="advanced-filter-element">
                    <td style="padding-left:0;">Individual:</td>
                    <td colspan="2">
                      <select id="filter-individual" name="filter-individual" style="width:300px;" class="select2">
                        <option value="">-- All Individuals --</option>
                        <?php
												foreach ($individualData as $individual) {?>
                        <option value="<? echo $individual['id'];?>" <? if(isset($_GET['individual']) && $_GET['individual'] == $individual['id']){?> selected<?}?> ><? echo $individual['Individual'];?></option>
                        <? }?>
                      </select>
										</td>
                </tr>

                <tr class="advanced-filter-element">
                    <td style="padding-left:0;">Status:</td>
                    <td>
                      <select name="filter-company-status" id="filter-company-status" style="width:153px;" class="select2">
                        <option value="">--All--</option>
                        <?php for( $y=0; $y < $companyStatusSize; $y++ ) { ?>
                          <option value="<?php  echo $companyStatus[$y]['id_companystatus']; ?>" <? if(isset($_GET['company_status']) && $_GET['company_status'] == $companyStatus[$y]['id_companystatus']){?> selected<?}?>><?php  echo $companyStatus[$y]['company_status']; ?></option>
                        <?php } ?>
                      </select>
										</td>
                    <td>&nbsp;</td>
								</tr>

                <tr class="advanced-filter-element">
                    <td style="padding-left:0;">Tier:</td>
                    <td>
                      <select name="filter-company-tier" id="filter-company-tier" style="width:153px;" class="select2">
                        <option value="">--All--</option>
                        <?php for( $y=0; $y < $companyTiersSize; $y++ ) { ?>
                          <option value="<?php  echo $companyTiers[$y]['id_companytier']; ?>" <? if(isset($_GET['company_tier']) && $_GET['company_tier'] == $companyTiers[$y]['id_companytier']){?> selected<?}?>><?php  echo $companyTiers[$y]['companytier']; ?></option>
                        <?php } ?>
                      </select>
										</td>
                    <td>&nbsp;</td>
								</tr>

                <tr class="advanced-filter-element">
                    <td style="padding-left:0;">Type:</td>
                    <td>
                      <select name="filter-company-type" id="filter-company-type" style="width:153px;" class="select2">
                        <option value="">--All--</option>
                        <?php for( $y=0; $y < $companyTypeSize; $y++ ) { ?>
                          <option value="<?php  echo $companyType[$y]['id_companytype']; ?>" <? if(isset($_GET['company_type']) && $_GET['company_type'] == $companyType[$y]['id_companytype']){?> selected<?}?>><?php  echo $companyType[$y]['companytype']; ?></option>
                        <?php } ?>
                      </select>
										</td>
                    <td>&nbsp;</td>
								</tr>

                <tr class="advanced-filter-element">
                    <td style="padding-left:0;">Note Type:</td>
                    <td>
                      <select name="filter-note-type" id="filter-note-type" style="width:300px;" class="select2">
						<option value="-webviewopen" selected="selected">--All except Webview opens--</option>
                        <?php for( $y=0; $y < $noteTypeSize; $y++ ) { ?>
                          <option value="<?php  echo $noteType[$y]; ?>" <? if(isset($_GET['note_type']) && $_GET['note_type'] == $noteType[$y]){?> selected<?}?>><?php  echo $noteType[$y]; ?></option>
                        <?php } ?>
                      </select>
										</td>
                    <td>&nbsp;</td>
								</tr>
            </tbody>
        </table>

      <hr>
     	<?php if(!$row_rsRecCompCons) {?><p>No results</p><?php } else { ?>
      <table width="100%" cellpadding="2" class="stripeMe">
      <thead>
      <tr class="tabbhead">
      	<td>Date</td><td>Institution</td><td>Individual</td><td>Note Type</td><td>By</td>
      </tr>
      </thead>

        <?php do { ?>
          <tr>
            <td class="nw"><?php echo $row_rsRecCompCons['note_date']; ?></td>
			<?php if ($row_rsRecCompCons['Company_id'] != '' ){ ?>
			<td><a href="companydetails-view.php?id=<?php echo $row_rsRecCompCons['Company_id']; ?>" title="Click to view Institution" ><?php echo $row_rsRecCompCons['Institution']; ?></a></td>
			<?php } else { ?>
				<td><?php echo $row_rsRecCompCons['Institution']; ?></td>
			<?php } ?>
			<td><a href="clientdetails.php?cnt_Id=<?php echo $row_rsRecCompCons['Note_by_id']; ?>"><?php echo $row_rsRecCompCons['Individual']; ?></a></td>
			<td><?php echo $row_rsRecCompCons['note_type']; ?></td>
			<td><?php echo $row_rsRecCompCons['Note_by']; ?></td>
          </tr>
          <?php } while ($row_rsRecCompCons = mysql_fetch_assoc($rsRecCompCons)); ?>
      </table>
      <?php } ?>
      </fieldset></div>
      <div id="tabs2-2">
      </fieldset></div>
      <div id="tabs2-3"></div>
    </div>
 </div>
<!-- Mitel Pop Up dialog box !-->

<div id="mitel_pop_up" title="Initiating a call ...">
	<div> Placing a call to <span id='call_number'></span> .... </div>
</div>

<!-- End Code!-->

 <div id="phonebox" title="Call Interface">
 <form id="cnoteadd2" name="cnoteadd2">
        <textarea name="Notetxt" cols="29" rows="2" id="Notetxt"></textarea>
        <input type="button" value="Load prev note" onclick="javascript : popb.Notetxt.value='<?php echo $row_lastnote['note_txt']; ?>';"/>
<br />
		  <input name="note_date" type="text" id="note_date" value="<?php echo date("Y-m-d"); ?>" />
		  	Type:
		  	  <select name="notetype" id="notetype">
		  	    <?php
do {
?>
		  	    <option value="<?php echo $row_rsCtype['ContactType']?>"><?php echo $row_rsCtype['ContactType']?></option>
		  	    <?php
} while ($row_rsCtype = mysql_fetch_assoc($rsCtype));
  $rows = mysql_num_rows($rsCtype);
  if($rows > 0) {
      mysql_data_seek($rsCtype, 0);
	  $row_rsCtype = mysql_fetch_assoc($rsCtype);
  }
?>
	  	      </select><br />
	  	      <input type="hidden" name="notebynicker" value="<?php echo $row_rsuserDet['user_fname']; ?>" />
			   <input type="button" style="display:none !important;" value="Stop Call" onClick="makeRequest('phoneevent.php?keyvent=CANCEL&addressout=<?php echo $row_rsUserDetails['user_phone_ip']; ?>','invdiv')"/><input name="Submit" type="submit" value="Save Note" />
			  <input type="hidden" name="cntNoteCompany" value="<?php echo $row_rsCnt['cnt_company']; ?>" />
			  <input type="hidden" name="cntNoteNameDesc" value="<?php echo $row_rsCnt['cnt_Fname']; ?> <?php echo $row_rsCnt['cnt_Lastname']; ?>" />
			  <input type="hidden" name="cntNoteidval" value="<?php echo $row_rsCnt['cnt_Id']; ?>" />
			  <input type="hidden" name="cntNoteByUser" value="<?php echo $_SESSION['MM_Username']; ?>" />
              <input type="hidden" name="MM_insert" value="cnoteadd">
              <input type="hidden" name="phonenum" value="">
              <input type="button" value="Close" onClick="$('#phonebox').dialog('close');"/>
			 </form>

 </div>
 <div id="phonebox2" style="visibility:none"></div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($userd);


mysql_free_result($userlists);

mysql_free_result($rsresplists);

mysql_free_result($specresplists);

mysql_free_result($availlists);

mysql_free_result($lastnote);

mysql_free_result($rsCtype);

mysql_free_result($rsRecCompCons);
?>
