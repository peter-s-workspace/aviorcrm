
<?php
require_once('Connections/CRMconnection.php');

if (!isset($_SESSION)) {
  session_start();
}

/* Note Search function
   1) Query db using date range and note creator
   2) Search results with query string
   3) Query db to display search results

*/
function note_search($searchstr, $creator_fname, $creator_lname, $lower_date_bound, $upper_date_bound) //,  $database_CRMconnection, $CRMconnection
{
  global $database_CRMconnection;
  global $CRMconnection;

  // Allows for date range to be open ended
  $lower_date = ($lower_date_bound!="" AND $lower_date_bound!=NULL) ? $lower_date_bound : "1000-01-01";
  $upper_date = ($upper_date_bound!="" AND $upper_date_bound!=NULL) ? $upper_date_bound : "9999-01-01";

  $query_notes = sprintf("SELECT tblnotes.note_Id, tblnotes.cntCompany,tblnotes.note_cnt_name, tbluser.user_name, tbluser.user_email, tblnotes.note_txt,
                          tbluser.user_fname, tbluser.user_lname
                          FROM tblnotes, tbluser
                          WHERE tbluser.user_name=tblnotes.note_by 
                          AND (%s IS NULL OR tbluser.user_fname=%s)
                          AND (%s IS NULL OR tbluser.user_lname=%s)
                          AND (tblnotes.note_date BETWEEN %s AND %s)
                          ORDER BY tblnotes.note_date DESC
                          "
                          , GetSQLValueString($creator_fname, "text"), GetSQLValueString($creator_fname, "text"), GetSQLValueString($creator_lname, "text"), 
                          GetSQLValueString($creator_lname, "text"), GetSQLValueString($lower_date, "text"), GetSQLValueString($upper_date, "text"));
  
  mysql_select_db($database_CRMconnection, $CRMconnection);
  $note_results = mysql_query($query_notes, $CRMconnection) or die(mysql_error());
  $item="";  

  // Merge results into string for simplified searching

  if ($searchstr)
  {
    $needle = explode(" ", strtolower($searchstr));
    $sort_array=array();
    
    // Use php substr_count to tally the number of occurences of each word in a note result
    // Search metric is the product of all subcounts+1 (to handle zero)
    // Product used over sum to positively bias records that contain all search terms at least once.
    if( $note_results ) {
      while ($row = mysql_fetch_assoc($note_results)) {
        
        $haystack = strtolower(implode(" ", $row));
        $count = 1;
        $subcount=0;
        foreach ($needle as $substring) {
          $subcount=substr_count( $haystack, $substring);
          $count *=($subcount+1); 
        }
        $sort_array[$row['note_Id']] = $count;
      }
    }
    mysql_free_result($note_results);
    
    //php assoc array sort by value descending
    arsort($sort_array);
    $in_clause_str="";

    // Used to maintain query ranking in following db query
    foreach($sort_array as $i => $value){
      if ($sort_array[$i] > 0){
        $in_clause_str.=",".$i;
      }
    }
    
    if( $sort_array ) {
    $query_match = sprintf("SELECT DISTINCT tblnotes.note_Id, tblnotes.cntCompany,tblnotes.note_cnt_name, tbluser.user_fname, tbluser.user_lname, tbluser.user_name, tbluser.user_email, tblnotes.note_txt, tblnotes.note_date
                    FROM tblnotes, tbluser
                    WHERE tblnotes.note_Id IN (%s)
                    AND tbluser.user_name=tblnotes.note_by
                    ORDER BY FIELD(tblnotes.note_Id%s)", mysql_real_escape_string(substr($in_clause_str,1)), mysql_real_escape_string($in_clause_str));
    }
    else{
      ?> 
        No Results
      <?php 
      return NULL;
    }

    mysql_select_db($database_CRMconnection, $CRMconnection);
    $match_results = mysql_query($query_match, $CRMconnection) or die(mysql_error());
  }
  else
  {
    $match_results = $note_results;
  }


?> 
<table style="border:2px solid #ddedde">
<tr><td>Note Id</td><td>Company</td><td>Date</td><td>Note Contact Name</td><td>Note Creator</td><td>Text</td></tr>
<?php  //Display results
  if( $match_results ) {
    while ($row = mysql_fetch_assoc($match_results)) {
    
      ?>
          <tr>
            <td style="border:2px solid #ddedde;width:4%"><?php echo $row['note_Id']; ?></td>
            <td style="border:2px solid #ddedde;width:10%"><?php echo $row['cntCompany']; ?></td> 
            <td style="border:2px solid #ddedde;width:8%"><?php echo $row['note_date']; ?></td>
            <td style="border:2px solid #ddedde;width:14%"><?php echo $row['note_cnt_name']; ?></td> 
            <td style="border:2px solid #ddedde;width:14%"><?php echo $row['user_fname']." ".$row['user_lname']?></td>  
            <td style="border:2px solid #ddedde;width:50%"><?php echo $row['note_txt']; ?></td>
                     
          </tr>
  <?php
    }
  }
  mysql_free_result($match_results);
  mysql_free_result($note_results);

  ?> 
  </table>
  <?php 

  return NULL;
  
}


$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  $isValid = False; 
  if (!empty($UserName)) { 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}
$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

?>

<?php require_once('includes/sitevars.php');?>
<?php require_once('includes/abovehead.php'); ?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
<link rel="manifest" href="favicons/manifest.json">
<link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
 <link type="text/css" href="css/kdes1/jquery-ui-1.9.2.custom.css" rel="stylesheet" />		
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="js/jquery.validate/jquery.validate.min.js"></script>


	
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<div class="content60"> 
<form action="search_notes.php" method="POST">
        <h1>Contact Note Search function</h1>
        <input type="text" name="note_search_name" id="note_search_id" size="45" placeholder="Enter search term.."/><br/>
        Search string searches Company Name, Contact Name, Note Creators Username, Note Creators Email, Note Content
        <br/><br/><br/>
        Note Creator:<br/> First Name
        <input type="text" name="note_creator_fname" id="note_creator_fid" size="21" /><br/>
        Last Name
        <input type="text" name="note_creator_lname" id="note_creator_lid" size="21" />
        <br/>
        <br/>
        <br/>
        
        Start Date
        <input type="text" name="note_date_lower" id="note_date_lower_id" size="18" placeholder="YYYY-MM-DD"/>
        <br/>
        <br/>
        End Date
        <input type="text" name="note_date_upper" id="note_date_upper_ids" size="18" placeholder="YYYY-MM-DD"/>
        <br/>
        <br/>
        <input type="submit" id="search_note_global" value="Search"  />

        </br>
        <p>
        <?php 
        // Pass form data to the Note Search function
        // Note: The db connection must be passed in because of php scoping
        if (isset($_POST) && isset($_POST["note_search_name"]))
        {
          echo note_search($_POST["note_search_name"] ,$_POST["note_creator_fname"], $_POST["note_creator_lname"], $_POST["note_date_lower"], $_POST["note_date_upper"]); //,  $database_CRMconnection, $CRMconnection);
        }?> 

        </p>
        <p>
        <?php //var_dump($_POST); ?>
        </p>
</form>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>