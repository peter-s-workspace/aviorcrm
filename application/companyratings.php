<?php require_once('Connections/CRMconnection.php');

if (!isset($_SESSION)) {
  session_start();
}

/*==============================
	Developer Note:
	References to institution refer to company/companies in the db
 ===============================
*/


$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
  // For security, start by assuming the visitor is NOT authorized.
  $isValid = False;

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
  // Therefore, we know that a user is NOT logged in if that Session variable is blank.
  if (!empty($UserName)) {
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
    // Parse the strings into arrays.
    $arrUsers = Explode(",", $strUsers);
    $arrGroups = Explode(",", $strGroups);
    if (in_array($UserName, $arrUsers)) {
      $isValid = true;
    }
    // Or, you may restrict access to only certain users based on their username.
    if (in_array($UserGroup, $arrGroups)) {
      $isValid = true;
    }
    if (($strUsers == "") && true) {
      $isValid = true;
    }
  }
  return $isValid;
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0)
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo);
  exit;
}

require_once 'includes/classes/company.class.php';

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$companyObj = new Company();

if( isset( $_GET['export'] ) && $_GET['export'] == 'colorcodes' ) {
	$companyObj->ExportCompanyCodes();
}

$companyInfo = $companyObj->getAllCompanyProperties();
$companyTiers = $companyObj->getCompanyTiers();
$companyTiersSize = sizeof($companyTiers);
$companyStatus = $companyObj->getCompanyStatuses();
$companyStatusSize = sizeof($companyStatus);
$companyType = $companyObj->getCompanyTypes();
$companyTypeSize = sizeof($companyType);

$companyLocation = $companyObj->getLocation();
$companyLocationSize = sizeof($companyLocation);

require_once('includes/sitevars.php');
require_once('includes/abovehead.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
<link rel="manifest" href="favicons/manifest.json">
<link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<link type="text/css" href="css/kdes1/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<link type="text/css" href="css/datatables/dataTables.twitter.bootstrap.css" rel="stylesheet" />
<script src="js/jquery-1.11.1.min.js"></script>

<script src="js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.custom.js"></script>


<script type="text/javascript">
	$(function(){
		// Tabs
		$('#tabs1').tabs();
		$('#tabs2').tabs();
		$('.itemDelete').live('click', function() {
			$(this).closest('li').remove();
		});
		// Dialog
		$('#dialog').dialog({
			autoOpen: false,
			width: 600,
			buttons: {
				"Ok": function() {
					$(this).dialog("close");
				},
				"Cancel": function() {
					$(this).dialog("close");
				}
			}
		});
		// Dialog Link
		$('#dialog_link').click(function(){
			$('#dialog').dialog('open');
			return false;
		});
		//hide add reminder box
		$('#addreminder').hide();
		//reminder button -open form
		$('#btnaddreminder').click(function(){
			$('#addreminder').show('fold');
			return false;
		});
		//close reminder form
		$('#btnCancelRemind').click(function(){
			$('#addreminder').hide('fold');
			return false;
		});
		// Datepicker
		$('#datepicker').datepicker({
			inline: true
		});
		// Slider
		$('#slider').slider({
			range: true,
			values: [17, 67]
		});
		// Progressbar
		$("#progressbar").progressbar({
			value: 20
		});
		//hover states on the static widgets
		$('#dialog_link, ul#icons li').hover(
			function() { $(this).addClass('ui-state-hover'); },
			function() { $(this).removeClass('ui-state-hover'); }
		);

		$('.stripeMe tr:even').addClass('alt');
		$('#filterclist').quicksearch('#clist tbody tr');
	});

function filtercolor(color) {
	var columnWidth = $("#company_column").width();
	if (color =='All'){
		$("#avclientbody tr").show();
	} else {
		$('#avclientbody tr').each(function(){
			$(this).hide();
		});
		$("#avclientbody tr td:contains('"+color+"')").each(function(){
			$(this).closest("tr").show();
		});
	}
		$("#company_column").width(columnWidth)
}

// check for change in a specific div
function updateCompany(update_column, val ,comp_id)
{
	var targetdiv = '#companycode_'+comp_id;
	$.ajax({
			url : "ajaxopscompanyupdate.php",
			type: "POST",
			data : {'update_column':update_column, 'val': val,'comp_id':comp_id },
			success: function(data, textStatus, jqXHR) {
				switch(update_column) {
					case 'tier':
						$(targetdiv).html('Tier Updated');
					break;
					case 'status':
						$(targetdiv).html('Status Updated');

					break;
					case 'type':
						$(targetdiv).html('Type Updated');
					break;
					case 'location':
							$(targetdiv).html('Geography Updated');
					break;
				}
			},
			 error: function (jqXHR, textStatus, errorThrown){
				 $(targetdiv).html('Update failed');
			}
		});
}

</script>

<script type="text/javascript">


	$(document).ready(function(){

		aType = $('#a_type').val();

		if( aType == 'Administrator' ) {


		var oTable;

		 var oTable = $('#clist').dataTable({
				 "bPaginate": false,
				 "sDom": '<"H"pr>t<"F"l><"clear">',
				 "columns": [
					null,
					{"orderDataType": "dom-select"},
					{"orderDataType": "dom-select"},
					{"orderDataType": "dom-select"},
					{"orderDataType": "dom-select"}
					]
			});

		$('#filterclist').keyup(function(){
			$('#company_status_filter').val('');
			$('#company_rating_filter').val('');
			$('#company_type_filter').val('');
			$('#company_location_filter').val('');

		});

		$('select[id^="company_rating__value_"]').each(function(){
			var selectionText = $(this).find('option:selected').text();
			$(this).find('option:selected').parent().removeClass('gold_option').removeClass('platinum_option').removeClass('silver_option').removeClass('blue_option').removeClass('bronze_option');
			switch(selectionText) {
				case 'Gold':
						$(this).find('option:selected').parent().addClass('gold_option');
				break;
				case 'Platinum':
						$(this).find('option:selected').parent().addClass('platinum_option');
				break;
				case 'Silver':
						$(this).find('option:selected').parent().addClass('silver_option');
				break;
				case 'Blue':
						$(this).find('option:selected').parent().addClass('blue_option');
				break;
				case 'Bronze':
						$(this).find('option:selected').parent().addClass('bronze_option');
				break;
			}
		}).change(function(){
			$(this).find('option:selected').parent().removeClass('gold_option').removeClass('platinum_option').removeClass('silver_option').removeClass('blue_option').removeClass('bronze_option');
			var selectionText = $(this).find('option:selected').text();

			switch(selectionText) {
				case 'Gold':
						$(this).find('option:selected').parent().addClass('gold_option');
				break;
				case 'Platinum':
						$(this).find('option:selected').parent().addClass('platinum_option');
				break;
				case 'Silver':
						$(this).find('option:selected').parent().addClass('silver_option');
				break;
				case 'Blue':
						$(this).find('option:selected').parent().addClass('blue_option');
				break;
				case 'Bronze':
						$(this).find('option:selected').parent().addClass('bronze_option');
				break;

			}
		});

		var noShowItems = function() {
			var noShow = ['Broker','Other','Private Client'];
			$('#avclientbody .type_select :selected').each(function(){
				$(this).closest('tr').show();
				if( $.inArray($.trim($(this).text()),noShow) != -1 ) {
					$(this).closest('tr').hide();
				}
			});
		};

		noShowItems();

		$(".col_filter").change(function(){
			var selectedIds = [];
				$('#filterclist').val('');
				$(".col_filter").each(function(){
					var selected_value = $(this).val();
					var select_ids = $(this).attr('id');

					if (selected_value != '') {
						switch(select_ids) {
							case 'company_location_filter': /*Company Filter */
									$('#avclientbody .location_select').each(function(){
										if($(this).val()== selected_value){
											$(this).closest('tr').addClass('s_location');
										} else {
											$(this).closest('tr').removeClass('s_location');
										}
									});
							case 'company_status_filter': /*Status Filter */
									$('#avclientbody .status_select').each(function(){
										if($(this).val()== selected_value){
											$(this).closest('tr').addClass('s_status');
										} else {
											$(this).closest('tr').removeClass('s_status');
										}
									});
							break;
							case 'company_rating_filter': /*Tier Filter */
									$('#avclientbody .rating_select').each(function(){
										if($(this).val()== selected_value){
											$(this).closest('tr').addClass('s_tier');
										} else {
											$(this).closest('tr').removeClass('s_tier');
										}
									});
							break;
							case 'company_type_filter': /* Type Filter */
									$('#avclientbody .type_select').each(function(){
										if($(this).val() == selected_value){
											$(this).closest('tr').addClass('s_type');
										} else{
											$(this).closest('tr').removeClass('s_type');
										}
									});
							break;
						}
					} else {

						switch(select_ids) {
							case 'company_location_filter': /*Status Filter */
									$('#avclientbody .location_select').each(function(){
										$(this).closest('tr').removeClass('s_location');
									});
							break;
							case 'company_status_filter': /*Status Filter */
									$('#avclientbody .status_select').each(function(){
										$(this).closest('tr').removeClass('s_status');
									});
							break;
							case 'company_rating_filter': /*Tier Filter */
									$('#avclientbody .rating_select').each(function(){
										$(this).closest('tr').removeClass('s_tier');
									});
							break;
							case 'company_type_filter': /* Type Filter */
									$('#avclientbody .type_select').each(function(){
										$(this).closest('tr').removeClass('s_type');
									});
							break;
						}
					}
				});

				var selectedIds = []
				$(".col_filter").each(function(){
					if( $(this).val() != '' ) {
						 selectedIds.push($(this).attr('id'));
					}
				});

				if (selectedIds.length > 0 ) {

					$('#avclientbody tr').each(function(){

					var numberOfFoundItems = $(this).hasClass('s_status') ? 1 : 0;
						numberOfFoundItems += $(this).hasClass('s_tier') ? 1 : 0;
						numberOfFoundItems += $(this).hasClass('s_type') ? 1 : 0;
						numberOfFoundItems += $(this).hasClass('s_location') ? 1 : 0;
						if (numberOfFoundItems == selectedIds.length) {
							$(this).show();
						} else {
							$(this).hide();
						}
					});

				} else {
					$('#avclientbody tr').show();
				}
		});

		} else {

			 oTable = $('#clist').dataTable({
				 "bPaginate": false,
				 "sDom": '<"H"pr>t<"F"l><"clear">',
				 initComplete: function () {
						var api = this.api();
						api.columns().indexes().flatten().each( function ( i ) {
							$('.col_filter').on( 'change', function () {
									var columndId = $(this).attr('id');
									var columnNumber = 0;
									if(columndId == 'company_location_filter') {
										columnNumber = 1;
									} else if(columndId == 'company_status_filter') {
										columnNumber = 2;
									} else if(columndId == 'company_rating_filter') {
										columnNumber = 3;
									} else if(columndId == 'company_type_filter') {
										columnNumber = 4;
									}
									var column = api.column(columnNumber);
									var val = $(this).val();

									column.search( val ).draw();
								} );
						} );
					}
			});


			var noShowItems = function() {
			var noShow = ['Broker','Other','Private Client'];
			$('#avclientbody .type_text').each(function(){
					$(this).closest('tr').show();
					if( $.inArray($.trim($(this).text()),noShow) != -1 ) {
						$(this).closest('tr').hide();
					}
				});
			};
			noShowItems();

			$('#avclientbody td.companytier').each(function(){
				var selectionText = $(this).text();

				switch(selectionText) {
					case 'Gold':
							$(this).addClass('gold_option');
					break;
					case 'Platinum':
							$(this).addClass('platinum_option');
					break;
					case 'Silver':
							$(this).addClass('silver_option');
					break;
					case 'Blue':
							$(this).addClass('blue_option');
					break;
					case 'Bronze':
							$(this).addClass('bronze_option');
					break;
				}
			});

			/*
			$('#company_rating_filter').change(function(){
				//Tier
				var selectedValue = $(this).val();
				var columnWidth = $("#company_column").width();
				if( selectedValue == '' ) {
					$('#avclientbody tr').show();
					noShowItems();
				} else {
					$("#company_status_filter").find('option:selected').removeAttr("selected");
					$("#company_type_filter").find('option:selected').removeAttr("selected");
					$('#avclientbody tr').hide();
					$('#avclientbody tr td:nth-child(3):contains("'+selectedValue+'")').each(function(){
						$(this).closest('tr').show();
					});
					$("#company_column").width(columnWidth);
				}
			});

			$('#company_status_filter').change(function(){
				var selectedStatus = $(this).val();
				var columnWidth = $("#company_column").width();
				if( selectedStatus == '' ) {
					$('#avclientbody tr').show();
					noShowItems();
				} else {
					$("#company_rating_filter").find('option:selected').removeAttr("selected");
					$("#company_type_filter").find('option:selected').removeAttr("selected");
					$('#avclientbody tr').hide();
					$('#avclientbody tr td:nth-child(2):contains("'+selectedStatus+'")').each(function(){
						$(this).closest('tr').show();
					});
					$("#company_column").width(columnWidth);
				}
			});
			$('#company_type_filter').change(function(){
				var selectedType = $(this).val();
				var columnWidth = $("#company_column").width();
				if( selectedType == '' ) {
					$('#avclientbody tr').show();
					noShowItems();
				} else {
					$("#company_rating_filter").find('option:selected').removeAttr("selected");
					$("#company_status_filter").find('option:selected').removeAttr("selected");
					$('#avclientbody tr').hide();
					$('#avclientbody tr td:nth-child(4):contains("'+selectedType+'")').each(function(){
						$(this).closest('tr').show();
					});
					$("#company_column").width(columnWidth);
				}
			});
			*/


		}
	});
</script>

<!-- InstanceEndEditable -->
<script language="javascript">
	$(function(){

		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					},
					appendTo: '.quick-search-container'
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);

					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
	})
</script>
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			<div class="quick-search-container">
					<input type="text" id="quicksearch" size="16" />
			</div>
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<div class="content60">
	<div id="tabs1">
		<ul>
			<li><a href="#tabs1-1">Institutions</a></li>
		</ul>
		<div id="tabs1-1">
			<fieldset>
			  <legend>Institutions</legend>
			  <?php if( $_SESSION['MM_UserGroup'] == 'Administrator' ) { ?>
				 <div class="edit-link"><a href="adminmain.php?comp=add">Add Institution</a></div>
			 <?php } ?>
			  <p><input type="text" id="filterclist" />
			  <input type="hidden" name="a_type" id="a_type" value="<?php echo  $_SESSION['MM_UserGroup'];?>"  />
			  </p>
			  <?php if( $_SESSION['MM_UserGroup'] == 'Administrator' ) { ?>
			  <p><a href='companyratings.php?export=colorcodes'> Export Displayed List(CSV)</a></p>
			  <p><a href="adminmain.php?export_company=1">Export Full Detailed List (CSV)</a></p>
			  <?php } ?>

			  <br />
			  <table class="table-striped table dataTable" width="100%" id="clist">
				<thead>

					<?php if( $_SESSION['MM_UserGroup'] == 'Administrator' ) { ?>
						<tr class="search_elements">
							<td>&nbsp;</td>
							<td>Filter Geography<br />
							<select name="company_location_filter" class="clicksel col_filter" id="company_location_filter">
									<option value=""></option>
									<?php for( $y=0; $y < $companyLocationSize; $y++ ) { ?>
										<option value="<?php  echo $companyLocation[$y]['id_geography']; ?>"><?php  echo $companyLocation[$y]['name']; ?></option>
									<?php } ?>
								</select>
							</td>
							<td>Filter Status<br />
								<select name="company_status_filter" class="clicksel col_filter" id="company_status_filter">
									<option value=""></option>
									<?php for( $y=0; $y < $companyStatusSize; $y++ ) { ?>
										<option value="<?php  echo $companyStatus[$y]['id_companystatus']; ?>"><?php  echo $companyStatus[$y]['company_status']; ?></option>
									<?php } ?>
								</select>
							</td>
							<td>Filter Tier<br />
								<select name="company_rating_filter" class="clicksel col_filter" id="company_rating_filter" >
									<option value=""></option>
									<?php for( $y=0; $y < $companyTiersSize; $y++ ) { ?>
										<option value="<?php  echo $companyTiers[$y]['id_companytier']; ?>"><?php  echo $companyTiers[$y]['companytier']; ?></option>
									<?php } ?>
								</select>
							</td>
							<td>Filter Type<br />
								<select name="company_type_filter" class="clicksel col_filter" id="company_type_filter">
									<option value=""></option>
									<?php for( $y=0; $y < $companyTypeSize; $y++ ) { ?>
										<option value="<?php  echo $companyType[$y]['id_companytype']; ?>"><?php  echo $companyType[$y]['companytype']; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>

					<?php } else { ?>
						<tr class="search_elements">
							<td>&nbsp;</td>
							<td>Filter Geography<br />
							<select name="company_location_filter" class="clicksel col_filter" id="company_location_filter">
									<option value=""></option>
									<?php for( $y=0; $y < $companyLocationSize; $y++ ) { ?>
										<option value="<?php  echo $companyLocation[$y]['name']; ?>"><?php  echo $companyLocation[$y]['name']; ?></option>
									<?php } ?>
								</select>
							</td>
							<td>Filter Status<br />
								<select name="company_status_filter" class='clicksel col_filter' id="company_status_filter">
									<option value=""></option>
									<?php for( $y=0; $y < $companyStatusSize; $y++ ) { ?>
										<option value="<?php  echo $companyStatus[$y]['company_status']; ?>"><?php  echo $companyStatus[$y]['company_status']; ?></option>
									<?php } ?>
								</select>
							</td>
							<td>Filter Tier<br />
								<select name="company_rating_filter" class='clicksel col_filter' id="company_rating_filter" >
									<option value=""></option>
									<?php for( $y=0; $y < $companyTiersSize; $y++ ) { ?>
										<option value="<?php  echo $companyTiers[$y]['companytier']; ?>"><?php  echo $companyTiers[$y]['companytier']; ?></option>
									<?php } ?>
								</select>
							</td>
							<td>Filter Type<br />
								<select name="company_type_filter" class='clicksel col_filter' id="company_type_filter">
									<option value=""></option>
									<?php for( $y=0; $y < $companyTypeSize; $y++ ) { ?>
										<option value="<?php  echo $companyType[$y]['companytype']; ?>"><?php  echo $companyType[$y]['companytype']; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
					<?php } ?>
					<tr>
						<th id="company_column"><div>Institution</div></th>
						<th><div>Geography</div></th>
						<th><div>Status</div></th>
						<th><div>Tier</div></th>
						<th><div>Type</div></th>
					</tr>
				</thead>
				<?php if( $_SESSION['MM_UserGroup'] == 'Administrator' ) { ?>
				<tbody id="avclientbody">
					<?php for( $x=0; $x < sizeof($companyInfo); $x++ ) { ?>
						<tr>
							<td><a href="companydetails-view.php?id=<?php echo $companyInfo[$x]['comp_id']; ?>"><?php echo  $companyInfo[$x]['Company']; ?></a> <span style="float:leftl; display:inline; font-size:0.8em; color:#999;" id="companycode_<?php echo $companyInfo[$x]['comp_id']; ?>"></span></td>
							<td>
							<select name="company_location" class="clicksel location_select" id="company_location_value_<?php echo $x; ?>" onchange="updateCompany('location',this.options[this.selectedIndex].value,<?php echo $companyInfo[$x]['comp_id']; ?>)">
									<option value="">&nbsp;</option>
								<?php for( $y=0; $y < $companyLocationSize; $y++ ) {
									$selectedValue = strcmp($companyLocation[$y]['id_geography'], $companyInfo[$x]['id_geography']) == 0 ? 'selected="selected"' : '';
								?>
									<option value="<?php  echo $companyLocation[$y]['id_geography']; ?>" <?php echo $selectedValue; ?>><?php  echo $companyLocation[$y]['name']; ?></option>
								<?php } ?>
							</select>

							</td>
							<td>
							<select name="company_status" class="clicksel status_select" id="company_status_value_<?php echo $x; ?>" onchange="updateCompany('status',this.options[this.selectedIndex].value,<?php echo $companyInfo[$x]['comp_id']; ?>)">
									<option value="">&nbsp;</option>
								<?php for( $y=0; $y < $companyStatusSize; $y++ ) {
									$selectedValue = strcmp($companyStatus[$y]['id_companystatus'], $companyInfo[$x]['id_companystatus']) == 0 ? 'selected="selected"' : '';
								?>
									<option value="<?php  echo $companyStatus[$y]['id_companystatus']; ?>" <?php echo $selectedValue; ?>><?php  echo $companyStatus[$y]['company_status']; ?></option>
								<?php } ?>
							</select>
							</td>
							<td>
							<select name="company_rating" class="clicksel rating_select" id="company_rating__value_<?php echo $x; ?>" onchange="updateCompany('tier',this.options[this.selectedIndex].value,<?php echo $companyInfo[$x]['comp_id']; ?>)">
								<option value="">&nbsp;</option>
								<?php for( $y=0; $y < $companyTiersSize; $y++ ) {
									$selectedValue = strcmp($companyTiers[$y]['id_companytier'], $companyInfo[$x]['id_companytier']) == 0 ? 'selected="selected"' : '';
								?>
								<option value="<?php  echo $companyTiers[$y]['id_companytier']; ?>" <?php echo $selectedValue; ?>><?php  echo $companyTiers[$y]['companytier']; ?></option>
								<?php } ?>
							</select>
							</td>
							<td>
							<select name="company_type" class="clicksel type_select" id="company_type_value_<?php echo $x; ?>" onchange="updateCompany('type',this.options[this.selectedIndex].value,<?php echo $companyInfo[$x]['comp_id']; ?>)">
								<option value="">&nbsp;</option>
								<?php for( $y=0; $y < $companyTypeSize; $y++ ) {
									$selectedValue = strcmp($companyType[$y]['id_companytype'], $companyInfo[$x]['id_companytype']) == 0 ? 'selected="selected"' : '';
								?>
								<option value="<?php  echo $companyType[$y]['id_companytype']; ?>" <?php echo $selectedValue; ?>><?php  echo $companyType[$y]['companytype']; ?></option>
								<?php } ?>
							</select>
							</td>


						</tr>
					<?php } ?>
				</tbody>
			<?php } else { ?>
				<tbody id="avclientbody">
					<?php for( $x=0; $x < sizeof($companyInfo); $x++ ) { ?>
						<tr>
							<td><a href="companydetails-view.php?id=<?php echo $companyInfo[$x]['comp_id']; ?>"><?php echo  $companyInfo[$x]['Company']; ?></a></td>
							<td><?php echo  $companyInfo[$x]['geography_name']; ?></td>
							<td><?php echo  $companyInfo[$x]['company_status']; ?></td>
							<td class="companytier"><?php echo  $companyInfo[$x]['companytier']; ?></td>
							<td class="type_text"><?php echo  $companyInfo[$x]['companytype']; ?></td>
						</tr>
					<?php } ?>
				</tbody>
			<?php } ?>
				</table>
			</fieldset>
		</div>
	</div>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
require_once('includes/endbody.php');

//mysql_free_result($selRating);
?>
