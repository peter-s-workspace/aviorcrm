<?php 
require_once('Connections/CRMconnection.php'); 
require_once('includes/event_log/eventlog.class.php'); 
require_once('includes/classes/user.class.php'); 
require_once('includes/classes/platform.class.php');

$platform   = new Platform();
$currentEnv = $platform->getEnvironment();

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrator";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

$eventLogObj = new Eventlog();
$userObj = new User();
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "pwreset")) {
//Update password and send mailer.

$updateSQL = sprintf("UPDATE tbluser SET user_pword=%s WHERE user_id=%s",
                       GetSQLValueString(sha1($_POST['pwnew']), "text"),
                       GetSQLValueString($_POST['user_id'], "int"));
					   
 mysql_select_db($database_CRMconnection, $CRMconnection);
 $Result1 = mysql_query($updateSQL, $CRMconnection) or die(mysql_error());

	$sql = sprintf("SELECT CONCAT(tbluser.user_fname,' ',tbluser.user_lname) AS 'user_fullname'  FROM tbluser WHERE user_id=%s",
				   GetSQLValueString($_POST['user_id'], "int"));
	mysql_select_db($database_CRMconnection, $CRMconnection);
	$userRS = mysql_query($sql, $CRMconnection) or die(mysql_error());		
    $user_fullname  = mysql_result($userRS,0,'user_fullname');

	if( $Result1 && $user_fullname != '' ) {
	
	//Send Password
	$password = $_POST['pwnew'];
	$emailAddress = $_POST['email'];
	
	$passwordSent = $userObj->emailUserPassword($emailAddress, $password);
	echo  $passwordSent;

	 $eventLogObj->save(array(
		'event' => 'UPDATE_USER_PWD',
		'user' => $_SESSION['MM_Username'],
		'affected_user' => $user_fullname,
		'affected_type' => 'user'));
	}
	

}
$phoneNumberValidationFormat = "/^0[0-9]{9}$/i";
$phoneNumberValidationError = "10 numbers starting with 0 expected in Phone Number, please remove spaces and any other characters \n e.g 0xxxxxxxxx\n";
$emailAddressValidationError = "Please enter a valid email addresss";

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  //Get user profile before updating.

  $user_profile_before_update = isset($_POST['user_id']) ? $userObj->setType('user')->GetProfile($_POST['user_id']): '';
  $user_phone_entered = isset($_POST['user_phone']) && trim($_POST['user_phone']) != '' ? true : false;
  $continue_update = true;
  $phone_validation_error_found = false;
  if ($user_phone_entered) {
	  $phone_validation_error_found	= preg_match($phoneNumberValidationFormat, $_POST['user_phone'] ) == true ? false : true;
  } else {
  		// if the phone number is empty after an update display the error to the user
  		// this is passed back into the javascript variable and alerted
  		echo "Phone Number required";
  		exit;
  }
  

	/* Phone number validation failed. */  
   if ($phone_validation_error_found ) {
		echo $phoneNumberValidationError;
		exit();
   }
   
   if( isset($_POST['user_email']) && $_POST['user_email'] != '' && filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL) == false ) {
	   	echo $emailAddressValidationError;
		exit();
   }
  
	
  $updateSQL = sprintf("UPDATE tbluser SET user_name=%s, user_fname=%s, user_lname=%s, user_email=%s, user_level=%s, user_phone=%s, user_fname=%s, user_extension=%s, user_bday=%s, user_phone_ip=%s,user_status=%s WHERE user_id=%s",
					   GetSQLValueString(trim($_POST['user_name']), "text"),
					   GetSQLValueString(trim($_POST['user_fname']), "text"),
					   GetSQLValueString(trim($_POST['user_lname']), "text"),
					   GetSQLValueString(trim($_POST['user_email']), "text"),
					   GetSQLValueString(trim($_POST['user_level']), "text"),
					   GetSQLValueString(trim($_POST['user_phone']), "text"),
					   GetSQLValueString(trim($_POST['user_fname']), "text"),
					   GetSQLValueString(trim($_POST['user_extension']), "text"),
					   GetSQLValueString(trim($_POST['user_bday']), "date"),
					   GetSQLValueString(trim($_POST['user_phone_ip']), "text"),
					   GetSQLValueString(trim($_POST['user_status']), "int"),
					   GetSQLValueString(trim($_POST['user_id']), "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  //$Result1 = mysql_query($updateSQL, $CRMconnection) or die(mysql_error());
   $Result1 = mysql_query($updateSQL, $CRMconnection);//
	$mysql_error_number = mysql_errno();
	$actual_error = mysql_error();
	if($mysql_error_number > 0 ) {
		switch($mysql_error_number){
			case 1062:
					sscanf($actual_error, "Duplicate entry %s for key %s", $erro_entry,$error_column);
					switch($error_column){
						case "'user_name'":
								echo "Username already exists. Please enter new Username";
								break;
						case "'user_email'":
								echo "Email already exists. Please enter new Email";
								break;
					}
					break;
			case 1048:
					sscanf($actual_error, "Column %s cannot be null", $erro_column);
					switch($erro_column){
						case "'user_name'":
								echo "Username required";
								break;
						case "'user_level'":
								echo "User Level required";
								break;
						case "'user_phone_ip'":
								echo "Phone IP required";		
								break;
						case "'user_phone'":
								echo "Phone Number is required";
								break;
						break;
					}
					break;

		}
	} else{
			if ($user_profile_before_update != '') {
				$profile_before_update_array = (array) json_decode($user_profile_before_update);
				if( sizeof($profile_before_update_array) > 0 ){
					$oldUserName = trim($profile_before_update_array['User Name']);
					$newUserName  = isset($_POST['user_name']) && trim($_POST['user_name']) != '' ? trim($_POST['user_name']) : '';
					if( $newUserName  != '') {
						$userObj->updateUserNameReferences($oldUserName, $newUserName);
					}
				}
			}
		echo "Success";
	}

	  if( $Result1 ) {
		//Update event log 
		  $eventLogObj->save(array(
			'event' => 'UPDATE_USER',
			'user' => $_SESSION['MM_Username'],
			'affected_user' => $_POST['user_fname'].' '.$_POST['user_lname'],
			'affected_type' => 'user',
			'before' => $user_profile_before_update,
			'after'=> $userObj->setType('user')->GetReadableFields($_POST)
			));	
		}
		
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
	
	$user_phone_entered = isset($_POST['user_phone']) && trim($_POST['user_phone']) != '' ? true : false;
	$continue_update = true;
	$phone_validation_error_found = false;
	if ($user_phone_entered) {
	  $phone_validation_error_found	= preg_match($phoneNumberValidationFormat, $_POST['user_phone'] ) == true ? false : true;
	}
	if ($phone_validation_error_found ) {
		echo $phoneNumberValidationError;
		exit();
	}
	
	if( isset($_POST['user_email']) && $_POST['user_email'] != '' && filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL) == false ) {
	   	echo $emailAddressValidationError;
		exit();
   }
 
	$insertSQL = sprintf("INSERT INTO tbluser (user_name, user_pword, user_fname, user_lname, user_email, user_level, user_phone, user_bday, user_phone_ip,user_status,user_extension) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s,%s)",
					   GetSQLValueString(trim($_POST['user_name']), "text"),
					   GetSQLValueString(sha1($_POST['user_pword']), "text"),
					   GetSQLValueString(trim($_POST['user_fname']), "text"),
					   GetSQLValueString(trim($_POST['user_lname']), "text"),
					   GetSQLValueString(trim($_POST['user_email']), "text"),
					   GetSQLValueString(trim($_POST['user_level']), "text"),
					   GetSQLValueString(trim($_POST['user_phone']), "text"),
					   GetSQLValueString(trim($_POST['user_bday']), "date"),
					   GetSQLValueString(trim($_POST['user_phone_ip']), "text"),
					   GetSQLValueString(trim($_POST['user_status']), "int"),
					   GetSQLValueString(trim($_POST['user_extension']), "text"));

  mysql_select_db($database_CRMconnection, $CRMconnection);

	$Result1 = mysql_query($insertSQL, $CRMconnection);//
	$mysql_error_number = mysql_errno();
	$actual_error = mysql_error();
	if($mysql_error_number > 0 ) {
		switch($mysql_error_number){
			case 1062:
					sscanf($actual_error, "Duplicate entry %s for key %s", $erro_entry,$error_column);
					switch($error_column){
						case "'user_name'":
								echo "Username already exists. Please enter new Username";
								break;
						case "'user_email'":
								echo "Email already exists. Please enter new Email";
								break;
					}
					break;
			case 1048:
					sscanf($actual_error, "Column %s cannot be null", $erro_column);
					switch($erro_column){
						case "'user_name'":
								echo "Username required";
								break;
						case "'user_level'":
								echo "User Level required";
								break;
						case "'user_phone_ip'":
								echo "Phone IP required";		
								break;
						case "'user_phone'":
								echo "Phone Number required";
						break;
					}
					break;

		}
	} else{
		echo "Success";
	}

  if( $Result1 ) {
	  //add new user
	   $eventLogObj->save(array(
		'event' => 'ADD_USER',
		'user' => $_SESSION['MM_Username'],
		'affected_user' => $_POST['user_fname'].' '.$_POST['user_lname'],
		'affected_type' => 'user',
		'after'=> $userObj->setType('user')->GetReadableFields($_POST)
		));
	}
}
	
	



if ((isset($_POST["MM_remove"])) && ($_POST["MM_remove"] == "form1") && isset($_POST['user']) && $_POST['user'] > 0 ) {

  $deleteSQL = sprintf("DELETE FROM tbluser WHERE user_id=%s",
					   GetSQLValueString($_POST['user'], "int"));
	mysql_select_db($database_CRMconnection, $CRMconnection);
	$Result1 = mysql_query($deleteSQL, $CRMconnection) or die(mysql_error());
 
}


if ((isset($_POST['cnt_Id'])) && ($_POST['cnt_Id'] != "") && (isset($_POST['delcnt']))) {
	
	$sql = sprintf("SELECT CONCAT(contacts.first_name,' ',contacts.surname) AS 'user_fullname', tblcompanylist.Company AS 'cnt_company' FROM contacts WHERE contacts.company_id = tblcompanylist.comp_id AND contacts.id=%s", GetSQLValueString($_POST['cnt_Id'], "int"));
	mysql_select_db($database_CRMconnection, $CRMconnection);
	$userRS = mysql_query($sql, $CRMconnection) or die(mysql_error());		
	$user_fullname  = mysql_result($userRS,0,'user_fullname');
	$cnt_company  = mysql_result($userRS,0,'cnt_company');	
	
	$user_profile_before_delete = $userRS && isset($_POST['cnt_Id'] ) ? $userObj->setType('client')->GetProfile($_POST['cnt_Id']) : '';
	
  
	$deleteSQL = sprintf("DELETE FROM contacts WHERE id=%s",
					   GetSQLValueString($_POST['cnt_Id'], "int"));
	mysql_select_db($database_CRMconnection, $CRMconnection);
	$Result1 = mysql_query($deleteSQL, $CRMconnection) or die(mysql_error());

	if( $Result1 && $user_fullname != '' ) {
		//Log clinet details before deleting.
		 $eventLogObj->save(array(
			'event' => 'DELETE_CLIENT',
			'user' => $_SESSION['MM_Username'],
			'affected_user' => $user_fullname,
			'affected_type' => 'client',
			'company' => $cnt_company,
			'before' => $user_profile_before_delete
			));
	   }	  
  

  //also remove all entries in mail subscriptions
  $deleteSQL2 = sprintf("DELETE FROM contacts_interests WHERE contact_id=%s",
                       GetSQLValueString($_POST['cnt_Id'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result2 = mysql_query($deleteSQL2, $CRMconnection) or die(mysql_error());
  
  if( $Result2 && $user_fullname != '' ) {
  //Remove client from mailing list.
  $eventLogObj->save(array(
		'event' => 'REMOVE_CLIENT_MAILINGLIST',
		'user' => $_SESSION['MM_Username'],
		'affected_user' => $user_fullname,
		'company' => $cnt_company,
		'affected_type' => 'client'));
 } 
  
  
}

?>
