<?php 
require_once('Connections/CRMconnection.php'); 
require_once('includes/event_log/eventlog.class.php');
require_once('includes/classes/user.class.php');
?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrator";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO tbluser (user_name, user_pword, user_fname, user_lname, user_email, user_level, user_phone, user_bday, user_phone_ip,user_extension) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['user_name'], "text"),
                       GetSQLValueString($_POST['user_pword'], "text"),
                       GetSQLValueString($_POST['user_fname'], "text"),
                       GetSQLValueString($_POST['user_lname'], "text"),
                       GetSQLValueString($_POST['user_email'], "text"),
                       GetSQLValueString($_POST['user_level'], "text"),
                       GetSQLValueString($_POST['user_phone'], "text"),
                       GetSQLValueString($_POST['user_bday'], "date"),
                       GetSQLValueString($_POST['user_phone_ip'], "text"),
                       GetSQLValueString($_POST['user_extension'], "text"));

  mysql_select_db($database_CRMconnection, $CRMconnection);

  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());
  
  $eventLogObj = new Eventlog();
  $userObj = new User();
  if( $Result1 ) {
  
	  //Add new user
	  $eventLogObj->save(array(
		'event' => 'ADD_USER',
		'user' => $_SESSION['MM_Username'],
		'affected_user' => $_POST['user_fname'].' '.$_POST['user_lname'],
		'affected_type' => 'user',
		'after' => $userObj->setType('user')->GetReadableFields($_POST)
		));
	}
}

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_users = "SELECT * FROM tbluser ORDER BY user_level ASC, user_status DESC, user_fname ASC";
$users = mysql_query($query_users, $CRMconnection) or die(mysql_error());
$row_users = mysql_fetch_assoc($users);
$totalRows_users = mysql_num_rows($users);

?>
<script >
$(function() {

 $('#edituserform').dialog({
	 autoOpen:false,
	 position:'top',
	 width:'auto',
	 modal:true,
	 buttons : {
		 /*"Delete User" : function(){
		 	$('<div title="Confirm Delete"></div>').appendTo('body')
  			.html('<p> Clicking yes will Delete this user</p>')
  			.dialog({
      				modal: true, 
					position:'top',
					title: 'Confirm Delete', 
					zIndex: 10000, 
					autoOpen: true,
					width: 'auto', 
					resizable: false,
					buttons: {
						Yes: function () {
							//generate password
							
							var userid = $("#edituserf input[name=user_id]").val();
							if( userid > 0 ) {
								$.ajax({
									type: "POST",
									url: "ajaxupduser.php",
									data: { MM_remove: 'form1' , user: userid },
									success:function(message){
										location.reload();
									}
								});	
							}
							//$(this).dialog("close"); 
						},
          				No: function () {
							$(this).dialog("close");
						}
      				},
      			close: function (event, ui) {
          		 	$(this).remove();
			      }
			});
		 },*/
		 "Reset Password" : function(){
			//create additional dialog to confirm
			$('<div title="Confirm password reset"></div>').appendTo('body')
  			.html('<p> Clicking Yes will send an email to the user with the new password</p>')
  			.dialog({
      				modal: true, 
					position:'top',
					title: 'Confirm password reset', 
					zIndex: 10000, 
					autoOpen: true,
					width: 'auto', 
					resizable: false,
					buttons: {
						Yes: function () {
							//generate password
							var password = Math.random().toString(36).slice(-8);
							var userid = $("#edituserf input[name=user_id]").val();
							var emailv = $("#edituserf input[name=user_email]").val();
							$.ajax({
  								type: "POST",
  								url: "ajaxupduser.php",
  								data: { MM_update: 'pwreset' , user_id : userid , pwnew : password, email: emailv },
  								success:function(result){
									var that = this;
									if( result == "sent" ) {
										alert('Your password has been sent. Please check your email.');
									} else {
										alert('Could not send you your password at the moment, please try again.');
									}
								}
							});	
							$(this).dialog("close"); 
						},
          				No: function () {
							$(this).dialog("close");
						}
      				},
      			close: function (event, ui) {
          		 	$(this).remove();
			      }
			});
			
		 },
		 "Save and Close":function() {
		 	var that = this;
			 submitedits(function(result){
			 	if( result == "Success" ) {
					$(that).dialog("close");
					alert('Successfully updated user');
					window.location.href = 'adminmain.php?u=1'
				} else {
					alert(result);
					
				}
				});
			 //$(this).dialog("close");
		 }
	 }
 })

$('#duplicate').dialog({
  autoOpen:false,
  modal:true,
  position:'top',
  width:'400',
  buttons : {
     "Close":function() {
       $(this).dialog("close");
     }
   }
});

$('#adduserform').dialog({
	 autoOpen:false,
	 modal:true,
	 position:'top',
	 width:'auto',
	 buttons : {
		 "Close":function() {
			 $(this).dialog("close");
		 },
		 "Save and Add": function(){
		 	var that = this;
			 addnewuser(function(result){
			 	if( result == "Success" ) {
					$(that).dialog("close");
					alert('Successfully added user');
					window.location.href = 'adminmain.php?u=1'
				} else {
					alert(result);
				}
			 });

			 //
		 }
		 
		 
	 }
 })

		
})

function updeditform(userid, username, fname, lname,email, userlevel, phone,extension, bday, phoneip,userstatus){
	$("#edituserf input[name=user_id]").val(userid);
	$("#edituserf input[name=user_name]").val(username);
	$("#edituserf input[name=user_fname]").val(fname);
	$("#edituserf input[name=user_lname]").val(lname);
	$("#edituserf input[name=user_email]").val(email);
	var editline = "#edituserf select[name=user_level] option[value="+userlevel+"]"
	$(editline).attr('selected', 'selected');
	$("#edituserf input[name=user_phone]").val(phone);
	$("#edituserf input[name=user_bday]").val(bday);
	$("#edituserf input[name=user_phone_ip]").val(phoneip);
  $("#edituserf input[name=user_extension]").val(extension);
	$('#edituserf input[name="user_status"][value="' + userstatus + '"]').prop('checked', true);
	$('#edituserform').dialog('open');
}

function submitedits(callback) {
	var cdata = $('#edituserf').serialize();
	//window.alert(data);
	//send data via ajax 
	$.ajax({
  		type: "POST",
  		url: "ajaxupduser.php",
  		data: cdata,
  		success:function(message){
			callback(message);
			//window.alert('Successfully updated')
		}
		});
}

function addnewuser(callback) {
	var cdata = $('#adduserf').serialize();
	var result = '';
	$.ajax({
  		type: "POST",
  		url: "ajaxupduser.php",
  		data: cdata,
  		success:function(p){
			callback(p);
			//window.alert('Successfully added user')
		}
		});
	return result;
}

</script>
<style>

.mainmenu {
	margin-left:20px;
	padding:10px;
	border:#CCC 1px solid;
	width:250px;
	}
.mainmenu li {
	list-style-type:none;
	}

.mainmenu li a{
	background-color:#036;
	color:#FFF;
	text-decoration:none;
	display:block;
	padding: 4px;
	border:#FFF 1px solid;
	
}
.mainmenu li a:hover{
	background-color:#FFF;
	color:#036;
}

#userlist .inactive a{
	background-color:#999;
}


</style>


<h4><strong>CRM User administration</strong></h4>
<ul class="mainmenu">
<li><a href="javascript://" onClick="$('#adduserform').dialog('open');">Add user</a></li>
<li><a href='javascript://' onClick="$('#userlistbox').show('slide')">Edit User</a></li>
</ul>
<div id="userlistbox" class="mainmenu" style="margin-left:40px;"><a style=" text-align:right; float:right; clear:both;" href="#" onClick="$('#userlistbox').hide('slide')"> hide</a>
    <ul id="userlist">
   <?php
	$prev = "";
	do { 
      if ( $row_users['user_level'] != $prev){
	  echo "<li><b>".$row_users['user_level']."</b></li>";
	  $prev= $row_users['user_level']; 
	  }
     ?> <li <?php if ($row_users['user_status'] == 0) { echo 'class="inactive"'; } ?>><a onClick="updeditform('<?php echo $row_users['user_id']; ?>','<?php echo $row_users['user_name']; ?>','<?php echo $row_users['user_fname']; ?>','<?php echo $row_users['user_lname']; ?>','<?php echo $row_users['user_email']; ?>','<?php echo $row_users['user_level']; ?>','<?php echo $row_users['user_phone']; ?>','<?php echo $row_users['user_extension'];?>','<?php echo $row_users['user_bday']; ?>','<?php echo $row_users['user_phone_ip']; ?>','<?php echo $row_users['user_status']; ?>')">
        <?php echo $row_users['user_fname'];?> <?php echo $row_users['user_lname'];?> </a>
      </li>
      <?php } while ($row_users = mysql_fetch_assoc($users)); ?>
  </ul>
</div>
<script> $('#userlistbox').hide();</script>
<div id="edituserform">
<form action="<?php echo $editFormAction; ?>" method="post" name="edituserf" id="edituserf">
  <table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Username:</td>
      <td><input type="text" name="user_name" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">First name</td>
      <td><input type="text" name="user_fname" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Surname:</td>
      <td><input type="text" name="user_lname" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">e-Mail:</td>
      <td><input type="text" name="user_email" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User level:</td>
      <td><select name="user_level">
        <option value="General" >General User</option>
        <option value="Administrator">Administrator</option> 
        <option value="Guest" >Guest</option>
        <option value="NoteAdmin">Note Admin</option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Phone #:</td>
      <td><input type="text" name="user_phone" value="" size="32" /></td>
    </tr>
     <tr valign="baseline">
      <td nowrap="nowrap" align="right">Extension:</td>
      <td><input type="text" name="user_extension" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Birthday:</td>
      <td><input type="text" name="user_bday" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Phone IP</td>
      <td><input type="text" name="user_phone_ip" value="" size="32" /></td>
    </tr>
	<tr valign="baseline">
      <td nowrap="nowrap" align="right">Status</td>
      <td>
		  <label><input type="radio" name="user_status" value="1"> Active</label>
		 <label><input type="radio" name="user_status" value="0"> Inactive</label>
	  </td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="user_id" value="" />
</form>
</div>


<div id="adduserform" title="Create new user">
<form id="adduserf" method="post" name="form1">
<table>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Username:</td>
      <td><input type="text" name="user_name" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Password:</td>
      <td><input type="password" name="user_pword" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">First name:</td>
      <td><input type="text" name="user_fname" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Last name:</td>
      <td><input type="text" name="user_lname" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email:</td>
      <td><input type="text" name="user_email" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">User_level:</td>
      <td><select name="user_level">
        <option value="General" <?php if (!(strcmp("General", ""))) {echo "SELECTED";} ?>>General User</option>
        <option value="Administrator" <?php if (!(strcmp("Administrator", ""))) {echo "SELECTED";} ?>>Administrator</option> <option value="Guest" <?php if (!(strcmp("Guest", ""))) {echo "SELECTED";} ?>>Guest</option>
        <option value="NoteAdmin" <?php if (!(strcmp("NoteAdmin", ""))) {echo "SELECTED";} ?>>Note Admin</option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Phone #:</td>
      <td><input type="text" name="user_phone" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Extension:</td>
      <td><input type="text" name="user_extension" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">B day:</td>
      <td><input type="text" name="user_bday" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Phone IP:</td>
      <td><input type="text" name="user_phone_ip" value="" size="32" /></td>
    </tr>
	<tr valign="baseline">
      <td nowrap="nowrap" align="right">Status</td>
      <td>
		 <label><input type="radio" name="user_status" value="1" checked> Active</label>
		 <label><input type="radio" name="user_status" value="0"> Inactive</label>
	  <!-- <select name="user_status">
        <option value="1" >Active</option>
        <option value="0" >Inactive</option>
      </select>
	  -->
	  </td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />
</form>
</div>
<?php
mysql_free_result($users);
?>
