<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}


 
 //open directories and build file display + operations
function displaydir($dir){
 global $countr;
// Open a known directory, and proceed to read its contents
	$fileData = array();
	if (is_dir($dir)) {
    	if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
				if (strlen($file)>3){
					$fileData[] = $dir."/".$file;
				}
			}
		}
		closedir($dh);
		if(sizeof($fileData) > 0 ) {
			usort($fileData, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
		} 

        	//there are files - create a table
            $outvalue =''   ;
            $isadmin = isset($_SESSION['MM_Username']) && (isAuthorized("x","Administrator", $_SESSION['MM_Username'], $_SESSION['MM_UserGroup']));
			//while (($file = readdir($dh)) !== false) {
			foreach($fileData as $file ) {
				$file = ltrim(str_replace($dir,'',$file),'/');	
            	if (strlen($file)>3){

                if($isadmin){
          					$outvalue = ' <li id="pu'.$countr.'"><a href="'.$dir.'/'.$file.'" style="padding:3px;" > '.$file.' </a> | <form style="display: inline" id="pu'.$countr.'f" ><a href="#" style="clicklink" onclick="';
          					$outvalue.= "$.post(";
          					$outvalue.= "'userfilesremovefile.php',$('#pu".$countr."f').serialize(),function(data) {
                    $('#pu".$countr."f').html(data)},'json'); $('#pu".$countr."').remove()";
          					$outvalue.='" style="padding:3px;">Delete</a><input type="hidden" name="filedel" value="'.$dir.'/'.$file.'" ></form> </li>';
      					}
                else{
                  $outvalue = ' <li id="pu'.$countr.'"><a href="'.$dir.'/'.$file.'" style="padding:3px;" > '.$file.' </a></li>';
                } 
                $countr +=1;
      					echo $outvalue;
				      }
            }

	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>
</head>

<body>
<?php displaydir('uploads/public') ?>
</body>
</html>



