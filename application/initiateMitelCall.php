<?php
############### THIS WILL HANDLE THE MITEL CALL INITIATION #############
require_once('includes/classes/mitel.class.php');
require_once ('Connections/CRMconnection.php');

if (!isset($_SESSION)) {
	session_start();
}

$mitel_class 			= new Mitel($database_CRMconnection, $CRMconnection);
$check_valid_status 	= null;

if (isset($_POST) && !empty($_POST)) {
	################# VALIDATE THEM AS STRING AS THE MAIN JAVA FUNCTION EXPECTS THE PARAMETERS TO BE OF TYPE STRING ##################
	$mitel_extension 		= filter_var($_POST['user_extension'], FILTER_SANITIZE_STRING);
	$phone_number 			= filter_var($_POST['phone_number'], FILTER_SANITIZE_STRING);
	
	$check_valid_status 	= isAuthorized("","", $_SESSION['MM_Username'], $_SESSION['MM_UserGroup']);

	########### CHECK IF A USER HAS BEEN AUTHORISED BEFORE MAKING A CALL #################
	if ($check_valid_status) {
		$mitel_class->initiate_call($mitel_extension,$phone_number);
	} else {
		header("Location: login.php");
		exit;
	}
}

function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
		  // For security, start by assuming the visitor is NOT authorized.
		  $isValid = False;
		  if (!empty($UserName)) {
			    $arrUsers = Explode(",", $strUsers);
			    $arrGroups = Explode(",", $strGroups);
			    if (in_array($UserName, $arrUsers)) {
			      $isValid = true;
			    }
			   
			    if (in_array($UserGroup, $arrGroups)) {
			      $isValid = true;
			    }
			    if (($strUsers == "") && true) {
			      $isValid = true;
			    }
		  }
		  return $isValid;
	}
?>