<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$isadmin = isset($_SESSION['MM_Username']) && (isAuthorized("x","Administrator", $_SESSION['MM_Username'], $_SESSION['MM_UserGroup']));

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_mailists = "SELECT * FROM interests ORDER BY description ASC";
$mailists = mysql_query($query_mailists, $CRMconnection) or die(mysql_error());
$row_mailists = mysql_fetch_assoc($mailists);
$totalRows_mailists = mysql_num_rows($mailists);
//print_r( $row_mailists); exit();
?>
<?php require_once('includes/sitevars.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
<link rel="manifest" href="favicons/manifest.json">
<link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<link type="text/css" href="css/kdes1/jquery-ui-1.9.2.custom.css" rel="stylesheet" />		
<script src="js/jquery-1.10.2.js"></script>
<script src="js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript">
	//function to do loader more ubiquities
	function showloader(targetid){
		$(targetid).empty().html('<img src="images/ajax-loader.gif" />');
	}	
	$(function(){
		// Tabs
		$('#tabs1').tabs();
		$('#btnloadmlist').click( function(){

            var selected = false;

            $('#formmlist *').not("input[name ='tier[]']").filter(':input').each(function(){
                if($(this).attr('type') == 'checkbox'){
                    if($(this).is(':checked')){
                        selected = true;
                    }
                }
            });

            if(selected){
            	//check if one checkbox has been selected for the tier
            	var total_checked = $("input[name= 'tier[]']:checked").length;
            	if (total_checked != 0) {
            		var tmp = $('#formmlist').serialize();
	                showloader('#mail25');
	                //$('#fullmailer').load('mailmanagerloadall.php?'+tmp);
	                $('#mail25').load('mailmanagerload25.php?'+tmp);	
            	} else {
            		alert("Please Select Protection Status");
            		return false;
            	}
                
            } else {
                alert('Please select at least one Mailing List');
            }


								
		});
		
	});
</script>
<style>
#spec li {
	width:230px;
	margin:15px 0 0 0; 
	padding:0 10px 0 0; 
	line-height:15px; 
	float:left;
}
#spec {
	width:inherit;
	list-style-type:none;
}

</style>
<!-- InstanceEndEditable -->
<script language="javascript">
	$(function(){
		
		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					},
					appendTo: '.quick-search-container'
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);
				
					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
	})
</script>	
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			<div class="quick-search-container">
					<input type="text" id="quicksearch" size="16" />
			</div>
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<div class="content80">
<div id="tabs1">
      <ul>
        <li><a href="#tabs1-1">Mailing Lists</a></li>
        <?php
        if ($isadmin) {        
        ?>
         <li><a href="#tabs1-2">Mail List Management</a></li>
        <!-- <li><a href="#tabs1-3">Quick mail</a></li> -->
        <?php
        } 
        ?>
      </ul>
      <div id="tabs1-1">
      <fieldset id="mail25">
       <legend>Mailing Lists</legend>
       <form id="formmlist">
	      <p class="ui-widget-header" style="padding:0.2em;">Select lists to load:</p>
	      <ul id="spec">
<?php
		do {  
?>
        	<li><label><input type="checkbox" name="cnt_Interest[]" value="<?php echo $row_mailists['id']?>"><?php echo $row_mailists['description']?></label></li>
<?php
		} while ($row_mailists = mysql_fetch_assoc($mailists));
		  $rows = mysql_num_rows($mailists);
		  if($rows > 0) {
			  mysql_data_seek($mailists, 0);
			  $row_mailists = mysql_fetch_assoc($mailists);
		  }
?>
</ul>
		<fieldset style="display:inline-block; clear:right; float:right"> Protection Status: &nbsp;<br>
		 	
		  <input type="checkbox"  class="status" name="tier[]" value="1"> <label>Watermark</label><br>
		  <input type="checkbox" class="status"  name="tier[]" value="2"> <label>DRM</label><br>
		  <input type="checkbox" class="status"  name="tier[]" value="3"> <label>Web Viewer</label><br>	
		  
		   <!--		
			  <select name="stier">
					<option value="1">Normal</option><option value="2">DRM</option><option value="3">Web Viewer</option>
			  </select> to
			   <select name="etier">
				<option value="1">Normal</option><option value="2">DRM</option><option value="3">Web Viewer</option>
			  </select> 
		  !-->
		  <input type ="button" id="selectAll" value ="Select All">
		  <input type="button" id="btnloadmlist" value="Load" />
		</fieldset>
	</form>
    </fieldset>
	<!-- <fieldset id="fullmailer"> </fieldset>-->
      </div>
      <?php
        if ($isadmin) {        
        ?>
      <div id="tabs1-2">
      <fieldset>
      <legend>Add New mail list</legend>
      <?php
	  //mysql_data_seek($mailists, 0);?>
      <p><input type="text" name="newlist" id="newlist" />&nbsp;<input type="button" value="Add" id="addMailList"></p>
	  <p id="mailingListResponse"></p>
     Select other lists to merge into new list (if any):
     <fieldset id="otherListItems">
    <?php do { ?>
     <label><input type='checkbox' name='checkboxvar' class="otherLists" value="<?php echo $row_mailists['id']; ?>"><?php echo $row_mailists['description']; ?></label><br>
 <?php  }   while ($row_mailists = mysql_fetch_assoc($mailists)); ?>
     </fieldset>
      
	  <script type="text/javascript">
	  	$(document).ready(function(){
		
			$('.otherLists').click(function(){
				
				if( $(this).val()  == 'All' && $(this).attr('checked') ) {
					  $('.otherLists:checkbox').prop('checked', false);
					   $(this).prop('checked', true);
				} else if($(this).val()  != 'All' && $(this).attr('checked') ){
					  $('.otherLists:checkbox[value="All"]').prop('checked', false);
				}
			});
			$('#addMailList').click(function(){
			
				var newList = $('#newlist').val();
				var otherLists = [];
				$('.otherLists:checked').each(function(){
					otherLists.push( $(this).val() );
				});
				var merge_list = otherLists.join(",")
				if( newList != ''  ) {
					$('#addMailList').attr('disabled', true);
					$.ajax({
						type: "POST",
						url: "ajaxSaveMailingList.php",
						data: { listItem: newList, mergeLists: merge_list }
					}).done(function( msg ) {
						$('#addMailList').attr('disabled', false);
						$('#mailingListResponse').html(msg);
						if( msg == 'New List added.') {
							$('#otherListItems').append("<label><input type='checkbox' name='checkboxvar' class='otherLists' value='"+newList+"'>"+newList+" <span style='color:red;'>Added</span></label><br>");
						}
					});
				} else {
					$('#mailingListResponse').html('Please enter list to add');
				}
			});
			
			$("#selectAll").on("click" , function(e){
				e.preventDefault();
				$(".status").prop("checked", true);
				return false;
			});


			$('#remove_mailing_list').click(function(){
			   
					var ids = [];
                    var names = [];
					$('.delmailinglist:checked').each(function(){
						ids.push( $(this).val() );
                        names.push( $(this).val());
					});
					if( ids.length > 0 ) {
						var deleteconfirmed = confirm("Are you sure you want to delete the following list(s): "+ names.join(', '));
						if( deleteconfirmed ) {
							
							$('#remove_mailing_list').attr('disabled', true);
							$.ajax({
								type: "POST",
								url: "ajaxRemoveMailingList.php",
								data: { listItem: ids.join(',') }
							}).done(function( msg ) {
								$('#addMailList').attr('disabled', false);
								if( msg == 'Success') {
									$('.delmailinglist:checked').each(function(){
										$(this).parent().html('');
									});			
									$('#removeMailingListResponse').html('Successfully removed '+  names.join(','));
								} else {
									$('#removeMailingListResponse').html(msg);
								}
								$('#remove_mailing_list').attr('disabled', false);
							});
						}
					} else {
						$('#removeMailingListResponse').html('Please select atleast 1 item');
					}
				return false;
			});
		});
	  </script>
      
      </fieldset>

      <fieldset>
      <legend>Delete mail list</legend>
	  <div class="delete_maillist_container">
		  <?php
		  mysql_data_seek($mailists, 0);?>
		<?php do {
			if( $row_mailists['id'] != '' ) {
		 ?>
			 <label><input type='checkbox' name='delmailinglist[]' class="delmailinglist" value="<?php echo $row_mailists['id']; ?>"><?php echo $row_mailists['description']; ?><br></label>
		 <?php  
		 	}
		 }   while ($row_mailists = mysql_fetch_assoc($mailists)); ?>
		 
		<p id="removeMailingListResponse"></p>
		<p><input type="button" value="Delete" id="remove_mailing_list"></p>
      </fieldset>
      </div>
      <?php
        }        
        ?>
      <!-- <div id="tabs1-3">Nam dui erat, auctor a, dignissim quis, sollicitudin eu, felis. Pellentesque nisi urna, interdum eget, sagittis et, consequat vestibulum, lacus. Mauris porttitor ullamcorper augue.</div> -->
</div>
</div>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($mailists);
?>
