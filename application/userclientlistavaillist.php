<?php require_once('Connections/CRMconnection.php'); ?>
<?php

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>

<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}


if ((isset($_GET["MM_insert"])) && ($_GET["MM_insert"] == "frmaddnewlist")) {
  $vshared =0;
  $veditablebyothers =0;
  if (isset($_GET['shared'])){
	  $vshared =1;
  }
  if(isset($_GET['editablebyothers'])){
	  $veditablebyothers=1;
  }
  
  $insertSQL = sprintf("INSERT INTO cnt_listnames (listname, ownerusername, shared, editablebyothers) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_GET['listname'], "text"),
                       GetSQLValueString($_SESSION['MM_Username'], "text"),
                       GetSQLValueString($vshared, "int"),
                       GetSQLValueString($veditablebyothers, "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());

}
$opresult="";
if ((isset($_GET['tblFav_id'])) && ($_GET['tblFav_id'] != "") && (isset($_GET['MM_delete']))) {

  $deleteSQL = sprintf("DELETE FROM tblfavourites WHERE tblFav_id=%s",
                       GetSQLValueString($_GET['tblFav_id'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($deleteSQL, $CRMconnection) or die(mysql_error());
}


# Deletes the call list specified and removes the call list entry for all members of that call list
if ((isset($_GET['deletefullist'])) && $_GET['deletefullist'] !='' && ($_GET['selectulistd'] != 1)) {
  $deleteSQL = sprintf("DELETE FROM cnt_listnames WHERE listid=%s",
                       GetSQLValueString($_GET['selectulistd'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($deleteSQL, $CRMconnection) or die(mysql_error());
  
  $deleteSQL = sprintf("DELETE FROM tblfavourites WHERE listid=%s",
                       GetSQLValueString($_GET['selectulistd'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result2 = mysql_query($deleteSQL, $CRMconnection) or die(mysql_error());
$opresult="deleted";
}

# Updates the properties of the specified call list
if(isset($_GET['edit_call_list']) && $_GET['edit_call_list'] !='' && ($_GET['selectulistd'] != 1))
{
    
    $shared_status=0;
    $editable_status=0;

    if(isset($_GET['edit_shared'])){
      $shared_status=1;
    }
    if(isset($_GET['edit_editablebyothers'])){
      $editable_status=1;
    }

    if(isset($_GET['edit_listname']) && strlen($_GET['edit_listname'])>0)
    {

        $editSQL = sprintf("UPDATE cnt_listnames 
                            SET listname=%s, shared=%s, editablebyothers=%s
                            WHERE listid=%s",
                            GetSQLValueString($_GET['edit_listname'], 'text'),
                            GetSQLValueString($shared_status, "int"),
                            GetSQLValueString($editable_status, "int"),
                            GetSQLValueString($_GET['selectulistd'], "int")

                            );

        mysql_select_db($database_CRMconnection, $CRMconnection);
        $ResultUpdate = mysql_query($editSQL, $CRMconnection) or die(mysql_error());
    } 
}



$colname_availlist = "none";
if (isset($_GET['MM_Username'])) {
  $colname_availlist = $_GET['MM_Username'];
}
$colname2_availlist = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname2_availlist = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_availlist = sprintf("SELECT * FROM cnt_listnames WHERE cnt_listnames.listid NOT IN ( SELECT tblfavourites.listid FROM tblfavourites WHERE tblfavourites.fav_Username=%s AND tblfavourites.tbl_cnt_Id = %s ) ORDER BY cnt_listnames.listname ASC", GetSQLValueString($colname_availlist, "text"),GetSQLValueString($colname2_availlist, "int"),GetSQLValueString($colname_availlist, "text"));
$availlist = mysql_query($query_availlist, $CRMconnection) or die(mysql_error());
$row_availlist = mysql_fetch_assoc($availlist);
$totalRows_availlist = mysql_num_rows($availlist);

$colname_alllists = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_alllists = $_SESSION['MM_Username'];
}

# Reloads the drop down call list
# Check for admin rights
mysql_select_db($database_CRMconnection, $CRMconnection);
$sql = "";
$sql = "SELECT * FROM cnt_listnames ORDER BY cnt_listnames.listid ASC";

$query_alllists = sprintf($sql, GetSQLValueString($colname_availlists, "text"));
$alllists = mysql_query($query_alllists, $CRMconnection) or die(mysql_error());
$row_alllists = mysql_fetch_assoc($alllists);
$totalRows_alllists = mysql_num_rows($alllists);

?>



<?php 
$sp="";
if (isset($_GET['sourcepage'])){
	$sp = $_GET['sourcepage'];
}

if($sp=='clientdetails.php'){ do { ?>
<a id="avls<?php echo $row_availlist['listid']; ?>" href="#" onclick="$('#onlistspan').empty().html('<img src=images/ajax-loader.gif />'); $('#onlistspan').load('<?php echo("userclientlistlistajax.php?cnt_Id=".$_GET['cnt_Id']."&fav_Username=".$_GET['MM_Username']."&listid=".$row_availlist['listid']."&tbl_Tiering=1&fav_nick=none&MM_insert=addtolist") ?>'); $('#avls<?php echo $row_availlist['listid']; ?>').remove();" class="clicklink"><?php echo $row_availlist['listname']; ?><?php if($row_availlist['shared']==1){?><img src="images/redbox.png" width="5" height="5" border="0" /><?php } ?></a>
      <?php } while ($row_availlist = mysql_fetch_assoc($availlist)); 
	  
	  }
	  if($sp=="index.php"){ 
		 ?>
	    <?php
do {  
?>
	    <option value="<?php echo $row_alllists['listid']?>"<?php if (!(strcmp($row_alllists['listid'], 1))) {echo "selected=\"selected\"";} ?>><?php echo $row_alllists['listname']?><?php if( $row_alllists['shared']==1) {?>*<?php } ?>
        </option>
	    	    <?php
} while ($row_alllists = mysql_fetch_assoc($alllists));
      }
 
 if($sp=="index.phpd"){ 
		 ?>
	  <select name="selectulistd" id="selectulistd">
	    <?php
		do {  
		?>
	    <option value="<?php echo $row_alllists['listid']?>"<?php if (!(strcmp($row_alllists['listid'], 1))) {echo "selected=\"selected\"";} ?>><?php echo $row_alllists['listname']?><?php if( $row_allists['shared']==1) {?>*<?php } ?>
        </option>
	    <?php
} while ($row_alllists = mysql_fetch_assoc($alllists));
?>

</select>  
<?php
      }
	  
	 echo $opresult; 
	  
	?>
      
<?php

mysql_free_result($availlist);

mysql_free_result($alllists);
?>
