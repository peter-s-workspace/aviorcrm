<?php 
	require_once('Connections/CRMconnection.php'); 
	require_once('includes/event_log/eventlog.class.php'); 
	require_once('includes/classes/user.class.php');
  require_once('includes/classes/mitel.class.php');
?>
<?php
if (!isset($_SESSION)) {
  session_start();
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$eventLogObj = new Eventlog();
$userObj = new User();

//response results
$responsev ="";
$loadnotes= "false";

//update user details

if ((isset($_GET["MM_update"])) && ($_GET["MM_update"] == "updateuserdet")) {
	//Get Client Active status before updating the database
	$clientStatusSql = sprintf("SELECT active FROM contacts WHERE id=%s", GetSQLValueString($_GET['cnt_Id'], "int"));
	mysql_select_db($database_CRMconnection, $CRMconnection);
	$userRS = mysql_query($clientStatusSql, $CRMconnection) or die(mysql_error());		
	$active_status  = mysql_result($userRS,0,'active');
	$client_profile_before_updating = isset($_GET['cnt_id']) ? $userObj->setType('client')->GetProfile($_GET['cnt_id']) : '';

		
	$portal_access = isset($_GET['portal_access']) ? $_GET['portal_access']: '1';	
	$webview_print = isset($_GET['webview_print']) ? $_GET['webview_print']: '0';	

	if ($_GET['cnt_override_drm'] != 3) {
		$webview_print = 0;
	}	
		
	  $updateSQL = sprintf("UPDATE contacts SET first_name=%s, surname=%s, landline=%s, fax=%s, mobile=%s, email=%s, company_id=%s, jobtitle=%s, bloomberg_user=%s, dob=%s, active=%s, bloomberg_status=%s, drm_status=%s, portal_access=%s, webview_print=%s WHERE id=%s",

                       GetSQLValueString($_GET['cnt_Fname'], "text"),
                       GetSQLValueString($_GET['cnt_Lastname'], "text"),
                       GetSQLValueString($_GET['cnt_phone'], "text"),
                       GetSQLValueString('', "text"),
                       GetSQLValueString($_GET['cnt_cell'], "text"),
                       GetSQLValueString($_GET['cnt_email'], "text"),
                       GetSQLValueString($_GET['cnt_company'], "text"),
                       GetSQLValueString($_GET['cnt_jobtitle'], "text"),
                       GetSQLValueString($_GET['cnt_blmuser'], "text"),
                       GetSQLValueString('', "date"),
					   GetSQLValueString($_GET['cnt_active'], "int"),
					   GetSQLValueString($_GET['bloomberguser_status'], "text"),
             GetSQLValueString($_GET['cnt_override_drm'], "int"),
             GetSQLValueString($portal_access, "int"),
             GetSQLValueString($webview_print, "int"),						 						 
					   GetSQLValueString($_GET['cnt_Id'], "int"));





  
  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($updateSQL, $CRMconnection) or die(mysql_error());
//  $Result2 = mysql_query($updateSQL2, $CRMconnection) or die(mysql_error());
  
  //Log Active Status if it has changed.
  if( $Result1 && ( $active_status != $_GET['cnt_active'] ) ) {

		$eventLogObj->save(array(
			'event' => 'UPDATE_CLIENT_STATUS',
			'user' => $_SESSION['MM_Username'],
			'affected_user' => $_GET['cnt_Fname'].' '.$_GET['cnt_Lastname'],
			'affected_type' => 'client',
			'company' => $_GET['cnt_company'],
			'before' => json_encode(array('Status'=>$active_status),true),
			'after' => json_encode(array('Status'=> $_GET['cnt_active']),true)
			));

	}
	
  if( $Result1 ) {

    	//Check additional phone numbers.
  	$additionalPhoneNumbers = isset($_GET['phone_num']) && is_array($_GET['phone_num']) && sizeof($_GET['phone_num']) > 0 ? $_GET['phone_num'] : array();
  	$clientId  = isset($_GET['cnt_Id']) && $_GET['cnt_Id'] > 0 ? $_GET['cnt_Id'] : 0;
  	if( $clientId  > 0){
		//Use could have cleared all numbers, so additional numbers array would be empty. Remove numbers associated with client Id from phone number table.
		$additionalNumberUpdated = $userObj->addClientAdditionalNumbers($clientId, $additionalPhoneNumbers);
  	}

    ###### Mitel Logic goes here used hidden fields that should hold the previous stored client data there might be a better way of implementing this ###############

    $user_data          = array('last_name'           => $_GET['cnt_Lastname'],
                                'first_name'          => $_GET['cnt_Fname'],
                                'user_phone'          => $_GET['cnt_phone'],
                                'old_user_last_name'  => $_GET['old_user_surname'],
                                'old_user_first_name' => $_GET['old_user_name'], 
                                'old_user_phone'      => $_GET['old_user_number'],
                          );  

    $mitel_change = ( 
                      $user_data['last_name'] != $suser_data['old_user_last_name'] ||
                      $user_data['first_name'] != $suser_data['old_user_first_name'] ||
                      $user_data['user_phone'] != $suser_data['old_user_phone']
                    );

    if ( $mitel_change && $mitel = Mitel::create($database_CRMconnection, $CRMconnection) ) {

      $mitel_response     = $mitel->insert_update_mitel_telephone_directory('update', $user_data);
    
      if (!$mitel_response['status']) {
          echo $mitel_response['message'];
          //exit;
      } else {
        ###### update the mitel records / json array returned need one last testing before going to live######
        $mitel_response_data    = json_decode($mitel_response['message'], true);
  
        // "newRecord":"name=Gregory,Eckersley;number=0~971241500;prime_name=0;privacy=0;department=;location=;"
        $mitel_explode_data     = explode(";", $mitel_response_data['newRecord']);
        //$new_mitel_name         = str_replace(array('name=',','), array('',' '), $mitel_explode_data[0]);
        $new_mitel_name         = str_replace('name=','', $mitel_explode_data[0]);
        $new_mitel_names		  = explode(",", $new_mitel_name);
        $new_mitel_name		  = $new_mitel_names[1]." ".$new_mitel_names[0];      
  
        $new_mitel_number       = str_replace("number=0~",'', $mitel_explode_data[1]);
       
        $mitel_update_query     = "UPDATE avcrm.contacts set 
                                      mitel_full_name         = '$new_mitel_name', 
                                      mitel_phone_number      = '$new_mitel_number' 
                                      where id                = $clientId";
  
        error_log($mitel_response_data, 3, "error.log");
        mysql_select_db($database_CRMconnection, $CRMconnection);
        $run_query              = mysql_query($mitel_update_query,$CRMconnection);
        if (!$run_query) {
          echo "Successfully updated the mitel information";
          //exit;
        } else {
          error_log(mysql_error($CRMconnection), 3 , "error.log");
        }
      }
    }

    ############### end of mitel logic ################################

  $hipchat_rooms          = $_GET['hipchat_rooms'];
  	//Check additional phone numbers.
	$additionalPhoneNumbers = isset($_GET['phone_num']) && is_array($_GET['phone_num']) && sizeof($_GET['phone_num']) > 0 ? $_GET['phone_num'] : array();
	$clientId  = isset($_GET['cnt_Id']) && $_GET['cnt_Id'] > 0 ? $_GET['cnt_Id'] : 0;
	if( $clientId  > 0){
		//Use could have cleared all numbers, so additional numbers array would be empty. Remove numbers associated with client Id from phone number table.
		$additionalNumberUpdated = $userObj->addClientAdditionalNumbers($clientId, $additionalPhoneNumbers);
	}
  ############################## update the hipchat rooms here ######################## 
  if (!empty($hipchat_rooms)) {
      $remove_existing_rooms = "DELETE FROM tblclient_hipchatrooms WHERE client_id = ".$_GET['cnt_Id']."";
      $remove_query          = mysql_query($remove_existing_rooms, $CRMconnection);
      if (!$remove_query) {
        die("Error deleting the rooms " .mysql_error());
      } 

      foreach ($hipchat_rooms as $key => $value) {
        $insert_rooms_sql   = "INSERT  INTO tblclient_hipchatrooms (client_id, hipchat_room_id) VALUES (".$_GET['cnt_Id'].", $value) ";

        $insert_client      = mysql_query($insert_rooms_sql, $CRMconnection);
        if (!$insert_client) {
          die ("Error Updating " . mysql_error());
        }
     }


  }
   
    ########################## End logic here ###############################################
	  $eventLogObj->save(array(
			'event' => 'UPDATE_CLIENT_DETAILS',
			'user' => $_SESSION['MM_Username'],
			'affected_user' => $_GET['cnt_Fname'].' '.$_GET['cnt_Lastname'],
			'affected_type' => 'client',
			'company' => $_GET['cnt_company'],
			'before' => $client_profile_before_updating,
			'after' => $userObj->setType('client')->GetReadableFields($_GET)
		));	
 }
 


  $responsev = "Updated";
}

if ((isset($_GET["MM_noteupdate"])) && ($_GET["MM_noteupdate"] == "updateusernote")) {
  $updateSQL = sprintf("UPDATE contacts SET note=%s WHERE id=%s",
                       GetSQLValueString($_GET['cnt_note'], "text"),  
                       GetSQLValueString($_GET['cnt_Id'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($updateSQL, $CRMconnection) or die(mysql_error());

$responsev = "Updated";
}



if ((isset($_GET["MM_noteup"])) && ($_GET["MM_noteup"] == "doup")){
$updateSQL = sprintf("UPDATE tblnotes SET note_txt=%s WHERE note_Id=%s",
                       GetSQLValueString($_GET['note_txt'], "text"),
                       GetSQLValueString($_GET['note_Id'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($updateSQL, $CRMconnection) or die(mysql_error());
  
 //updated - now just replace the target txt with the updated note.
$responsev='<a href="#" onclick="' ;
$responsev.= "$('#rco".$_GET['note_Id']."').load('clientnoteupdate.php?note_Id=".$_GET['note_Id']."&idparent=rco".$_GET['note_Id']."')";
$responsev.= '">'.$_GET['note_txt'].'</a>';

} 

  
if ((isset($_GET["MM_noteadd"])) && ($_GET["MM_noteadd"] == "addnote"))  {
  $insertSQL = sprintf("INSERT INTO tblnotes (note_by, notebyNick, note_cnt_Id, note_cnt_name, cntCompany, note_txt, note_type, note_date, note_time) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_GET['note_by'], "text"),
                       GetSQLValueString($_GET['notebyNick'], "text"),
                       GetSQLValueString($_GET['note_cnt_Id'], "int"),
                       GetSQLValueString($_GET['note_cnt_name'], "text"),
                       GetSQLValueString($_GET['cntCompany'], "text"),
                       GetSQLValueString($_GET['note_txt'], "text"),
                       GetSQLValueString($_GET['note_type'], "text"),
                       GetSQLValueString($_GET['note_date'], "date"),
                       GetSQLValueString($_GET['note_time'], "text")
                       );

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());
  
 $responsev= "";
 $loadnotes= "true";
 
 $colname_rscontacts = "-1";
if (isset($_GET['note_cnt_Id'])) {
  $colname_rscontacts = $_GET['note_cnt_Id'];}

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rscontacts = sprintf("SELECT * FROM tblnotes WHERE note_cnt_Id = %s ORDER BY note_date , note_Id DESC", GetSQLValueString($colname_rscontacts, "double"));
$rscontacts = mysql_query($query_rscontacts, $CRMconnection) or die(mysql_error());
$row_rscontacts = mysql_fetch_assoc($rscontacts);
$totalRows_rscontacts = mysql_num_rows($rscontacts);
}

if(isset($_GET['deactivateKam']) && $_GET['deactivateKam'] == true){

    mysql_select_db($database_CRMconnection, $CRMconnection);

    $deactivateKam1Query = sprintf("UPDATE tblcompanylist SET id_kam1 = 0 WHERE id_kam1 = %s" , GetSQLValueString($_GET['cnt_Id'] , "int"));
    $deactivateKam2Query = sprintf("UPDATE tblcompanylist SET id_kam2 = 0 WHERE id_kam2 = %s" , GetSQLValueString($_GET['cnt_Id'] , "int"));

    $deactivateKam1Result = mysql_query($deactivateKam1Query , $CRMconnection) or die(mysql_error());
    $deactivateKam2Result = mysql_query($deactivateKam2Query , $CRMconnection) or die(mysql_error());

    if($deactivateKam1Result) { error_log('Removed Key Account Manager 1');} else { error_log('Nothing to remove');}
    if($deactivateKam2Result) { error_log('Removed Key Account Manager 2');} else { error_log('Nothing to remove');}

}
  
if ((isset($_GET['note_Id'])) && ($_GET['note_Id'] != "") && (isset($_GET['deletenote']))) {
  $deleteSQL = sprintf("DELETE FROM tblnotes WHERE note_Id=%s",
                       GetSQLValueString($_GET['note_Id'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($deleteSQL, $CRMconnection) or die(mysql_error());
  $responsev = "deleted";
}



?>

<html><head></head><body>
<?php
echo ($responsev);

if ((isset($_GET["MM_noteadd"])) && ($_GET["MM_noteadd"] == "addnote")) {
	 do { ?>
       <span id="rco<?php echo $row_rscontacts['note_Id']; ?>ab">
         <hr />
       <?php echo $row_rscontacts['note_date']; ?>| <span class="clicklink" id="rco<?php echo $row_rscontacts['note_Id']; ?>" > <a href="#" onClick="$('#rco<?php echo $row_rscontacts['note_Id']; ?>').load('clientnoteupdate.php?note_Id=<?php echo $row_rscontacts['note_Id']; ?>&idparent=rco<?php echo $row_rscontacts['note_Id']; ?>')"><?php if( $row_rscontacts['note_txt'] !=""){ echo ($row_rscontacts['note_txt']);} else{ echo ('');} ?></a></span>
		<h6 style="display:block; margin:0px; padding-top:2px; text-align:right"><?php echo $row_rscontacts['note_type']; ?> | <?php echo $row_rscontacts['notebyNick']; ?> | <a href="#" onClick="$('#rco<?php echo $row_rscontacts['note_Id']; ?>').load('clientdetailsajaxops.php?note_Id=<?php echo $row_rscontacts['note_Id']; ?>&deletenote=yes'); $('#rco<?php echo $row_rscontacts['note_Id']; ?>ab').remove()" >Delete</a></h6> </span>
       
       
       <?php } while ($row_rscontacts = mysql_fetch_assoc($rscontacts));
	   mysql_free_result($rscontacts);
	   } 
	   
	   ?>


</body></html>

