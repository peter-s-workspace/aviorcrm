<?php
if (!isset($_SESSION)) {
  session_start();
}
require_once('includes/classes/company.class.php');
require_once('includes/classes/user.class.php');
require_once('includes/classes/hipchat.class.php');
require_once('Connections/CRMconnection.php');

$hipchat_instance = new hipchat();

$get_hipchat_room_name = $hipchat_instance->get_all_rooms();


$companyId = isset( $_POST['id'] ) ? $_POST['id'] : 0;
$formSuccessfullyUpdated = (isset($_GET['updated']) && $_GET['updated'] == 'success') ? true : false;
$companyDetails = array();
if(isset($_SESSION['MM_Username']) && isset( $_SESSION['MM_UserGroup'] ) &&  $_SESSION['MM_UserGroup'] == 'Administrator' ) {

	if( $companyId == 0 ) { //Go back to admin main if there's no ID.
		header('Location: adminmain.php');
		exit();
	}
	
	$companyObj = new Company($companyId);
	if( isset( $_POST['comp_update'] ) ) {
	
		$formUpdateResult = $companyObj->updateCompany( $_POST );
		if( $formUpdateResult == 'success' ) {
			header("location: companydetails-edit.php?id=".$companyId."&updated=success");
			exit;
		}
	}
	$companyDetails = $companyObj->getCompanyDetails();
	$companyTypes = $companyObj->getCompanyTypes();
	$companyStatus = $companyObj->getCompanyStatuses();
	$companyTiers = $companyObj->getCompanyTiers();
	
	$userObj = new User();
	$clientList = $userObj->getClientList();
	$clientListSize = sizeof($clientList);
	
	$companyName = "Avior Capital Markets (Pty) Ltd";
	$aviorClientList = $userObj->GetClientListByCompanyName($companyName);
	$aviorClientListSize = sizeof($aviorClientList);
} else {
	header('Location: adminmain.php');
	exit();
}

$get_rooms                = "SELECT hipchat_room_id FROM tblcompany_hipchatrooms where company_id = ".$_POST['id']. "";

$rooms                    = null;
$run_rooms_query          = mysql_query($get_rooms, $CRMconnection) or die (mysql_error());
if (!$run_rooms_query)  {
  die ('Error fetching Data '. mysql_error($CRMconnection));
} else {  
  while ($row         	= mysql_fetch_array($run_rooms_query)) {
   	$company_rooms []  	= $row['hipchat_room_id'];
  } 
}
$hipchat_room_id 		= implode(",", $company_rooms);


if( sizeof($companyDetails) > 0 ) {
?>
<legend>Edit <?php  echo $companyDetails[0]['company'];?> </legend>
<form method="post" id="editCompany" name="editCompany" class="edit-form" action="">
	  <div id="editCompanyResponse"></div>
	  <table class="edit-table">
		<tr>
			<td class="label">Name:</td>
			<td><input type="text" id="company_name" name="company_name" value="<?php echo $companyDetails[0]['company']; ?>" />
			<span class="validation-error"></span>
			</td>
		</tr>
		<tr>
			<td class="label">Institution Type:</td>
			<td>
				<select id="company_type" name="company_type">
					<option value="">Select Company Type</option>
					<?php for($x=0; $x < sizeof($companyTypes); $x++ ) { 
						$selected_type = isset($companyDetails[0]['id_companytype']) && $companyDetails[0]['id_companytype'] == $companyTypes[$x]['id_companytype'] ? 'selected="selected"': ""; ?>
						<option value="<?php echo $companyTypes[$x]['id_companytype']; ?>" <?php echo $selected_type; ?>><?php echo $companyTypes[$x]['companytype']; ?></option>	
					<?php } ?>
				</select>
				<span class="validation-error"></span>
			</td>
		</tr>
		<tr>
			<td class="label">Status:</td>
			<td>
				<?php for($x=0; $x < sizeof($companyStatus); $x++ ) { 
					$selected_status = ''; 
					
					// Default is set on add 
					// New dbs entries will never run the else if
					// Used to handle legacy nulls in the db
					// Suggested to be removed at later stage.
					if(isset($companyDetails[0]['id_companystatus']) && $companyDetails[0]['id_companystatus'] == $companyStatus[$x]['id_companystatus'])
					{
						$selected_status = 'checked="checked"';
					}
					else if (!isset($companyDetails[0]['id_companystatus']) && $companyStatus[$x]['company_status']=="Prospect" )
					{
						//Set the default of status to Prospect
						$selected_status = 'checked="checked"';
					}	
					else
					{
						$selected_status = '';
					}
					?>
					<label><input type="radio" id="company_status" name="company_status" value="<?php echo $companyStatus[$x]['id_companystatus'];?>" <?php echo $selected_status ?> />&nbsp;<?php echo $companyStatus[$x]['company_status'];?></label><br />
				<?php } ?>
				<span class="validation-error"></span>
			</td>
		</tr>
		<tr>
			<td class="label">Tier:</td>
			<td>
				<select id="company_tier" name="company_tier">
					<option value="">Select Institution Tier</option>
					<?php for($x=0; $x < sizeof($companyTiers); $x++ ){ 
						$selected_tier = isset($companyDetails[0]['id_companytier']) && $companyDetails[0]['id_companytier'] == $companyTiers[$x]['id_companytier'] ? 'selected="selected"': "";?>
						<option value="<?php echo $companyTiers[$x]['id_companytier'];?>" <?php echo $selected_tier; ?>><?php echo $companyTiers[$x]['companytier'];?></option>
					<?php }?>
				</select>
				<span class="validation-error"></span>
			</td>
		</tr>
		<tr>
	         <td class="label">Hipchat Room:</td>
	          <td>
	             <?php
	             		echo $hipchat_instance->get_hipchat_rooms();
	               ?>
	             </select>
	          </td>
     	</tr>
		<tr>
			<td class="label">Address 1:</td>
			<td><input type="text" id="address_1" name="address_1" value="<?php echo $companyDetails[0]['address_1']; ?>" /></td>
		</tr>
		<tr>
			<td class="label">Address 2:</td>
			<td><input type="text" id="address_2" name="address_2" value="<?php echo $companyDetails[0]['address_2']; ?>" /></td>
		</tr>
		<tr>
			<td class="label">Address 3:</td>
			<td><input type="text" id="address_3" name="address_3" value="<?php echo $companyDetails[0]['address_3']; ?>" /></td>
		</tr>
		<tr>
			<td class="label">Address 4:</td>
			<td><input type="text" id="address_4" name="address_4" value="<?php echo $companyDetails[0]['address_4']; ?>" /></td>
		</tr>
		<tr>
			<td nowrap="nowrap" align="left" colspan="2" width="100px">
				<input type="hidden" name="company_imageajax" id="company_imageajax" value="<?php if(file_exists("uploads/companyp/".$companyDetails[0]['id']."/1.jpg"))  echo "uploads/companyp/".$companyDetails[0]['id']."/1.jpg"; ?>" >
				<input type="hidden" name="company_imagesizeajax" id="company_imagesizeajax" value="<?php if(file_exists("uploads/companyp/".$companyDetails[0]['id']."/1.jpg"))  echo filesize("uploads/companyp/".$companyDetails[0]['id']."/1.jpg"); ?>" >
				<div id="cpicture3" style="text-align:center; padding-bottom:3px;">
				  <div id="companypicturedrop" class="picturedrop">
					  <p class="my-drop-message dz-message">Drop or Click<br />here<br />add picture</p>
				  </div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="label">Switchboard 1</td>
			<td><input type="text" id="switchboard_1" name="switchboard_1" value="<?php echo $companyDetails[0]['switchboard_1']; ?>" /></td>
		</tr>
		<tr>
			<td class="label">Switchboard 2</td>
			<td><input type="text" id="switchboard_2" name="switchboard_2" value="<?php echo $companyDetails[0]['switchboard_2']; ?>" /></td>
		</tr>
		<tr>
			<td class="label">Website:</td>
			<td><input type="text" id="company_website" name="company_website" value="<?php echo $companyDetails[0]['website']; ?>" />
			<span class="validation-error"></span>
			</td>
		</tr>
		<tr>
			<td class="label">Facebook:</td>
			<td><input type="text" id="social_fb" name="social_fb" value="<?php echo $companyDetails[0]['social_fb']; ?>" />
			<span class="validation-error"></span>
			</td>
		</tr>
		<tr>
			<td class="label">Twitter:</td>
			<td><input type="text" id="social_twitter" name="social_twitter" value="<?php echo $companyDetails[0]['social_twitter']; ?>" />
			<span class="validation-error"></span>
			</td>
		</tr>
		<tr>
			<td class="label">LinkedIn:</td>
			<td><input type="text" id="social_linkedin" name="social_linkedin" value="<?php echo $companyDetails[0]['social_linkedin']; ?>" />
			<span class="validation-error"></span>
			</td>
		</tr>
		<?php if( $aviorClientListSize > 0 ) { ?>
		<tr>
			<td class="label">KAM 1:</td>
			<td>
				<select id="company_kam1" name="company_kam1">
					<option value="">Select KAM 1</option>
					<?php for( $x=0; $x < $aviorClientListSize; $x++ ){
						$selected_kam1 = isset($companyDetails[0]['id_kam1']) && $companyDetails[0]['id_kam1'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?> 
						<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_kam1; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>	
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td class="label">KAM 2:</td>
			<td>
				<select id="company_kam2" name="company_kam2">
					<option value="">Select KAM 2</option>
					<?php for( $x=0; $x < $aviorClientListSize; $x++ ){ 
						$selected_kam2 = isset($companyDetails[0]['id_kam2']) && $companyDetails[0]['id_kam2'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?> 
						<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_kam2; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>	
					<?php } ?>
				</select></td>
		</tr>
		<tr>
			<td class="label">Dealer 1:</td>
			<td>
				<select id="company_dealer1" name="company_dealer1">
					<option value="">Select Dealer 1</option>
					<?php 
						$selected_csa_only = isset($companyDetails[0]['id_dealer1']) && $companyDetails[0]['id_dealer1'] == -1 ? 'selected="selected"': "";
					?>
					<option value="-1" <?php echo $selected_csa_only; ?>>CSA Only</option>	
					<?php for( $x=0; $x < $aviorClientListSize; $x++ ){ 
						$selected_delear1 = isset($companyDetails[0]['id_dealer1']) && $companyDetails[0]['id_dealer1'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?> 
						<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_delear1; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>	
					<?php } ?>
					
					
				</select>
			</td>
		</tr>
		<tr>
			<td class="label">Dealer 2:</td>
			<td>
				<select id="company_dealer2" name="company_dealer2">
					<option value="">Select Dealer 2</option>
					<?php for( $x=0; $x < $aviorClientListSize; $x++ ){ 
						$selected_delear2 = isset($companyDetails[0]['id_dealer2']) && $companyDetails[0]['id_dealer2'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?> 
						<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_delear2; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>	
					<?php } ?>
				</select>
			</td>
		</tr>
		<?php } else { ?>
		<tr>
			<td colspan="2">Please <a href="/adminmain.php" title="" >add</a> Avior Employees.</td>
		</tr>
		<?php } ?>
		<tr>
			<td class="label">Select Protection Type:</td>
			<td>
				<select id="company_drm_setting" name="company_drm_setting">
					<?php $selected_drm = isset($companyDetails[0]['drm_status']) ? $companyDetails[0]['drm_status'] : ''; ?>
					<option value="1" <?php if( $selected_drm == 1 ) echo 'selected="selected"'; ?>>Normal</option>
					<option value="2" <?php if( $selected_drm == 2 ) echo 'selected="selected"'; ?>>DRM</option>
					<option value="3" <?php if( $selected_drm == 3 ) echo 'selected="selected"'; ?>>Web Viewer</option>
				</select>
			<br />
			<small> Please note - changing the tier here will change the e-mail tiering for all clients on all mailing lists at this specific company - use with caution - or contact Dirk before using it.</small>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="hidden" id="comp_id" name="comp_id" value="<?php echo $companyId;?>" />
				<p><input type="button" id="cancel_button" name="cancel_button" value="Cancel"  />&nbsp;
				<input type="submit" id="save-company-edit-changes" name="save-company-edit-changes" value="Save Changes"  /></p>
			</td>
		</tr>
	  </table>
	  <input type="hidden" name="comp_update" id="comp_update" value="true" />
</form>
<script type="text/javascript">
	$(document).ready(function(){
		
		$('.js-example-basic-multiple').select2().val([<?php echo $hipchat_room_id;?>]).trigger("change");

		// $('.js-example-basic-multiple').select2().val([<?php echo $client_rooms;?>]).trigger("change");
			$('#editCompany').validate({
            errorClass:'validate-error',
            errorPlacement: function(error, element) {		
                error.appendTo( element.parents('tr').find("span.validation-error").addClass('error') );													
            },
            rules: {
				company_name: { required: true },
				company_tier: { required: true },
				company_type: { required: true },
				company_website: { required: false, url: true },
				social_fb: { required: false, url: true },
				social_twitter: { required: false, url: true },
				social_linkedin: { required: false, url: true }
				
				
            },
            messages: {		
				company_name: { required: 'Please enter Institution Name' },
				company_tier: { required: 'Please select Tier' },
				social_fb: { url: 'Please enter a valid URL starting with http(s)://' },
				social_twitter: { url: 'Please enter a valid URL starting with http(s)://' },
				social_linkedin: { url: 'Please enter a valid URL starting with http(s)://' }
            }
        });

		var company_image = $('#company_imageajax').val();
		var company_imagesize = $('#company_imagesizeajax').val();
		var file_dropped = 0;

		console.log("WTTTFFFFFFFFFF");
		console.log(company_image);

		var companymyDropzone = new Dropzone("div#companypicturedrop", 
			{ url: "js/uploadifyclientp.php?folder=/uploads/companyp/<?php echo $companyDetails[0]['id']; ?>",
			addRemoveLinks :true,
			acceptedFiles: 'image/*',
			forceFallback: false,
			paramName:'Filedata',
			dictFallbackText: 'Please use the fallback form below to upload your files',
			dictRemoveFile: 'Clear Picture',
			maxFiles:1
			});

		if($('.dz-browser-not-supported').length == 0) {

				companymyDropzone.on("removedfile", function(file) {

					  if (!file.name) { return; }
						 companymyDropzone.removeAllFiles(true);
						 companymyDropzone.options.maxFiles = 1;
						 $('.my-drop-message').show();
						 $(".dz-error, .error").remove();
						 $.post("js/dropzone-remove-file.php",{cid:'<?php echo $companyDetails[0]['id']; ?>'}); 
					}).on("drop",function(file,error){
						  $('#cpicture3').append('<div class="notice error"><em>Note: Images need to be saved to desktop first before dropping</em></div>');
						});

				if( company_image != '' ) {
					$('.my-drop-message').hide();
					var mockFile = { name: "1.jpg", size: company_imagesize };
					companymyDropzone.emit("addedfile", mockFile);
					companymyDropzone.emit("thumbnail", mockFile, company_image);
					companymyDropzone.emit("complete", mockFile);
					var existingFileCount = 1; 
					companymyDropzone.options.maxFiles = companymyDropzone.options.maxFiles - existingFileCount;

				}
				companymyDropzone.on("error", function(file,error) {
					
					$(".error").remove();
					$(".dz-error").hide();
					if( error == 'You can not upload any more files.' ) {
						$('#cpicture3').append('<div class="error">You can not upload any more files. <br />Please clear image first</div>');
					} else {
						$('#cpicture3').append('<div class="error">'+error+'</div>');
					}
				});
				
				companymyDropzone.on("processing", function(file){
					$('.notice').remove();
					$('.my-drop-message').hide();
				});
			}  else {
				$('.my-drop-message').hide();
				$('.dz-browser-not-supported').append('<div id="fallback-preview"></div>');
					if (company_image != '') {
						$('#fallback-preview').html('<img src="'+company_image+'" alt="Preview" /><br /><a id="fallback-removefile" href="#">Remove File</a>');
						$('.dz-browser-not-supported form').hide();
					} else {
						$('#companypicturedrop .dz-fallback').html('<p>Please upload your image using the normal upload <a href="companydetails-edit.php?id=<?php echo $companyDetails[0]['id']; ?>" target="_blank" style="text-decoration:underline;" title="Click to Edit Institution">here</a>.</p>');
					}

					$('#fallback-removefile').click(function(){
						$('.dz-browser-not-supported form').show();
						$.post("js/dropzone-remove-file.php",{cid:'<?php echo $companyDetails[0]['id']; ?>'}); 
						$('#fallback-preview img, #fallback-removefile').hide();
						return false;
					});
				}
	});
</script>
<?php } else { ?>
	<p>Selected Institution does not exists.</p>
<?php }  ?>