<?php require_once('../Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}


?>
<?php require_once('../includes/sitevars.php'); ?>
<?php require_once('../includes/abovehead.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- TemplateBeginEditable name="doctitle" -->
<title>Avior Research CRM</title>
<!-- TemplateEndEditable -->
<!-- TemplateBeginEditable name="head" --><link type="text/css" href="css/kdes1/jquery-ui-1.8.4.custom.css" rel="stylesheet" />		
		<script src="js/jquery-1.9.1.min.js"></script>

<script src="js/jquery-migrate-1.1.1.min.js"></script>


		<script type="text/javascript" src="js/jquery-ui-1.8.4.custom.min.js"></script>
		<script type="text/javascript">
			$(function(){

				// Accordion
				//$("#accordion").accordion({ header: "h3" });
	
				// Tabs
				$('#tabs1').tabs();
				$('#tabs2').tabs();
				
				$('.itemDelete').live('click', function() {
    $(this).closest('li').remove();
});
				
				
				
				// Dialog			
				$('#dialog').dialog({
					autoOpen: false,
					width: 600,
					buttons: {
						"Ok": function() { 
							$(this).dialog("close"); 
						}, 
						"Cancel": function() { 
							$(this).dialog("close"); 
						} 
					}
				});
				
				// Dialog Link
				$('#dialog_link').click(function(){
					$('#dialog').dialog('open');
					return false;
				});
				
				//hide add reminder box
				$('#addreminder').hide();
				
				
				//reminder button -open form
				$('#btnaddreminder').click(function(){
					$('#addreminder').show('fold');
					return false;
				});
				//close reminder form
				$('#btnCancelRemind').click(function(){
					$('#addreminder').hide('fold');
					return false;
				});
			

				// Datepicker
				$('#datepicker').datepicker({
					inline: true
				});
				
				// Slider
				$('#slider').slider({
					range: true,
					values: [17, 67]
				});
				
				// Progressbar
				$("#progressbar").progressbar({
					value: 20 
				});
				
				//hover states on the static widgets
				$('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); }, 
					function() { $(this).removeClass('ui-state-hover'); }
				);
				
			});
		</script>

<!-- TemplateEndEditable -->
<script language="javascript">
	$(function(){
		
		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					}
				});
	})
</script>	
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header"><h1><?php echo $sitetitle; ?></h1></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Companies</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div>
				<fieldset>
					<Legend>Quick Search</Legend>
					<input type="text" id="quicksearch" size="16" />
				</fieldset>
			</div> 
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- TemplateBeginEditable name="Content" -->
<div class="content40">
<div id="tabs1">
      <ul>
        <li><a href="#tabs1-1">First</a></li>
        <li><a href="#tabs1-2">Second</a></li>
        <li><a href="#tabs1-3">Third</a></li>
      </ul>
      <div id="tabs1-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
      <div id="tabs1-2">Phasellus mattis tincidunt nibh. Cras orci urna, blandit id, pretium vel, aliquet ornare, felis. Maecenas scelerisque sem non nisl. Fusce sed lorem in enim dictum bibendum.</div>
      <div id="tabs1-3">Nam dui erat, auctor a, dignissim quis, sollicitudin eu, felis. Pellentesque nisi urna, interdum eget, sagittis et, consequat vestibulum, lacus. Mauris porttitor ullamcorper augue.</div>
</div>
</div>
<!-- TemplateEndEditable -->
</body>
</html>
<?php
require_once('../includes/endbody.php');
?>
