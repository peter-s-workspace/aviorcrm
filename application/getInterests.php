<?php require_once('Connections/CRMconnection.php'); ?>
<?php
//authentication settings
$authusers= "mikem";
 

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($hashauth,$strUsers) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 
  $nowdate = date("Ymd");
   
  // Parse the strings into arrays. 
  $arrUsers = Explode(",", $strUsers); 
  $arrlength = count($arrUsers);
  for ($i=0 ; $i < $arrlength; $i++ ){
		$arrUsers[$i] =  sha1($arrUsers[$i].$nowdate.$arrUsers[$i]);
	}
	if (in_array($hashauth, $arrUsers)) { 
      $isValid = true; 
    } 
     
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  return $isValid; 
}

//parsing sql value strings - preventing injections etc etc.
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}


//now get to the json output
//check whether authorised hash
if (isset($_GET['authash'])) {
	if (isAuthorized($_GET['authash'],$authusers)) {
	//okay - authorised - now structure the mysql query
	// GET variable "interest"
	/*
	$intlist="";
	if (is_array($_GET['interest'])){
		foreach($_GET['interest'] as $i => $val ){
        	$intlist .= " cnt_Interest=".GetSQLValueString($val,"text")." OR";
    	}
	//strip the last "OR" from the end of the string using a regular expression
	$fintlist = preg_replace('/OR$/', '', $intlist);
	} else {
	$fintlist = "cnt_Interest=".GetSQLValueString($_GET['interest'],"text");
	}
	*/
	mysql_select_db($database_CRMconnection, $CRMconnection);
	// now structure the rest of the query
	$query_string = sprintf("SELECT contacts_interests.* , interests.description from contacts_interests LEFT OUTER JOIN interests ON interests.id = contacts_interests.interest_id"); //WHERE
	//echo $query_string;
	
	//now generate the json output
	$sth = mysql_query($query_string);
	$rows = array();
	while($r = mysql_fetch_assoc($sth)) {
    	$rows[] = $r;
	}
	print json_encode($rows);
	//end of json output	
		
	} else {
		echo "not authorised";
	}
}else {
		echo "not authorised";
	}

?>

