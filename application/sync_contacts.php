<?php
/*
http://196.50.239.156/mitel/oig/rest/resources/v1/databases/views/telephonedirectory?databaseId=3&number=0~&name=Dennis,Cobi&primeName=0&privacy=0&newName=Dennis,Cobi&newNumber=0~{"result":"false","error":"On endpoint 192.168.105.2: The record is not found"}
http://196.50.239.156/mitel/oig/rest/resources/v1/databases/views/telephonedirectory?databaseId=3&number=0~&name=Dirk,Noeth&primeName=0&privacy=0{"result":"false","error":"On endpoint 192.168.105.2: At least one alpha tag digit must follow the alpha tag delimiter."}
http://196.50.239.156/mitel/oig/rest/resources/v1/databases/views/telephonedirectory?databaseId=3&number=0~&name=Rajay,Ambekar&primeName=0&privacy=0{"result":"false","error":"On endpoint 192.168.105.2: At least one alpha tag digit must follow the alpha tag delimiter."}
http://196.50.239.156/mitel/oig/rest/resources/v1/databases/views/telephonedirectory?databaseId=3&number=0~971241500&name=Gregory,Eckersley&primeName=0&privacy=0{"result":"true","newRecord":"name=Gregory,Eckersley;number=0~971241500;prime_name=0;privacy=0;department=;location=;"}
Gregory,Eckersley  971241500
'0097124150000'
*/

################### make sure the script is going to run without timing out when syching the contacts into mitel telephone directory ##################

ini_set('max_execution_time', 0);

require_once('Connections/CRMconnection.php');
require_once('includes/classes/mitel.class.php');

###### Initialize the mitel instance ############
$mitel_instance 		= new Mitel($database_CRMconnection, $CRMconnection);
$get_contacts_query 	= "SELECT contacts.id, contacts.first_name,contacts.surname,contacts.landline 
							FROM contacts 
							WHERE 
								contacts.mitel_full_name is null 
								AND contacts.mitel_phone_number is null 
								AND contacts.active = 1";

mysql_select_db($database_CRMconnection);

$run_query 				= mysql_query($get_contacts_query,$CRMconnection) or die(mysql_error());
$contacts_data_array 	= array();

while ($contact_row 	= mysql_fetch_array($run_query)) {
	   echo "Processing contact for ID ". $contact_row['id'].'<br/>';
	   $contacts_data_array['first_name'] 	= $contact_row['first_name'];
	   $contacts_data_array['last_name'] 	= $contact_row['surname'];
	   $contacts_data_array['user_phone']	= $contact_row['landline'];

	   echo "first: ".$contacts_data_array['first_name']."<br/>";
	   echo "last: ".$contacts_data_array['last_name']."<br/>";
	   echo "phone: ".$contacts_data_array['user_phone']."<br/>";

	   $mitel_response 		  = $mitel_instance->insert_update_mitel_telephone_directory('insert',$contacts_data_array);

	   var_dump( $mitel_response );
	   if ( is_null($mitel_response)) {  
			echo "Mitel update failed! Response was null."."<br/>";
	   }
	   else if (!$mitel_response['status']) {
			echo $mitel_response['message'].'<br/>';
	   }
	   else if ( strlen($mitel_response['message']) == 0 ) {
		    echo "Mitel update failed! Response was empty"."<br/>";		   
	   }
	   else {
 
		  $mitel_response_data    = json_decode($mitel_response['message'], true);

		  // {"result":"false","error":"On endpoint 192.168.105.2: Alpha tagging digit string must be unique."}
		  if ($mitel_response_data['result'] == "false" ) {
			echo $mitel_response_data['error'].'<br/>';
		  }
		  else {
			// {"result":"true","newRecord":"name=Van der Merwe,Philip;number=0~27216802088;prime_name=0;privacy=0;department=;location=;"}

			$mitel_explode_data     = explode(";", $mitel_response_data['newRecord']);
			$new_mitel_name         = str_replace('name=','', $mitel_explode_data[0]);
			$new_mitel_names		= explode(",", $new_mitel_name);
			$new_mitel_name			= $new_mitel_names[1]." ".$new_mitel_names[0];
			$new_mitel_number       = str_replace("number=0~",'', $mitel_explode_data[1]);

			$mitel_update_query     = "UPDATE avcrm.contacts set 
											mitel_full_name         = '$new_mitel_name', 
											mitel_phone_number      = '$new_mitel_number' 
											where id                = ".$contact_row['id']."";

			error_log($mitel_response_data.PHP_EOL, 3, "error.log");

			$mitel_query              = mysql_query($mitel_update_query,$CRMconnection);
			if ($mitel_query) {
				echo "Successfully updated the mitel information <br/>";
			} else {
				error_log(mysql_error($CRMconnection, 3 , "error.log"));
			}
		}
	  }
}
?>
