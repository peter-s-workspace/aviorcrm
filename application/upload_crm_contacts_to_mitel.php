<?php
  //**This is extra caution when processing large amounts of contacts on the live database
  ini_set('max_execution_time', 0);
	require_once('Connections/CRMconnection.php');
  ####################################################### Limit Query Results #############
  //** this limit the client 
  //** Pleas Note this values should be set to empty if processing all the clients
  $query_limit  = 10;
  #######################################################

	if (!isset($_SESSION)) {
		  session_start();
	}
	######  #########
  $mitel_data             = get_mitel_config_data($database_CRMconnection, $CRMconnection);  

  $mitel_server           = $mitel_data['mitel_server'];
  $mitel_jar_path         = $mitel_data['java_path'];
  $mitel_app_name         = $mitel_data['mitel_app_name'];
  $mitel_company_name     = $mitel_data['mitel_company_name'];
  $mitel_local_password   = $mitel_data['mitel_local_password'];
  $mitel_icp_address      = $mitel_data['mitel_icp_address'];
  $mitel_database_id      = $mitel_data['mitel_database_id'];
  $mitel_app_password     = $mitel_data['mitel_app_password'];
  #################################### Mitel Get the Session ID for both inserting and updating #####################
  exec("java -cp OIGJavaCallControlSample.jar oigjavacallcontrolsample/Login" ." ".$mitel_server ." ".$mitel_app_name ." ". $mitel_company_name ." ". $mitel_app_password . " " .$mitel_local_password, $sessionId, $return_var);
  $_SESSION['session_id'] = $sessionId[0]; 
 	##### End Session ID Code  Here ################# 

	$get_client_query 	= "SELECT CONCAT(first_name,' ',surname) as full_name , landline FROM contacts where active = 1 and landline <> '' group by first_name, landline limit $query_limit";
	
  mysql_select_db($database_CRMconnection);
	$run_query 			=  mysql_query($get_client_query,$CRMconnection) or die(mysql_error($CRMconnection));

	while($row = mysql_fetch_array($run_query)) {
	      $contact_phone_number 	        = "9~".$row['landline'];
        $full_name                      = $row['full_name'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL             => "http://$mitel_server/mitel/oig/rest/resources/v1/databases/views/telephonedirectory?databaseId=$mitel_database_id&number=$contact_phone_number&name=$full_name&primeName=0&privacy=0",
          CURLOPT_RETURNTRANSFER  => true,
          CURLOPT_ENCODING        => "",
          CURLOPT_MAXREDIRS       => 10,
          CURLOPT_TIMEOUT         => 30,
          CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST   => "POST", 
          CURLOPT_HTTPHEADER      => array(
                                    "authorization: ".$_SESSION['session_id']."",
                                    "cache-control: no-cache",
                                    "content-type: application/x-www-form-urlencoded",
                                    "postman-token: 6d1be134-4ae3-3588-b733-181d5c67829c"
                                    ),
                          ));

        $response                 = curl_exec($curl);
        $err                      = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            print_r('<pre>');
            print_r($response);
            print_r('</pre>');
            
        }
	}

  function get_mitel_config_data ($datasource, $connection_object) {
      mysql_select_db($datasource);
      $query              = "SELECT * from avcrm.config;";
      $run_query          = mysql_query($query, $connection_object);
      
      $config_data        = array();
      while ($response    = mysql_fetch_assoc($run_query)) {
          switch ($response['key']) {
            case 'mitel_server':
              $config_data ['mitel_server']           = $response['value'];
              break;
            case 'java_path':
             $config_data ['java_path']               = $response['value'];
              break;
            case 'mitel_app_name':
             $config_data ['mitel_app_name']          = $response['value'];
              break;  
            case 'mitel_company_name':
              $config_data ['mitel_company_name']     = $response['value'];
              break;  
            case 'mitel_local_password':
              $config_data ['mitel_local_password']   = $response['value']; 
              break;
            case 'mitel_icp_address':
              $config_data ['mitel_icp_address']      = $response['value']; 
              break;
            case 'mitel_database_id':
              $config_data ['mitel_database_id']      = $response['value'];
                break;        
            case 'mitel_app_password':
              $config_data ['mitel_app_password']     = $response['value'];
              break;   
            default:
              #### dont do nothing here #########
              break;
          }

      }
      //return back the array to the function call
      return $config_data;
  }
?>