<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!isset($_SESSION)) {
    session_start();
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_specresplists = "-1";
if (isset($_GET['respons_id'])) {
  $colname_specresplists = $_GET['respons_id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_specresplists = sprintf("SELECT cntresponsibility.*,contacts.*, tblcompanylist.comp_id, tblcompanylist.Company FROM cntresponsibility, contacts,tblcompanylist WHERE cntresponsibility.respons_id = %s AND contacts.id=cntresponsibility.cnt_id AND tblcompanylist.comp_id = contacts.company_id", GetSQLValueString($colname_specresplists, "int"));



$specresplists = mysql_query($query_specresplists, $CRMconnection) or die(mysql_error());
$row_specresplists = mysql_fetch_assoc($specresplists);

$totalRows_specresplists = mysql_num_rows($specresplists);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>
		<script src="js/jquery-1.9.1.min.js"></script>

<script src="js/jquery-migrate-1.1.1.min.js"></script>


		<script type="text/javascript" src="js/jquery-ui-1.8.4.custom.min.js"></script>		<script type="text/javascript" src="js/jquery.quicksearch.js">
        </script>
		<script type="text/javascript">
			$(function(){

				
				//stripe tables
				$('.stripeMe tr:even').addClass('alt');
				//for resplist
				$('#filterrlist').quicksearch('#responsiblist tbody tr');				


			});
			</script>
</head>

<body>
<table style="width:100%" id="responsiblist" class="stripeMe">
	<thead>
                <tr class="tabbhead">
                  <td>Name</td>
                  <td>Institution</td>
                  <td>Phone</td>
                  <td>Cell</td>
      </tr>
    </thead>
                <tbody>
                  <?php do { ?>
                    <tr>
                      <td><a href="clientdetails.php?cnt_Id=<?php echo $row_specresplists['id']; ?>"><?php echo $row_specresplists['first_name']; ?> <?php echo $row_specresplists['surname']; ?></a></td>
                      <?php if (!isset($_SESSION['user_extension'])) {?>
                      <td>
                        <a href="/companydetails-view.php?id=<?php echo $row_specresplists['comp_id']; ?>" title="Click to view Company"><?php echo $row_specresplists['Company']; ?></a></td>
                      <td><a href="#" onclick="$('#phonebox').load('http://<?php echo $_GET['user_phone_ip']; ?>/command.htm?number=<?php echo $row_specresplists['landline']; ?>')" > <?php echo $row_specresplists['landline']; ?></a></td>

                       <td align="right"><a href="#" onclick="$('#phonebox').load('http://<?php echo $_GET['user_phone_ip']; ?>/command.htm?number=<?php echo $row_specresplists['mobile']; ?>')"><?php echo $row_specresplists['mobile']; ?> </a></td>

                      <?php } else { ?>
                      <td><a href="javascript:void(0);" onclick="callMitel('<?php echo $_SESSION["user_extension"];?>','<?php echo $row_specresplists['landline'];?>');"><?php echo $row_specresplists['landline']; ?></a></td>

                        <td><a href="javascript:void(0);" onclick="callMitel('<?php echo $_SESSION["user_extension"];?>','<?php echo $row_specresplists['mobile'];?>');"><?php echo $row_specresplists['mobile']; ?></a></td>
                      <?php }?>
                      <!--
                      <td align="right"><a href="#" onclick="$('#phonebox').load('http://<?php echo $_GET['user_phone_ip']; ?>/command.htm?number=<?php echo $row_specresplists['mobile']; ?>')"><?php echo $row_specresplists['mobile']; ?> </a></td>
                      !-->
                  </tr>
                    <?php } while ($row_specresplists = mysql_fetch_assoc($specresplists)); ?>
                </tbody>
</table>
</body>
</html>
<?php
mysql_free_result($specresplists);
?>
