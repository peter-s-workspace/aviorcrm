<?php 
	require_once('Connections/CRMconnection.php'); 
	require_once('includes/event_log/eventlog.class.php'); 
	require_once('includes/classes/user.class.php');
?>
<?php
if (!isset($_SESSION)) {
  session_start();
}

function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 
  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}






if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

//variables specifying whether to show resp or rest list
$showmailsub="no";
$shownotmailsub="no";

$eventLogObj = new Eventlog();
$userObj = new User();

//add mail list
if ((isset($_GET["MM_addmailsub"])) && ($_GET["MM_addmailsub"] == "donow")) {

  $client_interest_before_inserts = isset( $_GET['contact_id'] ) ? $userObj->GetClientInterests( $_GET['contact_id'] ) : '';
  

  $insertSQL = sprintf("INSERT INTO contacts_interests (contact_id, interest_id) VALUES (%s, %s)",
                       GetSQLValueString($_GET['contact_id'], "int"),
					   GetSQLValueString($_GET['interest_id'], "int"));



  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());
  
  $sql = sprintf("SELECT CONCAT(contacts.first_name,' ',contacts.surname) AS 'user_fullname', tblcompanylist.Company AS 'cnt_company' FROM contacts, tblcompanylist WHERE contacts.company_id = tblcompanylist.comp_id AND contacts.id=%s", GetSQLValueString($_GET['contact_id'], "int"));
  mysql_select_db($database_CRMconnection, $CRMconnection);
  $userRS = mysql_query($sql, $CRMconnection) or die(mysql_error());		
  $user_fullname  = mysql_result($userRS,0,'user_fullname');
  $cnt_company = mysql_result($userRS,0,'cnt_company');
  if( $Result1 && $user_fullname ) {
	 //Add client to mailing list
	  $eventLogObj->save(array(
			'event' => 'ADD_CLIENT_TO_MAILINGLIST',
			'user' => $_SESSION['MM_Username'],
			'affected_user' => $user_fullname,
			'affected_type' => 'client',
			'company' => $cnt_company,
			'before' => $client_interest_before_inserts,
			'after' => $userObj->GetClientInterests( $_GET['contact_id'] ))
			);
  }
  $showmailsub="yes";
}

//remove from mail ist
if ((isset($_GET['removemail'])) && ($_GET['removemail'] != "")) {

  $client_interest_before_delete = isset( $_GET['contact_id'] ) ? $userObj->GetClientInterests( $_GET['contact_id'] ) : '';
  
  $deleteSQL = sprintf("DELETE FROM contacts_interests WHERE contact_id=%s AND interest_id=%s",
                       GetSQLValueString($_GET['contact_id'], "int"),GetSQLValueString($_GET['interest_id'], "int"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($deleteSQL, $CRMconnection) or die(mysql_error());	
  
 $sql = sprintf("SELECT CONCAT(contacts.first_name,' ',contacts.surname) AS 'user_fullname', tblcompanylist.Company AS 'cnt_company' FROM contacts , tblcompanylist WHERE contacts.company_id = tblcompanylist.comp_id AND contacts.id=%s", GetSQLValueString($_GET['contact_id'], "int"));
  mysql_select_db($database_CRMconnection, $CRMconnection);
  $userRS = mysql_query($sql, $CRMconnection) or die(mysql_error());		
  $user_fullname  = mysql_result($userRS,0,'user_fullname');
  $cnt_company = mysql_result($userRS,0,'cnt_company');
  if( $Result1 && $user_fullname ) {
	 //Remove client from mailing list
	  $eventLogObj->save(array(
			'event' => 'REMOVE_CLIENT_MAILINGLIST',
			'user' => $_SESSION['MM_Username'],
			'affected_user' => $user_fullname,
			'affected_type' => 'client',
			'company' => $cnt_company,
			'before' => $client_interest_before_delete,
			'after' => $userObj->GetClientInterests( $_GET['contact_id'] ))
			);
	 }
	 
  $shownotmailsub="yes";

}

$colname_userdet = "-1";
if (isset($_GET['contact_id'])) {
  $colname_userdet = $_GET['contact_id'];
} else if(isset($_GET['cnt_Id'])){
    $colname_userdet = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userdet = sprintf("SELECT * FROM contacts WHERE id = %s", GetSQLValueString($colname_userdet, "int"));
error_log($query_userdet);
$userdet = mysql_query($query_userdet, $CRMconnection) or die(mysql_error());
$row_userdet = mysql_fetch_assoc($userdet);
$totalRows_userdet = mysql_num_rows($userdet);



$contact_id = "-1";
if (isset($_GET['contact_id'])) {
  $contact_id = $_GET['contact_id'];
}else if(isset($_GET['cnt_Id'])){
    $contact_id = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsemail = sprintf("SELECT DISTINCT interests.id as `Interest_Id` , interests.description as `Interest_description` , contacts_interests.id as `ci_id` ,
              IF (ISNULL(contacts.drm_status), tblcompanylist.drm_status, contacts.drm_status) as drm_status
              FROM interests
              INNER JOIN contacts_interests
              ON interests.id = contacts_interests.interest_id
              INNER JOIN contacts
              ON contacts.id = contacts_interests.contact_id
              INNER JOIN tblcompanylist
              ON tblcompanylist.comp_id = contacts.company_id
              WHERE contacts_interests.contact_id = %s
              ORDER BY interests.description ASC", GetSQLValueString($contact_id, "int"));
$rsemail = mysql_query($query_rsemail, $CRMconnection) or die(mysql_error());
$row_rsemail = mysql_fetch_assoc($rsemail);
$totalRows_rsemail = mysql_num_rows($rsemail);


$contact_id = "-1";
if (isset($_GET['contact_id'])) {
  $contact_id = $_GET['contact_id'];
}else if(isset($_GET['cnt_Id'])){
   $contact_id = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsnotemail = sprintf("SELECT * FROM interests WHERE id NOT IN (SELECT interest_id FROM contacts_interests WHERE contact_id = %s) ORDER BY interests.description", GetSQLValueString($contact_id, "int"));
$rsnotemail = mysql_query($query_rsnotemail, $CRMconnection) or die(mysql_error());
$row_rsnotemail = mysql_fetch_assoc($rsnotemail);
$totalRows_rsnotemail = mysql_num_rows($rsnotemail);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>
</head>

<body>
<?php $isadmin = isset($_SESSION['MM_Username']) && (isAuthorized("x","Administrator", $_SESSION['MM_Username'], $_SESSION['MM_UserGroup']));?>  
<?php if ($showmailsub =="yes"){ ?>
<legend>Subscriptions</legend>
       <?php do { ?>
        <?php if($isadmin) { ?>
           <div id="ismailsub<?php echo $row_rsemail['Interest_Id']; ?>" class="clicklink" >  <a href="#" id="mailsub<?php echo $row_rsemail['Interest_Id']; ?>" onclick="console.log('#ismailsub<?php echo $row_rsemail['Interest_Id']; ?>');$('#ismailsub<?php echo $row_rsemail['Interest_Id']; ?>').remove();showloader('#isnotmailsub');$('#isnotmailsub').load('clientmailsubajax.php?contact_id=<?php echo $row_userdet['id']; ?>&interest_id=<?php echo $row_rsemail['Interest_Id']; ?>&removemail=yes')" ><?php echo $row_rsemail['Interest_description']; ?></a><span class="superscript">
    	   <?php if($row_rsemail['drm_status'] == 2){ echo "DRM" ; } else { echo "" ;}  ?></span></div>
       <?php }else { ?>
       <div id="ismailsub<?php echo $row_rsemail['Interest_Id']; ?>" class="clicklink" >  <?php echo $row_rsemail['Interest_description']; ?><span class="superscript">
         <?php if($row_rsemail['drm_status'] == 2){ echo "DRM" ; } else { echo "" ;}  ?></span></div>
       <?php } ?>
         <?php } while ($row_rsemail = mysql_fetch_assoc($rsemail)); ?>
 
      <?php }
		

if ($shownotmailsub =="yes"){
?>
 <legend>Other Available</legend>
      
	  <?php do { ?>

      <?php if($isadmin) { ?>
        <div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>" class="clicklink"> <a href="#"  onclick="$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>i').toggle();"> <?php echo $row_rsnotemail['description']; ?></a><div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>i" style="display:inline;">&nbsp;<button onclick="showloader('#ismailsub');doemailsubchange('<?php echo $row_userdet['id']; ?>','<?php echo $row_rsnotemail['id']; ?>', '#ismailsub' );$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>').remove()">Add</button></div><script language="javascript">$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>i').hide();</script></div>
      <?php }else { ?> 
        <div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>" class="clicklink"> <?php echo $row_rsnotemail['description']; ?><div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>i" style="display:inline;">&nbsp;<button onclick="showloader('#ismailsub');doemailsubchange('<?php echo $row_userdet['id']; ?>','<?php echo $row_rsnotemail['id']; ?>', '#ismailsub' );$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>').remove()">Add</button></div><script language="javascript">$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>i').hide();</script></div>
      <?php } ?>

        <?php } while ($row_rsnotemail = mysql_fetch_assoc($rsnotemail)); ?>
      <?php
}
?>
        


</body>
</html>
<?php
mysql_free_result($userdet);

mysql_free_result($rsemail);

mysql_free_result($rsnotemail);
?>
