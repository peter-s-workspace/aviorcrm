<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>

<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$maxRows_reminders = 10;
$pageNum_reminders = 0;
if (isset($_GET['pageNum_reminders'])) {
  $pageNum_reminders = $_GET['pageNum_reminders'];
}
$startRow_reminders = $pageNum_reminders * $maxRows_reminders;

$colname_reminders = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_reminders = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_reminders = sprintf("SELECT * FROM reminders WHERE user_id = %s ORDER BY remind_id DESC", GetSQLValueString($colname_reminders, "text"));
$query_limit_reminders = sprintf("%s LIMIT %d, %d", $query_reminders, $startRow_reminders, $maxRows_reminders);
$reminders = mysql_query($query_limit_reminders, $CRMconnection) or die(mysql_error());
$row_reminders = mysql_fetch_assoc($reminders);

if (isset($_GET['totalRows_reminders'])) {
  $totalRows_reminders = $_GET['totalRows_reminders'];
} else {
  $all_reminders = mysql_query($query_reminders);
  $totalRows_reminders = mysql_num_rows($all_reminders);
}
$totalPages_reminders = ceil($totalRows_reminders/$maxRows_reminders)-1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<table border="1">
  <tr>
    <td>user_id</td>
    <td>remind_topic</td>
    <td>remind_note</td>
    <td>remind_status</td>
    <td>remind_urgency</td>
  </tr>
  <?php do { ?>
    <tr>
      <td><?php echo $row_reminders['user_id']; ?></td>
      <td><?php echo $row_reminders['remind_topic']; ?></td>
      <td><?php echo $row_reminders['remind_note']; ?></td>
      <td><?php echo $row_reminders['remind_status']; ?></td>
      <td><?php echo $row_reminders['remind_urgency']; ?></td>
    </tr>
    <?php } while ($row_reminders = mysql_fetch_assoc($reminders)); ?>
</table>
</body>
</html>
<?php
mysql_free_result($reminders);
?>
