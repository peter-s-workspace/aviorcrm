<?php
# FileName="Connection_php_mysql.htm"
# Type="MYSQL"
# HTTP="true"
// first try relative to webroot
$platform_conf = '../platform.conf';

$platform = 'dev';
$hostname = 'aviorlib.co.za';
$schema = 'avcrm';

if (!function_exists('avlog')){
	function avlog($message) {
		error_log($message.PHP_EOL , 3, 'aviorcrm-error.log');	
	}
}

avlog('Scanning for platform.conf in: '.realpath($platform_conf));

if ( file_exists($platform_conf) ) {

	avlog('Found platform.conf!');
	
	$config = (array) json_decode(file_get_contents($platform_conf));
		
	if (isset($config['platform'])) {
		$platform = trim($config['platform']);
	}
	if (isset($config['schema'])) {
		$schema = trim($config['schema']);
	}

	avlog('platform: '.$platform);
	avlog('schema: '.$schema);

	if ($platform === 'production') {
		$hostname = 'aviorlib.co.za';
	}
	else if ($platform === 'staging') {
		$hostname = 'avior.cobiinteractive.com';
	}
	else if ($platform === 'dev') {
		$hostname = 'avior';
	}

}

$database_CRMconnection = $schema;

if ( isset($_ENV['AVOIR_DB_USER']) && isset($_ENV['AVOIR_DB_PASSWORD']) && isset($_ENV['AVOIR_DB_HOST']) ) {
	$username_CRMconnection = $_ENV['AVOIR_DB_USER'];
	$password_CRMconnection = $_ENV['AVOIR_DB_PASSWORD'];	
	$hostname_CRMconnection = $_ENV['AVOIR_DB_HOST'];
}
else {
	if ($platform === 'production') {
		$hostname_CRMconnection = "aviordbinstance.cgfvn3vetizv.eu-west-1.rds.amazonaws.com";
		$username_CRMconnection = "avior";
		$password_CRMconnection = "eye1%sivihath";
	}
	else if ( $platform === 'staging') {
		$hostname_CRMconnection = "127.0.0.1";
		$username_CRMconnection = "avior";
		$password_CRMconnection = "@v1OrSQLp@55";
	}
	else if ( $platform === 'dev') {
		$hostname_CRMconnection = "192.168.1.130";
		$username_CRMconnection = "root";
		$password_CRMconnection = "root";
	}
	else {
		$hostname_CRMconnection = "localhost";
		$username_CRMconnection = "root";
		$password_CRMconnection = "root";
	}	
}

$CRMconnection = mysql_connect($hostname_CRMconnection, $username_CRMconnection, $password_CRMconnection) or trigger_error(mysql_error(),E_USER_ERROR);

//Set data to utf8 because datatables for failing when loading all data
//mysql_query("SET NAMES 'utf8'", $CRMconnection);

?>
