<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";



// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>


<?php
if (!function_exists("GetSQLValueString")) {
    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
      if (PHP_VERSION < 6) {
        $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
      }

      $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

      switch ($theType) {
        case "text":
          $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
          break;
        case "long":
        case "int":
          $theValue = ($theValue != "") ? intval($theValue) : "NULL";
          break;
        case "double":
          $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
          break;
        case "date":
          $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
          break;
        case "defined":
          $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
          break;
      }
      return $theValue;
    }
}

$colname_userdets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userdets = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userdets = sprintf("SELECT * FROM tbluser WHERE user_name = %s", GetSQLValueString($colname_userdets, "text"));
$userdets = mysql_query($query_userdets, $CRMconnection) or die(mysql_error());
$row_userdets = mysql_fetch_assoc($userdets);
$totalRows_userdets = mysql_num_rows($userdets);


//now get the rest
 //or funct
 $ttd ='';
 $listmails ='';
 $fwdvars='';
if( isset($_GET['cnt_Interest']) ) {
	 foreach ($_GET['cnt_Interest'] as $i => $val)
	{ 
		//If user selected ALL and another option, ALL will still be returned. 
		if( $val != 'All'){
			$ttd .= sprintf("%s OR contacts_interests.interest_id=",GetSQLValueString($val, "text")) ;
		} else {
			$ttd .= sprintf("'' OR contacts_interests.interest_id !='' OR contacts_interests.interest_id=");
		}
		$fwdvars.= 'cnt_Interest[]='.urlencode($val).'&';
		
	}

    $arr = array();
    $count = 0;
    foreach ($_GET['cnt_Interest'] as $id ) {
        array_push($arr , $id);
    }

    $arr = join("," , $arr);

    $descriptionQuery = "SELECT DISTINCT description FROM interests WHERE id IN ($arr)";
    $result = mysql_query($descriptionQuery);
    $desc = array();
    while($res = mysql_fetch_array($result)){
        array_push($desc , $res['description']);
    }

	$listmails = implode(', ',$desc);
}


$ttd .="'asdfkhasncaskdfhaoehf'";  // just to fix up thing

$offset = "0";
if (isset($_GET['offset'])) {
  $offset = $_GET['offset'] > 0 ? $_GET['offset'] : 0; 
}


$tierNames = array( "Normal", "DRM", "Webview" );

$tierparam = "";
$tierString = "";
$tiervalout = "";

if (isset($_GET['tier'])) {

    $tiers = $_GET['tier'];

    $sep = "";
    $commaSep = "";
    $valSep = "";
    foreach ($tiers as $tierval) {
      //echo $tierval;

      $tierparam .= $sep."tier[]=".$tierval;
      $tierString .= $commaSep.$tierval;

      $tiervalout .= $valSep.$tierNames[$tierval-1];

      $sep = "&";
      $commaSep = ",";
      $valSep = ",and,";
    }   
    //echo $tierparam;
} 

$listSize = isset($_GET['lsize']) && $_GET['lsize'] > 0 ? $_GET['lsize'] : 25;

//Force 499 as a limit.
if ($listSize > 499) {
	$listSize = 499;
}


mysql_select_db($database_CRMconnection, $CRMconnection);
$ids = join(',' , $_GET['cnt_Interest']);
$query_rsmail1 = "SELECT DISTINCT Contacts.email FROM contacts as Contacts, contacts_interests as ContactsInterests, tblcompanylist as CompanyList
                                WHERE Contacts.id = ContactsInterests.contact_id 
                                AND Contacts.active = 1 
                                AND Contacts.company_id = CompanyList.comp_id
                                AND (IF (ISNULL(Contacts.drm_status), CompanyList.drm_status, Contacts.drm_status)) IN ($tierString) 
                                AND ContactsInterests.interest_id IN ($ids) order by Contacts.surname LIMIT $listSize OFFSET $offset";

$rsmail1 = mysql_query($query_rsmail1, $CRMconnection) or die(mysql_error());
$row_rsmail1 = mysql_fetch_assoc($rsmail1);
$totalRows_rsmail1 = mysql_num_rows($rsmail1);

$colname_rsmail2 =$ttd;

//echo $query_rsmail1;

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsmail2 = "SELECT DISTINCT Contacts.email FROM contacts as Contacts, contacts_interests as ContactsInterests, tblcompanylist as CompanyList
                                WHERE Contacts.id = ContactsInterests.contact_id 
                                AND Contacts.active = 1 
                                AND Contacts.company_id = CompanyList.comp_id
                                AND (IF (ISNULL(Contacts.drm_status), CompanyList.drm_status, Contacts.drm_status)) IN ($tierString) 
                                AND ContactsInterests.interest_id IN ($ids)  order by Contacts.surname";


$rsmail2 = mysql_query($query_rsmail2, $CRMconnection) or die(mysql_error());
$row_rsmail2 = mysql_fetch_assoc($rsmail2);
$totalRows_rsmail2 = mysql_num_rows($rsmail2);

?>
<script language="javascript">

function downloadcsv(){  
	    window.open("ajaxmaillistcsv.php?<?php echo $fwdvars; ?><?php echo $tierparam?>&lsize=<?php echo $listSize; ?>");
        return false;
	}
	
$(document).ready(function(){
		$('#listsize').keyup(function(){
			if($.isNumeric($(this).val()) === false || $(this).val() <= 0 ){
				$('#submit-listsize').attr('disabled',true);
				$('#list-size-error').html('');
			} else {
				$('#submit-listsize').removeAttr('disabled');
				if( $(this).val() > 499) {
					$('#submit-listsize').attr('disabled',true);
					$('#list-size-error').html('Please enter a value less than or equal to 499');
				} else {
					$('#list-size-error').html('');
				}
			}
		});
		
		$('#submit-listsize').click(function(){
		
			var listSize = $('#listsize').val();
			var fwdvars = '<?php echo $fwdvars; ?>';
			var stier = '<?php echo $stier; ?>';
			var etier = '<?php echo $etier; ?>';
			$('#mail25').load("mailmanagerload25.php?"+fwdvars+"&<?php echo $tierparam?>&lsize="+listSize);
			
		});
});

</script>

<legend>Mail List</legend> 
<p>
  <input type="submit" value="Back to mail lists" onclick="location.reload();" /><?php if ($row_userdets['user_level']=="Administrator"){?><button id="downcsv" onclick="downloadcsv()"> Download CSV</button><?php };?>
</p>
<p><b>Selected Lists</b>: <?php echo $listmails; ?></p>
<p><b>Protection Status</b> 
<?php
    $tier_names = "";
    if (strpos($tierString, ",") !== false) {
        $explode_tier = explode("," , $tierString); 

        foreach ($explode_tier as $key => $value) {
          if ($value == 1) {
            $tier_names [] = "Watermark";
          } else if ( $value == 2) {
            $tier_names [] = "DRM";
          } else {
            $tier_names [] = "Web Viewer";
          }
        }

    } else {
        switch ($tierString) {
          case 1:
            $tier_names = "Watermark";
            break;
          case 2:
            $tier_names = "DRM";
            break;
          case 3:
            $tier_names = "Web Viewer";
             break; 
          default:
            break;
        }
    }
    
    $remove_duplicates = array_unique($tier_names);
    if (is_array($remove_duplicates)) {
        switch (count($remove_duplicates)) {
          case '2':
            echo  $remove_duplicates[0] . " AND ". $remove_duplicates[1];
            break;
          case '3':
            echo  $remove_duplicates[0] . " AND ". $remove_duplicates[1] . " AND " .$remove_duplicates[2];
          break;
          default:
            echo $remove_duplicates[0];
            break;
        }
        
    } else {
          echo $tier_names;  
    }
?>
</p>
<table>
  <tr><td><textarea onFocus="this.select()" cols="100" rows="9"><?php do { ?><?php echo $row_rsmail1['email']; ?>;<?php } while ($row_rsmail1 = mysql_fetch_assoc($rsmail1)); ?>
  </textarea></td>
  </tr>
  <tr><td align="center">
  <table width="100%" border="0" cellpadding="3" cellspacing="3"><tr><td width="25%">&nbsp;
 <?php if ($offset > 0) { // Show if not first page ?>
          <a style="clicklink" href="#" onclick="showloader('#mail25');$('#mail25').load('mailmanagerload25.php?<?php echo $fwdvars; ?>&<?php echo $tierparam?>&lsize=<?php echo $listSize; ?>')">First</a>
          <?php }// Show if not first page ?></td><td width="25%">&nbsp;
      <?php if ($offset > 0) { // Show if not first page ?>
          <a style="clicklink" href="#" onclick="showloader('#mail25');$('#mail25').load('mailmanagerload25.php?<?php echo $fwdvars; ?>&<?php echo $tierparam?>&offset=<?php echo $offset-$listSize; ?>&lsize=<?php echo $listSize; ?>')">Previous</a>
          <?php } // Show if not first page ?></td><td width="25%">&nbsp;
      <?php if ($offset +$listSize < $totalRows_rsmail2) { // Show if not last page ?>
          <a style="clicklink" href="#" onclick="showloader('#mail25');$('#mail25').load('mailmanagerload25.php?<?php echo $fwdvars; ?>&<?php echo $tierparam?>&offset=<?php echo $offset+$listSize; ?>&lsize=<?php echo $listSize; ?>')">Next</a>
          <?php } // Show if not last page ?></td><td width="25%">&nbsp;
      <?php if ($offset +$listSize < $totalRows_rsmail2) { // Show if not last page ?>
          <a style="clicklink" href="#" onclick="showloader('#mail25');$('#mail25').load('mailmanagerload25.php?<?php echo $fwdvars; ?>&<?php echo $tierparam?>&offset=<?php echo ($totalRows_rsmail2 - $listSize); ?>&lsize=<?php echo $listSize; ?>')">Last</a>
          <?php } // Show if not last page ?></td>
    </tr>
    </table>
  </td>
  </tr>
  <tr><td align="center"> Showing <?php echo $offset+1; ?> to <?php echo min($offset+$listSize,$totalRows_rsmail2); ?> of <?php echo $totalRows_rsmail2 ?>  </td></tr>
  <tr>
  	<td align="center">List Size: <input type="text"  name="listsize" id="listsize" value="<?php echo $listSize; ?>" /><input type="button" name="submit-listsize" disabled="disabled" id="submit-listsize" value="Get List" />
	<p id="list-size-error" class="error"></p>
  </td></tr>
  
</table>

<?php


mysql_free_result($rsmail1);

mysql_free_result($rsmail2);

mysql_free_result($userdets);

?>
