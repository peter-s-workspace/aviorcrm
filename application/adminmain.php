<?php 
	require_once('Connections/CRMconnection.php'); 
	require_once('includes/classes/company.class.php');
	require_once('includes/event_log/eventlog.class.php'); 
	require_once('includes/classes/user.class.php');
	require_once('includes/classes/company.class.php');
	require_once('includes/classes/hipchat.class.php');
?>
<?php
if (!isset($_SESSION)) {
  session_start();
}

################### Hipchat room  logic here #######################
$new_hipchat_instance 	= new hipchat();
$hip_chat_rooms 	 	= $new_hipchat_instance->get_hipchat_rooms();

//echo $hip_chat_rooms;
//var_dump($get_hipchat_users);
################## End Logic Here ################################
/*==============================
	Developer Note:
	References to institution refer to company/companies in the db
 ===============================	
*/

$MM_authorizedUsers = "Administrator";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}


$eventLogObj = new Eventlog();
$userObj = new User();

# If we have redirected to adminmain after an add user action
# Need to set companyObj with id to allow for display of the company profile picture
if(isset( $_GET['comp'] ))
{
 	$companyObj = new Company($_GET['comp']);
}
else
{
	$companyObj = new Company();
}



$companyDetails = $companyObj->getCompanyDetails();
$companyTypes = $companyObj->getCompanyTypes();
$geographyTypes = $companyObj->getCompanyGeographyTypes();
$companyStatus = $companyObj->getCompanyStatuses();
$companyTiers = $companyObj->getCompanyTiers();

$aviorCompanyName = "Avior Capital Markets (Pty) Ltd";
$aviorClientList = $userObj->GetClientListByCompanyName($aviorCompanyName);
$aviorClientListSize = sizeof($aviorClientList);

$geography_option  = "<select id = 'company_geo' name='company_geo'>";
$geography_option .= "<option selected value=''>Select Geography Type </option>";
foreach ($geographyTypes as $key => $value) {
		$geo_name = $geographyTypes[$key]['name'];
		$geo_id   = $geographyTypes[$key]['id_geography'];
		$geography_option .= "<option value = $geo_id> $geo_name </option>";	

}
$geography_option .= "</select>";

//$clientList = $userObj->GetClientList();

if( isset( $_GET['export_log'] ) && $_GET['export_log'] == 1 ) {
	$eventLogObj->ExportLog();	
}

if( isset( $_GET['export_client'] ) && $_GET['export_client'] == 1 ) {
	$userObj->ExportAllClient();	
}

if( isset( $_GET['export_kam'] ) && $_GET['export_kam'] == 1 ) {
	$companyObj->ExportKAM();	
}


if( isset( $_GET['export_company'] ) && $_GET['export_company'] == 1 ) {
	$companyObj->ExportAllCompany();	
}

if (isset($_GET['export_rooms']) && $_GET['export_rooms'] == 1) {
	$companyObj->ExportAllRooms();

}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

require_once('includes/abovehead.php');





$editFormAction = $_SERVER['PHP_SELF'];

if (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != '') {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$colname_userdet = '';

if (isset($_POST["MM_insert"]) && $_POST["MM_insert"] == "addclient") {
 if( isset($_POST['cnt_Fname']) && trim($_POST['cnt_Fname']) != ''  && isset($_POST['cnt_Lastname']) && trim($_POST['cnt_Lastname']	) != ''
 && isset($_POST['cnt_email']) && trim($_POST['cnt_email']	) != '' && isset($_POST['cnt_company']) && trim($_POST['cnt_company']) != '') {
	  //cnt_blmuser - Will have to be removed once users start using the new bloomberg status.
	  $_POST['cnt_blmuser'] = '';	

	  error_log("ATTEMPTING INSERT...", 3, "error.log");

	  //check if the user has been added to the company to avoid duplicate
	  $contact_email 		= trim($_POST['cnt_email']);
	  $company_id 			= trim($_POST['cnt_company']);
	  $check_user_exists 	= "select count(*) as email_counter from contacts where email = '$contact_email' and company_id = $company_id";
	  mysql_select_db($database_CRMconnection, $CRMconnection);
	  $run_check 			= mysql_query($check_user_exists,$CRMconnection) or die(mysql_error());
	  $get_results 			= mysql_fetch_array($run_check);

	  if ($get_results['email_counter'] > 0) {
	  		 $duplicate = "true";
	  		 echo $duplicate;
	  } else {

	 foreach ($_POST as $key => $value) {
	 	if (strpos($key, "_hipchat")) {
	 		$hipchat_new [] = $value;
	 	}
	 }

	 error_log("CONSTRUCTING SQL...", 3, "error.log");


	  $insertSQL = sprintf("INSERT INTO contacts (first_name, surname, landline, fax, mobile, email, company_id, jobtitle, bloomberg_user, bloomberg_status, note, dob , active, drm_status) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 1, %s)",
						   GetSQLValueString(trim($_POST['cnt_Fname']), "text"),
						   GetSQLValueString(trim($_POST['cnt_Lastname']), "text"),
						   GetSQLValueString(trim($_POST['cnt_phone']), "text"),
						   GetSQLValueString(trim($_POST['cnt_fax']), "text"),
						   GetSQLValueString(trim($_POST['cnt_cell']), "text"),
						   GetSQLValueString(trim($_POST['cnt_email']), "text"),
						   GetSQLValueString(trim($_POST['cnt_company']), "text"),
						   GetSQLValueString(trim($_POST['cnt_jobtitle']), "text"),
						   GetSQLValueString(trim($_POST['cnt_blmuser']), "text"),
						   GetSQLValueString(trim($_POST['cnt_bloomberg_status']), "text"),
						   GetSQLValueString(trim($_POST['cnt_note']), "text"),
						   GetSQLValueString($_POST['cnt_bdate'], "date"),
						   GetSQLValueString($_POST['cnt_override_drm'], "text")						   
						   );

	  error_log($insertSQL, 3, "error.log");

	  mysql_select_db($database_CRMconnection, $CRMconnection);
	  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());

	  //Add new client
	  if( $Result1 ) {
		error_log("INSERT SUCCEEDED!", 3, "error.log");
		  
		$client_id = mysql_insert_id();

		if( $client_id > 0 ) {
				if( trim($_POST['phone_num']) != '' ) {
					$insertSQL = sprintf("INSERT INTO addphone (cnt_id, phone_num) VALUES (%s, %s)",
                       GetSQLValueString($client_id, "int"),
                       GetSQLValueString($_POST['phone_num'], "text"));

					  mysql_select_db($database_CRMconnection, $CRMconnection);
					  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());
				}

				##### insert the hipchat rooms for the client ##########
				foreach ($hipchat_new as $key => $value) {
					# code...
					$insert_query 	= "INSERT INTO tblclient_hipchatrooms (client_id, hipchat_room_id) VALUES  (".$client_id.",".$value.")";
					
					$run_query 		= mysql_query($insert_query);
					if (!$run_query) {
						die( "Error Saving the data" . mysql_error($CRMconnection));
					}
				}
				//Upload file if exists.
				if ((isset($_FILES["Filedata"]))){

					if (!empty($_FILES) && isset($client_id) && isset($_FILES["Filedata"]['name']) && isset($_FILES["Filedata"]['tmp_name'] ) &&  $_FILES["Filedata"]['size'] > 0 ) {

						$tempFile = is_array($_FILES['Filedata']['tmp_name']) ? $_FILES['Filedata']['tmp_name'][0] : $_FILES['Filedata']['tmp_name'];
						//$targetPath = $_SERVER['DOCUMENT_ROOT'].'/uploads/clientp/'.$client_id.'/';
						$targetPath = 'uploads/clientp/'.$client_id.'/';
						$fileDataName = is_array($_FILES['Filedata']['name']) ? $_FILES['Filedata']['name'][0] : $_FILES['Filedata']['name'];
						$targetFile =  str_replace('//','/',$targetPath) . $fileDataName;
						if( $tempFile != '' && $fileDataName != '' ) {
							//make directory if it doens not yet exist		
							if(!is_dir($targetPath)) {
								mkdir($targetPath, 0777);
							} 
							
							move_uploaded_file($tempFile,$targetFile);
							include("js/resize-class.php");
							
							//resize the file quickly
							$width = 150;
							$height = 150;
							// *** 1) Initialize / load image  
							$resizeObj = new resize($targetFile);  
							
							// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)  
							$resizeObj -> resizeImage($width,$height, 'auto');  
							
							// *** 3) Save image  
							$resizeObj -> saveImage($targetPath."1.jpg", 100);  
							//remove original
							unlink($targetFile);
						}
				}
			}
		}
	  }
		
	  $eventLogObj->save(array(
			'event' => 'ADD_CLIENT',
			'user' => $_SESSION['MM_Username'],
			'affected_user' => $_POST['cnt_Fname'].' '.$_POST['cnt_Lastname'],
			'affected_type' => 'client',
			'company' => $_POST['cnt_company'],
			'after'=> $userObj->setType('client')->GetReadableFields($_POST)
			));
		
		}
		$_POST['MM_insert_error'] = 0;
		$_POST['MM_insert_success'] = 0;
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$_POST['MM_insert_success'] = 1;
			echo  $client_id;
			exit;
			
		} else {
		  header('location: clientdetails.php?cnt_Id='.$client_id); 
		  exit;
		}
	}  else {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			echo "error"; 
			exit();
		} else {
			$add_new_client_error = "<p>Please fill in all required fields.</p>";
			$_POST['MM_insert_error'] = 1;
			}
		}
	}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "addcomp") && trim($_POST['Company']) != '') {

  //check if the company has not been added before
  $company_website 	= filter_var($_POST['company_website'], FILTER_SANITIZE_STRING);
  $company_name 	= filter_var($_POST['company'],FILTER_SANITIZE_STRING);

  $check_query  = "select count(*) as counter from tblcompanylist where company = '$company_name' and website = '$company_website'";
  mysql_select_db($database_CRMconnection);
  $run_query 	= mysql_query($check_query, $CRMconnection);
  $results 		= mysql_fetch_array($run_query);

  if ($results['counter'] > 0) {
  		$duplicate = "true";	
  } else {

	  $insertSQL    = sprintf("INSERT INTO tblcompanylist 
		(id_companytier,id_companytype,id_geography,Company, drm_status, id_companystatus, id_kam1, id_kam2, id_dealer1, id_dealer2, address_1, address_2, address_3, address_4, switchboard_1, switchboard_2, website, social_fb, social_twitter, social_linkedin) 
	  	VALUES (%s,%s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s)",
				    GetSQLValueString($_POST['company_tier'], "int"),
				    GetSQLValueString($_POST['company_type'], "int"),
				    GetSQLValueString($_POST['company_geo'], "int"),
                    GetSQLValueString($_POST['Company'], "text"),
                    GetSQLValueString($_POST['company_drm_setting'], "int"),
				    GetSQLValueString($_POST['company_status'], "int"),
		
					GetSQLValueString($_POST['company_kam1'], "int"),
					GetSQLValueString($_POST['company_kam2'], "int"),
					GetSQLValueString($_POST['company_dealer1'], "int"),
					GetSQLValueString($_POST['company_dealer2'], "int"),

					// GetSQLValueString($_POST['drm_status'], "int"),
	
					GetSQLValueString($_POST['address_1'], "text"),
					GetSQLValueString($_POST['address_2'], "text"),
					GetSQLValueString($_POST['address_3'], "text"),
					GetSQLValueString($_POST['address_4'], "text"),
					GetSQLValueString($_POST['switchboard_1'], "text"),
					GetSQLValueString($_POST['switchboard_2'], "text"),
					GetSQLValueString($_POST['company_website'], "text"),
					GetSQLValueString($_POST['social_fb'], "text"),
					GetSQLValueString($_POST['social_twitter'], "text"),
					GetSQLValueString($_POST['social_linkedin'], "text")
					);
						
              

	  $relocation = 'location: adminmain.php';
	  $compoostvar = $_POST['Company'];

	  mysql_select_db($database_CRMconnection, $CRMconnection);
	  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());
  }
  # If successfully inserted in db:
  # Query db for created company id and add it to url parameters.
  # On submission of Add Company form, this will redirect user to the correct edit form.
  if($Result1)
  {

	  $Result2 =  mysql_query("SELECT comp_id FROM tblcompanylist WHERE Company='".$compoostvar."'");
	  $id = mysql_fetch_assoc($Result2);
	  $id = $id['comp_id'];

	  ################ add the hipchat rooms #########
	  $hipchat_rooms 	= $_POST['hipchat_rooms'];
	  foreach ($hipchat_rooms as $key => $value) {
		  	$insert_hipchat_rooms 	= "INSERT INTO tblcompany_hipchatrooms (company_id, hipchat_room_id) VALUES ($id, $value);";
		  	$run_insert_query 		= mysql_query($insert_hipchat_rooms, $CRMconnection);
		  	if (!$run_insert_query) {
		  		die("Error inserting the rooms " . mysql_error());
		  	}
	  }
	  ################ end logic here #####################

	  # Previous relocation path. If client wants to switch back to this at a later stage
	  //$relocation = 'location: adminmain.php?comp='.$id;
	  $relocation = 'location: companydetails-view.php?id='.$id;
  	  rename("./uploads/companyp/".$_POST['hex_code']."/", "./uploads/companyp/".$id."/");

  }
    header($relocation);
    exit();
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "updatename") && trim($_POST['oldcompname']) != '' && trim($_POST['newcompname']) != '') {
  
  //change the company name in the table
  $insertSQL = sprintf("UPDATE tblcompanylist SET  Company= %s WHERE Company =%s",
                       GetSQLValueString($_POST['newcompname'], "text"),
                       GetSQLValueString($_POST['oldcompname'], "text"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());  
  
  header('location: adminmain.php');
  exit();
  //done
}

//changing tier
if ((isset($_POST["chgtier"])) && ($_POST["chgtier"] == "yes")) {

  //change the company Tier
  $insertSQL = sprintf("UPDATE tblcompanylist SET  drm_status= %s WHERE Company =%s",
                       GetSQLValueString($_POST['newtier'], "int"),
                       GetSQLValueString($_POST['chgcomp'], "text"));

  mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result1 = mysql_query($insertSQL, $CRMconnection) or die(mysql_error());  
   
  
  header('location: adminmain.php'); 
  exit();
}

$query_compnames = "SELECT DISTINCT
						tblcompanylist.*,tblcompanytier.companytier, tblcompanystatus.company_status,tblcompanytype.companytype
					FROM tblcompanylist 
					LEFT OUTER JOIN tblcompanytier ON tblcompanytier.id_companytier = tblcompanylist.id_companytier 
					LEFT OUTER JOIN tblcompanystatus ON tblcompanystatus.id_companystatus = tblcompanylist.id_companystatus
					LEFT OUTER JOIN tblcompanytype ON tblcompanytype.id_companytype = tblcompanylist.id_companytype
					ORDER BY  tblcompanylist.Company";
mysql_select_db($database_CRMconnection, $CRMconnection);
$compnames = mysql_query($query_compnames, $CRMconnection) or die(mysql_error());
if( $compnames ) {
	while ($row = mysql_fetch_assoc($compnames)) {
		$companyData[] = $row;
	}
}
//var_dump($companyData);
mysql_free_result($compnames);

//var_dump($companyData);
require_once('includes/sitevars.php');


#################### Get Chat Rooms For Editing #############
/*mysql_select_db($database_CRMconnection, $CRMconnection);
$get_rooms_query 		 	 = "SELECT * FROM avcrm.chat_rooms";
$run_query 					 = mysql_query($get_rooms_query, $CRMconnection);

$select_option 				 = "<select id='chat_rooms'>";
$select_option 				.= "<option selected value=''>Select room</option>";	
while ($row = mysql_fetch_array($run_query)) {
		$chat_room_id 		 = $row['id'];
		$chat_room_name 	 = $row['chat_room_name'];
		$chat_room_token	 = $row['chat_room_token'];
		$select_option 		.= "<option value = $chat_room_id".':'. $chat_room_token.':'.$chat_room_name.">".$chat_room_name."</option>";

}
$select_option 				.= "</select>"; */

#################### Get Chat Rooms For Editing #############
$get_company_hipchat_room_id 	= "SELECT hr.hipchat_room_id , cl.Company, CONCAT(ct.companytier,' ','Clients') 							   AS companytier FROM tblcompanylist cl
								   LEFT JOIN tblcompany_hipchatrooms hr ON cl.comp_id = hr.company_id
								   INNER JOIN tblcompanytier ct ON cl.id_companytier = ct.id_companytier
								   ORDER by cl.Company ASC
								";

mysql_select_db($database_CRMconnection);
$run_query 						= mysql_query($get_company_hipchat_room_id, $CRMconnection) or die('Error running the query ' . mysql_error());
$company_hipchat_id 			= array();
while ($row 					= mysql_fetch_array($run_query)) {
	$company					= $row['Company'];
		if (isset($prev_company) && $company 	== $prev_company) {
			$company_hipchat_id[$company][] = $row['hipchat_room_id'];
			$company_hipchat_id[$company][] = $row['companytier'];
		} else {
			$company_hipchat_id[$company][] = $row['hipchat_room_id'];
			$company_hipchat_id[$company][] = $row['companytier'];
		}
	$prev_company 				= $company;
}
######################### Client rooms ##################################################
$get_client_hipchat_room_id 	= "SELECT hr.hipchat_room_id , CONCAT(c.first_name,' ', c.surname) 
								   AS full_name , CONCAT(ct.companytier ,' ','Clients') as companytier FROM contacts c
								   LEFT JOIN tblclient_hipchatrooms hr ON  c.id = hr.client_id
								   INNER JOIN tblcompanylist cl ON c.company_id = cl.comp_id
								   INNER JOIN tblcompanytier ct ON cl.id_companytier = ct.id_companytier
								   ORDER by c.id ASC
												";
			mysql_select_db($database_CRMconnection);
			$run_client_query 						= mysql_query($get_client_hipchat_room_id, $CRMconnection) or die('Error running the query ' . mysql_error());
			$client_hipchat_id 				= array();

			while ($row 					= mysql_fetch_array($run_client_query)) {
				$client_name			    = $row['full_name'];
					if (isset($prev_client) && $client_name 		== $prev_client) {
						$client_hipchat_id[$client_name][]  = $row['hipchat_room_id'];
						$client_hipchat_id[$client_name][]  = $row['companytier'];
					} else {
						$client_hipchat_id[$client_name][]  = $row['hipchat_room_id'];
						$client_hipchat_id[$client_name][]  = $row['companytier'];
					}
				$prev_client 				= $client_name;
			}			
######################### EndClient rooms ##################################################
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" !-->
<head>
<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
<link rel="manifest" href="favicons/manifest.json">
<link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" !-->
<title>Avior Capital Markets CRM</title>

<!-- InstanceEndEditable !-->
<!-- InstanceBeginEditable name="head" !-->


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
 <link type="text/css" href="css/kdes1/jquery-ui-1.9.2.custom.css" rel="stylesheet" />		
<link type="text/css" href="css/dropzone.css" rel="stylesheet" />
<link type="text/css" href="css/uploadify.css" rel="stylesheet" />

<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet">

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="js/dropzone.js"></script>
<script type="text/javascript" src="js/jquery.validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/my-dropzone-picturedrop.js"></script>
<script type="text/javascript" src="js/adminmain.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery.linkify.min.js"></script>
<!-- <script type="text/javascript" src="js/companydetails.js"></script>
<script type="text/javascript" src="js/jquery.validate/jquery.validate.min.js"></script> !-->

<!-- InstanceEndEditable !-->
<script language="javascript">
 	var duplicate = "<?php if(isset($duplicate)) { echo $duplicate; }?>";
 	if (duplicate != "") {
 		$("#duplicate").dialog('open');
 		$("div#message").html("The Company already exists"); 		
 	}

	$(function(){
		
		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					},
					appendTo: '.quick-search-container'
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);
				
					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
	})
</script>	
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>

<div id="duplicate" title="Warning">  
  <div id ="message"></div>
</div>

<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			<div class="quick-search-container">
					<input type="text" id="quicksearch" size="16" />
			</div>
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" !-->
<div class="content60"> 
	<div id="tabs1"> 
		<ul>
			<li><a href="#tabs1-1">Client Admin</a></li>
			<li><a href="#tabs1-3">Institution Admin</a></li>
			<li><a href="#tabs1-4">User Admin</a></li>
			<!-- <li><a href="#tabs1-4">Company Colour Codes</a></li>!-->
			<!-- <li><a href="#tabs1-5">Mailing Lists</a></li>!-->
			<li><a href="#tabs1-6">Reports</a></li>
			<li><a href="#tabs1-7">Hipchat Rooms</a></li>
		</ul>
	<div id="tabs1-1">
		<fieldset>
			<legend>Client Admin</legend>
			<p><a href="javascript://" id="add_new_client_link"> Add New Client</a> | <a href="javascript://" id="edit_client_link" > Edit Client</a></p>
		</fieldset>
		<br />
		<div id="addnewclient">
			<fieldset>
				<legend>Add New Client</legend>
				<div id="error-messages"><?php if( isset($add_new_client_error)) echo $add_new_client_error; ?></div>
				<div id="success-messages"><?php if( isset($add_new_client_success)) echo $add_new_client_success; ?></div>
				
				<p class="required-field">* Required fields</p>
				<form action="adminmain.php" method="post" name="my-form-dropzone"  id="my-form-dropzone" class="dropzone" enctype="multipart/form-data">
					<table align="left" class="edit-table">
						<tr valign="baseline">
							<td nowrap="nowrap" align="right"><span class="required-field">*</span>Firstname:</td>
							<td><input type="text" name="cnt_Fname" id="cnt_Fname" value="<?php if( isset($_POST["cnt_Fname"])) echo $_POST["cnt_Fname"]; ?>" size="22" />
							<span class="validation-error"></span>
							</td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right"><span class="required-field">*</span>Lastname:</td>
							<td><input type="text" name="cnt_Lastname" id="cnt_Lastname" value="<?php if( isset($_POST["cnt_Lastname"])) echo $_POST["cnt_Lastname"]; ?>" size="22" />
							<span class="validation-error"></span>							
							</td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right">Phone</td>
							<td><input type="text" name="cnt_phone" id="cnt_phone" value="<?php if( isset($_POST["cnt_phone"])) echo $_POST["cnt_phone"]; ?>" size="22" /></td>
						</tr>
						<tr valign="baseline" class="more_phone_num">
							<td nowrap="nowrap" align="right"></td>
							<td><input type="text" name="phone_num" value="<?php if( isset($_POST["phone_num"])) echo $_POST["phone_num"]; ?>" size="22" /></td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right">Fax:</td>
							<td><input type="text" name="cnt_fax" value="<?php if( isset($_POST["cnt_fax"])) echo $_POST["cnt_fax"]; ?>" size="22" /></td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right">Cell:</td>
							<td><input type="text" name="cnt_cell" value="<?php if( isset($_POST["cnt_cell"])) echo $_POST["cnt_cell"]; ?>" size="22" /></td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right"><span class="required-field">*</span>Email:</td>
							<td><input type="text" name="cnt_email" id="cnt_email" value="<?php if( isset($_POST["cnt_email"])) echo $_POST["cnt_email"]; ?>" size="22" />
								<span class="validation-error"></span>
							</td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right"><span class="required-field">*</span>Institution:</td>
							<td>
								<select name="cnt_company" id="cnt_company">
								<option value="">Select Institution</option>	
								<?php for( $x=0; $x < sizeof($companyData);$x++ ) { ?>
								<option data-tier="<?php echo $companyData[$x]['companytier']?>" value="<?php echo $companyData[$x]['comp_id']?>" ><?php echo $companyData[$x]['Company']?></option>
								<?php } ?>
								</select>
								<span class="validation-error"></span>
							</td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right"><span class="required-field">*</span>HipChat Room:</td>
							<td>
							
			        		<select class="js-example-basic-multiple" name="hipchat_rooms[]" multiple="multiple" id="hipchat">
			        			<?php
					 			$available_rooms = $new_hipchat_instance->get_all_rooms();
			    				for($i =0 ; $i <count($available_rooms); $i++) {
			    					
			    					echo "<option value=".$available_rooms[$i]->getId().">".$available_rooms[$i]->getName()."</option>";
			    				}
					    		?>
							</select>
		    
					        <span class="validation-error"></span>
					   
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td id="selected_company_tier"></td>
						</tr>
						<tr id = "rooms" style="display:none;">
							<td>&nbsp;</td>
							<td id="hipchat_rooms"></td>	
						</tr>
						<tr>
							<td nowrap="nowrap" align="right">&nbsp;</td>
							<td nowrap="nowrap" align="left" colspan="2">
							<div id="cpicture" class="dz-message" style="text-align:center; padding-bottom:3px;">
							  <div id="picturedrop" class="picturedrop">
								<p class="my-drop-message dz-message">Drop or Click<br />here<br />add picture</p>
								<div class="fallback">
									<p>Please use the fallback form below to upload your files.</p>
									<input type="file" multiple="multiple" name="Filedata[0]">
								</div>	  
							  </div>
							  
							</div>
						</td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right">Jobtitle:</td>
							<td><input type="text" name="cnt_jobtitle" value="<?php if( isset($_POST["cnt_jobtitle"])) echo $_POST["cnt_jobtitle"]; ?>" size="22" /></td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right">Protection Type</td>
							<td>
								<select name="cnt_override_drm" id="cnt_override_drm">
										<option value="" <?php echo ''; ?>>Company Protection Type</option>
										<option value="1" <?php echo '1'; ?>>Normal Protection</option>
										<option value="2" <?php echo '2'; ?>>DRM Protection</option>
										<option value="3" <?php echo '3'; ?>>Web Viewer</option>
								</select>
							</td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right">Bloomberg User:</td>
							<td>
								  <?php 
									  $bloombergUserYes = isset($_POST['cnt_bloomberg_status']) && $_POST['cnt_bloomberg_status'] == '1' ? 'selected="selected"' : '';
									  $bloombergUserNo = isset($_POST['cnt_bloomberg_status']) && $_POST['cnt_bloomberg_status'] == '0' ? 'selected="selected"' : '';
									  $bloombergUserNotSet = ($bloombergUserYes == '' && $bloombergUserNo == '' ) ? 'selected="selected"' : '';
								  ?>
									<select name="cnt_bloomberg_status" id="cnt_bloomberg_status">
										<option value="" <?php echo $bloombergUserNotSet; ?>></option>
										<option value="0" <?php echo $bloombergUserNo; ?>>No</option>
										<option value="1" <?php echo $bloombergUserYes; ?>>Yes</option>
									</select>
							</td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right">Notes:</td>
							<td><textarea name="cnt_note" cols="18"><?php if( isset($_POST["cnt_note"])) echo $_POST["cnt_note"]; ?></textarea></td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right">Birthday:</td>
							<td><input type="text" name="cnt_bdate" id="cnt_bdate" value="<?php if( isset($_POST["cnt_bdate"])) echo $_POST["cnt_bdate"]; ?>" size="22" readonly="readonly" /></td>
						</tr>
						<tr valign="baseline">
							<td nowrap="nowrap" align="right"><input type="button"  id="btncanceladdnewc" value="Cancel" /></td>
							<td><input type="submit" name="saveNewClient" id="saveNewClient" value="Save" onClick="this.disabled=false; return false;" /></td>
						</tr>
				</table>
					<input type="hidden" name="MM_insert" value="addclient" />
					<input type="hidden" name="MM_clientID" id="MM_clientID" value="<?php if( isset($_POST['MM_clientID']) && $_POST['MM_clientID'] > 1) echo $_POST['MM_clientID']; else echo 0; ?>" />
					<input type="hidden" name="MM_insert_error" id="MM_insert_error" value="<?php if( isset($_POST['MM_insert_error']) && $_POST['MM_insert_error'] == 1) echo $_POST['MM_insert_error']; else echo 0; ?>" />
					<input type="hidden" name="MM_insert_success" id="MM_insert_success" value="<?php if( isset($_POST['MM_insert_success']) && $_POST['MM_insert_success'] == 1) echo $_POST['MM_insert_success']; else echo 0; ?>" />
			</form>
			
		</fieldset>
		</div>
	<div id="removeclient"></div>
	<div id="editclient">
		<fieldset>
			<legend>Edit Client</legend>
			<form method="get" action="clientdetails-edit.php">
				<table>
					<tr>
						<td>Client:</td>
						<td><input type="text"  name="cnt_search" id="cnt_search" value="" />
							<input type="hidden" id="cnt_Id" name="cnt_Id" value="" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="button" name="btncanceleditclient" id="btncanceleditclient" value="Cancel" />&nbsp;&nbsp; 
							<input type="submit" id="btnsubmitEditClient" value="Edit" disabled="disabled"  />
						</td>
					</tr>
				</table>
			</form>
		</fieldset>
		</div>
	</div>
	 <div id="tabs1-3">
	 	<fieldset>
			<legend>Institution Admin</legend> 
			 <p><a href="javascript://" id="add-new-company" title="Add new company">Add New Institution</a>&nbsp;|&nbsp;<a href="javascript://" id="edit-company" title="Edit Company">Edit Institution</a></p>
		</fieldset> 
		<br />

		<fieldset id="add-new-company-container">
			<legend>Add company</legend>
			<form method="post" action="<?php echo $editFormAction; ?>" name="frmaddcomp" id="frmaddcomp" class="edit-form">
				<table align="left" class="edit-table">
					<tr valign="baseline">
						<td nowrap="nowrap" align="right">Institution:</td>
						<td><input type="text" name="Company" value="" size="22" />
							<span class="validation-error"></span>
						</td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" align="right">Tier:</td>
						<td>
						<select id="company_tier" name="company_tier">
							<option value="">Select Institution Tier</option>
							<?php for($x=0; $x < sizeof($companyTiers); $x++ ){ ?>
								<option value="<?php echo $companyTiers[$x]['id_companytier'];?>"><?php echo $companyTiers[$x]['companytier'];?></option>
							<?php }?>
						</select>
						<span class="validation-error"></span>
						</td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" align="right">Institution Type:</td>
						<td>
						<select id="company_type" name="company_type">
							<option value="">Select Institution Type</option>
							<?php for($x=0; $x < sizeof($companyTypes); $x++ ) { ?>
								<option value="<?php echo $companyTypes[$x]['id_companytype']; ?>"><?php echo $companyTypes[$x]['companytype']; ?></option>	
							<?php } ?>
						</select>
						<span class="validation-error"></span>
						</td>
					</tr>
					<!-- add geography logic !-->
						<tr valign="baseline">
						<td nowrap="nowrap" align="right">Geography Type:</td>
						<td>
									<?php echo $geography_option;?>
									<span class="validation-error"></span>
						</td>
					</tr>
					</tr>
					<!-- add geography logic !-->
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">Hipchat Room:</td>
						<td>
						<select class="js-example-basic-multiple" name="hipchat_rooms[]" multiple="multiple">
		        			<?php
					 			$available_rooms = $new_hipchat_instance->get_all_rooms();
								
			    				for($i =0 ; $i <count($available_rooms); $i++) {	
			    					echo "<option value=".$available_rooms[$i]->getId().">".$available_rooms[$i]->getName()."</option>";
			    				}
				    	
				    		?>
						</select>
						</td>
					</tr>
					<!-- end adding !-->
					<tr valign="baseline">
							<td nowrap="nowrap" class="label">Status:</td>
							<td>
								<?php for($x=0; $x < sizeof($companyStatus); $x++ ) { 

									// Default is set on add 
									// New dbs entries will never run the else if
									// Used to handle legacy nulls in the db
									// Suggested to be removed at later stage.
									$selected_status = ''; 
									
									if($companyStatus[$x]['company_status']=="Prospect" )
									{
										//Set the default of status to Prospect
										$selected_status = 'checked="checked"';
									}	
									
									?>
									<label><input type="radio" id="company_status" name="company_status" value="<?php echo $companyStatus[$x]['id_companystatus'];?>" <?php echo $selected_status ?> />&nbsp;<?php echo $companyStatus[$x]['company_status'];?></label><br />
								<?php } ?>
							</td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">Address 1:</td>
						<td><input type="text" id="address_1" name="address_1" value="" /></td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">Address 2:</td>
						<td><input type="text" id="address_2" name="address_2" value="" /></td>
					</tr>
					<tr valign="baseline">
						<td  nowrap="nowrap"class="label">Address 3:</td>
						<td><input type="text" id="address_3" name="address_3" value="" /></td>
					</tr>
					<tr valign="baseline">
						<td  nowrap="nowrap" class="label">Address 4:</td>
						<td><input type="text" id="address_4" name="address_4" value="" /></td>
					</tr>
					<tr  valign="baseline">

							<?php
							$hex_var = '';
							for( $i=0; $i<20; $i++ ) {
							   $hex_var .= chr( rand( 65, 90 ) );
							}?>
													    		

							<td  nowrap="nowrap" align="left" colspan="4">
                                <?php if(isset($companyDetails[0]['id']))
                                { ?>
                                    <input type="hidden" name="company_image" id="company_image" value="<?php if(file_exists("uploads/companyp/".$companyDetails[0]['id']."/1.jpg"))  echo "uploads/companyp/".$companyDetails[0]['id']."/1.jpg"; ?>" >
                                    <input type="hidden" name="company_imagesize" id="company_imagesize" value="<?php if(file_exists("uploads/companyp/".$companyDetails[0]['id']."/1.jpg"))  echo filesize("uploads/companyp/".$companyDetails[0]['id']."/1.jpg"); ?>" >
                                <?php }
                                else
                                { ?>
                                    <input type="hidden" name="company_image" id="company_image" value="" >
                                    <input type="hidden" name="company_imagesize" id="company_imagesize" value="" >
                                <?php } ?>
								<input type="hidden" id="hex_code" name="hex_code" value="<?php echo $hex_var;?>" />

							<div id="cpicture2" class="dz-message" style="text-align:center; padding-bottom:3px;">
							  <div id="picturedrop2" class="picturedrop">
								<p class="my-drop-message dz-message">Drop or Click<br />here<br />add picture</p>
									  
							  </div>
							  
							</div>
						
						</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">Switchboard 1</td>
						<td><input type="text" id="switchboard_1" name="switchboard_1" value="" /></td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">Switchboard 2</td>
						<td><input type="text" id="switchboard_2" name="switchboard_2" value="" /></td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">Website:</td>
						<td><input type="text" id="company_website" name="company_website" value="" />
						<span class="validation-error"></span>
						</td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">Facebook:</td>
						<td><input type="text" id="social_fb" name="social_fb" value="" />
							<span class="validation-error"></span>
						</td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">Twitter:</td>
						<td><input type="text" id="social_twitter" name="social_twitter" value="" />
						<span class="validation-error"></span>
						</td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">LinkedIn:</td>
						<td><input type="text" id="social_linkedin" name="social_linkedin" value="" />
						<span class="validation-error"></span>
						</td>
					</tr>
					<?php if( $aviorClientListSize > 0 ) { ?>
					<trvalign="baseline">
						<td nowrap="nowrap" class="label">KAM 1:</td>
						<td>
							<select id="company_kam1" name="company_kam1">
								<option value="">Select KAM 1</option>
								<?php for( $x=0; $x < $aviorClientListSize; $x++ ){
									$selected_kam1 = isset($companyDetails[0]['id_kam1']) && $companyDetails[0]['id_kam1'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?>
									<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_kam1; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">KAM 2:</td>
						<td>
							<select id="company_kam2" name="company_kam2">
								<option value="">Select KAM 2</option>
								<?php for( $x=0; $x < $aviorClientListSize; $x++ ){ 
									$selected_kam2 = isset($companyDetails[0]['id_kam2']) && $companyDetails[0]['id_kam2'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?>
									<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_kam2; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>
								<?php } ?>
							</select></td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">Dealer 1:</td>
						<td>
							<select id="company_dealer1" name="company_dealer1">
								<option value="">Select Dealer 1</option>
								<?php 
									$selected_csa_only = isset($companyDetails[0]['id_dealer1']) && $companyDetails[0]['id_dealer1'] == -1 ? 'selected="selected"': "";
								?>
								<option value="-1" <?php echo $selected_csa_only; ?>>CSA Only</option>	
								<?php for( $x=0; $x < $aviorClientListSize; $x++ ){ 
									$selected_delear1 = isset($companyDetails[0]['id_dealer1']) && $companyDetails[0]['id_dealer1'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?>
									<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_delear1; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>
								<?php } ?>
								
								
							</select>
						</td>
					</tr>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">Dealer 2:</td>
						<td>
							<select id="company_dealer2" name="company_dealer2">
								<option value="">Select Dealer 2</option>
								<?php for( $x=0; $x < $aviorClientListSize; $x++ ){ 
									$selected_delear2 = isset($companyDetails[0]['id_dealer2']) && $companyDetails[0]['id_dealer2'] == $aviorClientList[$x]['id'] ? 'selected="selected"': "";?>
									<option value="<?php echo $aviorClientList[$x]['id']; ?>" <?php echo $selected_delear2; ?>><?php echo $aviorClientList[$x]['first_name'].' '.$aviorClientList[$x]['surname']; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<?php } else { ?>
					<tr>
						<td colspan="2">Please <a href="adminmain.php" title="" >add</a> Avior Employees.</td>
					</tr>
					<?php } ?>
					<tr valign="baseline">
						<td nowrap="nowrap" class="label">Select Protection Type:</td>
						<td>
							<select id="company_drm_setting" name="company_drm_setting">
								<?php $selected_drm = isset($companyDetails[0]['drm_status']) ? $companyDetails[0]['drm_status'] : ''; ?>
								<option value="1" <?php if( $selected_drm == 1 ) echo 'selected="selected"'; ?> >Watermark</option>
								<option value="2" <?php if( $selected_drm == 2 ) echo 'selected="selected"'; ?>>DRM</option>
								<option value="3" <?php if( $selected_drm == 3 ) echo 'selected="selected"'; ?>>Web Viewer</option>
							</select>
						<br />
						<small> Please note - changing the tier here will change the e-mail tiering for all clients on all mailing lists at this specific institution - use with caution - or eaher contact Dirk before using it.</small>
						</td>
					</tr>
					
					<tr valign="baseline">
						<td nowrap="nowrap" align="right">&nbsp;</td>
						<td><input type="submit" value="Add Institution" /></td>
					</tr>
				</table>
				<input type="hidden" name="CompanyTier" value="1" />
				<input type="hidden" name="MM_insert" value="addcomp" />
			</form>
			<br />
		</fieldset>
		<br />
		<fieldset id="edit-company-container">
			<legend>Edit Institution</legend>
			<form method="get" action="companydetails-edit.php" name="editCompanyForm" id="editCompanyForm">
			  <table align="left">
				<tr valign="baseline">
				  <td nowrap="nowrap" align="right">Institution:</td>
				  <td>
					<select name="id" id="id">
						<option value="">Select Institution</option>
						<?php for( $x=0; $x < sizeof($companyData);$x++ ) { ?>
						<option value="<?php echo $companyData[$x]['comp_id']?>"><?php echo $companyData[$x]['Company']?></option>
						<?php } ?>
					  </select>
				  </td>
				</tr>
				<tr valign="baseline">
				  <td nowrap="nowrap" align="right">&nbsp;</td>
				  <td><input type="submit" value="Edit Institution" /></td>
				</tr>
			  </table>
			  </form>
				<br />
			  </tr>
		</fieldset>
		<br />
		<fieldset id="edit-company-form" style="margin-bottom:20px;"></fieldset>

		<fieldset id="rename-company-container">
			<legend>Rename Institution</legend>
			<form method="post" action="<?php echo $editFormAction; ?>">
			
			  <p>Select institution to rename:<br />
			  <select name="oldcompname" id="oldcompname">
			  	<option value=""></option>
			  <?php for( $x=0; $x < sizeof($companyData);$x++ ) { ?>
				<option value="<?php echo $companyData[$x]['Company']?>"><?php echo $companyData[$x]['Company']?></option>  
			  <?php } ?>
		  </select>
			  </p>
			  New Name:
			  <br />
			  <input name="newcompname" type="text" size="22" />
			  <input type="submit" name="btnsavenewname" id="btnsavenewname" value="Submit" />
				<input type="hidden" name="MM_insert" value="updatename" />
			  </form>
			<br />			  
		</fieldset>
		<br />
		<fieldset id="edit-company-drm">
			<legend>Institution Protection settings</legend>
			<p>please note- changing the tier here will change the e-mail tiering for all clients on all mailing lists at this specific institution - use with caution - or rather contact Dirk before using it </p>
			<form  action="<?php echo $editFormAction; ?>" method="post" name="form3" id="form3">
				<p>Select Institution:<br />
				<select name="chgcomp" id="chgcomp">
					<?php for( $x=0; $x < sizeof($companyData);$x++ ) { ?>
					<option value="<?php echo $companyData[$x]['comp_id']?>"><?php echo $companyData[$x]['Company']?></option>
					<?php } ?>
				</select>
				</p>
				<p>Select Protection Type :<br />
				<select name="newtier" id="newtier">
					<option value="1">Watermark</option>
					<option value="2">DRM</option>
					<option value ="3">Web Viewer</option>
				</select>
				</p>
				<input type="submit" value="Save change" />
				<input type="hidden" name="chgtier" value="yes" />
			</form>
			<br />
		</fieldset>
		 <br />
		</div>
		<div id="tabs1-4">
			<fieldset id="edit-user-admin">
			
			</fieldset>
		</div>
		<!-- 
		  <div id="tabs1-5">
			  <fieldset>
					<legend>Mailing List Admin</legend>   
					<p>&nbsp;</p>
			  </fieldset>
		  </div>
		  !-->
		  <div id="tabs1-6">
			<fieldset>
				<legend>Reports</legend>   
				<ul>
					<li><a href="adminmain.php?export_client=1">Export Client List (CSV)</a></li>
					<li><a href="adminmain.php?export_log=1">Export Logs (CSV)</a></li>
					<li><a href="adminmain.php?export_kam=1">Export KAM information (CSV)</a></li>
					<li><a href="adminmain.php?export_rooms=1">Export Hipchat Rooms(CSV)</a></li>
				</ul>
		  </fieldset>
		  </div>
		  <div id="tabs1-7">
				  	<fieldset>
						<legend>Hipchat Admin</legend>
						<p><a href="javascript://" id="add_chat_room"> Add New Hipchat room</a></p>
					</fieldset>
					<br/>
					<fieldset id ="chat_room_insert">
							<legend>Add New Room</legend>
							<p class="required-field">* Required fields</p>
							<div id="add_new_chat_room">
							    <form id="addchatroomform" name="addchatroomform">
							    <table>
							    <tr valign="baseline">
							        <td nowrap="nowrap" align="right"><span class="required-field">*</span>Chatroom Name:</td>
							        <td><input type="text" name="chat_room_name" id="chat_room_name" value="" size="32" /></td>
							        <span class="validation-error"></span>
							    </tr>
							    <!--
							    <tr valign="baseline">
							        <td nowrap="nowrap" align="right"><span class="required-field">*</span>Chatroom Token:</td>
							        <td><input type="text" name="chat_room_token" id="chat_room_token" value="" size="32" /></td>
							        <span class="validation-error"></span>
							    </tr> !-->
							    <tr valign="baseline">
									<td nowrap="nowrap" align="right">&nbsp;</td>
									<td><input type="submit" value="Submit" id="saveChatRoom" />&nbsp; <button type="button" id="closeChat">Cancel</button></td>
								</tr>
								
								<input type="hidden" value="insert" name="post_type"/>	
							    </table>
							    	
							    </form>

							    <hr>

							    <div style="padding-bottom:20px;"></div>
							    <fieldset>
							    	<legend> Available Hipchat Rooms</legend>
							    	<?php
							 			$available_rooms = $new_hipchat_instance->get_all_rooms();

										$data_array 	= array();
										foreach ($company_hipchat_id as $key1 => $value1) {
											foreach ($value1 as $key => $value) {
												foreach ($available_rooms as $room_key => $room_name) {

							 							if ( $room_name->getId() == $value || strtolower($room_name->getName()) == strtolower($value)) {
							 								$data_array[$room_name->getName()][] = $key1;
							 							} else {
							 								if ($room_name->getName() != $old_key) {
							 									
							 									$data_array [$room_name->getName()][]= $room_name->getName();
							 								}
							 								$old_key 	= $room_name->getName();
							 							}
							 							
							 						}
							 					
											}
										}

										$client_data_array = array();
											foreach ($client_hipchat_id as $key1 => $value1) {
												foreach ($value1 as $key => $value) {
													foreach ($available_rooms as $room_key => $room_name) {

							 							if ( $room_name->getId() == $value || strtolower($room_name->getName()) == strtolower($value)) 
								 							{
								 								$client_data_array[$room_name->getName()][] = $key1;
								 							} else {
								 								if ($room_name->getName() != $old_key) {
								 									
								 									$client_data_array [$room_name->getName()][]= $room_name->getName();
								 								}
								 								$old_key 	= $room_name->getName();
								 							}
								 							
								 						}
								 					
												}
											}
					    			
										$hipchat_rooms_table  = "<table id='hipchatrooms' class='display' cellspacing='0' width='100%''>";
										$hipchat_rooms_table .= "<thead>";
										$hipchat_rooms_table .= "<tr>";
										$hipchat_rooms_table .= "<th>Hipchat Room</th>";
										$hipchat_rooms_table .= "<th>Company(s)</th>";
										$hipchat_rooms_table .= "<th>Client(s)</th>";
										$hipchat_rooms_table .= "</tr>";
										$hipchat_rooms_table .= "</thead>";
										$hipchat_rooms_table .= "<tbody>";

										foreach ($data_array as $key => $value) {
										
											if (count(array_unique($value)) == 1 && count(array_unique($client_data_array[$key])) == 1) {
												$hipchat_rooms_table .= "<tr>";
												$hipchat_rooms_table .= "<td>".$key."</td>";
												$hipchat_rooms_table .= "<td></td>";
												$hipchat_rooms_table .= "<td></td>";
												$hipchat_rooms_table .= "</tr>";

											} else {
												$format_data = str_replace($key,',', implode(",",array_unique($value)));
												$companys 	 = preg_replace('/,{2,}/', ',', trim($format_data, ','));

												$format_client_data  =  str_replace($key,',', implode(",",array_unique($client_data_array[$key])));
												$client 			 =  preg_replace('/,{2,}/', ',', trim($format_client_data, ','));

												$hipchat_rooms_table .= "<tr>";
												$hipchat_rooms_table .= "<td>".$key."</td>";
												$hipchat_rooms_table .= "<td class='dt-right'>".$companys."</td>";
												$hipchat_rooms_table .= "<td class='dt-right'>".$client."</td>";
												$hipchat_rooms_table .= "</tr>";
												
											}
											
										}
										$hipchat_rooms_table .= "</tbody>";
										$hipchat_rooms_table .= "</table>";

										echo $hipchat_rooms_table;

							    	?>
							    </fieldset>
							  
							</div>
					</fieldset>
				<!-- end the code here  !-->

		  </div>
		 </div>
	
</div>


<!-- <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script src="js/jquery-migrate-1.1.1.min.js"></script>
 !--><!-- <script type="text/javascript" src="js/jquery.linkify.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>	!-->

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<script type="text/javascript" src="js/companydetails.js"></script>
<script type="text/javascript" src="js/jquery.validate/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

			
			$('.js-example-basic-multiple').select2();
			 $('#hipchatrooms').DataTable({
			 	dom: 'Bfrtip',
				        buttons: [
				             'csv','excel','print'
				        ]
			 });

		

			$('#frmaddcomp').validate({
            errorClass:'validate-error',
            errorPlacement: function(error, element) {		
                error.appendTo( element.parents('tr').find("span.validation-error").addClass('error') );													
            },
            rules: {
				company_name: { required: true },
				company_tier: { required: true },
				company_type: { required: true },
				company_website: { required: false, url: true },
				social_fb: { required: false, url: true },
				social_twitter: { required: false, url: true },
				social_linkedin: { required: false, url: true }
				
				
            },
            messages: {		
				company_name: { required: 'Please enter Institution Name' },
				company_tier: { required: 'Please select Tier' },
				social_fb: { url: 'Please enter a valid URL starting with http(s)://' },
				social_twitter: { url: 'Please enter a valid URL starting with http(s)://' },
				social_linkedin: { url: 'Please enter a valid URL starting with http(s)://' }
            }
        });

		
		var company_image = $('#company_image').val();
		var company_imagesize = $('#company_imagesize').val();
		var file_dropped = 0;


		var companymyDropzone2 = new Dropzone("div#picturedrop2", 
			{ url: "js/uploadifyclientp.php?folder=/uploads/companyp/<?php echo  $hex_var;?>",
			addRemoveLinks :true,
			acceptedFiles: 'image/*',
			forceFallback: false,
			paramName:'Filedata',
			dictFallbackText: 'Please use the fallback form below to upload your files',
			dictRemoveFile: 'Clear Picture',
			maxFiles:1
			});

		if($('.dz-browser-not-supported').length == 0) {

				companymyDropzone2.on("removedfile", function(file) {

					  if (!file.name) { return; }
						 companymyDropzone2.removeAllFiles(true);
						 companymyDropzone2.options.maxFiles = 1;
						 $('.my-drop-message').show();
						 $(".dz-error, .error").remove();
						 $.post("js/dropzone-remove-file.php",{cid:'<?php echo  $hex_var;?>'}); 
					}).on("drop",function(file,error){
						  $('#cpicture2').append('<div class="notice error"><em>Note: Images need to be saved to desktop first before dropping</em></div>');
						});

				if( company_image != '' ) {
					$('.my-drop-message').hide();
					var mockFile = { name: "1.jpg", size: company_imagesize };
					companymyDropzone2.emit("addedfile", mockFile);
					companymyDropzone2.emit("thumbnail", mockFile, company_image);
					companymyDropzone2.emit("complete", mockFile);
					var existingFileCount = 1; 
					companymyDropzone2.options.maxFiles = companymyDropzone2.options.maxFiles - existingFileCount;

				}
				companymyDropzone2.on("error", function(file,error) {
					
					$(".error").remove();
					$(".dz-error").hide();
					if( error == 'You can not upload any more files.' ) {
						$('#cpicture2').append('<div class="error">You can not upload any more files. <br />Please clear image first</div>');
					} else {
						$('#cpicture2').append('<div class="error">'+error+'</div>');
					}
				});
				
				companymyDropzone2.on("processing", function(file){
					$('.notice').remove();
					$('.my-drop-message').hide();
				});
			}  else {
				$('.my-drop-message').hide();
				$('.dz-browser-not-supported').append('<div id="fallback-preview"></div>');
					if (company_image != '') {
						$('#fallback-preview').html('<img src="'+company_image+'" alt="Preview" /><br /><a id="fallback-removefile" href="#">Remove File</a>');
						$('.dz-browser-not-supported form').hide();
					} else {
						$('#picturedrop2 .dz-fallback').html('<p>Please upload your image using the normal upload <a href="companydetails-edit.php?id=<?php if(isset($companyDetails[0]['id'])){echo $companyDetails[0]['id'];}else{echo "";} ?>" target="_blank" style="text-decoration:underline;" title="Click to Edit Institution">here</a>.</p>');
					}

					$('#fallback-removefile').click(function(){
						$('.dz-browser-not-supported form').show();
						$.post("js/dropzone-remove-file.php",{cid:'<?php if(isset($companyDetails[0]['id'])){echo $companyDetails[0]['id'];}else{echo "";} ?>'});
						$('#fallback-preview img, #fallback-removefile').hide();
						return false;
					});
				}

			


	});
</script>


</body>
<!-- InstanceEnd !--></html>