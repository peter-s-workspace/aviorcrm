<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsShares = "SELECT * FROM compliance_sharelist ORDER BY sharecode ASC";
$rsShares = mysql_query($query_rsShares, $CRMconnection) or die(mysql_error());
$row_rsShares = mysql_fetch_assoc($rsShares);
$totalRows_rsShares = mysql_num_rows($rsShares);
?>
<?php require_once('includes/sitevars.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><link type="text/css" href="css/kdes1/jquery-ui-1.8.4.custom.css" rel="stylesheet" />		
		<script src="js/jquery-1.9.1.min.js"></script>

<script src="js/jquery-migrate-1.1.1.min.js"></script>


		<script type="text/javascript" src="js/jquery-ui-1.8.4.custom.min.js"></script>
		<script type="text/javascript">
				function showloader(targetid){
					$(targetid).empty().html('<img src="images/ajax-loader.gif" />');
				}
			$(function(){

				// Accordion
				//$("#accordion").accordion({ header: "h3" });
	
				// Tabs
				$('#tabs1').tabs();


				//for loading custom client lists
				$('#sharelist').change( function () {
  				var tmp =$('#sharelist').val();
				showloader('#shareresult');
				$('#shareresult').load('complianceq1.php?shareid='+tmp)
});

				
				
			});
		</script>

<!-- InstanceEndEditable -->
<script language="javascript">
	$(function(){
		
		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					}
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);
				
					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
	})
</script>	
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			<div>
				<fieldset>
					<Legend>Quick Search</Legend>
					<input type="text" id="quicksearch" size="16" />
				</fieldset>
			</div> 
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<div class="content40">
<div id="tabs1">
      <ul>
        <li><a href="#tabs1-1">Share ownership</a></li>
        <li><a href="#tabs1-2">Share trading</a></li>
        <li><a href="#tabs1-3">Third</a></li>
      </ul>
      <div id="tabs1-1">
      <fieldset>
      <legend>Share ownership</legend>
      Select share:
             <select name="sharelist" id="sharelist">
          <option value="None" <?php if (!(strcmp("None", "None"))) {echo "selected=\"selected\"";} ?>></option>
          <?php
do {  
?>
          <option value="<?php echo $row_rsShares['share_id']?>"<?php if (!(strcmp($row_rsShares['share_id'], "None"))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsShares['sharecode']?></option>
          <?php
} while ($row_rsShares = mysql_fetch_assoc($rsShares));
  $rows = mysql_num_rows($rsShares);
  if($rows > 0) {
      mysql_data_seek($rsShares, 0);
	  $row_rsShares = mysql_fetch_assoc($rsShares);
  }
?>
        </select> 
      </fieldset>
      <fieldset>
      <div id="shareresult"></div>
      </fieldset>
      
    </div>
      <div id="tabs1-2">
            <fieldset>
      <legend>Share Trading</legend>
      Select share:  or  <button id="addshare"> Add</button> if not on the list.
      
      </fieldset>
      <fieldset>
      <div id="sharetraderesult"></div>
      </fieldset>
</div>
      <div id="tabs1-3">Nam dui erat, auctor a, dignissim quis, sollicitudin eu, felis. Pellentesque nisi urna, interdum eget, sagittis et, consequat vestibulum, lacus. Mauris porttitor ullamcorper augue.</div>
</div>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsShares);
?>
