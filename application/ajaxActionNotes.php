<?php

require_once('Connections/CRMconnection.php'); 
require_once('includes/classes/notes.class.php');

if (!isset($_SESSION)) {
  session_start();
}
/**
* Check if user session is set.
*/
if (isset($_SESSION['MM_UserGroup']) && isset( $_SESSION['MM_Username'])) {
	
	$notesObj = new Notes();
	/**
	* Check if addnote button is clicked.
	**/
	if (isset($_POST['MM_noteadd']) && $_POST['MM_noteadd'] == 'addnote') {
		/*Get All data */
		$notesData = array();
		
		/* Get data only if there's an actual note. */
		if(isset($_POST['note_txt']) && trim($_POST['note_txt']) != '') { 
			$notesData['note_text'] = isset($_POST['note_txt']) ? trim($_POST['note_txt']) : '';
			$notesData['note_date'] = isset($_POST['note_date']) ? trim($_POST['note_date']) : '';
			$notesData['note_type'] = isset($_POST['note_type']) ? trim($_POST['note_type']) : '';
			$notesData['note_time'] = isset($_POST['note_time']) ? trim($_POST['note_time']) : '';
			$notesData['note_by'] = isset($_POST['note_by']) ? trim($_POST['note_by']) : '';
			$notesData['notebyNick'] = isset($_POST['notebyNick']) ? trim($_POST['notebyNick']) : '';
			$notesData['note_cnt_Id'] = isset($_POST['note_cnt_Id']) ? trim($_POST['note_cnt_Id']) : '';
			$notesData['note_cnt_name'] = isset($_POST['note_cnt_name']) ? trim($_POST['note_cnt_name']) : '';
			$notesData['cntCompany'] = isset($_POST['cntCompany']) ? trim($_POST['cntCompany']) : '';
			if (isset($_POST['analyst_list']) && !empty($_POST['analyst_list'])) {
				$notesData['note_by'] = trim($_POST['analyst_list']);
			}

			$returnData = $notesObj->add($notesData);
			$noteId = $returnData['note_id'];

			$userNote = $notesObj->getUserNote($noteId);
			echo json_encode($userNote,true);
			exit();
		} 
	} else if (isset($_POST['action']) && $_POST['action'] == 'delete' && isset($_POST['noteId']) && $_POST['noteId'] > 0) {
		/* Delete Note by ID */
		$noteId = $_POST['noteId'];
		$result = $notesObj->remove($noteId);
		echo $result;
	} else if (isset($_POST['action']) && $_POST['action'] == 'update' && isset($_POST['noteId']) && $_POST['noteId'] > 0 && isset($_POST['noteText']) && trim($_POST['noteText']) != '') {
		/* Update Note by ID */
		$noteText = $_POST['noteText'];
		$noteId = $_POST['noteId'];
		$result = $notesObj->update($noteId, $noteText);
		echo $result; 
	} else if (isset($_POST['action']) && $_POST['action'] == 'update-note' && isset($_POST['clientId']) && $_POST['clientId'] > 0 && isset($_POST['noteText']) && trim($_POST['noteText']) != '') {

		$noteText = $_POST['noteText'];
		$clientId = $_POST['clientId'];
		$result = $notesObj->updateClientNote($clientId, $noteText);
		echo $result;
	}
	
}