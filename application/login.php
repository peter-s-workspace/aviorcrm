<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];

if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['username'])) {
  $loginUsername=$_POST['username'];
  $password=sha1($_POST['password']);
  $MM_fldUserAuthorization = "user_level";
  $MM_redirectLoginSuccess = "index.php";
  $MM_redirectLoginFailed = "login.php";
  $MM_redirecttoReferrer = true;
  mysql_select_db($database_CRMconnection, $CRMconnection);
  	
  $LoginRS__query=sprintf("SELECT user_name,user_email, user_pword, user_level FROM tbluser WHERE user_status = 1 AND (user_name=%s OR user_email=%s) AND user_pword=%s",
  GetSQLValueString($loginUsername, "text"),GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  
  $LoginRS = mysql_query($LoginRS__query, $CRMconnection) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
 
 $rs="Failure";
  if ($loginFoundUser) {
    $rs= "Success";
    $loginStrGroup  = mysql_result($LoginRS,0,'user_level');
    $loginUsernameb = mysql_result($LoginRS,0,'user_name');
    $loginUserEmail = mysql_result($LoginRS,0,'user_email');
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsernameb;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      
    $_SESSION['MM_UserEmail'] = $loginUserEmail;
  //log the username request, and ip
  $logentry=sprintf("INSERT INTO aclog (username, ip, success) VALUES (%s, %s, %s)",GetSQLValueString($loginUsername, "text"), GetSQLValueString($_SERVER['REMOTE_ADDR'], "text"),$rs);
    mysql_select_db($database_CRMconnection, $CRMconnection);
  $Result2 = mysql_query($logentry, $CRMconnection) or die(mysql_error());
  
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
?>
<?php require_once('includes/sitevars.php'); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
<link rel="manifest" href="favicons/manifest.json">
<link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>
<link type="text/css" href="css/kdes1/jquery-ui-1.8.4.custom.css" rel="stylesheet" />	
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.4.custom.min.js"></script>
		<script type="text/javascript">
			$(function(){
				// Tabs
				$('#tabs1').tabs();
				
			})
</script>
	


<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>

<body>

<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
<p style="clear:both">

<div id="tabs1" style="width:280px;">
      <ul>
        <li><a href="#tabs1-1">Login</a></li>
      </ul>
      <div id="tabs1-1">

      <form METHOD="POST" name="loginuser" id="loginuser" action="<?php echo $loginFormAction; ?>">
      <table>
      <tr><td>Username</td><td><input type="text" name="username" id="username"  autofocus/></td></tr>
      <tr><td>Password</td><td><input type="password" name="password" id="password" /></td></tr>
      <tr>
        <td colspan="2" align="right"><input name="submit" type="submit" value="Go" id="submit" /></td></tr>
      </table>
      </form>
      </div>
    </div>

<p style="clear:both; margin-bottom:20px;">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<?php
if ($schema != 'avcrm') {
?>
<td style="text-align:center; padding:0;">
	<a href="/crm"><img src="images/logos/logo-crm.jpg" alt="Institutional CRM" style="padding:10px;" /><br />Institutional CRM</a> <br>
</td>
<?php
}
?>

<?php
if ($schema != 'avcrm_cf') {
?>
<td style="text-align:center; padding:0;">
	<a href="/crm_cf"><img src="images/logos/logo-ccrm.jpg" alt="Corporate Finance CRM" style="padding:10px;" /><br />Corporate Finance CRM</a> <br>
</td>  
<?php
}
?>

<?php
if ($schema != 'crm_pc') {
?>
<td style="text-align:center; padding:0;">
<a href="/crm_pc"><img src="images/logos/logo-pcrm.jpg" alt="Private Finance CRM" style="padding:10px;" /><br />Private Client CRM</a>
</td>
<?php
}
?>

</tr>
</table>

</body>
</html>