  var feedUrl = "http://www.google.com/calendar/feeds/asu.edu_q065bkoducouhbr42c83548dgk%40group.calendar.google.com/public/full?orderby=starttime&sortorder=ascending&futureevents=true&max-results=30";
  // Atom feed for the Google Doodle calendar

  // Google is a little fuzzy about its birthday:
  // http://www.google.com/support/bin/answer.py?answer=4866
  var startDate = new Date(1998, 9, 1);
  var endDate = new Date();

  var getParams = '?start-min=' + convertToGDataDate(startDate) +
                  '&start-max=' + convertToGDataDate(endDate) +
                  '&alt=json-in-script' +
                  '&callback=loadGDataCallback' +
                  '&max-results=5000'; // choose 5000 as an arbitrarily large number
  feedUrl += getParams;
  var scriptTag = document.createElement('script');
  scriptTag.src = feedUrl;
  document.body.appendChild(scriptTag);