<?
/**
 * 2015 Loyiso Mtshali
 * Made with all the love in the world
 *
 * This script will read DRM values from a CSV file and update the drm_status in the database for each company.
 * The contacts_interests table will also be updated with the correct tier/drm_status.
 */

$platform_conf = '../platform.conf';

$platform = '';

if( file_exists($platform_conf)){
    $platform = trim(file_get_contents($platform_conf));
}

$host = '';
$db_name = '';
$username = '';
$password = '';

if($platform ===  'production'){
    $host = '127.0.0.1';
    $db_name = 'avcrm';
    $username = 'root';
    $password = 'eye1%sivihath';
} else if ($platform === 'staging') {
    $host = '127.0.0.1';
    $db_name = 'avcrm';
    $username = 'avior';
    $password = '@v1OrSQLp@55';
} else {
    $host = '127.0.0.1:8889';
    $db_name = 'avcrm';
    $username = 'root';
    $password = 'root';
}

/*Database Constants */
define('DB_HOST' , $host);
define('DB_NAME' , $db_name);
define('DB_USERNAME' , $username);
define('DB_PASSWORD' , $password);

/*Script Vars*/
$conn; //Connection to database

function work(){

    $conn = mysql_connect(DB_HOST , DB_USERNAME , DB_PASSWORD);
    mysql_select_db(DB_NAME);

    $handle = fopen("Company_drm_settings.csv" , "r");
    if($handle){
        while( ($line = fgets($handle)) !== false){

            $companyData = explode(";" , $line);

            $companyName = trim($companyData[1]);
            $drmStatus = trim($companyData[5]);

            echo "Processing: " . $companyName . "\n";

            $newDrmStatus = $drmStatus == "DRM" ? 2 : 1;

            $updateDRMQuery = "UPDATE tblcompanylist SET CompanyTier = $newDrmStatus WHERE Company = '$companyName'";
            $updateDRMResult = mysql_query($updateDRMQuery);

            if($updateDRMResult){
                echo "-- Successful.\n";
                echo "-- Updating Contacts/Interests table.\n";

                $updateContactsInterestsQuery = "UPDATE tblcntinterests SET tbl_int_Tier = $newDrmStatus WHERE cnt_id IN (SELECT cnt_id FROM tblcnt WHERE tblcnt.cnt_company = '$companyName')";
                $updateContactsInterestsResult = mysql_query($updateContactsInterestsQuery);

                if($updateContactsInterestsResult){
                    echo "--Successful.\n";
                } else {
                    echo "ERROR!";
                }
            } else {
                echo "Error updating drm status";
            }
        }

        fclose($handle);
    } else {
        echo "Error opening the file";
    }
}

work();
?>