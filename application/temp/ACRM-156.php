<?php
/**
 * 2015 Loyiso Mtshali
 * Made with all the love in the world
 */

$platform_conf = '../platform.conf';

$platform = '';

if( file_exists($platform_conf)){
    $platform = trim(file_get_contents($platform_conf));
}

$host = '';
$db_name = '';
$username = '';
$password = '';

if($platform ===  'production'){
    $host = '127.0.0.1';
    $db_name = 'avcrm';
    $username = 'root';
    $password = 'eye1%sivihath';
} else if ($platform === 'staging') {
    $host = '127.0.0.1';
    $db_name = 'avcrm';
    $username = 'avior';
    $password = '@v1OrSQLp@55';
} else {
    $host = '127.0.0.1:8889';
    $db_name = 'avcrm-old';
    $username = 'root';
    $password = 'root';
}



/*Database Constants */
define('DB_HOST' , $host);
define('DB_NAME' , $db_name);
define('DB_USERNAME' , $username);
define('DB_PASSWORD' , $password);

/*Script Vars*/
$conn; //Connection to database

function doWork(){

    //First we add the new column to the 'tblcompanylist' table
    $conn = mysql_connect(DB_HOST , DB_USERNAME , DB_PASSWORD);
    mysql_select_db(DB_NAME);

    $alterCompanyListQuery = "ALTER TABLE tblcompanylist ADD COLUMN tier INT after CompanyTier";
    $result = mysql_query($alterCompanyListQuery);

    if(!$result){
        echo "Error adding new column";
        exit;
    }


    //Get current company tier values
    $companyTierQuery = "SELECT comp_id , Company, CompanyTier FROM tblcompanylist ORDER BY Company ASC";
    $companyTierResult = mysql_query($companyTierQuery);

    $notUpdated = array();

    if(mysql_numrows($companyTierResult) > 0){

        while($company = mysql_fetch_assoc($companyTierResult)){

            //First only update companies who's current tier is either 1 or 2
            if($company['CompanyTier'] == 1 || $company['CompanyTier'] == 2){

                $updateTierQuery = "UPDATE tblcompanylist SET tier = " . $company['CompanyTier'] . " WHERE comp_id = " . $company['comp_id'];
                $updateTierResult = mysql_query($updateTierQuery);

                if($updateTierResult){
                    echo "Successfully updated: " . $company['Company'];
                    echo "\n";
                } else {
                    echo "Error updating company: " . $company['Company'];
                    echo "\n";
                    continue;
                }

                updateContactsInterestsTable($company['CompanyTier'] , $company['Company']);

            } else {
                //Send the company name to the 'not updated' array to be used later
                array_push($notUpdated , "'" . $company['Company'] . "'");
            }

        }

    }

    //Now for the companies that have not been updated we use their tier averages
    useAverages($notUpdated);

}

function is_decimal($val){
    return is_numeric($val) && floor($val) != $val;
}

function useAverages($companyNameArray){

    echo "\n";
    echo "******** useAverages() ********";
    echo "\n";

    $companyNameArray = join(',' , $companyNameArray);

    //Populate new column using tier averages generated from tblcntinterests values
    $averagesQuery = "select
                        c.cnt_company as 'Company',
                        count(ci.cnt_Id) as 'Total Subscriptions',
                        avg(ci.tbl_int_Tier) as 'Average' ,
                        comp.CompanyTier as 'Company Tier',
                        count(case ci.tbl_int_Tier when 1 then 1 else null end) as 'Number Normal',
                        count(case ci.tbl_int_Tier when 2 then 1 else null end) as 'Number DRM',
                        sts.company_status as 'Company Status'

                        from
                        tblcnt as c,
                        tblcntinterests as ci,
                        tblcompanylist as comp,
                        tblcompanystatus as sts

                        where
                        c.cnt_Id = ci.cnt_Id
                        and
                        c.cnt_company = comp.Company
                        and
                        comp.Company IN ($companyNameArray)
                        and
                        sts.id_companystatus = comp.id_companystatus

                        group by c.cnt_company";

    $result = mysql_query($averagesQuery);

    if(mysql_num_rows($result) > 0){

        while($company = mysql_fetch_assoc($result)){

            $tier = 0;

            if(!is_decimal($company['Average'])){
                $tier = floor($company['Average']);
            } else {
                //Use number of DRM/Normal contacts to determine which tier to apply to company
                $tier = $company['Number Normal'] > $company['Number DRM'] ? 1 : 2;
            }

            //Update column with 'average' value
            $updateColumnQuery = "update tblcompanylist set tier = " . $tier . " where company = '" . $company['Company'] ."'";
            $updateColumnResult = mysql_query($updateColumnQuery);

            if($updateColumnResult){
                echo "Successfully updated: " . $company['Company'];
                echo "\n";
            } else {
                echo "Error updating company: " . $company['Company'];
                echo "\n";
                continue;
            }

            updateContactsInterestsTable($tier , $company['Company']);

            echo "----";
            echo "\n";

        }

    } else {
        echo "Error fetching tier averages";
        exit;
    }

}

function updateContactsInterestsTable($tier , $companyName){
    //Update tblcntinterests table for each employee of this company
    $updateInterestsQuery = "update tblcntinterests set tbl_int_Tier = " . $tier . " where cnt_Id in (select cnt_Id from tblcnt where cnt_company = '" . $companyName . "')";
    $updateInterestResult = mysql_query($updateInterestsQuery);

    if($updateInterestResult){
        echo "Successfully updated records in the tblcntinterests table for " . $companyName;
        echo "\n";
    } else {
        echo "Error updating tblcntinterests table for " . $companyName;
        echo "\n";
    }
}

doWork();

?>