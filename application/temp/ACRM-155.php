<?
/**
 * 2015 Loyiso Mtshali
 * Made with all the love in the world
 *
 * This script will remove all duplicate records from the tblcntinterests table
 * JIRA Ticket : ACRM-155
 */

$platform_conf = '../platform.conf';

$platform = '';

if( file_exists($platform_conf)){
    $platform = trim(file_get_contents($platform_conf));
}

$host = '';
$db_name = '';
$username = '';
$password = '';

if($platform ===  'production'){
    $host = '127.0.0.1';
    $db_name = 'avcrm';
    $username = 'root';
    $password = 'eye1%sivihath';
} else if ($platform === 'staging') {
    $host = '127.0.0.1';
    $db_name = 'avcrm';
    $username = 'avior';
    $password = '@v1OrSQLp@55';
} else {
    $host = '127.0.0.1:8889';
    $db_name = 'avcrm-old';
    $username = 'root';
    $password = 'root';
}

/*Database Constants */
define('DB_HOST' , $host);
define('DB_NAME' , $db_name);
define('DB_USERNAME' , $username);
define('DB_PASSWORD' , $password);

/*Script Vars*/
$conn; //Connection to database

function doWork(){

    $conn = mysql_connect(DB_HOST , DB_USERNAME , DB_PASSWORD);
    mysql_select_db(DB_NAME);

    //Get Records that have duplicated
    $duplicatesQuery = "SELECT t1.*
                        FROM tblcntinterests t1 , tblcntinterests t2
                        WHERE
                        t1.cnt_Int_id != t2.cnt_Int_id
                        AND
                        t1.cnt_id = t2.cnt_id
                        AND
                        t1.cnt_Interest = t2.cnt_Interest
                        GROUP BY
                        t1.cnt_id
                        ORDER BY
                        t1.cnt_Int_id";
    $duplicatesResult = mysql_query($duplicatesQuery);

    if(mysql_num_rows($duplicatesResult) > 0){

        while($record = mysql_fetch_row($duplicatesResult)){

            $id = $record[0]; //This is the ID of the first occurrence, which will be the lowest of its duplicates
            $contact_id = $record[2];
            $interest = $record[3];
            $tier = $record[4];

            echo "ID:" . $id . " Contact: " . $contact_id . " Interest: " . $interest . " Tier: " . $tier . "\n";

            $deleteQuery = "DELETE FROM tblcntinterests WHERE cnt_id = $contact_id AND cnt_Interest = '$interest' AND tbl_int_Tier = $tier AND cnt_Int_id != $id"; //Deletes duplicates of this record
            $deleteResult = mysql_query($deleteQuery);

            if($deleteResult){
                $count = mysql_affected_rows();
                echo "Deleted " . $count . " duplicate records \n";
            } else {
                echo "Error deleting record \n";
            }
        }

    } else {
        echo "Error fetching duplicate records \n";
    }

}

doWork();
?>