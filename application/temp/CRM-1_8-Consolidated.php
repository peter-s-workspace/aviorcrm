<?
$platform_conf = '../platform.conf';

$platform = '';

if( file_exists($platform_conf)){
    $platform = trim(file_get_contents($platform_conf));
}

$host = '';
$db_name = '';
$username = '';
$password = '';

if($platform ===  'production'){
    $host = '127.0.0.1';
    $db_name = 'avcrm';
    $username = 'root';
    $password = 'eye1%sivihath';
} else if ($platform === 'staging') {
    $host = '127.0.0.1';
    $db_name = 'avcrm';
    $username = 'avior';
    $password = '@v1OrSQLp@55';
} else {
    $host = '127.0.0.1:8889';
    $db_name = 'avcrm-old';
    $username = 'root';
    $password = 'root';
}

/*Database Constants */
define('DB_HOST' , $host);
define('DB_NAME' , $db_name);
define('DB_USERNAME' , $username);
define('DB_PASSWORD' , $password);

/*Script Vars*/
$conn; //Connection to database

$conn = mysql_connect(DB_HOST , DB_USERNAME , DB_PASSWORD);
mysql_select_db(DB_NAME);

/**
 * ACRM-155
 * Removes Duplicate entries in the 'tblcntinterests' table
 */
function removeDuplicates(){

    //Get Records that have duplicated
    $duplicatesQuery = "SELECT t1.*
                        FROM tblcntinterests t1 , tblcntinterests t2
                        WHERE
                        t1.cnt_Int_id != t2.cnt_Int_id
                        AND
                        t1.cnt_id = t2.cnt_id
                        AND
                        t1.cnt_Interest = t2.cnt_Interest
                        GROUP BY
                        t1.cnt_id
                        ORDER BY
                        t1.cnt_Int_id";
    $duplicatesResult = mysql_query($duplicatesQuery);

    if(mysql_num_rows($duplicatesResult) > 0){

        while($record = mysql_fetch_row($duplicatesResult)){

            $id = $record[0]; //This is the ID of the first occurrence, which will be the lowest of its duplicates
            $contact_id = $record[2];
            $interest = $record[3];
            $tier = $record[4];

            echo "ID:" . $id . " Contact: " . $contact_id . " Interest: " . $interest . " Tier: " . $tier . "\n";

            $deleteQuery = "DELETE FROM tblcntinterests WHERE cnt_id = $contact_id AND cnt_Interest = '$interest' AND tbl_int_Tier = $tier AND cnt_Int_id != $id"; //Deletes duplicates of this record
            $deleteResult = mysql_query($deleteQuery);

            if($deleteResult){
                $count = mysql_affected_rows();
                echo "Deleted " . $count . " duplicate records \n";
            } else {
                echo "Error deleting record \n";
                return false;
            }
        }

        return true;

    } else {
        echo "Error fetching duplicate records \n";
    }

    return false;
}

/**
 * ACRM-156
 * Sets Company Tiers from CSV file
 */
function setCompanyTiers(){

    $alterCompanyListQuery = "ALTER TABLE tblcompanylist ADD COLUMN tier INT after CompanyTier";
    $result = mysql_query($alterCompanyListQuery);

    if(!$result){
        echo "Error adding new column";
        return false;
    }

    $handle = fopen("Company_drm_settings.csv" , "r");
    if($handle){
        while( ($line = fgets($handle)) !== false){

            $companyData = explode(";" , $line);

            $companyName = trim($companyData[1]);
            $drmStatus = trim($companyData[5]);

            echo "Processing: " . $companyName . "\n";

            $newDrmStatus = $drmStatus == "DRM" ? 2 : 1;

            $updateDRMQuery = "UPDATE tblcompanylist SET tier = $newDrmStatus WHERE Company = '$companyName'";
            $updateDRMResult = mysql_query($updateDRMQuery);

            if($updateDRMResult){
                echo "-- Successful.\n";
                echo "-- Updating Contacts/Interests table.\n";

                $updateContactsInterestsQuery = "UPDATE tblcntinterests SET tbl_int_Tier = $newDrmStatus WHERE cnt_id IN (SELECT cnt_id FROM tblcnt WHERE tblcnt.cnt_company = '$companyName')";
                $updateContactsInterestsResult = mysql_query($updateContactsInterestsQuery);

                if($updateContactsInterestsResult){
                    echo "--Successful.\n";
                } else {
                    echo "ERROR!";
                }
            } else {
                echo "Error updating drm status";
            }
        }
        fclose($handle);

        // Copy CompanyTier values to Tier column for companies not included in the CSV file
        $zeroDrmQuery = "SELECT comp_id , Company , CompanyTier FROM tblcompanylist WHERE tier is null";
        $zeroDrmResult = mysql_query($zeroDrmQuery);

        if(mysql_num_rows($zeroDrmResult) > 0){

            while($record = mysql_fetch_row($zeroDrmResult)){
                $updateQuery = "UPDATE tblcompanylist set tier = " . $record[2] . " WHERE comp_id =" . $record[0];
                $updateResult = mysql_query($updateQuery);

                if($updateResult){
                    echo "-- Successful.\n";
                    echo "-- Updating Contacts/Interests table.\n";

                    $updateContactsInterestsQuery = "UPDATE tblcntinterests set tbl_int_Tier = " . $record[2] . " WHERE cnt_id IN (SELECT cnt_id FROM tblcnt WHERE tblcnt.cnt_company = '" . $record[1] . "')";
                    $updateContactsInterestsResult = mysql_query($updateContactsInterestsQuery);

                    if($updateContactsInterestsResult){
                        echo "--Successful.\n";
                    } else {
                        echo "ERROR!";
                    }
                } else {
                    echo "Error copying CompanyTier";
                }
            }
        } else {
            echo "No companies have a tier of zero..";
        }

        return true;
    } else {
        echo "Error opening the file";
    }

    return false;

}

if(removeDuplicates()){
        echo "Duplicates removed.\n";
    if(setCompanyTiers()){
        echo "Company Tiers Complete.\n";
    } else {
        echo "Failed to set company tiers.\n";
    }
} else {
    echo "Failed to remove duplicates.\n";
}
?>