<?php require_once('Connections/CRMconnection.php'); ?>
<?php

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>


<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_onclientlist = "none";
if (isset($_SESSION["MM_Username"])) {
  $colname_onclientlist = $_SESSION["MM_Username"];
}
$colname2_onclientlist = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname2_onclientlist = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_onclientlist = sprintf("SELECT tblfavourites.tblFav_id, tblfavourites.fav_Username, tblfavourites.tbl_cnt_Name_Desc, tblfavourites.tbl_cnt_Id, tblfavourites.tbl_Tiering, tblfavourites.fav_nick, tblfavourites.listid, cnt_listnames.listname, cnt_listnames.ownerusername, cnt_listnames.shared, cnt_listnames.editablebyothers FROM tblfavourites, cnt_listnames WHERE cnt_listnames.listid=tblfavourites.listid AND ( tblfavourites.fav_Username= %s OR cnt_listnames.shared=1) AND tblfavourites.tbl_cnt_Id=%s", GetSQLValueString($colname_onclientlist, "text"),GetSQLValueString($colname2_onclientlist, "int"));
$onclientlist = mysql_query($query_onclientlist, $CRMconnection) or die(mysql_error());
$row_onclientlist = mysql_fetch_assoc($onclientlist);
$totalRows_onclientlist = mysql_num_rows($onclientlist);

$colname_availlist = "none";
if (isset($_SESSION['MM_Username'])) {
  $colname_availlist = $_SESSION['MM_Username'];
}
$colname2_availlist = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname2_availlist = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_availlist = sprintf("SELECT * FROM cnt_listnames WHERE cnt_listnames.listid NOT IN ( SELECT tblfavourites.listid FROM tblfavourites WHERE tblfavourites.fav_Username=%s AND tblfavourites.tbl_cnt_Id = %s ) ORDER BY cnt_listnames.listname ASC", GetSQLValueString($colname_availlist, "text"),GetSQLValueString($colname2_availlist, "int"),GetSQLValueString($colname_availlist, "text"));
$availlist = mysql_query($query_availlist, $CRMconnection) or die(mysql_error());
$row_availlist = mysql_fetch_assoc($availlist);
$totalRows_availlist = mysql_num_rows($availlist);

$colname_rsclient = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rsclient = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsclient = sprintf("SELECT * FROM contacts WHERE id = %s", GetSQLValueString($colname_rsclient, "int"));
$rsclient = mysql_query($query_rsclient, $CRMconnection) or die(mysql_error());
$row_rsclient = mysql_fetch_assoc($rsclient);
$totalRows_rsclient = mysql_num_rows($rsclient);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>
</head>

<body>
<h3>Selected Client: <?php echo $row_rsclient['first_name']; ?> <?php echo $row_rsclient['surname']; ?></h3>
<fieldset><legend>On Lists</legend>
<span id="onlistspan">

						<?php if(isset($_SESSION['MM_Username']) && isset( $_SESSION['MM_UserGroup'] ) &&  $_SESSION['MM_UserGroup'] == 'Administrator' ) {
       					?>	
       						
									<?php do { ?>
									  <a id="onli<?php echo $row_onclientlist['tblFav_id'] ?>" href="#" onclick="$('#availlistspan').empty().html('<img src=images/ajax-loader.gif />');
									$('#availlistspan').load('<?php echo("userclientlistavaillist.php?tblFav_id=".$row_onclientlist['tblFav_id']."&MM_delete=removefromlist&cnt_Id=".$row_userdet['id']."&sourcepage=clientdetails.php&MM_Username=".$_SESSION['MM_Username']) ?>'); $('#onli<?php echo $row_onclientlist['tblFav_id'] ?>').remove();" class="clicklink"><?php echo $row_onclientlist['listname']; ?><?php if($row_onclientlist['shared']==1){?><img src="images/redbox.png" width="5" height="5" border="0" /><?php } ?></a>
									  <?php } while ($row_onclientlist = mysql_fetch_assoc($onclientlist)); ?>
						<?php
				   	    }
				   		else
				   		{ ?>
				   					<?php do { ?>
									  <a id="onli<?php echo $row_onclientlist['tblFav_id'] ?>" class="clicklink"><?php echo $row_onclientlist['listname']; ?><?php if($row_onclientlist['shared']==1){?><img src="images/redbox.png" width="5" height="5" border="0" /><?php } ?></a>
									  <?php } while ($row_onclientlist = mysql_fetch_assoc($onclientlist)); ?>
				   				
				   		<?php } ?>
  
</span>
</fieldset>
<img src="images/redbox.png" width="5" height="5" />= shared with other users</td>
  <td><fieldset><legend>Available lists</legend>
    <span id="availlistspan">

						<?php if(isset($_SESSION['MM_Username']) && isset( $_SESSION['MM_UserGroup'] ) &&  $_SESSION['MM_UserGroup'] == 'Administrator' ) {
       					?>	
								<?php do { ?>
								  <a id="avls<?php echo $row_availlist['listid']; ?>" href="#" onclick="$('#onlistspan').empty().html('<img src=images/ajax-loader.gif />'); $('#onlistspan').load('<?php echo("userclientlistlistajax.php?cnt_Id=".$colname2_availlist."&fav_Username=".$_SESSION['MM_Username']."&listid=".$row_availlist['listid']."&tbl_cnt_Name_Desc=". urlencode($row_userdet['first_name']." ".$row_userdet['surname'])."&tbl_Tiering=1&fav_nick=none&MM_insert=addtolist") ?>'); $('#avls<?php echo $row_availlist['listid']; ?>').remove()" class="clicklink"><?php echo $row_availlist['listname']; ?><?php if($row_availlist['shared']==1){?><img src="images/redbox.png" width="5" height="5" border="0" /><?php } ?></a>
								  <?php } while ($row_availlist = mysql_fetch_assoc($availlist)); ?>
						<?php
				   	    }
				   		else
				   		{ ?>
				   				<?php do { ?>
								  <a id="avls<?php echo $row_availlist['listid']; ?>" class="clicklink"><?php echo $row_availlist['listname']; ?><?php if($row_availlist['shared']==1){?><img src="images/redbox.png" width="5" height="5" border="0" /><?php } ?></a>
								  <?php } while ($row_availlist = mysql_fetch_assoc($availlist)); ?>
				   		<?php } ?>
    
    

      
    </span>
</fieldset>
</body>
</html>
<?php
mysql_free_result($onclientlist);

mysql_free_result($availlist);

mysql_free_result($rsclient);
?>
