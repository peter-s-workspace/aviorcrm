<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>

<?php

// function to display directory and file 

function getDirectoryList ($directory) 
  {
	
	//check if is_dir
	if(is_dir($directory)){
		
    	// create an array to hold directory list
    	$results = array();

    	// create a handler for the directory
    	$handler = opendir($directory);

    	// open directory and walk through the filenames
    	while ($file = readdir($handler)) {

      		// if file isn't this directory or its parent, add it to the results
      		if ($file != "." && $file != "..") {
        		$results[] = $file;
      		}

    	}

    	// tidy up: close the handler
    closedir($handler);

    // done!
    return $results;
	}
  }



//file upload script
 $success= "none"; 
//needs user to be loggd in (MM_Username)as well as POST variables private and upfiles to be set.
//set private = 'yes' for private file list, or 'no' for public
//array upfiles contains all the selected files  for upload

if ((isset($_POST['username'])) && isset($_POST['do_upload'])) {
 $success.= "| working"; 
 
  $uname = $_POST['username'];
  $uploaddir='';
  //select either user's private directory or public directory for file upload
   $success.= "| determining upload directory"; 
  if( isset($_POST['private']) && ($_POST['private']=='yes')){
  	$uploaddir = 'uploads/'.sha1(md5($uname)).'/';
 	} else { $uploaddir = 'uploads/public/';
	}
  $success.= "|upload directory set : ". $uploaddir ; 
//make directory if it doens not yet exist		
  if(!is_dir($uploaddir))
 	 {
	$success.= "| directory does not exist - creating"; 
	mkdir($uploaddir, 0777);
	} 
 
 
 foreach ($_FILES["upfile"]["error"] as $key => $error) {
    if ($error == UPLOAD_ERR_OK) {
       $tmp_name = $_FILES["upfile"]["tmp_name"][$key];
	   $name = $_FILES["upfile"]["name"][$key];
	   $success.= "| attempting file upload".$name ;
        move_uploaded_file($tmp_name, $uploaddir.$name);
    }
}

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>

</head>

<body>
<?php echo $success; ?>
</body>
</html>