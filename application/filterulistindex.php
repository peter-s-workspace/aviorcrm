<?php require_once('Connections/CRMconnection.php'); ?>
<?php

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userd = "-1";
if (isset($_GET['cusername'])) {
  $colname_userd = $_GET['cusername'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userd = sprintf("SELECT * FROM tbluser WHERE tbluser.user_name = %s ", GetSQLValueString($colname_userd, "text"));
$userd = mysql_query($query_userd, $CRMconnection) or die(mysql_error());
$row_userd = mysql_fetch_assoc($userd);


$totalRows_userd = "-1";
if (isset($_GET['cusername'])) {
  $totalRows_userd = $_GET['cusername'];
}

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userd = sprintf("SELECT * FROM tbluser WHERE tbluser.user_name = %s ", GetSQLValueString($colname_userd, "text"));
$userd = mysql_query($query_userd, $CRMconnection) or die(mysql_error());
$row_userd = mysql_fetch_assoc($userd);
$totalRows_userd = mysql_num_rows($userd);

$colname1_userlists = "-1";
if (isset($_GET['cusername'])) {
  $colname1_userlists = $_GET['cusername'];
}

$colname2_userlists = "-1";
if (isset($_GET['listid'])) {
  $colname2_userlists = $_GET['listid'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);

$query_userlists="";

$userlists = "";
$row_userlists = "";
$totalRows_userlists = 0;




if($colname2_userlists >1 ){
  $query_userlists = sprintf("SELECT * FROM tblfavourites, contacts , tblcompanylist , tblcompanytier
  WHERE contacts.id=tblfavourites.tbl_cnt_Id
  AND contacts.company_id = tblcompanylist.comp_id
  AND tblcompanytier.id_companytier = tblcompanylist.id_companytier
  AND tblfavourites.listid = %s
  ORDER BY first_name ASC, surname ASC ", GetSQLValueString($colname2_userlists, "int"));

  $userlists = mysql_query($query_userlists, $CRMconnection) or die(mysql_error());
  $row_userlists = mysql_fetch_assoc($userlists);
  $totalRows_userlists = mysql_num_rows($userlists);
}
elseif ($colname2_userlists== 1) {
  // This is legacy code that seemed to attempt to add this line to the query above
  // That was never and probably will never be valid SQL
  // It is left here in case it some purpose in some other feature.
  // $query_userlists .= " AND tblfavourites.fav_Username = " . GetSQLValueString($colname1_userlists, "text") ;
}
else{
  error_log("The list id is not a postive integer.");
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avior Capital Markets CRM</title>
<link href="css/2.css" rel="stylesheet" type="text/css" />

</head>

<body>
     Quick Filter: <input name="filterulist" id="filterulist" type="text" />
    <table cellpadding="2" id="userlist" style="width:100%" class="stripeMe">
	<thead>
                <tr class="tabbhead">
                  <td>Name</td>
                  <td>Institution</td>
                  <td>Phone</td>
                  <td>Cell</td>
                  <td></td>
                   </tr>
    </thead>
                <tbody>
                  <?php
                  # If statement to prevent column formating on the empty list
                  if(isset($row_userlists['id']))
                  {
                    do {
                            $outcolor="";
                            $color = "";
                            $tve= $row_userlists['id_companytier'];

                            switch ($tve) {
                                case '1':
                                    $outcolor = "#E5E4E2";
                                    $color = "#333333";
                                    break;
                                case '2' :
                                    $outcolor = "#B8860B";
                                    $color = "#FFF";
                                    break;
                                case '3' :
                                    $outcolor = "#C0C0C0";
                                    $color = "#333333";
                                    break;
                                case '8' :
                                    $outcolor = "#0A238C";
                                    $color = "#FFF";
                                    break;
                                case '9' :
                                    $outcolor = "#f4a460";
                                    $color = "#333333";
                                    break;
                                case '10' :
                                    $outcolor = "#fff";
                                    $color = "#333333";
                                    break;
                                case '4' :
                                    $outcolor = "#fff";
                                    $color = "#333333";
                                    break;
                                default :
                                    $outcolor = "#CE1620";
                                    $color = "#FFF";
                            }
                                ?>
                              <tr>
                                <td><a href="clientdetails.php?cnt_Id=<?php echo $row_userlists['id']; ?>" <? if($row_userlists['active'] == 0){ ?> style="color: #666666;" <? }?>><?php echo $row_userlists['first_name']; ?> <?php echo $row_userlists['surname']; ?></a></td>
                                <td style="background-color: <? echo $outcolor;?>; color: <? echo $color;?>;"><?php echo $row_userlists['Company']; ?></td>
                                <?php if (!isset($row_userd['user_extension'])) { ?>
                                
                                <td><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userlists['landline']; ?>'); $('#phonebox').dialog('open');" <? if($row_userlists['active'] == 0){ ?> style="color: #666666;" <? }?>><?php echo $row_userlists['landline']; ?></a></td>
                                <td align="right"><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userlists['mobile']; ?>')" <? if($row_userlists['active'] == 0){ ?> style="color: #666666;" <? }?>><?php echo $row_userlists['mobile']; ?></a></td>
                                <?php } else { ?>
                                
                                <!--
                                 <td><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userlists['landline']; ?>'); $('#phonebox').dialog('open');" <? if($row_userlists['active'] == 0){ ?> style="color: #666666;" <? }?>><?php echo $row_userlists['landline']; ?></a></td>
                                  !-->
                                 <td><a href="javascript:void(0);" onclick="callMitel('<?php echo $_SESSION["user_extension"];?>','<?php echo $row_userlists['landline'];?>');$('#phonebox').dialog('open');"<? if($row_userlists['active'] == 0){ ?> style="color: #666666;" <? }?>><?php echo $row_userlists['landline']; ?></a></td>

                                 <td><a href="javascript:void(0);" onclick="callMitel('<?php echo $_SESSION["user_extension"];?>','<?php echo $row_userlists['mobile'];?>');$('#phonebox').dialog('open');"<? if($row_userlists['active'] == 0){ ?> style="color: #666666;" <? }?>><?php echo $row_userlists['mobile']; ?></a></td>
                                 <?php } ?>
                                <td><span style="text-align:right;"><a href="mailto:<?php echo $row_userlists['email']; ?>"><img border="0" src="images/email-icon-16.gif" width="16" height="16" align="right" /></a></span></td>
                            </tr>
                      <?php } while ($row_userlists = mysql_fetch_assoc($userlists));
                  }

                    ?>
                </tbody>
  </table>
</body>
</html>
<?php
mysql_free_result($userlists);

mysql_free_result($userd);
?>
