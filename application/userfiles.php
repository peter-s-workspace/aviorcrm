<?php require_once('includes/sitevars.php'); ?>
<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

function createli($filename){
	 global $countr;
	$tve = '<li id="p'.$countr.'"><a href="'.$filename.'" style="padding:3px;" > '.trim(strrchr($filename, "/"), "/").'</a> | <form style="display: inline" id="p'.$countr.'f" ><a href="#" style="clicklink" onclick="';
	$tve.= "$.post(";
	$tve.= "'userfilesremovefile.php',$('#p".$countr."f').serialize(),function(data) {
  $('#p".$countr."f').html(data)},'json'); $('#p".$countr."').remove()";
	$tve.='" style="padding:3px;">Delete</a><input type="hidden" name="filedel" value="'.$filename.'" ></form> </li>';

 echo $tve;

}

//for uploads
$uploaddir = array();
$uploaddir[] = 'uploads/'.sha1(md5($_SESSION['MM_Username']));
$uploaddir[] = 'uploads/public';

//make directory if it doens not yet exist		
  
  foreach ($uploaddir as $i => $value) {
  if(!is_dir($uploaddir[$i]))
	 {
	mkdir($uploaddir[$i], 0777);
	} 
  }

 $countr=1;

//open directories and build file display + operations
function displaydir($dir){


$pubpriv="";
if($dir==='uploads/public')
{
	$pubpriv="pub";
}
else
{
	$pubpriv="priv";
}
 	


 global $countr;
// Open a known directory, and proceed to read its contents
	$fileData = array();
	if (is_dir($dir)) {
    	if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
				if (strlen($file)>3){
					$fileData[] = $dir."/".$file;
				}
			}
		}
		closedir($dh);
		if(sizeof($fileData) > 0 ) {
			usort($fileData, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
		} 

        	//there are files - create a table
            $outvalue =''   ;
            $isadmin = isset($_SESSION['MM_Username']) && (isAuthorized("x","Administrator", $_SESSION['MM_Username'], $_SESSION['MM_UserGroup']));
			//while (($file = readdir($dh)) !== false) {
			foreach($fileData as $file ) {
				$file = ltrim(str_replace($dir,'',$file),'/');	
            	if (strlen($file)>3){
            		if($isadmin){
            			$outvalue = ' <li id="p'.$pubpriv.$countr.'"><a href="'.$dir.'/'.$file.'" style="padding:3px;" > '.$file.' </a> | <form style="display: inline" id="p'.$pubpriv.$countr.'f" ><a href="#" style="clicklink" onclick="';
						$outvalue.= "$.post(";
						$outvalue.= "'userfilesremovefile.php',$('#p".$pubpriv.$countr."f').serialize(),function(data) {
	  					$('#p".$pubpriv.$countr."f').html(data)},'json'); $('#p".$pubpriv.$countr."').remove()";
						$outvalue.='" style="padding:3px;">Delete</a><input type="hidden" name="filedel" value="'.$dir.'/'.$file.'" ></form> </li>';
            		}
            		else{
            			$outvalue = ' <li id="p'.$pubpriv.$countr.'"><a href="'.$dir.'/'.$file.'" style="padding:3px;" > '.$file.' </a></li>';
            		}	
					$countr +=1;
					echo $outvalue;
				}
        	}
			//close table load(;
        	
    	//}
	}
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
<link rel="manifest" href="favicons/manifest.json">
<link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<link type="text/css" href="css/kdes1/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<link type="text/css" href="css/uploadify.css" rel="stylesheet" />
<script src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.quicksearch.js">	</script>
<script src="js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="js/swfobject.js"></script>
<script type="text/javascript" src="js/jquery.uploadify.v2.1.0.min.js"></script>
        
		<script type="text/javascript">
			$(function(){

				// Accordion
				$("#accordion").accordion({ header: "h3" });
	
				// Tabs
				$('#tabs1').tabs();
				$('#tabs2').tabs();
	
				//uploadify
				//privat file uploads
				
				$('#prvfileUpload').uploadify(
				{'uploader': 'js/uploadify.swf',
				'script': 'js/uploadify.php',
				'folder': 'uploads/<?php echo sha1(md5($_SESSION['MM_Username'])); ?>',
				'cancelImg': 'js/cancel.png',
				'multi': false,
				'buttonText': 'Select Files',
				'checkScript': 'js/check.php',
				'displayData': 'speed',
				'simUploadLimit': 2,
				'auto': true,
				'onComplete': function(event, queueID, fileObj, response, data) {
					$('#privlists').load('userfilesdisplaydirpriv.php');
     			
				}
								
				});
				
				//shared file uploads
				$('#pubfileUpload').uploadify(
				{'uploader': 'js/uploadify.swf',
				'script': 'js/uploadify.php',
				'folder': 'uploads/public',
				'cancelImg': 'js/cancel.png',
				'multi': false,
				'buttonText': 'Select Files',
				'checkScript': 'js/check.php',
				'displayData': 'speed',
				'simUploadLimit': 2	,
				'auto': true,
				'onComplete': function(event, queueID, fileObj, response, data) {
					$('#publists').load('userfilesdisplaydirpub.php');
     			
				}
				
				
				});
				
				//stripe tables
				//$('.stripeMe tr:even').addClass('alt');
				//quick serach boxes
				
								
		});
			
	</script>

<!-- InstanceEndEditable -->
<script language="javascript">
	$(function(){
		
		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					},
					appendTo: '.quick-search-container'
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);
				
					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
	})
</script>	
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
			<div class="quick-search-container">
					<input type="text" id="quicksearch" size="16" />
			</div>
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<div class="content40">
<div id="tabs1">
      <ul>
        <li><a href="#tabs1-1">Private Files</a></li>
      </ul>
      <div id="tabs1-1">
      
      <fieldset>
      <legend>Upload Private files</legend>
<div id="prvfileUpload">You have a problem with your javascript</div>
      </fieldset>
      <fieldset>
      <legend>Private Files</legend><ul id="privlists">
      <?php displaydir('uploads/'.sha1(md5($_SESSION['MM_Username']))); ?>      
      </ul>
      </fieldset></div>
    </div>
 </div>
<div class="content40">
<div id="tabs2">
      <ul>
        <li><a href="#tabs2-1">Shared Files</a></li>
        
      </ul>
      <div id="tabs2-1">
       <fieldset>
      <legend>Upload Shared files</legend>
<div id="pubfileUpload">You have a problem with your javascript</div>
      </fieldset>
        <fieldset>
      <legend>Shared Files</legend>
      <ul id="publists">
      <?php displaydir('uploads/public'); ?>      
      </ul>
      </fieldset></div>
    </div>
 </div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
