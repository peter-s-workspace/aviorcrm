<?php 
require_once('Connections/CRMconnection.php'); 
require_once('includes/classes/user.class.php');
require_once('includes/classes/notes.class.php');
require_once('includes/classes/hipchat.class.php');
?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$hipchat_instance   = new hipchat();
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

$userEmail = isset($_SESSION['MM_UserEmail']) ? $_SESSION['MM_UserEmail'] : '';

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 
  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}
//Administrator 
$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
$colname_onclientlist = "none";

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$currentPage = $_SERVER["PHP_SELF"];

$colname_reminders = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_reminders = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_reminders = sprintf("SELECT * FROM reminders WHERE user_id = %s ORDER BY remind_id DESC", GetSQLValueString($colname_reminders, "text"));

$reminders = mysql_query($query_reminders, $CRMconnection) or die(mysql_error());
$row_reminders = mysql_fetch_assoc($reminders);
$totalRows_reminders = mysql_num_rows($reminders);


$colname_userdet = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_userdet = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userdet = sprintf("SELECT *, contacts.portal_access as 'user_portal_access', contacts.webview_print as 'user_webview_print', tblcompanylist.portal_access as 'company_portal_access', tblcompanylist.webview_print as 'company_webview_print' FROM contacts, tblcompanylist,tblcompanytier WHERE contacts.id = %s AND tblcompanylist.comp_id=contacts.company_id AND tblcompanytier.id_companytier = tblcompanylist.id_companytier", GetSQLValueString($colname_userdet, "int"));

$userdet = mysql_query($query_userdet, $CRMconnection) or die(mysql_error());
$row_userdet = mysql_fetch_assoc($userdet);

$totalRows_userdet = mysql_num_rows($userdet);

$client_full_name     =  $row_userdet['first_name']. ' ' .$row_userdet['surname'];
$not_sync             = false; ##### must always default to false first

################## Logic to validate the numbers #####################
####
if (isset($row_userdet['mitel_full_name']) && $row_userdet['mitel_full_name'] == $client_full_name) {
	if($row_userdet['mitel_phone_number'] == $row_userdet['landline']) {
		$not_sync = true;
	} else if (substr($row_userdet['landline'],0,2) == '00') {
		$temp_number 	= preg_replace('/00/', '', $row_userdet['landline'],2);
		if ($temp_number == $row_userdet['mitel_phone_number']) {
			$not_sync = true;
		}
	}  else if (substr($row_userdet['landline'], 0,1) == '0') {
		$temp_number = preg_replace('/0/', '27', $row_userdet['landline'],1);
		if ($temp_number == $row_userdet['mitel_phone_number']) {
			$not_sync = true;
		}
	 }

}


mysql_select_db($database_CRMconnection, $CRMconnection);
$query_complist = "SELECT * FROM tblcompanylist ORDER BY Company ASC";
$complist = mysql_query($query_complist, $CRMconnection) or die(mysql_error());
$row_complist = mysql_fetch_assoc($complist);
$totalRows_complist = mysql_num_rows($complist);

$colname_responsib = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_responsib = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_responsib = sprintf("SELECT responsiblist.response_desc, responsiblist.respons_id, cntresponsibility.idcntresponsibility FROM cntresponsibility, responsiblist WHERE cntresponsibility.cnt_id=%s AND responsiblist.respons_id=cntresponsibility.respons_id ORDER BY responsiblist.response_desc", GetSQLValueString($colname_responsib, "int"));
$responsib = mysql_query($query_responsib, $CRMconnection) or die(mysql_error());
$row_responsib = mysql_fetch_assoc($responsib);
$totalRows_responsib = mysql_num_rows($responsib);

$colname_resplist = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_resplist = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_resplist = sprintf("SELECT * FROM responsiblist WHERE responsiblist.respons_id NOT IN ( SELECT respons_id FROM cntresponsibility WHERE cntresponsibility.cnt_id=%s) ORDER BY responsiblist.response_desc", GetSQLValueString($colname_resplist, "int"));
$resplist = mysql_query($query_resplist, $CRMconnection) or die(mysql_error());
$row_resplist = mysql_fetch_assoc($resplist);
$totalRows_resplist = mysql_num_rows($resplist);

$maxRows_rscontacts = 6;
$pageNum_rscontacts = 0;
if (isset($_GET['pageNum_rscontacts'])) {
  $pageNum_rscontacts = $_GET['pageNum_rscontacts'];
}
$startRow_rscontacts = $pageNum_rscontacts * $maxRows_rscontacts;

$colname_rscontacts = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rscontacts = $_GET['cnt_Id'];
}

$notesObj = new Notes();
$doesContactBelongToAvior = $notesObj->doesContactBelongToAvior($colname_rscontacts);
if ($doesContactBelongToAvior == false) {
	$notesData = $notesObj->getUserNotes($colname_rscontacts);
} else {
	$notesData = $notesObj->getAviorUserNotes($colname_rscontacts);	
}

$notesDataSize = sizeof($notesData);

$colname_rsemail = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rsemail = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsemail = sprintf("SELECT interests.id, interests.description, contacts_interests.id AS cid, contacts_interests.interest_id, contacts_interests.contact_id,
						  IF (ISNULL(contacts.drm_status), tblcompanylist.drm_status, contacts.drm_status) as drm_status
						  FROM interests
						  INNER JOIN contacts_interests
						  ON interests.id = contacts_interests.interest_id
						  INNER JOIN contacts
						  ON contacts.id = contacts_interests.contact_id
						  INNER JOIN tblcompanylist
						  ON tblcompanylist.comp_id = contacts.company_id
						  WHERE contacts_interests.contact_id = %s
						  ORDER BY interests.description ASC", GetSQLValueString($colname_rsemail, "text"));						
$rsemail = mysql_query($query_rsemail, $CRMconnection) or die(mysql_error());
$row_rsemail = mysql_fetch_assoc($rsemail);
$totalRows_rsemail = mysql_num_rows($rsemail);

$colname_rsnotemail = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rsnotemail = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsnotemail = sprintf("SELECT * FROM interests 
							WHERE description NOT IN (SELECT interests.description FROM interests , contacts_interests 
							WHERE contacts_interests.contact_id = %s AND contacts_interests.interest_id = interests.id)  
							ORDER BY interests.description", GetSQLValueString($colname_rsnotemail, "int"));
$rsnotemail = mysql_query($query_rsnotemail, $CRMconnection) or die(mysql_error());
$row_rsnotemail = mysql_fetch_assoc($rsnotemail);
$totalRows_rsnotemail = mysql_num_rows($rsnotemail);

$colname_userd = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userd = $_SESSION['MM_Username'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userd = sprintf("SELECT * FROM tbluser WHERE tbluser.user_name = %s", GetSQLValueString($colname_userd, "text"));
$userd = mysql_query($query_userd, $CRMconnection) or die(mysql_error());
$row_userd = mysql_fetch_assoc($userd);
$totalRows_userd = mysql_num_rows($userd);

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_userdrm = sprintf("SELECT drm_status FROM contacts WHERE contacts.id = %s ", GetSQLValueString($colname_rsnotemail, "int"));
$userdrm = mysql_query($query_userdrm, $CRMconnection) or die(mysql_error());
$row_userdrm = mysql_fetch_assoc($userdrm);
$totalRows_userdrm = mysql_num_rows($userdrm);

mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsCtype = "SELECT * FROM tblcontacttype ORDER BY ContactType ASC";
$rsCtype = mysql_query($query_rsCtype, $CRMconnection) or die(mysql_error());
$row_rsCtype = mysql_fetch_assoc($rsCtype);
$totalRows_rsCtype = mysql_num_rows($rsCtype);


if (isset($_SESSION["MM_Username"])) {
  $colname_onclientlist = $_SESSION["MM_Username"];
}
$colname2_onclientlist = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname2_onclientlist = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_onclientlist = sprintf("SELECT tblfavourites.tblFav_id, tblfavourites.fav_Username, tblfavourites.tbl_cnt_Name_Desc, tblfavourites.tbl_cnt_Id, tblfavourites.tbl_Tiering, tblfavourites.fav_nick, tblfavourites.listid, cnt_listnames.listname, cnt_listnames.ownerusername, cnt_listnames.shared, cnt_listnames.editablebyothers FROM tblfavourites, cnt_listnames WHERE cnt_listnames.listid=tblfavourites.listid AND ( tblfavourites.fav_Username= %s OR cnt_listnames.shared=1) AND tblfavourites.tbl_cnt_Id=%s", GetSQLValueString($colname_onclientlist, "text"),GetSQLValueString($colname2_onclientlist, "int"));
$onclientlist = mysql_query($query_onclientlist, $CRMconnection) or die(mysql_error());
$row_onclientlist = mysql_fetch_assoc($onclientlist);
$totalRows_onclientlist = mysql_num_rows($onclientlist);

$colname_availlist = "none";
if (isset($_SESSION['MM_Username'])) {
  $colname_availlist = $_SESSION['MM_Username'];
}
$colname2_availlist = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname2_availlist = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_availlist = sprintf("SELECT * FROM cnt_listnames WHERE cnt_listnames.listid NOT IN ( SELECT tblfavourites.listid FROM tblfavourites WHERE tblfavourites.fav_Username=%s AND tblfavourites.tbl_cnt_Id = %s ) AND  (cnt_listnames.ownerusername= %s OR cnt_listnames.listid=1 OR (cnt_listnames.shared= 1 AND cnt_listnames.editablebyothers=1))  ORDER BY cnt_listnames.listname ASC", GetSQLValueString($colname_availlist, "text"),GetSQLValueString($colname2_availlist, "int"),GetSQLValueString($colname_availlist, "text"));
$availlist = mysql_query($query_availlist, $CRMconnection) or die(mysql_error());
$row_availlist = mysql_fetch_assoc($availlist);
$totalRows_availlist = mysql_num_rows($availlist);

$colname_rsaddphone = "-1";
if (isset($_GET['cnt_Id'])) {
  $colname_rsaddphone = $_GET['cnt_Id'];
}
mysql_select_db($database_CRMconnection, $CRMconnection);
$query_rsaddphone = sprintf("SELECT * FROM addphone WHERE cnt_id = %s", GetSQLValueString($colname_rsaddphone, "int"));
$rsaddphone = mysql_query($query_rsaddphone, $CRMconnection) or die(mysql_error());
$row_rsaddphone = mysql_fetch_assoc($rsaddphone);
$totalRows_rsaddphone = mysql_num_rows($rsaddphone);

$queryString_rscontacts = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rscontacts") == false && 
        stristr($param, "totalRows_rscontacts") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rscontacts = "&" . htmlentities(implode("&", $newParams));
  }
}
//$queryString_rscontacts = sprintf("&totalRows_rscontacts=%d%s", $totalRows_rscontacts, $queryString_rscontacts);

$userObj = new User();
$kamDetails = array();
$dealerDetails = array();

if( isset($_GET['cnt_Id']) ) { 
	$kamDetails = $userObj->GetClientKAM( $_GET['cnt_Id'] );
	$dealerDetails = $userObj->GetClientDealers( $_GET['cnt_Id'] );
} 

############################################## GET THE Chatroom Bearer and Chat room name ##################################
$company_tier 				= $row_userdet['companytier'] .' '.'Clients';
$get_chatroom_details_sql 	= "SELECT `config`.`key`,
							    `config`.`value`
							  FROM `avcrm`.`config` where `config`.`key` = '$company_tier'";

$run_sql_query 				=  mysql_query($get_chatroom_details_sql, $CRMconnection);
if (!$run_sql_query) {
	die( "There was an error running the query " . mysql_error($CRMconnection));
}
$row 						      = mysql_fetch_assoc($run_sql_query);
$chat_room 					  = $row['key'];  //room names have client prepending the name where the company tier doesn't
$bearer 					    = $row['value'];

############################################ End Code here ################################################################
try {
	$get_hipchat_room_name    = $hipchat_instance->get_all_rooms();
}
catch( Exception $e ) {
	$get_hipchat_room_name = array();
}

###### get hipchat rooms query  / this code can be moved into the class instead to keep the code neat and tidy#########################
$get_rooms                = "SELECT hipchat_room_id FROM tblclient_hipchatrooms where client_id = ".$_GET['cnt_Id']. "";

$client_rooms_id          = array();
$rooms                    = null;
$run_rooms_query          = mysql_query($get_rooms, $CRMconnection);
if (!$run_rooms_query)  {
  die ('Error fetching Data '. mysql_error($CRMconnection));
} else {  
  while ($row         = mysql_fetch_array($run_rooms_query)) {
    $client_room      = $row['hipchat_room_id'];
    //for the client
    foreach ($get_hipchat_room_name as $room_key => $room) {
        if ($client_room == $room->getId()) {
            $client_rooms ['client_rooms'][] = $room->getName();
            $client_rooms_id ['client_rooms_id'][] = $room->getId();
        }
    }
  } 
}


if (!empty($row_userdet['comp_id'])) {
    $get_rooms                = "SELECT hipchat_room_id FROM tblcompany_hipchatrooms where company_id = ".$row_userdet['comp_id']. "";

    $rooms                    = null;
    $run_rooms_query          = mysql_query($get_rooms, $CRMconnection);
    if (!$run_rooms_query)  {
      die ('Error fetching Data '. mysql_error($CRMconnection));
    } else {  
      while ($row         = mysql_fetch_array($run_rooms_query)) {
        $company_room      = $row['hipchat_room_id'];
        //for the client
        foreach ($get_hipchat_room_name as $room_key => $room) {
            if ($company_room == $room->getId()) {
                $client_rooms ['company_rooms'][] = $room->getName();
                 $client_rooms_id ['company_rooms_id'][] = $room->getId();
            }
        }
      } 
  }
}
########################## Get the platform from the Db for Hipchat code #################
$get_platfrom = "SELECT * FROM avcrm.config";
$run_query    = mysql_query($get_platfrom, $CRMconnection) or die("Error Getting Data" . mysql_error());

while ($config_row = mysql_fetch_array($run_query)) {
  $key   = $config_row['key'];
  switch ($key) {
    case 'platform':
      # code...
      $platform = $config_row['value'];
      break;
    default:
      # code...
      break;
  }
}


########################### Get Analyst Data #############
 $note_admins_allowed = array('Administrator','NoteAdmin');

  if (in_array($_SESSION['MM_UserGroup'], $note_admins_allowed)) {
      $show_analyst_dropdown = "";
  } else {
      $show_analyst_dropdown = "none";
  }

  $analyst_query      = "SELECT CONCAT(u.user_fname,' ',u.user_lname) as full_name, u.user_name FROM avcrm.tbluser u 
                     WHERE u.user_level <> 'Administrator' OR u.user_level <> 'NoteAdmin' ORDER BY full_name ASC;";

 $run_analyst_query   = mysql_query($analyst_query, $CRMconnection);
  
 if ($run_analyst_query) {
    $analyst_select         = "<select name=analyst_list id=analyst_list>";

    $analyst_select        .= "<option selected value=''> -- Select Analyst -- </option>";
    while ($analyst_row     = mysql_fetch_array($run_analyst_query)) {
     $analyst_option_name   = urlencode($analyst_row['full_name']);
     $analyst_display_name  = $analyst_row['full_name'];
     $analyst_username      = $analyst_row['user_name'];
     ### Spaces have an issue when setting them as option values need to be encoded ##########
     $analyst_select       .= "<option value=$analyst_username>".$analyst_display_name."</option>";
    }
    $analyst_select        .= "</select>";
 }                 
########################### End Analyst Data #############

########################## Get the platform from the Db for Hipchat code #################
$get_platfrom = "SELECT * FROM avcrm.config";
$run_query    = mysql_query($get_platfrom, $CRMconnection) or die("Error Getting Data" . mysql_error());

while ($config_row = mysql_fetch_array($run_query)) {
  $key   = $config_row['key'];
  switch ($key) {
    case 'platform':
      # code...
      $platform = $config_row['value'];
      break;
    default:
      # code...
      break;
  }
}

########################## Get the platform from the Db for Hipchat code #################

?>
<?php require_once('includes/sitevars.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/t2.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
<link rel="manifest" href="favicons/manifest.json">
<link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Avior Capital Markets CRM</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->

<style type="text/css">
  .timer {
    visibility: hidden;
  }
</style>
<link type="text/css" href="css/kdes1/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<link type="text/css" href="css/datatables/dataTables.twitter.bootstrap.css" rel="stylesheet" />
<link type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css" rel="stylesheet" />
<link type="text/css" href="css/skipjack.css" rel="stylesheet" />
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/jquery.linkify.min.js"></script>
<script type="text/javascript" src="js/zeroclipboard-2.2.0/dist/ZeroClipboard.min.js"></script>
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/mobile-device-sync.js"></script>
<script type="text/javascript" src="js/clientnotes.js"></script>
<script type="text/javascript" src="js/getEmails.js"></script>
<script type="text/javascript" src="js/mitel-call.js"></script>
<script type="text/javascript" src="js/jquery.simple.timer.js"></script>
<script type="text/javascript">
			var chat_room                 = "<?php echo $chat_room;?>";
      var platform                  = "<?php echo $platform;?>";
			var bearer                    = "<?php echo $bearer;?>";
      var client_hipchat_rooms      = <?php echo json_encode($client_rooms_id);?>;
     
			$(function(){

        $('.timer').startTimer({
            onComplete: function(){
              $("#mitel_pop_up").dialog('close');
            }
         });
				// Accordion
				//$("#accordion").accordion({ header: "h3" });
				// Tabs
				$('#tabs1').tabs();
				$('#tabs2').tabs();
				// Dialog Link
				$('#dialog_link').click(function(){
					$('#dialog').dialog('open');
					return false;
				});
				// Datepicker
				$('#note_date').datepicker({
					dateFormat: 'yy-mm-dd'
				});
				//hide add reminder box
				$('#addreminder').hide();
				
				$('.itemDelete').live('click', function  () {
					$(this).closest('li').remove();
					//$(this).closest('li').
				});
				//reminder button -open form
				$('#btnaddreminder').click(function(){
					$('#addreminder').show('fold');
					return false;
				});
				//close reminder form
				$('#btnCancelRemind').click(function(){
					$('#addreminder').hide('fold');
					return false;
				});
				
				//hide id's on initial load;
				$('#addnoteform').hide();
				$('#addreminderform').hide();
				$('#cancelnotebutton').hide();
				$('#newulist').hide();
				$('#removeulist').hide();
				$('#modulist').hide();
				
				
				//hide add notes
				$('#addnotebutton').click( function(){
					$('#addnotebutton').hide();
					$('#addnoteform').show('fold');
					$('#cancelnotebutton').show('fold');
					return false;
				});
				
				$('#cadremin').click(function(){
					$('#addreminderform').show();
					$('#cancelnotebutton').show();
				});
				
				//user list hide/show
				$('#btnnewulist').click( function(){
					$('#removeulist').hide();
					$('#modulist').hide();
					$('#newulist').show('fold');
				});
				
				$('#btnremoveulist').click( function(){
					$('#newulist').hide();
					$('#modulist').hide();
					$('#removeulist').show('fold');
				});
				
				$('#btnmodulist').click( function(){
					$('#newulist').hide();
					$('#removeulist').hide();
					$('#modulist').show('fold');
					
				});
				
				//cancel buttons in user list operation boxes
				$('#btncancelnewulist').click( function(){
					$('#newulist').hide('fold');
				});
				
				$('#btncancelremoveulist').click( function(){
					$('#removeulist').hide('fold');
				});
				$('#btncancelmodulist').click( function(){
					$('#modulist').hide('fold');
				});
					
				//operations for user lists
				//new user list
				$('#confaddulist').click(function (){
				// do this
					//get variables from frmaddnewlist
					var tmp =$('#frmaddnewlist').serialize();
					//do query and load into #avvaillist div
					///show loader
					$('#availlistspan').empty().html('<img src="images/ajax-loader.gif" />');
					$('#availlistspan').load('userclientlistavaillist.php?'+tmp);
					$('#newulist').hide('fold');
				})
				//cancel new note button operations
				$('#cancelnotebutton').click( function(){
					$('#addnoteform').hide();
					$('#addreminderform').hide();
					$('#cancelnotebutton').hide();
					$('#addnotebutton').show();
					$('#cadremin').show();
					return false;
				});
				
				
				
				
				var client_image = $('#client_image').val();
				var client_imagesize = $('#client_imagesize').val();
				
				
			});
		// applies email subscription change
		function doemailsubchange(contact_id,interest_id, targetid){
		 	var loadline= "clientmailsubajax.php?contact_id=" + contact_id + "&MM_addmailsub=donow&interest_id="+interest_id;
			$(targetid).load(loadline);
		}
				
		//function to do loader more ubiquities
		function showloader(targetid){
		$(targetid).empty().html('<img src="images/ajax-loader.gif" />');
				}	
				
				//function to retrieve form values from a specific form  - identified by id and put them into a line for GET

function noteupdate(cnt_id,targetid){
	var queryline =  "clientdetailsajaxops.php?MM_noteupdate=updateusernote&cnt_Id=" + cnt_id + "&cnt_note=" ;
	queryline +=	escape($('#cnt_note').val()) ;
	$(targetid).load(queryline);
}

function loadColleagues(cnt_id) {
	var queryline =  "ajaxClientColleagues.php?cnt_Id=" + cnt_id ;
	$("#tabs2-2").load(queryline);
}
	
//gets form data from formid, puts it into url data, GET's results from targetURL, and resturns results to resultsout div tag
function ajaxformget(formid, targeturl, resultsout) {
	//gets form inputs from specified form
	var tt = $(formid).serialize();
	var turl = targeturl + "?" + tt;
	$(resultsout).load(turl);
}
</script>

<!-- InstanceEndEditable -->
<script language="javascript">
	$(function(){
		
		$("#quicksearch").autocomplete({
					source: "quicksearchauto.php",
					minLength: 2,
					select: function( event, ui ) {
						window.location.href = ('clientdetails.php?cnt_Id='+ui.item.id );
					},
          appendTo: '.quick-search-container'
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					var listItem = $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);
				
					if (item.active == 0) {
						listItem.addClass("inactive");
					}
					return listItem;
					};
	})
</script>	
<?php require_once('includes/inhead.php'); ?>
<link href="css/2.css" rel="stylesheet" type="text/css" />
</head>
<div class="timer" data-seconds-left ='7'></div>
<body>
		<div class="header" id="main-header"><h1><?php echo $sitetitle; ?></h1><p><? echo $subtitle; ?></p></div>
		<div class="content200p">
			<div><h3><a href="index.php" target="_parent">Home</a></h3></div>
			<div><h3><a href="mailmanager.php" target="_parent">Mailing Lists</a></h3></div>
			<div><h3><a href="companyratings.php" target="_parent">Institutions</a></h3></div>
			<div><h3><a href="userfiles.php" target="_parent">Files</a></h3></div>
			<div><h3><a href="adminmain.php" target="_parent">Admin</a></h3></div>
			<div><h3><a href="index.php?logout=1" target="_parent">Logout</a></h3></div>
      <div class="quick-search-container">
          <input type="text" id="quicksearch" size="16" />
      </div>
			<?php require_once('includes/belownav.php'); ?>
	  </div>
<!-- InstanceBeginEditable name="Content" -->
<h2>Client Details : <?php echo $row_userdet['first_name']; ?> <?php echo $row_userdet['surname']; ?></h2>

<div class="content30">
<div id="tabs1">
      <ul>
        <li><a href="#tabs1-1">Details</a></li>
        <li><a href="#tabs1-2">Responsibility List</a></li>
        <li><a href="#tabs1-3">Email subscriptions</a></li>
      </ul>
      <div id="tabs1-1">
		  <div id="ajaxt2" style="border:1px solid #CCC;">
			<fieldset>
				<legend>Client details</legend>
				<?php if( $_SESSION['MM_UserGroup'] == 'Administrator' ) { ?>
				 <div class="edit-link"><a href="clientdetails-edit.php?cnt_Id=<?php  echo $row_userdet['id'];?>">Edit</a></div>
				 <?php } ?>
				 													
				  <table align="left" border="0" class="client-details-table">
					<tr>
					<td rowspan="7" valign="top" align="center"><div class="main-profile">
							<?php  if (file_exists("uploads/clientp/".$colname_userdet."/1.jpg")){ ?>
									<img src="uploads/clientp/<?php echo $colname_userdet ;?>/1.jpg" />
							<?php } else {
								// echo "No picture yet";
								?>									
									<img src="<?php echo $userObj->getGravatar(htmlentities($row_userdet['email'], ENT_COMPAT, 'utf-8'));?>" />
							<?php } ?>
							</div>							
							</td>
					  <td align="left"><?php echo htmlentities($row_userdet['first_name'], ENT_COMPAT, 'utf-8')." ".htmlentities($row_userdet['surname'], ENT_COMPAT, 'utf-8'); ?></td>
					</tr>
					<tr>
						<td align="left"><?php echo htmlentities($row_userdet['jobtitle'], ENT_COMPAT, 'utf-8'); ?></td>
					</tr>
					<tr>
					  <td align="left">
					  <?php if (isset($row_userd['user_extension'])) {?>
					  		<a href="javascript:void(0);" class ="phone_disable" onclick="callMitel('<?php echo $row_userd['user_extension'];?>','<?php echo $row_userdet['landline'];?>','<?php echo $row_userdet['first_name'];?>','<?php echo $row_userdet['surname'];?>')"> <img src="images/phoneicon.png" width="20" height="20" align="absmiddle" /></a>
					  <?php } else { ?>
					  	 	<a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userdet['landline']; ?>')"> <img src="images/phoneicon.png" width="20" height="20" align="absmiddle" /></a>
					  <?php } ?>
					  	 
					  <?php echo htmlentities($row_userdet['landline'], ENT_COMPAT, 'utf-8'); ?> <br />
					   
					   <?php if ($totalRows_rsaddphone >0){?>  <?php
					   do { ?>
					   <?php if (isset($row_userd['user_extension'])) { ?>
					   		<a href="javascript:void(0);" class ="phone_disable" onclick="callMitel('<?php echo $row_userd['user_extension'];?>','<?php echo $row_rsaddphone['phone_num'];?>','<?php echo $row_userdet['first_name'];?>','<?php echo $row_userdet['surname'];?>')"><img src="images/phoneicon.png" width="20" height="20" align="absmiddle" /></a>
					   <?php } else { ?>
					   	 	<a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_rsaddphone['phone_num']; ?>')"><img src="images/phoneicon.png" width="20" height="20" align="absmiddle" /></a>
					   <?php } ?>
						 <?php echo $row_rsaddphone['phone_num']; ?><br />
						  <?php } while ($row_rsaddphone = mysql_fetch_assoc($rsaddphone)); } ?>
						<!-- <a href="additphonenum.php?cnt_Id=<?php echo $row_userdet['id']; ?>">Manage</a> -->
						</td>
					</tr>
					<!-- 
					<tr>
					  <td nowrap="nowrap" align="right">Fax:</td>
					  <td><?php echo htmlentities($row_userdet['fax'], ENT_COMPAT, 'utf-8'); ?></td>
					</tr>
					-->
					<tr>
					 <?php if (isset($row_userd['user_extension'])) { ?>
					  	<td align="left"><a href="javascript:void(0)" class="phone_disable" onclick="callMitel('<?php echo $row_userd['user_extension'];?>','<?php echo $row_userdet['mobile']; ?>','<?php echo $row_userd['first_name'];?>','<?php echo $row_userd['surname'];?>')"> <img src="images/mobile_icon.png" width="20" height="20" align="absmiddle" /></a> <?php echo htmlentities($row_userdet['mobile'], ENT_COMPAT, 'utf-8'); ?></td>

					 <?php } else {?>
					 	<td align="left"><a href="#" onclick="$('#phonebox').load('http://<?php echo $row_userd['user_phone_ip']; ?>/command.htm?number=<?php echo $row_userdet['mobile']; ?>')"> <img src="images/mobile_icon.png" width="20" height="20" align="absmiddle" /></a> <?php echo htmlentities($row_userdet['mobile'], ENT_COMPAT, 'utf-8'); ?></td>
					 <?php } ?>	
					 </tr>
					</tr>
					<tr>
					  <td align="left"><a href="mailto:<?php echo htmlentities($row_userdet['email'], ENT_COMPAT, 'utf-8'); ?>"> <img src="images/email-icon24.png" width="16" height="16" align="absmiddle" /></a> <?php echo htmlentities($row_userdet['email'], ENT_COMPAT, 'utf-8'); ?>
					  <?php if ($row_userdet['email'] != '') { ?>
					   &nbsp;<a href="#" title="Copy Email" id="copy-email" data-clipboard-text="<?php echo htmlentities($row_userdet['email'], ENT_COMPAT, 'utf-8'); ?>"><img src="images/copyimage.png" alt="Copy Email" border="0" /></a> 
					   <span>

					   <a href="javascript:void(0);" onClick="sendVcard('<?php echo htmlentities($row_userdet['first_name'], ENT_COMPAT, 'utf-8');?>',
				   			'<?php echo htmlentities($row_userdet['surname'], ENT_COMPAT, 'utf-8');?>','<?php echo $row_userdet['Company'];?>',
				   			'<?php echo htmlentities($row_userdet['email'], ENT_COMPAT, 'utf-8');?>',
				   			'<?php echo htmlentities($row_userdet['mobile'], ENT_COMPAT, 'utf-8');?>',
				   			'<?php echo $row_userdet['landline'];?>','uploads/clientp/<?php echo $colname_userdet ;?>/1.jpg','<?php echo $userEmail;?>','<?php echo htmlentities($row_userdet['jobtitle'], ENT_COMPAT, 'utf-8');?>'
				   			);">

						   		<img src="images/vcard.png" width='14' height="16"  alt="Send Email" border="0" align="absmiddle" >
					   </a>
					   </span>
   					   <span>
							<div id='ajax-loader' align="absmiddle" style="text-align: center; display:none;">
								<img src="images/ajax-loader.gif"/>
							</div>
					   </span>
					   <br/>
					   <span id="copy-email-error"></span>
					   <span id="copy-email-success"></span>
					   <?php } ?>
					   </td>
					</tr>
					<!--
					<tr>
					  <td nowrap="nowrap" align="right">Birthday:</td>
					  <td><?php echo htmlentities($row_userdet['dob'], ENT_COMPAT, 'utf-8'); ?> </td>
					</tr>
					-->
					<tr>
					  <td>
					   <?php 
					  $color=""; 
					  $outcolor="";
					  $tve= $row_userdet['companytier'] ;
					  switch ($tve){ 
					  case 'Platinum':
						$outcolor= "#E5E4E2";
						$color = "#333333";	
						break;
					  case 'Gold' :
						$outcolor= "#B8860B";
						$color = "#FFF";	
						break;
					  case 'Silver' :
						$outcolor= "#C0C0C0";	
						$color = "#333333";	
						break;
					  case 'Blue' :
						$outcolor= "#0A238C";	
						$color = "#FFF";
						break;	
					case 'Bronze' :
						$outcolor= "#f4a460";
						$color = "#333333";		
						break;
					case 'N/A' :
						case 'Not Assigned' :
						$outcolor= "#fff";	
						break;	
					  default :
						$outcolor= "#CE1620";
						$color = "#FFF";	
					  }
					  ?>
					  <span style="width:100%;display:block; font-size:1em; background:<?php echo $outcolor; ?>; padding:4px; border-radius:3px;">
					  <a href="companydetails-view.php?id=<?php echo $row_userdet['comp_id']; ?>&back_cid=<?php echo $colname_userdet; ?>" style="color: <?php echo $color; ?>"><?php echo $row_userdet['Company'];?></a>
					  </span>
					  </td>
					</tr>

					<tr>
						<td nowrap="nowrap" align="left" colspan="2">
							
							<div class="other-cnt">
							<?php if( sizeof($kamDetails) >0 ) { ?>
						
							<?php  if(isset( $kamDetails[0]['id_kam1'])){ ?>
								<div class="image-circle">
									<?php if(file_exists("uploads/clientp/".$kamDetails[0]['id_kam1']."/1.jpg") ) { ?>
									<div class="image"><a href="clientdetails.php?cnt_Id=<?php echo $kamDetails[0]['id_kam1']; ?>" title="View Profile"><img src="uploads/clientp/<?php echo $kamDetails[0]['id_kam1'];?>/1.jpg" /></a></div>
									<?php } else { echo '<div class="no-image">No picture yet.</div>'; } ?>
									<div class="desc">KAM 1</div>
								</div>
							<?php }  ?>
							
							<?php  if(isset( $kamDetails[0]['id_kam2'])){ ?>
								<div class="image-circle">
									<?php if(file_exists("uploads/clientp/".$kamDetails[0]['id_kam2']."/1.jpg") ) { ?>
									<div class="image"><a href="clientdetails.php?cnt_Id=<?php echo $kamDetails[0]['id_kam2']; ?>" title="View Profile"><img src="uploads/clientp/<?php echo $kamDetails[0]['id_kam2'];?>/1.jpg" /></a></div>
									<?php } else { echo '<div class="no-image">No picture yet.</div>'; } ?>
									<div class="desc">KAM 2</div>
								</div>
							<?php }  ?>
							
							<?php  if(isset( $dealerDetails[0]['id_dealer1'])){ ?>
								<div class="image-circle">
									<?php if(file_exists("uploads/clientp/".$dealerDetails[0]['id_dealer1']."/1.jpg") ) { ?>
									<div class="image"><a href="clientdetails.php?cnt_Id=<?php echo $dealerDetails[0]['id_dealer1']; ?>" title="View Profile"><img src="uploads/clientp/<?php echo $dealerDetails[0]['id_dealer1'];?>/1.jpg" /></a></div>
									<?php } else { echo '<div class="no-image">No picture yet.</div>'; } ?>
									<div class="desc">Dealer 1</div>
								</div>
							<?php }  ?>
							
							<?php  if(isset( $dealerDetails[0]['id_dealer2']) ){ ?>
								<div class="image-circle">
									<?php if(file_exists("uploads/clientp/".$dealerDetails[0]['id_dealer2']."/1.jpg") ) { ?>
									<div class="image"><a href="clientdetails.php?cnt_Id=<?php echo $dealerDetails[0]['id_dealer2']; ?>" title="View Profile"><img src="uploads/clientp/<?php echo $dealerDetails[0]['id_dealer2'];?>/1.jpg" /></a></div>
									<?php } else { echo '<div class="no-image">No picture yet.</div>'; } ?>
									<div class="desc">Dealer 2</div>
								</div>
							<?php } } ?>
								</div>
							</div>
						</td>
					</tr>
					
					<?php if($row_userdet['bloomberg_status'] == '1') { ?>
					
					<tr>
						<td nowrap="nowrap" valign="bottom"  align="right">Bloomberg User:</td>
						<td>Yes <img src="images/bloomberg-user.jpg" width="100" alt="Bloomberg User" border="0" /></td>
					</tr>
					<?php } else { ?>
					<tr>
						<td nowrap="nowrap" align="right">Bloomberg User:</td>
						<td><?php if ($row_userdet['bloomberg_status'] == '0') { echo "No"; } else { echo "Not set"; } ?> </td>
					</tr>
					<?php } ?>
					<tr>
						<td nowrap="nowrap" align="right">User of:&nbsp;</td>
						<td>
						<div id="mobile-sync-links">
							<div class="device-icon" id="android-activity-image"><a href="javascript://" id="android-activity" title="Click to view android activity"><img src="images/android_phone_avior2.png" width="40" align="Android" border="0" /></a></div>
							<div class="device-icon" id="ipad-activity-image"><a href="javascript://" id="ipad-activity" title="Click to view ipad activity"><img src="images/ipad_avior2.png" width="40" align="Ipad" border="0" /></a></div>
						</div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<fieldset>
								<legend>Notes</legend>
								<p id="client-notes"><?php echo nl2br($row_userdet['note']); ?></p>
								<!--
								<textarea name="cnt_note" id="cnt_note" onblur="noteupdate(<?php echo $row_userdet['id']; ?>,'#noteupresult');" cols="45" rows="5"><?php echo $row_userdet['note']; ?></textarea>-->
								<p><small>Notes and keywords here are searchable from the quicksearch box</small></p>
								<div id="noteupresult"></div>
								<div id="client-notes-edit-form" title="Edit Note">
								<form id="client-notes-form" method="post" action="">
									<label for="editnote">Note:</label><br />
									 <textarea id="clienteditnote" name="clienteditnote" rows="10" cols="45"></textarea>
								</form>
							</div>
							</fieldset>
						</td>
					</tr>
					
          <tr>
            <td colspan="2">
              <fieldset>
                <legend>Hipchat Room(s)</legend>
                <p>
                  <table>
                    <tr>
                        <td> 
                           Company Tier
                        </td>
                        <td style="background-color: <?php echo $outcolor;?>">
                          <div style="color: <?php echo $color;?>">
                           <?php   echo !empty($row_userdet['companytier']) ? $row_userdet['companytier']: "Company tier not set"; ?>
                          </div>
                        </td>
										</tr>
										
                    <tr>
                      <td>
                        Company Rooms
                      </td>
                      <td>
                        <?php echo !empty($client_rooms['company_rooms']) ? 
                            implode(",", array_unique($client_rooms["company_rooms"])) : "Company Rooms not set";
                        ?>
                      </td>
										</tr>
																				
                    <tr>
                      <td>
                        Client Rooms
                      </td>
                      <td>
                        <?php echo !empty($client_rooms['client_rooms']) ? 
                            implode(",", array_unique($client_rooms["client_rooms"])) : "Client Rooms not set";
                        ?>
                      </td>
                    </tr>

                  </table>
                </p>
              </fieldset>
            </td>
          </tr>

					<tr style="display:none"> 
						<td nowrap="nowrap" align="right">BB Username:</td>
						<td><?php echo htmlentities($row_userdet['bloomberg_user'], ENT_COMPAT, 'utf-8'); ?></td>
					</tr>
					<tr style="display:none">
						<td nowrap="nowrap" align="right">Cnt_bdate:</td>
						<td><?php echo htmlentities($row_userdet['dob'], ENT_COMPAT, 'utf-8'); ?></td>
					</tr> 
					<tr> 
						<td colspan="2">
							<fieldset>
							<legend>Protection Type</legend>
								<?php if ($row_userdrm['drm_status']=="") {

									if($row_userdet['drm_status']=='1'){
										echo 'Company Protection (Watermark)';
									}
									elseif($row_userdet['drm_status']=='2'){
										echo 'Company Protection (DRM) ';
									}	
									elseif($row_userdet['drm_status']=='3'){
										echo 'Company Protection (Web Viewer) ';
									}	
									else{
										echo 'Inactive';
									}
								}
								elseif ($row_userdrm['drm_status']=='1') {
									echo 'Watermark';
								}
								elseif ($row_userdrm['drm_status']=='2') {
									echo 'DRM';
								}
								elseif ($row_userdrm['drm_status']=='3') {
									echo 'Web Viewer';
								}

								else { echo "Inactive"; } ?>

								<?php                
                if ($row_userdrm['drm_status']=='3') {
	                if ($row_userdet['user_webview_print']=='1') {	
										echo '<div><br />Web Viewer Printing Allowed - YES</div>';
									} elseif ($row_userdet['user_webview_print']=='0') {									
										echo '<div><br />Web Viewer Printing Allowed - NO</div>';
									}
								}
								?>
                
							</fieldset>
						</td>
					</tr>
					<tr> 
						<td colspan="2">
							<fieldset>
							<legend>Portal Access</legend>          
              <?php							
												
							if ($row_userdet['user_portal_access'] == '1') {
								echo 'Enabled';
							} elseif ($row_userdet['user_portal_access'] == '0') {
								echo 'Disabled';								
							} else {
								
							$companyPortalAccessSetting = 'Disabled';
							if ($row_userdet['company_portal_access'] == '1') {
								$companyPortalAccessSetting = 'Enabled';
							}
													
								echo "Company Setting [$companyPortalAccessSetting]";
							}								              
							?>
            </fieldset>
					</td>            

					<tr> 
						<td colspan="2">
							<fieldset>
							<legend>Active client ?</legend> 
								<?php if ($row_userdet['active']==1) {echo 'Active';} else { echo "Inactive"; } ?>
							</fieldset>
						</td>
					</tr>
				</table>
		 </fieldset>
	<div>
	 <fieldset>
		<legend>Contact List Management</legend>
		<input type="hidden" name="MM_insert" value="addnewlist" />
		<table width="100%">
			<tr style="vertical-align:top">
				<td width="50%">
					<fieldset>
						<legend>On Lists</legend>
						<span id="onlistspan">

						<?php if(isset($_SESSION['MM_Username']) && isset( $_SESSION['MM_UserGroup'] ) &&  $_SESSION['MM_UserGroup'] == 'Administrator' ) {
       					?>	
       						
									<?php do { ?>
									  <a id="onli<?php echo $row_onclientlist['tblFav_id'] ?>" href="#" onclick="$('#availlistspan').empty().html('<img src=images/ajax-loader.gif />');
									$('#availlistspan').load('<?php echo("userclientlistavaillist.php?tblFav_id=".$row_onclientlist['tblFav_id']."&MM_delete=removefromlist&cnt_Id=".$row_userdet['id']."&sourcepage=clientdetails.php&MM_Username=".$_SESSION['MM_Username']) ?>'); $('#onli<?php echo $row_onclientlist['tblFav_id'] ?>').remove();" class="clicklink"><?php echo $row_onclientlist['listname']; ?><?php if($row_onclientlist['shared']==1){?><img src="images/redbox.png" width="5" height="5" border="0" /><?php } ?></a>
									  <?php } while ($row_onclientlist = mysql_fetch_assoc($onclientlist)); ?>
						<?php
				   	    }
				   		else
				   		{ ?>
				   					<?php do { ?>
									  <a id="onli<?php echo $row_onclientlist['tblFav_id'] ?>" class="clicklink"><?php echo $row_onclientlist['listname']; ?><?php if($row_onclientlist['shared']==1){?><img src="images/redbox.png" width="5" height="5" border="0" /><?php } ?></a>
									  <?php } while ($row_onclientlist = mysql_fetch_assoc($onclientlist)); ?>
				   				
				   		<?php } ?>
						</span>
					</fieldset>
					<img src="images/redbox.png" width="5" height="5" />= shared with other users
				</td>
			 	<td>
					<fieldset>
						<legend>Available lists</legend>
						<span id="availlistspan">

						<?php if(isset($_SESSION['MM_Username']) && isset( $_SESSION['MM_UserGroup'] ) &&  $_SESSION['MM_UserGroup'] == 'Administrator' ) {
       					?>	
								<?php do { ?>
								  <a id="avls<?php echo $row_availlist['listid']; ?>" href="#" onclick="$('#onlistspan').empty().html('<img src=images/ajax-loader.gif />'); $('#onlistspan').load('<?php echo("userclientlistlistajax.php?cnt_Id=".$row_userdet['id']."&fav_Username=".$_SESSION['MM_Username']."&listid=".$row_availlist['listid']."&tbl_cnt_Name_Desc=". urlencode($row_userdet['first_name']." ".$row_userdet['surname'])."&tbl_Tiering=1&fav_nick=none&MM_insert=addtolist") ?>'); $('#avls<?php echo $row_availlist['listid']; ?>').remove()" class="clicklink"><?php echo $row_availlist['listname']; ?><?php if($row_availlist['shared']==1){?><img src="images/redbox.png" width="5" height="5" border="0" /><?php } ?></a>
								  <?php } while ($row_availlist = mysql_fetch_assoc($availlist)); ?>
						<?php
				   	    }
				   		else
				   		{ ?>
				   				<?php do { ?>
								  <a id="avls<?php echo $row_availlist['listid']; ?>" class="clicklink"><?php echo $row_availlist['listname']; ?><?php if($row_availlist['shared']==1){?><img src="images/redbox.png" width="5" height="5" border="0" /><?php } ?></a>
								  <?php } while ($row_availlist = mysql_fetch_assoc($availlist)); ?>
				   		<?php } ?>


						</span>
					</fieldset>
					<input type="button" value="Add" id="btnnewulist" /><input type="button" value="Remove"  id="btnremoveulist"/><input type="button" value="Modify"  id="btnmodulist"/>
					<fieldset id="newulist">
						<legend>Add new list</legend>
						<form  id="frmaddnewlist" name="frmaddnewlist">
						<span style="text-align:right;">
						Listname: <input type="text" name="listname" value="" size="24" /><br />
							  <input type="checkbox" name="shared" value="1" style="display:none;"/> <br />
							  <input type="checkbox" name="editablebyothers" value="1" style="display:none;" checked/><br /><input type="button" value="Cancel" id="btncancelnewulist" /><input type="button" value="Add" id="confaddulist" /></span><input type="hidden" name="MM_Username" value="<?php echo $colname_onclientlist ?>"  />
							  <input type="hidden" name="MM_insert" value="frmaddnewlist" />
							  <input type="hidden" name="cnt_Id" value="<?php echo $colname_rscontacts ?>" />
							  <input type="hidden" name="sourcepage" value="clientdetails.php" />
						</form>
					</fieldset>
					<fieldset id="removeulist">
						<legend>Remove list</legend>
						<input type="button" value="Cancel" id="btncancelremoveulist" />
					</fieldset>
					<fieldset id="modulist">
						<legend>Modify list</legend>
						<input type="button" value="Cancel" id="btncancelmodulist" />
					</fieldset>
				</td>
			</tr>
		</table> 
	 </fieldset>
	</div>
	</div>
 </div>
 	  <?php $isadmin = isset($_SESSION['MM_Username']) && (isAuthorized("x","Administrator", $_SESSION['MM_Username'], $_SESSION['MM_UserGroup']));?>
      <div id="tabs1-2">
		  <fieldset id="isresp" style="display:block; border:#CCC thin solid">
			  <legend>Responsibilities<?php if($isadmin) {echo " - Admin View"; }?></legend>
			  	<?php do { ?>
			  		<?php if($isadmin) { ?>
						<a href="#" id="r<?php echo $row_responsib['idcntresponsibility']; ?>" onclick="$('#r<?php echo $row_responsib['idcntresponsibility']; ?>').remove();showloader('#notresp'); $('#notresp').load('clientresponsibajax.php?cnt_Id=<?php echo $row_userdet['id']; ?>&idcntresponsibility=<?php echo $row_responsib['idcntresponsibility']; ?>&MM_removeresp=yes')" class="clicklink"><?php echo $row_responsib['response_desc']; ?></a>
					<?php }else { ?>
						<div id="r<?php echo $row_responsib['idcntresponsibility']; ?>"  class="clicklink"><?php echo $row_responsib['response_desc']; ?></div>
					<?php } ?>


				<?php } while ($row_responsib = mysql_fetch_assoc($responsib)); ?>
		  </fieldset>

      <fieldset id="notresp" style="display:block; border:#CCC thin solid">
      		<legend>Available List</legend> 
      			<?php do { ?>
      				<?php if($isadmin) { ?>
        				<a href="#" id="nr<?php echo $row_resplist['respons_id']; ?>" onclick="$('#nr<?php echo $row_resplist['respons_id']; ?>').remove();showloader('#isresp'); $('#isresp').load('clientresponsibajax.php?cnt_Id=<?php echo $row_userdet['id']; ?>&MM_addresp=donow&respons_id=<?php echo $row_resplist['respons_id']; ?>')" class="clicklink"><?php echo $row_resplist['response_desc']; ?></a>
        	    	<?php }else { ?>
        	    		<div id="nr<?php echo $row_resplist['respons_id']; ?>" class="clicklink"><?php echo $row_resplist['response_desc']; ?></div>
        	    	<?php } ?>	
        		<?php } while ($row_resplist = mysql_fetch_assoc($resplist)); ?>
      </fieldset>
      </div>
      <div id="tabs1-3">
      <fieldset id="ismailsub" style="display:block; border:#CCC thin solid">
      <?php $isadmin = isset($_SESSION['MM_Username']) && (isAuthorized("x","Administrator", $_SESSION['MM_Username'], $_SESSION['MM_UserGroup']));?>

      <legend>Subscriptions</legend>
       <?php do { ?>
       <?php if($isadmin) { ?>
         <div id="ismailsub<?php echo $row_rsemail['id']; ?>" class="clicklink" ><a href="#"  onclick="$('#ismailsub<?php echo $row_rsemail['id']; ?>').remove();showloader('#isnotmailsub');$('#isnotmailsub').load('clientmailsubajax.php?contact_id=<?php echo $row_userdet['id']; ?>&interest_id=<?php echo $row_rsemail['id']; ?>&removemail=yes')" ><?php echo $row_rsemail['description']; ?></a><span class="superscript"><?php if($row_rsemail['drm_status'] == "2"){ echo "DRM" ; } else { echo '';}  ?></span></div>
        <?php }else { ?> 
         <div id="ismailsub<?php echo $row_rsemail['id']; ?>" class="clicklink" ><?php echo $row_rsemail['description']; ?><span class="superscript"><?php if($row_rsemail['drm_status'] == "2"){ echo "DRM" ; } else { echo '';}  ?></span></div>
        <?php } ?>
         <?php } while ($row_rsemail = mysql_fetch_assoc($rsemail)); ?>
      </fieldset>
      <fieldset id="isnotmailsub" style="display:block; border:#CCC thin solid">
      <legend>Other Available</legend>
      <?php do { ?>
      <?php if($isadmin) { ?>


       <div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>" class="clicklink"> <a href="#"  onclick="showloader('#ismailsub');doemailsubchange('<?php echo $row_userdet['id']; ?>','<?php echo urlencode($row_rsnotemail['id']); ?>', '#ismailsub' );$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>').remove()"> <?php echo $row_rsnotemail['description']; ?></a><div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>i" style="display:inline;">&nbsp;</div><script language="javascript">$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>i').hide();</script></div>
      <?php }else { ?> 
      <div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>" class="clicklink"> <?php echo $row_rsnotemail['description']; ?><div id="isnotmailsub<?php echo $row_rsnotemail['id']; ?>i" style="display:inline;">&nbsp;</div><script language="javascript">$('#isnotmailsub<?php echo $row_rsnotemail['id']; ?>i').hide();</script></div>
      <?php } ?>
      <?php } while ($row_rsnotemail = mysql_fetch_assoc($rsnotemail)); ?>
      </fieldset>
      </div>
  </div>
</div>
<div class="content50">
	<div id="tabs2">
		  <ul>
			<li><a href="#tabs2-1">Recent Contact</a></li>
			<li><a href="#tabs2-2" onclick="loadColleagues(<?php echo $row_userdet['id']; ?>)">Colleagues</a></li>
			<li id="ipad-activity-tab"><a href="#tabs2-3">iOS Activity</a></li>
			<li id="android-activity-tab"><a href="#tabs2-4">Android Activity</a></li>
			<li id="emails-read-tab"><a href="#tabs2-5">Read Emails</a></li>
			<!-- <li><a href="#tabs2-3">Other Notes</a></li>-->
		  </ul>
		  <div id="tabs2-1">
			  <fieldset>
				  <legend>Notes and reminders</legend>
				  <input type="button" id="addnotebutton" value="Add Note" /><input type="button" value="Add reminder" id="cadremin" />
				  <input type="button" id="cancelnotebutton" value="Cancel" />
				  <!-- add reminder form -->
				  <form id="addreminderform" style="background:#ccc;" >
				  <table>
				  	<tr>
						<td>Reminder:<br />
						<input name="remtext" type="text" size="30" />
						<input type="button" value="Save" />
						</td>
					</tr>
				  </table>
				  </form>
				  <!-- add notes form -->
				  <form id="addnoteform" style="background:#ccc;">
					  <table>
						  <tr>
						  	<td valign="top">Note:<br /> 
						  	<textarea name="note_txt" rows="4" id="note-txt"></textarea>
							</td>
						  	<td valign="top">Date:<br /> 
								<input name="note_date" type="text" id="note_date" value="<?php echo date("Y-m-d"); ?>" /><br />
								Time<br />
								<input type="time" name="note_time" id="note_time" value=""><br />
								Type<br />
								<select name="note_type" id="note_type">
							  <?php do { ?>
								  <option value="<?php echo $row_rsCtype['ContactType']?>"><?php echo $row_rsCtype['ContactType']?></option>
							  <?php
						} while ($row_rsCtype = mysql_fetch_assoc($rsCtype));
						  $rows = mysql_num_rows($rsCtype);
						  if($rows > 0) {
							  mysql_data_seek($rsCtype, 0);
							  $row_rsCtype = mysql_fetch_assoc($rsCtype);
						  }
						?>
								</select>
                <div style="display: <?php echo $show_analyst_dropdown;?>">
                    Note By<br/>
                    <?php 
                      echo $analyst_select;
                    ?>
                </div>
						</td>
						<td>
              <input type="hidden" id="client_company" name = "client_company" value="<?php echo $row_userdet['Company'];?>"/>
              <input type="hidden" id="client_name" name = "client_company" value="<?php echo $row_userdet['first_name'] ." ".$row_userdet['surname'];?>"/>
							<input type="button" id="savenewnotebutton" value="Save" /><input type="hidden" name="MM_noteadd" value="addnote" />
							<input type="hidden" name="note_by" value="<?php echo $_SESSION['MM_Username']; ?>" />
							<input type="hidden" name="notebyNick" value="<?php echo $row_userd['user_fname']; ?>" />
							<input type="hidden" name="note_cnt_Id" value="<?php echo $row_userdet['id']; ?>" />
							<input type="hidden" id="note_cnt_name" name="note_cnt_name" value="<?php echo $row_userdet['first_name']; ?> <?php echo $row_userdet['surname']; ?>" />
							<input type="hidden" id="cntCompany" name="cntCompany" value="<?php echo $row_userdet['Company']; ?>" />

					   </td>
				   </tr>
				  </table>
			  </form>
			  </fieldset>
	   		<fieldset>
			<legend>Contact notes</legend>
			<div id="notes-edit-form" title="Edit Note">
				<form id="update-notes-form" method="post" action="">
					<label for="editnote">Note:</label><br />
					 <textarea id="editnote" name="editnote" rows="10" cols="45"></textarea>
					 <input type="hidden" id="editnoteId" name="editnoteId" value="" />
					 <input type="hidden" id="editRowIdx" name="editRowIdx" value="" />
				</form>
			</div>
			<div id="confirm-delete" title="Delete note?">
				<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you would like to delete this note?</p>
				<input type="hidden" id="deleteNoteId" name="deleteNoteId" value=""  />
				<input type="hidden" id="delRowIndx" name="delRowIndx" value=""  />
				
			</div>
			 <div id="cnotes">
       
					 <input type="hidden" id="cnt_Id" name="cnt_Id" value="<?php echo isset($_GET['cnt_Id']) ? $_GET['cnt_Id'] : ''?>" />              
       <table border="0" cellspacing="0" cellpadding="0" style="margin:10px 0; width:100%;">
            <tbody>
                <tr>
                    <td style="padding-left:0;">Start Date:</td>
                    <td><input name="mindate" id="mindate" type="text"></td>
                    <td style="text-align:right;"><button style="display: inline-block; height: 30px; width: 130px; float:right;" id="export-contact-list-table">Export Data</button></td>
                </tr>
                <tr>
                    <td style="padding-left:0;">End Date:</td>
                    <td><input name="maxdate" id="maxdate" type="text"></td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>               
			 	<table border="0" width="100%" id="contact-list-table" class="table table-striped dataTable" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th>Date</th>
							<th>Note</th>
							<th>Type</th>
							<th>By</th>
							<th>Action</th>
						</tr>
					</thead>          
					<tfoot class="filters top">
						<tr>
							<th>&nbsp;</th>
							<th>Note</th>
							<th>Type</th>
							<th>By</th>
							<th>&nbsp;</th>
						</tr>
					</tfoot>                    
					<tbody>
						<?php if ($notesDataSize > 0) {
							for($x=0; $x < $notesDataSize; $x++ ){
						 ?>
						<tr>
							<td class="nw"><?php echo str_replace(' ', '<br>', $notesData[$x]['note_date']); ?></td>
							<td><?php echo nl2br($notesData[$x]['note_txt']); ?></td>
							<td><?php echo $notesData[$x]['note_type']; ?></td>
							<td><?php echo $notesData[$x]['note_by']; ?></td>
							<td>
              	<?php if($notesData[$x]['note_Id'] != 0) { ?>
              	<a href="javascript://" class="edit-button" data-id="<?php echo $notesData[$x]['note_Id']; ?>" title="Click to edit"><small>Edit</small></a><br /><br />
								<a href="javascript://" class="delete-button" data-id="<?php echo $notesData[$x]['note_Id']; ?>" title="Click to delete"><small>Delete</small></a>
                <?php } ?>
							</td>
						</tr>
						<?php } 
						 }
						?>
					</tbody>
				</table>
				<p>&nbsp;</p>

			 </div>
		   </fieldset>            
       
       <div id="WebviewDialog">
          <table border="0" width="100%" id="webview-opens-count" class="table table-striped dataTable" cellpadding="0" cellspacing="0">
            <thead>
              <tr>
                <th>Date</th>
                <th>Ip Address</th>
                <th>Reader</th>
                <th>Report Title</th>
              </tr>
            </thead>
        </table>
       </div>        
       
	  </div>
	  <div id="tabs2-2"></div>
	   <div id="tabs2-3">
	   <h3>iOS activity</h3>
			<h4>Last Sync</h4>
			<table class="sync-table" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<th>Device Name</th>
						<th>Device Model</th>
						<th>OS Version</th>
						<th>Time</th>
						<th>Date</th>
				</thead>
				<tbody id="ios-sync-data">
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</tbody>
			</table>
		</div>
	   <div id="tabs2-4">
	   <h3>Android activity</h3>
			<h4>Last Sync</h4>
			<table class="sync-table" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<th>Device Name</th>
						<th>Device Model</th>
						<th>OS Version</th>
						<th>Time</th>
						<th>Date</th>
				</thead>
				<tbody id="android-sync-data">
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</tbody>
			</table>
	   </div>
	   <div id="tabs2-5">
	   		 <div id="emailRead">
			 	<table border="0" width="100%" id="email-reads-table" class="table table-striped dataTable" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th>Count</th>
							<th>Date</th>
							<th>Sender</th>
							<th>Report Title</th>
						</tr>
					</thead>
				</table>
			</div>
	   </div>
	   <!-- Dialog for showing all the email counts !-->
	   <div id="EmailDialog">
	   		<table border="0" width="100%" id="email-reads-count" class="table table-striped dataTable" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th>Date</th>
							<th>Ip Address</th>
							<th>Host Name</th>
							<th>Reader</th>
							<th>Report Title</th>
						</tr>
					</thead>
			</table>
	   </div>
  </div>
</div><div id="phonebox"></div>

<div id="VcardDialog" title="Status Info">
	<p id='message'></p>
</div>

<div id="mitel_pop_up" title="Initiating a call ...">
  <div>Placing a call to <span id='call_number'></span> .... </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		//Preload images
		$('<img/>')[0].src = "images/loader-xsm.gif";
		$('<img/>')[0].src = "images/opened.png";
		$('<img/>')[0].src = "images/pages.png";
		$('<img/>')[0].src = "images/spacer.gif";

		$("#emails-read-tab, #VcardDialog , #EmailDialog, #WebviewDialog").hide();

		var oTable 			= $('#email-reads-table').DataTable({"autoWidth" :false, "order":[[1, "desc"]]});
		var oTableCount		= $('#email-reads-count').DataTable({"autoWidth" :false, "order":[[0, "desc"]]});

		var emailAddress = '<?php if( isset($row_userdet['email'])) echo $row_userdet['email']; ?>';

		console.log(" <?php echo $platform ?> ");
		console.log(" <?php echo $hostname ?> ");
		var hostname = "<?php echo $hostname ?>";
		var activeMonthMax  = 6, apiUrl = "https://"+hostname+"/api/devices?";
		var getEmailsURL = "https://"+hostname+"/api/getEmails";

		addDeviceSync(apiUrl, emailAddress, activeMonthMax);
		getReadEmails(getEmailsURL, emailAddress, oTable, oTableCount);
		$('#ipad-activity').click(function(){
			$('#tabs2').tabs({active: 2});
		});
		$('#android-activity').click(function(){
			$('#tabs2').tabs({active: 3});
		});
		
		var clip = new ZeroClipboard( document.getElementById("copy-email") );
		clip.on( "ready", function( readyEvent ) {
			
			$('#copy-email-error').html('');
			
			clip.on( "aftercopy", function( event ) {
			$('#copy-email-success').html('<small>Email copied</small>');
			
			});
		});
		clip.on( 'error', function(event) {
			$('#copy-email-success').html('');
			$('#copy-email-error').html('<small> To Copy: Press Ctrl-C / Cmd-C after selecting the text</small>');
			$("#copy-email").hide();
			ZeroClipboard.destroy();
			});
			
		// function to add handlers to pagination buttons:
		addClick()
			

			
	});

    $("#mitel_pop_up").dialog({
          autoOpen    : false,
          modal     : true,
          resizable     : false,
          height      : 100,
          closeOnEscape   : false
          
		});
	/**
	 * ensure generateWebviewOpenBadges funcrtion called is during pagination 
	 */	
	function addClick(){
		// var click = $(this).attr('id');
		// console.log(click);
		generateWebviewOpenBadges();
		// paging buttons
		$("a[aria-controls='contact-list-table']").on("click", addClick);
		// page size dropdown
		$("#contact-list-table_length").on("change", addClick);
		// change in filter options
		$("tfoot.filters.top").on("change", addClick);
		// date filter
		$("input.hasDatepicker").on("change", addClick);
	}

function generateWebviewOpenBadges() {
	
	//Add animating image
	$("[data-notetype='webviewopen']").removeClass('badge').text('');
	$("[data-notetype='webviewopen']").parent('td').append('<img src="images/loader-xsm.gif" class="loading-image" style="margin-left:10px;">');			

	$("[data-notetype='webviewopen']").each(function () {	
		var reportid = $(this).data('reportid');
		var email = $(this).data('email');
		var count = $(this).data('count');

		var contactName = $(this).data('contact');
		var reportTicker = $(this).data('rticker');		
		var reportTitle = $(this).data('rtitle');			

		if (parseInt(reportid) > 1 && email != '') {			

			var postData = { 'entity_alias': 'webview-metadata', 'reportid': reportid, 'email':email};
			var webviewOpenElement = $(this);
			
			$.ajax({
				dataType: 'json',
				type: "POST",
				url: "ajaxDatatables.php",
				data: postData,
				async: true
			}).done(function (result) {					

				//console.log('Ajax result');
				//console.log(result);	
				
				var numOpened = result.opened;
				var numPages = result.pages;
				var duration = result.duration;

				var openedBin = calculateOpendedBinValue(numOpened);
				var numpagesBin = calculateNumPagesBinValue(numPages);
				var durationBin = calculateDurationBinValue(result.secs);
		
				//Remove animating image
				webviewOpenElement.parent('td').find('.loading-image').remove();
				webviewOpenElement.parent('td').find('.webviewopen-metadata').remove();				
				webviewOpenElement.parent('td').append(
				'<div class="webviewopen-metadata">'+
				'<span class="metabadge tooltip opened ' + openedBin + 
						'" data-notetype="' + "webviewopen" + '"' +
						'" data-reportid="' + reportid+ '"' +
						'" data-rticker="' + reportTicker + '"' +
						'" data-rtitle="' + reportTitle + '"' +
						'" data-contact="' + contactName + '"' +
						'" data-email="' + email + '"' +
						'" data-count="' + count + '"' +
				'>' + numOpened + ' <img src="images/opened.png" />'+
				'<span class="tooltiptext">Opened ' + numOpened + '  times</span></span>'+
				'<span class="tooltip pages ' + numpagesBin + '">' + numPages + ' <img src="images/pages.png" />'+
				'<span class="tooltiptext">Number of pages ' + numPages + ' </span></span>'+
				'<span class="duration ' + durationBin + '">' + duration +' <img src="images/spacer.gif" /></span>'+
				'</div>');
			});
			
		}		
	});

	bindWebviewMetaBadges();

}

function bindWebviewMetaBadges() {

	$( ".dataTable" ).off("click", ".metabadge");
	
	$( ".dataTable" ).on("click", ".metabadge", function(){
		var notetype = $(this).data('notetype');		
		
		if (notetype == 'webviewopen') {
			var count = $(this).data('count');	
			var contactName = $(this).data('contact');
			var reportTicker = $(this).data('rticker');		
			var reportTitle = $(this).data('rtitle');			
			var email = $(this).data('email');						
									
			if (parseInt(count) > 1) {
				openWebviewOpensAviorCRM(email, reportTitle, reportTicker, contactName, count);
			}			
		}		
	});
}

function calculateOpendedBinValue(value) {
	var binValue = 1;
	var max = 3;
	var divided = value / max;

	if (divided > 0.6666666666666666) {
		binValue = 3;
	} else if (divided > 0.3333333333333333) {
		binValue = 2;
	}	
	return 'bin' + binValue;
}

function calculateNumPagesBinValue(value) {
	var binValue = 1;
	var max = 5;
	var divided = value / max;

	if (divided > 0.6666666666666666) {
		binValue = 3;
	} else if (divided > 0.3333333333333333) {
		binValue = 2;
	}	
	return 'bin' + binValue;
}

function calculateDurationBinValue(value) {
	var binValue = 1;
	var max = 300;
	var divided = value / max;

	if (divided > 0.6666666666666666) {
		binValue = 3;
	} else if (divided > 0.3333333333333333) {
		binValue = 2;
	}	
	return 'bin' + binValue;
}
</script>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<script type="text/javascript">
	function sendVcard(name, surname, company,email, mobile,phone,imageLocation, userEmail,jobtitle) 
	{

		$.ajax({
			url 		: "createVcard.php",
			method 		: "POST",
			dataType 	: "text",
			data 		: ({'name': name,'surname' : surname, 'company' : company, 'email' : email,'mobile'
							:mobile,'phone' : phone,'userEmail' : userEmail,'imageLocation' : imageLocation, 'jobtitle':jobtitle}),
			beforeSend 	: function()
			{
				$("#ajax-loader").css("display","");
			},
			success 	: function(responseData)
			{
					$("#ajax-loader").css("display","none");
					var jsonData 	= $.parseJSON(responseData);
					var status 		= jsonData['status'];
					var message 	= jsonData['message'];
					if (status != " ") {
						$("#message").text(message);
						$('#VcardDialog').show().dialog();

					}
			},
			error 		: function(XHRq, XHRStatus)
			{
					console.log(XHRStatus + " : " + XHRq.responseText);
			}
		});
	}
</script>

<?php
mysql_free_result($reminders);
mysql_free_result($userdet);
mysql_free_result($complist);
mysql_free_result($responsib);
mysql_free_result($resplist);
mysql_free_result($rsemail);
mysql_free_result($rsnotemail);
mysql_free_result($userd);
mysql_free_result($rsCtype);
mysql_free_result($onclientlist);
mysql_free_result($availlist);
mysql_free_result($rsaddphone);
?>
