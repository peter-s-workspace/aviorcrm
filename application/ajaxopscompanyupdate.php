<?php
if (!isset($_SESSION)) session_start();
if( !isset($_SESSION['MM_Username'])) exit('login required');

require_once('includes/classes/company.class.php');

$updatecolumn = isset($_POST['update_column']) && $_POST['update_column'] != '' ? $_POST['update_column'] : '';
$new_value = isset($_POST['val']) && $_POST['val'] != '' ? $_POST['val'] : '';
$companyId = isset($_POST['comp_id']) && $_POST['comp_id'] != '' ? $_POST['comp_id'] : '';

if( $updatecolumn != '' && $new_value != '' && $companyId != '' ) {

	$companyObj = new Company($companyId);
	switch($updatecolumn) {

		case "tier":
			$result = $companyObj->UpdateCompanyTier($new_value);
			break;

		case "status":
			$result = $companyObj->UpdateCompanyStatus($new_value);
			break;

		case "type":
			$result = $companyObj->UpdateCompanyType($new_value);
			break;

		case "location":
			$result = $companyObj->UpdateCompanyLocation($new_value);
			break;

	}
}

echo $result;

?>


