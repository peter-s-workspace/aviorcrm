<?php require_once('Connections/CRMconnection.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Administrator";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}


$ttd="";
$listmails="";

 foreach ($_GET['cnt_Interest'] as $i => $val)
{
    if( $val != 'All'){
        $ttd .= sprintf("%s OR contacts_interests.interest_id=",GetSQLValueString($val, "text")) ;
    } else {
        $ttd = sprintf("'' OR contacts_interests.interest_id != 0 OR contacts_interests.interest_id=");
        break;
    }
    
	$listmails .= $val." ,";
}
$ttd .="'asdfkhasncaskdfhaoehf'";  // just to fix up thing

$colname_rsmail2 = $ttd;

$tierNames = array( "Normal", "DRM", "Webview" );

$temp = "";  //default to zero as in the above code
$tierparam = "";
$tierString = "";
$tiervalout = "";

if (isset($_GET['tier'])) {

    $tiers = $_GET['tier'];

    $sep = "";
    $commaSep = "";
    $valSep = "";
    foreach ($tiers as $tierval) {
      //echo $tierval;

      $tierparam .= $sep."tier[]=".$tierval;
      $tierString .= $commaSep.$tierval;

      $tiervalout .= $valSep.$tierNames[$tierval-1];

      $sep = "&";
      $commaSep = ",";
      $valSep = ",and,";
    }   
    //echo $tierparam;
}  

mysql_select_db($database_CRMconnection, $CRMconnection);

$query_rsmail2 = sprintf("SELECT DISTINCT contacts.first_name, contacts.surname, contacts.landline, contacts.email, contacts.mobile, tblcompanylist.Company, contacts.active, IF (ISNULL(contacts.drm_status), tblcompanylist.drm_status, contacts.drm_status) as drm_status
                  FROM contacts_interests, contacts , tblcompanylist, interests 
                  WHERE contacts.active = 1 
                  AND contacts.company_id = tblcompanylist.comp_id 
                  AND (interests.id=%s ) AND interests.id = contacts_interests.interest_id 
                  AND contacts.id = contacts_interests.contact_id 
                  AND (IF (ISNULL(contacts.drm_status), tblcompanylist.drm_status, contacts.drm_status)) IN ($tierString) order by contacts.surname" 
                  , $colname_rsmail2) ;


//echo $query_rsmail2;

$rsmail2 = mysql_query($query_rsmail2, $CRMconnection) or die(mysql_error());
$row_rsmail2 = mysql_fetch_assoc($rsmail2);
$totalRows_rsmail2 = mysql_num_rows($rsmail2);

//$outvar= "sql query,".$query_rsmail2."\n"; 
$outvar="Mail lists:,".$listmails."\n";
$outvar.= "Protection Status:,".$tiervalout;
$outvar.="\n";
$outvar.="First name, Last Name, Company, Protection Status, Phone, Cell, Email, Status\n";

$dv="";
$tierval="";

if($totalRows_rsmail2>0){
do { 
    if($row_rsmail2['active']==1){$dv="Active";}else{$dv="Inactive";}
    if($row_rsmail2['drm_status']==1)
    {
      $tierval="Normal";
    }
    else if ($row_rsmail2['drm_status']==2)
    {
      $tierval="DRM";
    }
    else 
    {
      $tierval="Web Viewer";
    }

    $outvar .= $row_rsmail2['first_name'].",".$row_rsmail2['surname'].",\"".$row_rsmail2['Company']."\",".$tierval.",=\"".$row_rsmail2['landline']."\",=\"".$row_rsmail2['mobile']."\",".$row_rsmail2['email'].",".$dv."\n ";

} while ($row_rsmail2 = mysql_fetch_assoc($rsmail2));
}

$file_out = $outvar;
$filename="mailout.csv";
$out = strlen($file_out);
if (isset($file_out)) {
header("Content-Length: $out");
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=$filename");
echo $file_out;
exit;
}

?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

</body>
</html>

<?php
//echo $outvar;


mysql_free_result($rsmail2);

?>